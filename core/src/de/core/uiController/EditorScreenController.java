package de.core.uiController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import java.util.Observable;
import java.util.Observer;

import lombok.Getter;
import de.core.MainGame;
import de.core.model.Map;
import de.core.model.Player;
import de.core.network.events.map.AddMapEvent;
import de.core.screens.EditorScreenInterface;

public class EditorScreenController implements Observer
{
    private final MainGame game;
    private final EditorScreenInterface screen;
    @Getter
    private Player currentPlayer;

    private final String loggingTag = getClass().getSimpleName();

    public EditorScreenController(final MainGame game,
                                  final EditorScreenInterface screen)
    {
        this.game = game;
        this.screen = screen;

        game.getConnection().getMapClient().addObservers(this);
    }

    @Override
    public void update(
            final Observable observable,
            final Object event)
    {
        if(event instanceof AddMapEvent)
        {
                game.getMapHandler().safeMap(((AddMapEvent)event).getMapId().getIdentifier(),
                             game.getMapHandler().getEditableMap());
        }
    }

    public InputListener getSaveButtonListener()
    {
        return new SaveButtonListener();
    }

    public InputListener getReturnButtonListener()
    {
        return new ReturnButtonListener();
    }

    public InputListener getChoosePlayerButtonListener()
    {
        return new ChoosePlayerButtonListener();
    }

    private class ChoosePlayerButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.log(loggingTag,
                        "ChoosePlayerButton touchUp()");
            final Map editableMap = game.getMapHandler().getEditableMap();

            if (editableMap != null)
            {
                if (currentPlayer != null)
                {
                    if (currentPlayer.getName().equals(
                            "Player" + (editableMap.getNumberOfPlayers() - 1)))
                    {
                        currentPlayer = null;
                        editableMap.getGame().setCurrentPlayer(
                                editableMap
                                        .getGame()
                                        .getCurrentPlayer()
                                        .getNext());
                    }
                    else
                    {
                        currentPlayer = editableMap.getGame().getCurrentPlayer()
                                                   .getNext();
                        editableMap.getGame().setCurrentPlayer(currentPlayer);
                    }
                }
                else
                {
                    currentPlayer = editableMap.getGame().getCurrentPlayer();
                    editableMap.getGame().setCurrentPlayer(currentPlayer);
                }
                if (currentPlayer != null)
                {
                    ((TextButton) screen.getChoosePlayerButton()).setText(currentPlayer.getName());
                }
                else
                {
                    ((TextButton) screen.getChoosePlayerButton()).setText("Player");
                }
            }
        }
    }

    private class ReturnButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            screen.dispose();
            game.setScreen(game.getScreenHandler()
                               .getScreen(screen.getCanonicalNameOfMainScreen()));
        }
    }

    private class SaveButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            game.getConnection().getMapClient()
                .postMap(game.getMapHandler().getEditableMap());
        }
    }
}

package de.core.uiController.windows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.Observable;
import java.util.Observer;

import de.core.MainGame;
import de.core.model.User;
import de.core.network.events.account.LoginEvent;
import de.core.network.events.account.RegistrationEvent;
import de.core.screens.windows.LoginScreenInterface;

public class LoginScreenController implements Observer
{
    private final MainGame game;
    private final LoginScreenInterface screen;

    private final String loggingTag = getClass().getSimpleName();

    public LoginScreenController(
            final MainGame game,
            final
            LoginScreenInterface screen)
    {
        this.game = game;
        this.screen = screen;

        game.getConnection().getAccountClient().addObservers(this);
    }

    @Override
    public void update(
            final Observable observable,
            final Object event)
    {
        if (event instanceof LoginEvent)
        {
            handleLoginEvent((LoginEvent) event);
        }
        else if (event instanceof RegistrationEvent)
        {
            handleRegistrationEvent((RegistrationEvent) event);
        }
    }

    private void handleRegistrationEvent(final RegistrationEvent event)
    {
        if (event.isSuccessful())
        {
            login();
        }
        else
        {
            screen.getNameTextField().setText("Somethings went wrong!");
            screen.getPasswordTextField().setText("");
        }
    }

    private void handleLoginEvent(final LoginEvent event)
    {
        if (event.isSuccessful())
        {
            game.initializeUser(event.getUser());
        }
        screen.loginSuccessful(event.isSuccessful());
    }

    public InputListener getLoginButtonListener()
    {
        return new LoginButtonListener();
    }

    public InputListener getExitButtonListener()
    {
        return new ExitButtonListener();
    }

    public InputListener getRegistrationListener()
    {
        return new RegistrationButtonListener();
    }

    private class RegistrationButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            try
            {
                game.getConnection().getAccountClient()
                    .sendRegistrate(screen.getNameTextField().getText(),
                                    screen.getPasswordTextField().getText());
            }
            catch (JsonProcessingException e)
            {
                Gdx.app.error(loggingTag,
                              "Exception",
                              e);
            }
        }
    }

    private class ExitButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.exit();
        }
    }

    private class LoginButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            if (screen.getNameTextField().getText().equals("test") &&
                    screen.getPasswordTextField().getText().equals("test"))
            {
                game.initializeUser(new User("TestUser"));
                screen.close();
            }
            login();
        }
    }

    private void login()
    {
        game.getConnection().getAccountClient()
            .login(screen.getNameTextField().getText(),
                   screen.getPasswordTextField().getText());
    }
}

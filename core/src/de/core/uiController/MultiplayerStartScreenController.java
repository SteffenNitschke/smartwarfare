package de.core.uiController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

import lombok.RequiredArgsConstructor;
import de.core.screens.MultiplayerStartScreenInterface;

@RequiredArgsConstructor
public class MultiplayerStartScreenController
{
    private final MultiplayerStartScreenInterface screen;

    private final String loggingTag = getClass().getSimpleName();

    public InputListener newInternetButtonListener()
    {
        return new InternetButtonListener();
    }

    public InputListener newPartyButtonListener()
    {
        return new PartyButtonListener();
    }

    public InputListener newBackButtonListener()
    {
        return new BackButtonListener();
    }

    private class BackButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.log(loggingTag,
                        "Press BackButton");

            screen.gotoMainScreen();
        }

        @Override
        public boolean touchDown(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            return true;
        }
    }

    private class PartyButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.log(loggingTag,
                        "Press PartyMultiplayerButton");
//                if(game.getScreen(PartyMultiplayerScreen.class.getCanonicalName()) == null) {
//                    game.addScreen(new PartyMultiplayerScreen(game));
//                }
//                game.setScreen(game.getScreen(PartyMultiplayerScreen.class.getCanonicalName()));
        }
    }

    private class InternetButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.log(loggingTag,
                        "Press InternetButton");
            if (screen.existInternetMultiPlayerScreen())
            {
                screen.addInternetMultiPlayerScreen();
            }
            screen.gotoInternetMultiPlayerScreen();
        }
    }
}

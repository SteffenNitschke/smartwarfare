package de.core.uiController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

import lombok.RequiredArgsConstructor;
import de.core.MainGame;
import de.core.screens.MainScreenInterface;

import static com.kotcrab.vis.ui.VisUI.dispose;

@RequiredArgsConstructor
public class MainScreenController
{
    private final MainScreenInterface screen;
    private final MainGame game;

    private final String loggingTag = getClass().getSimpleName();

    public InputListener getSinglePlayerButtonListener()
    {
        return new SinglePlayerButtonListener();
    }

    public InputListener getMultiPlayerButtonListener()
    {
        return  new MultiPlayerButtonListener();
    }

    public InputListener getEditorScreenButtonListener()
    {
        return new EditorScreenButtonListener();
    }

    public InputListener getSettingsScreenListener()
    {
        return new SettingScreenButtonListener();
    }

    public InputListener getContactsScreenListener()
    {
        return new ContactsScreenButtonListener();
    }

    public InputListener getExitButtonListener()
    {
        return new ExitButtonListener();
    }

    //TODO
    public void showLoginScreen()
    {
//        if (!game.getScreenHandler().isScreenInFrontOf())
//        {
//            loginScreen = null;
//            inputProcessor.idle(false);
//        }
//        else
//        {
//            if (game.getUser() != null)
//            {
//                game.getScreenHandler().setScreenInFrontOf(false);
//            }
//            if (loginScreen != null)
//            {
//                loginScreen.draw();
//            }
//        }
    }

    private class ExitButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.log(loggingTag,
                        "ExitButton");
            screen.dispose();
            //TODO handle Logout event
            game.getConnection().getAccountClient().logout();
            Gdx.app.exit();
        }

        @Override
        public boolean touchDown(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            return true;
        }
    }

    private class ContactsScreenButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.log(loggingTag,
                        "ContacsButton");
            if (game.getScreenHandler().getScreen(screen.getCanonicalNameOfContactsScreen()) ==
                    null)
            {
                game.getScreenHandler().addScreen(screen.getContactsScreen());
            }
            game.setScreen(game.getScreenHandler()
                               .getScreen(screen.getCanonicalNameOfContactsScreen()));
        }

        @Override
        public boolean touchDown(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            return true;
        }
    }

    private class SettingScreenButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.log(loggingTag,
                        "SettingButton");
            if (game.getScreenHandler().getScreen(screen.getCanonicalNameOfSettingsScreen()) ==
                    null)
            {
                game.getScreenHandler().addScreen(screen.getSettingsScreen());
            }
            game.setScreen(game.getScreenHandler()
                               .getScreen(screen.getCanonicalNameOfSettingsScreen()));
        }

        @Override
        public boolean touchDown(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            return true;
        }
    }

    private class EditorScreenButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.log(loggingTag,
                        "EditorButton");
            if (game.getScreenHandler().getScreen(screen.getCanonicalNameOfEditorScreen()) ==
                    null)
            {
                game.getScreenHandler().addScreen(screen.getEditorScreen());
            }
            game.setScreen(game.getScreenHandler()
                               .getScreen(screen.getCanonicalNameOfEditorScreen()));
        }

        @Override
        public boolean touchDown(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            return true;
        }
    }

    private class MultiPlayerButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.log(loggingTag,
                        "MultiplayerButton");
            if (game.getScreenHandler().getScreen(screen.getCanonicalNameOfMultiPlayerScreen()) == null)
            {
                game.getScreenHandler().addScreen(screen.getMultiPlayerScreen());
            }
            game.setScreen(game.getScreenHandler()
                               .getScreen(screen.getCanonicalNameOfMultiPlayerScreen()));
        }

        @Override
        public boolean touchDown(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            return true;
        }
    }

    private class SinglePlayerButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.log(loggingTag,
                        "Press SingelplayerButton");
            dispose();
            if (game.getScreenHandler().getScreen(screen.getCanonicalNameOfSinglePlayerScreen()) == null)
            {
                game.getScreenHandler().addScreen(screen.getSinglePlayerScreen());
            }
            game.setScreen(game.getScreenHandler()
                               .getScreen(screen.getCanonicalNameOfSinglePlayerScreen()));
        }

        @Override
        public boolean touchDown(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            return true;
        }
    }
}

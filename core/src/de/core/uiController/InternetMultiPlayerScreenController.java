package de.core.uiController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.stream.Collectors;

import de.core.model.Id;
import de.core.network.events.map.GetMapImageEvent;
import de.core.network.messages.news.GameInviteClientRequest;
import lombok.Getter;
import lombok.Setter;
import de.core.MainGame;
import de.core.network.events.game.invite.GameInvitationEvent;
import de.core.network.events.map.GetAllMapsEvent;
import de.core.network.messages.PlayerMessage;
import de.core.network.messages.SmallMapMessage;
import de.core.screens.InternetMultiPlayerScreenInterface;
import de.core.util.GameTypes;

public class InternetMultiPlayerScreenController implements Observer
{
    private final MainGame game;
    private final InternetMultiPlayerScreenInterface screen;
    @Getter
    private HashSet<SmallMapMessage> allMaps;
    private Map<Id, FileHandle> mapImages = new HashMap<>();

    @Getter
    private final HashSet<PlayerMessage> invitedPlayer = new HashSet<>();
    @Getter
    @Setter
    private GameTypes gameType;
    @Getter
    @Setter
    private String mapName;
    @Getter
    @Setter
    private String mapId;

    public InternetMultiPlayerScreenController(
            final MainGame game,
            final InternetMultiPlayerScreenInterface screen)
    {
        this.game = game;
        this.screen = screen;

        game.getConnection().getMapClient().addObservers(this);
        game.getConnection().getGameInvitationClient().addObservers(this);
    }

    private final String loggingTag = getClass().getSimpleName();

    public InputListener getPartyButtonListener()
    {
        return new PartyButtonListener();
    }

    public InputListener getNewGameButtonListener()
    {
        return new NewGameButtonListener();
    }

    public InputListener getMatchmakingButtonListener()
    {
        return new MatchmakingButtonListener();
    }

    public InputListener getBackButtonListener()
    {
        return new BackButtonListener();
    }

    public InputListener getChooseGameTypeListListener()
    {
        return new ChooseGameTypeListListener();
    }

    public InputListener getStartGameButtonListenr()
    {
        return new StartGameButtonListener();
    }

    @Override
    public void update(
            final Observable observable,
            final Object event)
    {
        if (event instanceof GetAllMapsEvent)
        {
            this.allMaps = ((GetAllMapsEvent) event).getMaps();

            for (SmallMapMessage smallMapMessage : this.allMaps)
            {

                Gdx.app.log(loggingTag,
                            "SmallMapMessage: " + smallMapMessage);
                game.getConnection().getMapImageClient()
                    .getMapImageById(new Id(smallMapMessage.getMapId()))
                    .addObserver(this);
            }
        }
        else if (event instanceof GameInvitationEvent)
        {
            final GameInvitationEvent gameInvitationEvent = (GameInvitationEvent) event;
            game.getUser().getGames().add(gameInvitationEvent.getGame());

            this.game.notificationHandler().showUserNotification("Spiel wurde erfolgreich erstellt", true);

            screen.clearNewGame();
        }
        else if (event instanceof GetMapImageEvent)
        {
            Gdx.app.log(loggingTag,
                        "Event is GetMapImageEvent");
            this.mapImages.put(
                    ((GetMapImageEvent) event).getMapId(),
                    ((GetMapImageEvent) event).getImage());
        }
    }

    public void addInvitedPlayer(final PlayerMessage friend)
    {
        invitedPlayer.add(friend);
    }

    public void removeMapPane()
    {
        screen.removeMapPane();
    }

    public void showMapImageOfGame(final Id gameId)
    {
        Gdx.app.log(loggingTag,
                    "Show mapImageOfGame");
        game.getUser().getGames().stream().filter(
                game1 -> game1.getGameID().equals(gameId.getIdentifier()))
            .findFirst()
            .ifPresent(
                    game1 -> showMapOfMapId(new Id(game1.getMap().get_id())
                    ));
    }

    public void showMapOfMapId(final Id mapId)
    {
        Gdx.app.log(loggingTag,
                    "showMapOfMapId " + mapId.getIdentifier());
        screen.showMapImage(mapImages.get(mapId));
    }

    private class BackButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            game.setScreen(game.getScreenHandler()
                               .getScreen(screen.getCanonicalNameOfMPStartScreen()));
        }
    }

    private class MatchmakingButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.log(loggingTag,
                        "Press Matchmaking Button");
            screen.showMatchmaking();
        }
    }

    private class NewGameButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.log(loggingTag,
                        "Press NewGame Button");

            screen.showNewGame();
        }
    }

    private class PartyButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.log(loggingTag,
                        "Press PartyMultiplayerButton");
            if (game.getScreenHandler()
                    .getScreen(screen.getCanonicalNameOfPartyMultiPlayerScreen()) == null)
            {
                game.getScreenHandler().addScreen(screen.getPartyMultiPlayerScreen());
            }
            game.setScreen(game.getScreenHandler()
                               .getScreen(screen.getCanonicalNameOfPartyMultiPlayerScreen()));
        }
    }

    private class ChooseGameTypeListListener extends InputListener
    {
        @Override
        public void touchUp(
                final InputEvent event,
                final float x,
                final float y,
                final int pointer,
                final int button)
        {
            Gdx.app.log(loggingTag,
                        "ListListener");
            final GameTypes gameType = GameTypes.valueOf(((Label) ((List) event
                    .getTarget())
                    .getSelected()).getText().toString());
            Gdx.app.log(loggingTag,
                        "GameType: " + gameType);
            setGameType(gameType);
            screen.getChooseGameTypeButton().setText(getGameType().name());
            screen.getGameTypePane().setVisible(false);
            screen.getGameTypePane().remove();
        }

        @Override
        public boolean touchDown(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            return true;
        }
    }

    private class StartGameButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                final InputEvent event,
                final float x,
                final float y,
                final int pointer,
                final int button)
        {
            System.out.println("Press Start Button");

            final GameInviteClientRequest inviteClientRequest = new GameInviteClientRequest();
            inviteClientRequest.setMapId(mapId);
            inviteClientRequest.setGameType(gameType);
            inviteClientRequest.setPlayerIds(invitedPlayer.stream()
                                                          .map(PlayerMessage::getId)
                                                          .map(Id::getIdentifier)
                                                          .collect(Collectors.toList()));
            try
            {
                game.getConnection().getGameInvitationClient()
                    .sendGameInvitation(inviteClientRequest);
            }
            catch (JsonProcessingException e)
            {
                Gdx.app.log(loggingTag,
                            "",
                            e);
            }
        }

        @Override
        public boolean touchDown(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            return true;
        }
    }
}

package de.core.uiController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

import lombok.RequiredArgsConstructor;
import de.core.MainGame;
import de.core.screens.SinglePlayerScreenInterface;

@RequiredArgsConstructor
public class SinglePlayerScreenController
{
    private final SinglePlayerScreenInterface screen;
    private final MainGame game;

    private final String loggingTag = getClass().getSimpleName();
    private boolean hideMenuBar = false;

    public InputListener newHideButtonListener()
    {
        return new HideButtonListener();
    }

    public InputListener newNewGameButtonListener()
    {
        return new NewGameButtonListener();
    }

    public InputListener newBackButtonListener()
    {
        return new BackButtonListener();
    }

    private class BackButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.log(loggingTag,
                        "Press BackButton");
            game.setScreen(game.getScreenHandler()
                               .getScreen(screen.getCanonicalNameOfMainScreen()));
        }

        @Override
        public boolean touchDown(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            return true;
        }
    }

    private class NewGameButtonListener extends InputListener
    {
        @Override
        public boolean touchDown(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            return true;
        }

        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            game.setScreen(screen.getGameScreen());
        }
    }

    private class HideButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            if (!hideMenuBar)
            {
                screen.minimizeMenuBar();
                hideMenuBar = true;
            }
            else
            {
                screen.expandMenuBar();
                hideMenuBar = false;
            }
        }

        @Override
        public boolean touchDown(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            return true;
        }
    }
}

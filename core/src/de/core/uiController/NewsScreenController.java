package de.core.uiController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

import de.core.model.Id;
import de.core.network.messages.news.NewsDto;
import de.core.screens.NewsScreenInterface;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class NewsScreenController
{
    private final NewsScreenInterface screen;

    private final String loggingTag = getClass().getSimpleName();

    public InputListener newNewsButtonListener()
    {
        return new NewsButtonListener();
    }

    public void updateNewsTable(final NewsDto message)
    {
        screen.getNewsTable().viewNews(message);

        increaseNewsNumberOfNewsLabel();
    }

    private void increaseNewsNumberOfNewsLabel()
    {
        final int oldNumber = Integer.parseInt(screen.getNumberOfNewsLabel().getText().toString());

        screen.getNumberOfNewsLabel().setText((oldNumber + 1) + "");
        makeLabelVisible();
    }

    public void removeNewsFromTable(final Id newsId)
    {
        screen.getNewsTable().removeNewsFromTable(newsId);
    }

    private class NewsButtonListener extends InputListener
    {

        @Override
        public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            Gdx.app.log(loggingTag,
                        "NewsButton");
            if (!screen.isVisible())
            {
                screen.setVisible(true);
                hideLabel();
            }
            else
            {
                screen.setVisible(false);
            }
        }
    }

    private void makeLabelVisible()
    {
        screen.getNumberOfNewsLabel().setVisible(true);
    }

    private void hideLabel()
    {
        screen.getNumberOfNewsLabel().setText("0");
        screen.getNumberOfNewsLabel().setVisible(false);
    }
}

package de.core.uiController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

import de.core.MainGame;
import de.core.gameController.FieldController;
import de.core.gameController.UnitController;
import de.core.handler.OwenFileHandler;
import de.core.model.Field;
import de.core.model.units.UnitInterface;
import de.core.network.events.DrawEvent;
import de.core.screens.GameScreenInterface;
import de.core.util.FieldType;

public class GameScreenController implements Observer
{
    private final MainGame game;
    private final GameScreenInterface screen;

    private final String loggingTag = getClass().getSimpleName();

    public GameScreenController(
            final MainGame game,
            final GameScreenInterface screen)
    {
        this.game = game;
        this.screen = screen;

        game.getConnection().getDrawClient().addObservers(this);
    }

    @Override
    public void update(
            final Observable observable,
            final Object event)
    {
        if (event instanceof DrawEvent)
        {
            if (screen.getCurrentGame().getGameID().equals(
                    ((DrawEvent) event).getGameId()))
            {
                screen.showNotification("Draw Successful");
            }
        }
    }

    public void viewCurrentFieldInInfoBox(final FieldController currentFieldController)
    {
        Gdx.app.log(loggingTag,
                    "viewCurrentFieldInInfoBox " + currentFieldController);

        final Texture texture;
        if (!screen.isDragged())
        {
            if (currentFieldController != null)
            {
                final Field field = currentFieldController.getField();

                Gdx.app.log(loggingTag,
                            "Field: " + field.getType());

                screen.getInfoBoxFieldTypeLabel().setText(field.getType().name());
                screen.getInfoBoxDefenceValueLabel().setText("Defence: " + field.getDefence());

                if (!field.getType().equals(FieldType.FIELD_TYPE_BUILDING))
                {
                    texture = fieldIsNoBuilding(field);
                }
                else
                {
                    texture = fieldIsBuilding(field);
                }
            }
            else
            {
                texture = noCurrentField();
            }

            final Drawable drawable
                    = new TextureRegionDrawable(
                    new TextureRegion(Objects.requireNonNull(texture)));
            screen.getInfoBoxFieldImage().setDrawable(drawable);
        }
    }

    private Texture noCurrentField()
    {
        Texture texture;
        Gdx.app.log(loggingTag,
                    "currentField == null");

        screen.getInfoBoxFieldTypeLabel().setText("");
        screen.getInfoBoxFieldPlayerNameLabel().setText("");
        screen.getInfoBoxDefenceValueLabel().setText("");
        screen.getInfoBoxCaptureValueLabel().setText("");
        texture = new Texture(OwenFileHandler.getFile("background",
                                                      ".jpg"));
        return texture;
    }

    private Texture fieldIsNoBuilding(final Field field)
    {
        Texture texture;
        Gdx.app.log(loggingTag,
                    "field is no building");

        screen.getInfoBoxCaptureValueLabel().setText("");
        screen.getInfoBoxFieldPlayerNameLabel().setText("");

        texture = new Texture(OwenFileHandler.getFile(field.getType().name(),
                                                      ".jpg"));
        return texture;
    }


    public void viewCurrentUnitInInfoBox(final UnitController currentUnit)
    {
        Gdx.app.log(loggingTag,
                    "viewCurrentUnitInInfoBox " + currentUnit);
        Gdx.app.log(loggingTag,
                    "Time: " + (System.currentTimeMillis() / 1000));
        if (!screen.isDragged())
        {
            if (currentUnit != null)
            {
                viewCurrentUnit(currentUnit);
            }
            else
            {
                noCurrentUnit();
            }
        }
    }

    private void viewCurrentUnit(UnitController currentUnit)
    {
        final UnitInterface unit = currentUnit.getUnit();
        Gdx.app.log(loggingTag,
                    "Unit: " + unit);
        screen.getInfoBoxUnitPlayerNameLabel().setText("Owner: " + unit.getPlayer().getName());
        screen.getInfoBoxUnitNameTypeLabel().setText("Type: " + unit.getType());
        screen.getInfoBoxUnitAmmoLabel().setText("Ammo: " + unit.getAmmo());
        screen.getInfoBoxUnitFuelLabel().setText("Fuel: " + unit.getFuel());
        final Texture image = new Texture(OwenFileHandler.getFile(unit.getType(),
                                                                  ".jpg"));
        final Drawable drawable = new TextureRegionDrawable(new TextureRegion(image));
        screen.getInfoBoxUnitImage().setDrawable(drawable);
    }

    private void noCurrentUnit()
    {
        screen.getInfoBoxUnitPlayerNameLabel().setText("");
        screen.getInfoBoxUnitNameTypeLabel().setText("");
        screen.getInfoBoxUnitAmmoLabel().setText("");
        screen.getInfoBoxUnitFuelLabel().setText("");
        final Texture image = new Texture(OwenFileHandler.getFile("emptyUnitImage",
                                                                  ".jpg"));
        final Drawable drawable =
                new TextureRegionDrawable(
                        new TextureRegion(image));

        screen.getInfoBoxUnitImage().setDrawable(drawable);
    }


    private Texture fieldIsBuilding(final Field field)
    {
        Texture texture;
        Gdx.app.log(loggingTag,
                    "field is Building");

        screen.getInfoBoxCaptureValueLabel().setText("Capture: " + field.getBuilding().getCaptureValue());

        if (field.getBuilding().getPlayer() != null)
        {
            screen.getInfoBoxFieldPlayerNameLabel().setText(field.getBuilding().getPlayer().getName());
        }

        texture = new Texture(OwenFileHandler.getFile(field.getBuilding().getType(),
                                                      ".jpg"));
        return texture;
    }


    public InputListener getInputListener()
    {
        return new NextRoundButtonListener();
    }

    private class NextRoundButtonListener extends InputListener
    {
        @Override
        public void touchUp(
                final InputEvent event,
                final float x,
                final float y,
                final int pointer,
                final int button)
        {
            if (screen.getCurrentGame().getCurrentPlayer().getUserID().equals(game.getUser()
                                                                                  .get_id()
                                                                                  .getIdentifier()))
            {
                screen.getCurrentGame().nextRound();
                screen.getNextRoundButton().setText(screen.getCurrentGame().getRound() + "");
                screen.getCurrentPlayerName().setText(screen.getCurrentGame().getCurrentPlayer().getName());
                try
                {
                    game.getConnection().getDrawClient().sendDraw(
                            game.getEventListener().getTurnOf(
                                    screen.getCurrentGame().getGameID(),
                                    false),
                            screen.getCurrentGame().getGameID());
                }
                catch (JsonProcessingException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}

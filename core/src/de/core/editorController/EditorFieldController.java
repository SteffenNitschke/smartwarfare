package de.core.editorController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import de.core.handler.OwenFileHandler;
import de.core.model.Field;
import de.core.model.buildungs.BuildingInterface;
import de.core.model.units.UnitInterface;
import de.core.util.FieldType;
import de.core.util.Utils;
import de.core.model.buildungs.BuildingBuilder;
import de.core.model.units.UnitBuilder;

public class EditorFieldController extends InputListener implements PropertyChangeListener
{
    private EditorWorldController controller;
    private Image fieldImage;
    private Field field;

    private String loggingTag = getClass().getSimpleName();
    private EditorUnitController unitController;

    EditorFieldController(
            final EditorWorldController controller,
            final Image fieldImage,
            final Field field)
    {
        this.controller = controller;
        this.fieldImage = fieldImage;
        this.field = field;

        Gdx.app.log(loggingTag,
                    "Constructor " + field);
        field.getChange().addPropertyChangeListener(this);
        fieldImage.addListener(this);

        viewPicture(false);
    }

    public void enter(
            final InputEvent event,
            final float x,
            final float y,
            final int pointer,
            final Actor fromActor)
    {
        Gdx.app.log(loggingTag,
                    "enter");
        viewPicture(true);
    }

    public void exit(
            final InputEvent event,
            final float x,
            final float y,
            final int pointer,
            final Actor toActor)
    {
        Gdx.app.log(loggingTag,
                    "exit");
        viewPicture(false);
    }

    public boolean touchDown(
            final InputEvent event,
            final float x,
            final float y,
            final int pointer,
            final int button)
    {
        final EditorController selected = this.controller.getScreen().getSelectedType();
        Gdx.app.log(loggingTag,
                    "touchDown");
        if (selected != null)
        {
            final String type = selected.getType();
            Gdx.app.log(loggingTag,
                        "Type: " + type);
            if (Utils.isBuilding(type))
            {
                setBuildingOnField(type);
            }
            else if (Utils.isUnit(type))
            {
                if (this.field.getUnit() != null)
                {
                    this.unitController.removeYou();
                }
                final UnitInterface unit = createUnit(type);

                this.field.setUnit(unit);
            }
            else
            {
                field.setType(FieldType.valueOf(type));
            }
            viewPicture(false);
        }
        return true;
    }

    private void setBuildingOnField(final String type)
    {
        //you want to set Buildings
        final BuildingInterface building = BuildingBuilder.build(type);
        building.setId(field.getFieldID());
        if (this.controller.getScreen().getController().getCurrentPlayer() != null)
        {
            building.setPlayer(this.controller.getScreen().getController()
                                              .getCurrentPlayer());
        }
        this.field.setBuilding(building);
        this.field.setType(FieldType.FIELD_TYPE_BUILDING);
    }

    private UnitInterface createUnit(final String type)
    {
        Gdx.app.log(loggingTag,
                    "createUnit");
        final UnitInterface unit = UnitBuilder.build(type);
        if (this.controller.getScreen().getController().getCurrentPlayer() != null)
        {
            unit.setPlayer(this.controller.getScreen().getController().getCurrentPlayer());
            unit.setUnitId(unit.getPlayer().getName() + "x" + unit.getPlayer().getTroops().size());
        }
        this.unitController = new EditorUnitController(
                fieldImage,
                unit,
                this);

        return unit;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt)
    {

    }

    void viewPicture(final boolean hover)
    {
        Gdx.app.log(loggingTag,
                    "viewPicture");
        if (this.field.getUnit() == null)
        {
            final String type = !field.getType().equals(FieldType.FIELD_TYPE_BUILDING) ?
                    field.getType().name() : field.getBuilding().getType();

            final String textureName = !hover ? type : type + "_hover";

            final Texture image = new Texture(
                    OwenFileHandler.getFile(textureName,
                                            ".jpg"));
            final Drawable drawable = new TextureRegionDrawable(new TextureRegion(image));

            this.fieldImage.setDrawable(drawable);

            if (this.field.getBuilding() != null && this.field.getBuilding().getPlayer() != null)
            {
                this.fieldImage.setColor(this.field.getBuilding().getPlayer().getColor());
            }
            if (this.field.getUnit() != null && this.field.getUnit().getPlayer() != null)
            {
                this.fieldImage.setColor(this.field.getUnit().getPlayer().getColor());
            }
        }
    }

    void removeYou()
    {
        this.fieldImage.remove();
    }
}

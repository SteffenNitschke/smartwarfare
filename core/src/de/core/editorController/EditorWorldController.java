package de.core.editorController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.HashSet;

import de.core.model.Field;
import de.core.model.Game;
import de.core.screens.EditorScreenInterface;

public class EditorWorldController
{

    private EditorScreenInterface screen;
    private HashSet<EditorFieldController> fieldControllers = new HashSet<>();

    private final String loggingTag = getClass().getSimpleName();

    public EditorWorldController(
            final Game currentGame,
            final Stage stage,
            final EditorScreenInterface screen)
    {
        this.screen = screen;
        Gdx.app.log(loggingTag,
                    currentGame.getMap() + "");
        int mapWidth = Gdx.graphics.getWidth();
        int mapHeight = Gdx.graphics.getHeight();
        int width = currentGame.getMap().getMaxX();
        int height = currentGame.getMap().getMaxY();

        float fieldSize = ((float) mapWidth / width) > ((float) mapHeight / height)
                ? ((float) mapHeight / width) : ((float) mapHeight / height);
        screen.setMapSize(width * fieldSize,
                          height * fieldSize);

        //create Map

        Field[][] map = new Field[width][height];
        Gdx.app.log(loggingTag,
                    "Array Sizes: " + width + ", " + height);
        for (Field field :
                currentGame.getMap().getFields())
        {
//            Gdx.app.log(loggingTag, "Field: " + field.getX() + ", " + field.getY());
            if (field != null)
            {
                map[field.getX()][field.getY()] = field;
            }
        }
        //show map
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
//                Gdx.app.log(loggingTag, i + ", " + j);
                if (map[i][j] != null)
                {
                    Field field = map[i][j];
                    Image image = new Image();
                    image.setSize(fieldSize,
                                  fieldSize);
                    image.setPosition(i * fieldSize,
                                      j * fieldSize);
                    image.setTouchable(Touchable.enabled);
                    fieldControllers.add(new EditorFieldController(this,
                                                                   image,
                                                                   field));
                    stage.addActor(image);
                }
            }
        }
        //create neighbors
        for (int i = 0; i < map.length; i++)
        {
            for (int j = 0; j < map[i].length; j++)
            {
                if (map[i][j] != null)
                {
                    if (i + 1 < map.length)
                    {
                        map[i][j].setRight(map[i + 1][j]);
                    }
                    if (i - 1 >= 0)
                    {
                        map[i][j].setLeft(map[i - 1][j]);
                    }
                    if (j + 1 < map[i].length)
                    {
                        map[i][j].setLower(map[i][j + 1]);
                    }
                    if (j - 1 >= 0)
                    {
                        map[i][j].setUpper(map[i][j - 1]);
                    }
                }
            }
        }

    }

    public EditorScreenInterface getScreen()
    {
        return screen;
    }

    public void removeYou()
    {
        for (EditorFieldController field :
                this.fieldControllers)
        {
            Gdx.app.log(loggingTag,
                        "remove you");
            field.removeYou();
        }
    }
}

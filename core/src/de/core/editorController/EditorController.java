package de.core.editorController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.core.handler.OwenFileHandler;
import de.core.screens.EditorScreenInterface;

public class EditorController extends InputListener
{

    private EditorScreenInterface screen;
    private Image typeImage;
    private String type;
    private String loggingTag = getClass().getSimpleName();

    public EditorController(
            final EditorScreenInterface screen,
            final Image image,
            final String type)
    {
        Gdx.app.log(loggingTag,
                    "EditorController");
        this.screen = screen;
        this.typeImage = image;
        this.type = type;
    }

    public boolean touchDown(
            final InputEvent event,
            final float x,
            final float y,
            final int pointer,
            final int button)
    {
        Gdx.app.log(loggingTag,
                    "Touch down EditorController");

        Texture image = new Texture(OwenFileHandler.getFile(type + "_hover",
                                                            ".jpg"));
        Drawable drawable = new TextureRegionDrawable(new TextureRegion(image));
        this.typeImage.setDrawable(drawable);

        this.screen.setSelectedType(this);

        return true;
    }

    public String getType()
    {
        return this.type;
    }

    public void removeMarked()
    {
        Texture image = new Texture(OwenFileHandler.getFile(type,
                                                            ".jpg"));
        Drawable drawable = new TextureRegionDrawable(new TextureRegion(image));
        this.typeImage.setDrawable(drawable);
    }

}

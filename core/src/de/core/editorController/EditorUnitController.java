package de.core.editorController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import de.core.handler.OwenFileHandler;
import de.core.model.units.UnitInterface;

public class EditorUnitController extends InputListener implements PropertyChangeListener
{

    private final UnitInterface unit;
    private final EditorFieldController fieldController;
    private Image unitImage;

    private String loggingTag = getClass().getSimpleName();

    EditorUnitController(
            Image unitImage,
            UnitInterface unit,
            EditorFieldController fieldController)
    {
        this.unitImage = unitImage;
        this.unit = unit;
        this.fieldController = fieldController;
        Gdx.app.log(loggingTag,
                    "Constructor");

        unit.getChange().addPropertyChangeListener(this);
        unitImage.addListener(this);

        viewPicture(false);
    }

    public void enter(
            InputEvent event,
            float x,
            float y,
            int pointer,
            Actor fromActor)
    {
        Gdx.app.log(loggingTag,
                    "enter");
        viewPicture(true);
    }

    public void exit(
            InputEvent event,
            float x,
            float y,
            int pointer,
            Actor toActor)
    {
        Gdx.app.log(loggingTag,
                    "exit");
        viewPicture(false);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt)
    {

    }

    private void viewPicture(boolean hover)
    {
        Gdx.app.log(loggingTag,
                    "viewPicture");
        final String textureName = !hover ? unit.getType() : unit.getType() + "_hover";

        final Texture image = new Texture(OwenFileHandler.getFile(textureName,
                                                                  ".jpg"));
        final Drawable drawable = new TextureRegionDrawable(new TextureRegion(image));

        this.unitImage.setDrawable(drawable);

        if (this.unit.getPlayer() != null)
        {
            this.unitImage.setColor(this.unit.getPlayer().getColor());
        }
    }

    void removeYou()
    {
        fieldController.viewPicture(false);
    }
}

package de.core.screens;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import de.core.screens.windows.NewsTableBuilderInterface;

public interface NewsScreenInterface
{
    boolean isVisible();

    void setVisible(boolean visible);

    Label getNumberOfNewsLabel();

    NewsTableBuilderInterface getNewsTable();
}

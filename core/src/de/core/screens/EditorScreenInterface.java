package de.core.screens;

import com.badlogic.gdx.scenes.scene2d.ui.Button;

import de.core.editorController.EditorController;
import de.core.uiController.EditorScreenController;

public interface EditorScreenInterface
{
    void dispose();

    String getCanonicalNameOfMainScreen();

    Button getChoosePlayerButton();

    EditorScreenController getController();

    EditorController getSelectedType();

    void setSelectedType(EditorController editorController);

    void setMapSize(
            float width,
            float height);
}

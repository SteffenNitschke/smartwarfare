package de.core.screens;

public interface MultiplayerStartScreenInterface
{
    boolean existInternetMultiPlayerScreen();
    void addInternetMultiPlayerScreen();
    void gotoInternetMultiPlayerScreen();
    void gotoMainScreen();
}

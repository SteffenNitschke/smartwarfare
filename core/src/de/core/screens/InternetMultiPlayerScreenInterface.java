package de.core.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import de.core.handler.NotificationHandler;


public interface InternetMultiPlayerScreenInterface
{

    String getCanonicalNameOfPartyMultiPlayerScreen();

    Screen getPartyMultiPlayerScreen();

    void showNewGame();

    void showMatchmaking();

    String getCanonicalNameOfMPStartScreen();

    void removeMapPane();

    void clearNewGame();

    void showMapImage(final FileHandle fileHandle);

    TextButton getChooseGameTypeButton();

    ScrollPane getGameTypePane();
}

package de.core.screens;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import de.core.MainGame;
import de.core.screens.windows.LoginScreenInterface;

public interface MainScreenInterface extends Screen, InputProcessor {

    MainGame getMainGame();

    void dispose();

    String getCanonicalNameOfSinglePlayerScreen();

    Screen getSinglePlayerScreen();

    String getCanonicalNameOfMultiPlayerScreen();

    Screen getMultiPlayerScreen();

    String getCanonicalNameOfEditorScreen();

    Screen getEditorScreen();

    String getCanonicalNameOfSettingsScreen();

    Screen getSettingsScreen();

    String getCanonicalNameOfContactsScreen();

    Screen getContactsScreen();
}

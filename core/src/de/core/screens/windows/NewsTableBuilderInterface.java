package de.core.screens.windows;

import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.core.model.Id;
import de.core.network.messages.news.NewsDto;

public interface NewsTableBuilderInterface
{
    void viewNews(NewsDto news);

    void removeNewsFromTable(Id newsId);

    Table fill();
}

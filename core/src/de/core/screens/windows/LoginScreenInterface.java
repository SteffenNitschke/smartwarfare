package de.core.screens.windows;

import com.badlogic.gdx.scenes.scene2d.ui.TextField;


public interface LoginScreenInterface
{
    void loginSuccessful(boolean successful);

    TextField getNameTextField();

    TextField getPasswordTextField();

    void close();
}

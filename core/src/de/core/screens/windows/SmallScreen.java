package de.core.screens.windows;

public interface SmallScreen
{
    void draw();

    void close();
}

package de.core.screens.windows;

public interface FactoryWindowInterface extends SmallScreen
{
    void resize();
}

package de.core.screens;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import de.core.util.HudInputProcessor;
import de.core.MainGame;
import de.core.model.Game;
import de.core.model.buildungs.BuildingInterface;
import de.core.screens.windows.FactoryWindowInterface;
import de.core.uiController.GameScreenController;
import de.core.util.TurnEventListener;

public interface GameScreenInterface
{
    Game getCurrentGame();

    GameScreenController getScreenController();

    void showNotification(String message);

    ImageTextButton getNextRoundButton();

    Label getCurrentPlayerName();

    Label getInfoBoxFieldTypeLabel();

    Label getInfoBoxDefenceValueLabel();

    boolean isDragged();

    Image getInfoBoxFieldImage();

    Label getInfoBoxFieldPlayerNameLabel();

    Label getInfoBoxCaptureValueLabel();

    void setMapSize(
            float width,
            float height);

    HudInputProcessor getHudProcessor();

    void setScreen(FactoryWindowInterface factoryWindowInterface);

    Label getInfoBoxUnitPlayerNameLabel();

    Label getInfoBoxUnitNameTypeLabel();

    Label getInfoBoxUnitAmmoLabel();

    Label getInfoBoxUnitFuelLabel();

    Image getInfoBoxUnitImage();

    FactoryWindowInterface getNewFactoryWindow(
            HudInputProcessor hudProcessor,
            BuildingInterface building,
            TurnEventListener eventListener,
            MainGame mainGame);
}

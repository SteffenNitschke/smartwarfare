package de.core.screens;

import com.badlogic.gdx.Screen;

public interface SinglePlayerScreenInterface
{
    void expandMenuBar();

    void minimizeMenuBar();

    String getCanonicalNameOfMainScreen();

    Screen getGameScreen();
}

package de.core.network.clients;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashSet;
import java.util.Observable;

import de.core.MainGame;
import de.core.model.Id;
import de.core.network.messages.SmallMapMessage;
import de.core.model.Map;
import de.core.network.NetworkClient;
import de.core.network.events.map.AddMapEvent;
import de.core.network.events.map.GetAllMapsEvent;
import de.core.network.events.map.GetMapEvent;
import de.core.network.mapper.GameMapper;
import de.core.util.Utils;

public class MapClient extends Client implements NetworkClient
{
    private final GameMapper gameMapper = new GameMapper();
    private final ObjectMapper objectMapper = new ObjectMapper();

    private static String BASE_URL = "http://localhost:4567";
    private final String loggingTag = getClass().getSimpleName();
    private PostMapListener postMapListener = new PostMapListener();
    private GetAllMapsListener getAllMapsListener = new GetAllMapsListener();
    private GetMapListener getMapListener = new GetMapListener();

    public MapClient(MainGame game)
    {
        super(game);
    }

    public void postMap(final Map map)
    {
        final String content = gameMapper.writeMapAsString(
                null,
                map);

        Gdx.app.log(loggingTag,
                    "postMap: " + content);

        final Net.HttpRequest httpRequest =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.POST)
                        .url(BASE_URL + "/maps")
                        .header(Utils.AUTHENTICATION_HEADER,
                                "Bearer " + game.getUser().getToken())
                        .content(content)
                        .build();

        Gdx.net.sendHttpRequest(httpRequest,
                                postMapListener);
    }

    public void getAllMaps()
    {
        Gdx.app.log(loggingTag,
                    "getAllMaps ");

        final Net.HttpRequest httpRequest =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.GET)
                        .header(Utils.AUTHENTICATION_HEADER,
                                "Bearer " + game.getUser().getToken())
                        .url(BASE_URL + "/maps")
                        .build();

        Gdx.net.sendHttpRequest(httpRequest,
                                getAllMapsListener);
        Gdx.app.log(loggingTag,
                    "End of Method");
    }

    public void getMapById(final String mapId)
    {
        Gdx.app.log(loggingTag,
                    "getMapById");

        final Net.HttpRequest request =
                new HttpRequestBuilder()
                .newRequest()
                .method(Net.HttpMethods.GET)
                .header(Utils.AUTHENTICATION_HEADER,
                        "Bearer " + game.getUser().getToken())
                .url(BASE_URL + "/maps" + mapId)
                .build();

        Gdx.net.sendHttpRequest(request,
                                getMapListener);
    }

    @Override
    public HashSet<Observable> getObservable()
    {
        final HashSet<Observable> observables = new HashSet<>();
        observables.add(postMapListener);
        observables.add(getAllMapsListener);

        return observables;
    }

    private class GetMapListener extends Observable implements Net.HttpResponseListener
    {

        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            if(httpResponse.getStatus().getStatusCode() == 200)
            {
                try
                {
                    setChanged();
                    notifyObservers(new GetMapEvent(
                            gameMapper.mapDBMapToMap(null,
                                                     new JSONObject(httpResponse.getResultAsString()))
                    ));
                }
                catch (final IOException e)
                {
                    Gdx.app.error(loggingTag,
                                  "Mapping Map",
                                  e);
                }
            }
        }

        @Override
        public void failed(final Throwable t)
        {
            Gdx.app.error(loggingTag,
                          "getMap failed",
                          t);
        }

        @Override
        public void cancelled()
        {
            Gdx.app.error(loggingTag,
                          "getMap cancelled");
        }
    }

    private class PostMapListener extends Observable implements Net.HttpResponseListener
    {
        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            try
            {
                setChanged();
                notifyObservers(new AddMapEvent(
                        objectMapper.readValue(httpResponse.getResultAsString(),
                                               Id.class)));
            }
            catch (final IOException e)
            {
                Gdx.app.error(loggingTag,
                              "Mapping MapId",
                              e);
            }
        }

        @Override
        public void failed(final Throwable t)
        {
            Gdx.app.error(loggingTag,
                          "postMap failed",
                          t);
        }

        @Override
        public void cancelled()
        {
            Gdx.app.error(loggingTag,
                          "postMap cancelled");
        }
    }

    private class GetAllMapsListener extends Observable implements Net.HttpResponseListener
    {
        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            String result = httpResponse.getResultAsString();
            Gdx.app.log(loggingTag,
                        "Get All Maps result: " + result);

            if (httpResponse.getStatus().getStatusCode() != 200)
            {
                //TODO
                return;
            }

            try
            {
                setChanged();
                notifyObservers(new GetAllMapsEvent(
                        objectMapper
                                .readValue(result,
                                           new TypeReference<HashSet<SmallMapMessage>>()
                                           {
                                           })
                ));
            }
            catch (final IOException e)
            {
                Gdx.app.error(loggingTag,
                              "getAllMaps",
                              e);
            }

        }

        @Override
        public void failed(final Throwable throwable)
        {
            Gdx.app.error(loggingTag,
                          "Get All Maps failed",
                          throwable);
        }

        @Override
        public void cancelled()
        {
            Gdx.app.error(loggingTag,
                          "Get All Maps canceled");
        }
    }
}

package de.core.network.clients;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.Collections;
import java.util.HashSet;
import java.util.Observable;

import de.core.MainGame;
import de.core.network.NetworkClient;
import de.core.network.events.DrawEvent;
import de.core.network.messages.news.TurnRequest;
import de.core.network.messages.news.turnMessages.TurnMapper;
import de.core.util.Utils;

public class DrawClient extends Client implements NetworkClient
{
    private String loggingTag = getClass().getSimpleName();
    private DrawListener drawListener = new DrawListener();

    public DrawClient(final MainGame game)
    {
        super(game);
    }

    public void sendDraw(
            final TurnRequest turn,
            final String gameId) throws JsonProcessingException
    {

        Gdx.app.log("sendDraw ",
                    turn.toString());

        final Net.HttpRequest httpRequest =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.POST)
                        .url("http://localhost:4567" + "/draw/" + gameId)
                        .header(Utils.AUTHENTICATION_HEADER,
                                "Bearer " + game.getUser().getToken())
                        .content(new TurnMapper()
                                         .writeValueAsString(turn))
                        .build();


        Gdx.net.sendHttpRequest(httpRequest,
                                drawListener);
        Gdx.app.log(loggingTag,
                    "End of Method");
    }

    @Override
    public HashSet<Observable> getObservable()
    {
        return new HashSet<>(Collections.singleton(drawListener));
    }

    private class DrawListener extends Observable implements Net.HttpResponseListener
    {
        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            final String result = httpResponse.getResultAsString();
            Gdx.app.log(loggingTag,
                        "Send draw: " + result);

            if (httpResponse.getStatus().getStatusCode() == 200)
            {
                setChanged();
                notifyObservers(new DrawEvent(result));
            }
            else
            {
                Gdx.app.error(loggingTag,
                              "Draw not successfully: " + result);
            }
        }

        @Override
        public void failed(final Throwable throwable)
        {
            Gdx.app.error(loggingTag,
                          "failed",
                          throwable);
        }

        @Override
        public void cancelled()
        {
            Gdx.app.error(loggingTag,
                          "canceled");
        }
    }
}

package de.core.network.clients;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashSet;
import java.util.Observable;

import de.core.model.Id;
import lombok.RequiredArgsConstructor;
import de.core.MainGame;
import de.core.network.NetworkClient;
import de.core.network.events.user.invite.FriendAnswerEvent;
import de.core.network.events.user.invite.FriendInvitationEvent;
import de.core.network.messages.news.FriendAnswerRequest;
import de.core.util.Utils;

public class FriendInvitationClient extends Client implements NetworkClient
{
    private final ObjectMapper objectMapper = new ObjectMapper();

    private String loggingTag = getClass().getSimpleName();
    private static String BASE_URL = "http://localhost:4567";
    private FriendInvitationListener friendInvitationListener = new FriendInvitationListener();

    public FriendInvitationClient(final MainGame game)
    {
        super(game);
    }

    public Observable sendFriendAnswer(
            final FriendAnswerRequest answer,
            final Id newsId) throws JsonProcessingException
    {
        Gdx.app.log(loggingTag + " sendFriendAnswer ",
                    answer.toString());

        final Net.HttpRequest httpRequest =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.POST)
                        .url(BASE_URL + "/users/friends/answer")
                        .content(objectMapper.writeValueAsString(answer))
                        .header(Utils.AUTHENTICATION_HEADER,
                                "Bearer " + game.getUser().getToken())
                        .build();

        final FriendInvitationAnswerListener httpResponseListener
                = new FriendInvitationAnswerListener(newsId);

        Gdx.net.sendHttpRequest(httpRequest,
                                httpResponseListener);

        return httpResponseListener;
    }

    private void sendFriendInvitation(final String friendId)
    {
        Gdx.app.log(loggingTag,
                    "sendFreidnInvitation " + friendId);

        final Net.HttpRequest request =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.POST)
                        .url(BASE_URL + "/users/friends/invite/" + friendId)
                        .header(Utils.AUTHENTICATION_HEADER,
                                "Bearer " + game.getUser().getToken())
                        .build();

        Gdx.net.sendHttpRequest(request,
                                friendInvitationListener);
    }

    @Override
    public HashSet<Observable> getObservable()
    {
        final HashSet<Observable> observables = new HashSet<>();
        observables.add(friendInvitationListener);

        return observables;
    }

    private class FriendInvitationListener extends Observable implements Net.HttpResponseListener
    {
        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            if (httpResponse.getStatus().getStatusCode() == 204)
            {
                setChanged();
                notifyObservers(new FriendInvitationEvent(true));
            }
            else
            {
                FriendInvitationClient.this.failed("FriendInvitation",
                                                   httpResponse.getStatus(),
                                                   null);
            }
        }

        @Override
        public void failed(final Throwable throwable)
        {
            FriendInvitationClient.this.failed("FriendInvitation",
                                               null,
                                               throwable);
        }

        @Override
        public void cancelled()
        {
            FriendInvitationClient.this.cancelled("FriendInvitation");
        }
    }

    @RequiredArgsConstructor
    private class FriendInvitationAnswerListener extends Observable
            implements Net.HttpResponseListener
    {
        private final Id newsId;

        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            if (httpResponse.getStatus().getStatusCode() == 204)
            {
                setChanged();
                notifyObservers(new FriendAnswerEvent(
                        newsId,true));
            }
            else
            {
                FriendInvitationClient.this.failed("FriendInvitationAnswer",
                                                   httpResponse.getStatus(),
                                                   null);
            }
        }

        @Override
        public void failed(final Throwable throwable)
        {
            FriendInvitationClient.this.failed("FriendInvitationAnswer",
                                               null,
                                               throwable);
        }

        @Override
        public void cancelled()
        {
            FriendInvitationClient.this.cancelled("FriendInvitationAnswer");
        }
    }
}

package de.core.network.clients;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashSet;
import java.util.Observable;

import de.core.MainGame;
import de.core.network.messages.PlayerMessage;
import de.core.model.User;
import de.core.network.NetworkClient;
import de.core.network.events.user.GetSmallUserEvent;
import de.core.network.events.user.RefreshUserEvent;
import de.core.util.Utils;

public class UserClient extends Client implements NetworkClient
{
    private final String loggingTag = getClass().getSimpleName();
    private static final String BASE_URL = "http://localhost:4567";

    private final ObjectMapper objectMapper = new ObjectMapper();

    private GetUserListener userListener = new GetUserListener();
    private GetSmallUserListener smallUserListener = new GetSmallUserListener();

    public UserClient(MainGame game)
    {
        super(game);
    }

    public void getUser(final String userId)
    {
        Gdx.app.log(loggingTag,
                    "getUser " + userId);


        final Net.HttpRequest httpRequest =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.GET)
                        .url(BASE_URL + "/users")
                        .header(Utils.AUTHENTICATION_HEADER,
                                "Bearer " + game.getUser().getToken())
                        .build();

        Gdx.net.sendHttpRequest(httpRequest,
                                userListener);
    }

    private void getSmallUser(final String userId)
    {
        final Net.HttpRequest request =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.GET)
                        .url(BASE_URL + "/users/" + userId)
                        .header(Utils.AUTHENTICATION_HEADER,
                                "Bearer " + game.getUser().getToken())
                        .build();

        Gdx.net.sendHttpRequest(request,
                                smallUserListener);
    }

    @Override
    public HashSet<Observable> getObservable()
    {
        final HashSet<Observable> observables = new HashSet<>();
        observables.add(userListener);
        observables.add(smallUserListener);

        return observables;
    }

    private class GetUserListener extends Observable implements Net.HttpResponseListener
    {
        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            try
            {
                final User user = objectMapper.readValue(
                        httpResponse.getResultAsString(),
                        User.class);

                Gdx.app.log(loggingTag,
                            "Refresh User result: " + user);

                setChanged();
                notifyObservers(new RefreshUserEvent(user));
            }
            catch (final IOException e)
            {
                e.printStackTrace();
            }
        }

        @Override
        public void failed(final Throwable throwable)
        {
            Gdx.app.error(loggingTag,
                          "Refresh User",
                          throwable);
        }

        @Override
        public void cancelled()
        {
            Gdx.app.error(loggingTag,
                          "Refresh User cancelled");
        }
    }

    private class GetSmallUserListener extends Observable implements Net.HttpResponseListener
    {
        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            try
            {
                final PlayerMessage smallUser =
                        objectMapper.readValue(
                                httpResponse.getResultAsString(),
                                PlayerMessage.class);

                setChanged();
                notifyObservers(new GetSmallUserEvent(smallUser));
            }
            catch (final IOException e)
            {
                e.printStackTrace();
            }
        }

        @Override
        public void failed(final Throwable throwable)
        {
            Gdx.app.error(loggingTag,
                          "Get small User",
                          throwable);
        }

        @Override
        public void cancelled()
        {
            Gdx.app.error(loggingTag,
                          "Get small User cancelled");
        }
    }
}

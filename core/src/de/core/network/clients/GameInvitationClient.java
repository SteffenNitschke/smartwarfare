package de.core.network.clients;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashSet;
import java.util.Observable;

import de.core.model.Id;
import lombok.RequiredArgsConstructor;
import de.core.MainGame;
import de.core.network.NetworkClient;
import de.core.network.events.game.invite.GameInvitationAnswerEvent;
import de.core.network.events.game.invite.GameInvitationEvent;
import de.core.network.mapper.GameMapper;
import de.core.network.messages.news.GameInviteClientRequest;
import de.core.network.messages.news.NewsDto;
import de.core.util.Utils;

public class GameInvitationClient extends Client implements NetworkClient
{
    private final GameMapper gameMapper = new GameMapper();
    private final ObjectMapper objectMapper = new ObjectMapper();

    private String loggingTag = getClass().getSimpleName();
    private static String BASE_URL = "http://localhost:4567";
    private GameInvitationListener gameInvitationListener = new GameInvitationListener();

    public GameInvitationClient(final MainGame game)
    {
        super(game);
    }

    public void sendGameInvitation(final GameInviteClientRequest inviteClientRequest) throws JsonProcessingException
    {
        Gdx.app.log(loggingTag + " sendGameInvitation ",
                    inviteClientRequest.toString());


        Gdx.app.log(loggingTag,
                    inviteClientRequest.toString());

        final Net.HttpRequest httpRequest =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.POST)
                        .url(BASE_URL + "/invite")
                        .content(objectMapper.writeValueAsString(inviteClientRequest))
                        .header(Utils.AUTHENTICATION_HEADER,
                                "Bearer " + game.getUser().getToken())
                        .build();

        Gdx.net.sendHttpRequest(httpRequest,
                                gameInvitationListener);
    }

    public Observable sendGameAnswer(
            final NewsDto gameAnswer,
            final Id newsId) throws JsonProcessingException
    {
        Gdx.app.log(loggingTag + " sendGameAnswer ",
                    gameAnswer.toString());

        final Net.HttpRequest httpRequest =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.POST)
                        .url(BASE_URL + "/answer/games")
                        .content(objectMapper.writeValueAsString(gameAnswer))
                        .header(Utils.AUTHENTICATION_HEADER,
                                "Bearer " + game.getUser().getToken())
                        .build();

        final GameInvitationAnswerListener listener = new GameInvitationAnswerListener(newsId);

        Gdx.net.sendHttpRequest(httpRequest,
                                listener);

        return listener;
    }

    @Override
    public HashSet<Observable> getObservable()
    {
        final HashSet<Observable> observables = new HashSet<>();
        observables.add(gameInvitationListener);

        return observables;
    }

    @RequiredArgsConstructor
    private class GameInvitationAnswerListener extends Observable
            implements Net.HttpResponseListener
    {
        private final Id newsId;

        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            if (httpResponse.getStatus().getStatusCode() == 200)
            {
                try
                {
                    setChanged();
                    notifyObservers(new GameInvitationAnswerEvent(
                            newsId,
                            gameMapper.mapDBGameStringToGame(httpResponse.getResultAsString())
                    ));
                }
                catch (final IOException exception)
                {
                    GameInvitationClient.this.failed("Notify GameInvitationAnswer",
                                                     httpResponse.getStatus(),
                                                     exception);
                }
            }
            else
            {
                game.notificationHandler().showUserNotification(
                        "Antwort konnte nicht gesendet werden. Code: " +
                                httpResponse.getStatus().getStatusCode(),
                        false);
            }
        }

        @Override
        public void failed(final Throwable throwable)
        {
            GameInvitationClient.this.failed("GameInvitationAnswer",
                                             null,
                                             throwable);
        }

        @Override
        public void cancelled()
        {
            GameInvitationClient.this.cancelled("GameInvitationAnswer");
        }
    }

    private class GameInvitationListener extends Observable implements Net.HttpResponseListener
    {
        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            if (httpResponse.getStatus().getStatusCode() == 200)
            {
                try
                {
                    setChanged();
                    notifyObservers(
                            new GameInvitationEvent(
                                    gameMapper
                                            .mapDBGameStringToGame(httpResponse
                                                                           .getResultAsString())));
                }
                catch (final IOException exception)
                {
                    GameInvitationClient.this.failed("Notify GameInvitation",
                                                     httpResponse.getStatus(),
                                                     exception);
                }
            }
            else
            {
                game.notificationHandler().showUserNotification(
                        "Spiel konnte nicht erstellt werden. Code: " +
                                httpResponse.getStatus().getStatusCode(),
                        false);
            }
        }

        @Override
        public void failed(final Throwable throwable)
        {
            GameInvitationClient.this.failed("GameInvitation",
                                             null,
                                             throwable);
        }

        @Override
        public void cancelled()
        {
            GameInvitationClient.this.cancelled("GameInvitation");
        }
    }
}

package de.core.network.clients;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.net.HttpStatus;

import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;

import de.core.MainGame;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class Client
{
    protected final MainGame game;

    public abstract HashSet<Observable> getObservable();

    public void addObservers(final Observer observer)
    {
        for (Observable observable : getObservable())
        {
            observable.addObserver(observer);
        }
    }

    protected void failed(
            final String message,
            final HttpStatus status,
            final Throwable throwable)
    {
        Gdx.app.error(getClass().getCanonicalName(),
                      message + " failed",
                      throwable);
        game.notificationHandler().showSystemNotification(
                message + " failed " +
                        (status != null ? " Code: " + status.getStatusCode(): ""));
    }

    protected void cancelled(final String message)
    {
        Gdx.app.error(getClass().getCanonicalName(),
                      message + " canceled");
        game.notificationHandler().showSystemNotification(message + " cancelled");
    }
}

package de.core.network.clients;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashSet;
import java.util.Observable;

import de.core.MainGame;
import de.core.model.Id;
import de.core.network.NetworkClient;
import de.core.network.events.game.GetAllGamesEvent;
import de.core.network.events.game.GetGameEvent;
import de.core.network.mapper.GameMapper;
import de.core.util.Utils;

public class GameClient extends Client implements NetworkClient
{
    private final GameMapper gameMapper = new GameMapper();
    private final ObjectMapper objectMapper = new ObjectMapper();

    private String loggingTag = getClass().getSimpleName();
    private static String BASE_URL = "http://localhost:4567";
    private GetAllGamesListener getAllGamesListener = new GetAllGamesListener();
    private GetGameByIdListener getGameByIdListener = new GetGameByIdListener();

    public GameClient(final MainGame game)
    {
        super(game);
    }

    public void getAllGameOf(final String userId)
    {
        Gdx.app.log(loggingTag,
                    "getAllGamesOf " + userId);

        final Net.HttpRequest httpRequest =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.GET)
                        .url(BASE_URL + "/games/of/" + userId)
                        .header(Utils.AUTHENTICATION_HEADER,
                                "Bearer " + game.getUser().getToken())
                        .build();

        Gdx.net.sendHttpRequest(httpRequest,
                                getAllGamesListener);
        Gdx.app.log(loggingTag,
                    "End of Method");
    }

    public void getGameById(final String gameId)
    {
        Gdx.app.log(loggingTag,
                    "getGameById " + gameId);

        final Net.HttpRequest request =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.GET)
                        .url(BASE_URL + "/games/" + gameId)
                        .header(Utils.AUTHENTICATION_HEADER,
                                "Bearer " + game.getUser().getToken())
                        .build();

        Gdx.net.sendHttpRequest(request,
                                getGameByIdListener);
    }

    @Override
    public HashSet<Observable> getObservable()
    {
        final HashSet<Observable> observables = new HashSet<>();
        observables.add(getAllGamesListener);
        observables.add(getGameByIdListener);

        return observables;
    }

    private class GetGameByIdListener extends Observable implements Net.HttpResponseListener
    {

        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            if (httpResponse.getStatus().getStatusCode() == 200)
            {
                try
                {
                    setChanged();
                    notifyObservers(new GetGameEvent(gameMapper.mapDBGameStringToGame(httpResponse
                                                                                              .getResultAsString())));
                }
                catch (final IOException exception)
                {
                    GameClient.this.failed("Notify GetGame",
                                           httpResponse.getStatus(),
                                           exception);
                }
            }
            else
            {
                GameClient.this.failed("GetGame",
                                       httpResponse.getStatus(), null);
            }
        }

        @Override
        public void failed(final Throwable throwable)
        {
            GameClient.this.failed("GetGame",
                                   null,
                                   throwable);
        }

        @Override
        public void cancelled()
        {
            GameClient.this.cancelled("GetGame");
        }
    }

    private class GetAllGamesListener extends Observable implements Net.HttpResponseListener
    {
        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            if (httpResponse.getStatus().getStatusCode() == 200)
            {
                try
                {
                    setChanged();
                    notifyObservers(new GetAllGamesEvent(
                            objectMapper.readValue(httpResponse.getResultAsString(),
                                                   new TypeReference<HashSet<Id>>()
                                                   {
                                                   })
                    ));
                }
                catch (final IOException exception)
                {
                    GameClient.this.failed("Notify GetAllGames",
                                           httpResponse.getStatus(),
                                           exception);
                }
            }
            else
            {
                GameClient.this.failed("GetAllGames", httpResponse.getStatus(), null);
            }
        }

        @Override
        public void failed(final Throwable throwable)
        {
            GameClient.this.failed("GetAllGames", null, throwable);
        }

        @Override
        public void cancelled()
        {
            GameClient.this.cancelled("GetAllGames");
        }
    }
}

package de.core.network.clients;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashSet;
import java.util.Observable;

import de.core.MainGame;
import de.core.model.Id;
import de.core.network.messages.LoginRequest;
import de.core.network.messages.news.turnMessages.RegistrateRequest;
import de.core.model.User;
import de.core.network.NetworkClient;
import de.core.network.events.account.LoginEvent;
import de.core.network.events.account.LogoutEvent;
import de.core.network.events.account.RegistrationEvent;
import de.core.util.Utils;

public class AccountClient extends Client implements NetworkClient
{

    private static String BASE_URL = "http://localhost:4567";
    private final String loggingTag = getClass().getSimpleName();

    private final ObjectMapper objectMapper = new ObjectMapper();
    private LoginListener loginListener = new LoginListener();
    private LogoutListener logoutListener = new LogoutListener();
    private RegistrationListener registrationListener = new RegistrationListener();

    public AccountClient(MainGame game)
    {
        super(game);
    }

    public void login(
            final String userName,
            final String password)
    {

        Gdx.app.log(loggingTag,
                    "login: " + userName + " " + password);

        final Net.HttpRequest httpRequest =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.POST)
                        .url(BASE_URL + "/accounts/login")
                        .basicAuthentication(userName,
                                             password)
                        .build();

        Gdx.net.sendHttpRequest(httpRequest,
                                loginListener);
    }

    public void logout()
    {
        Gdx.app.log(loggingTag,
                    "logout");

        final Net.HttpRequest httpRequest =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.POST)
                        .url(BASE_URL + "/accounts/logout")
                        .header(Utils.AUTHENTICATION_HEADER,
                                game.getUser().getToken())
                        .build();

        Gdx.net.sendHttpRequest(httpRequest,
                                logoutListener);

    }

    public void sendRegistrate(
            final String name,
            final String password) throws JsonProcessingException
    {
        Gdx.app.log(loggingTag + " sendRegistrate ",
                    name + " " + password);

        final RegistrateRequest message =
                new RegistrateRequest(name,
                                      password);


        final Net.HttpRequest httpRequest =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.POST)
                        .url(BASE_URL + "/accounts/registrate")
                        .content(objectMapper.writeValueAsString(message))
                        .build();

        Gdx.net.sendHttpRequest(httpRequest,
                                registrationListener);
    }

    @Override
    public HashSet<Observable> getObservable()
    {
        final HashSet<Observable> observables = new HashSet<>();
        observables.add(loginListener);
        observables.add(logoutListener);
        observables.add(registrationListener);

        return observables;
    }

    private class LogoutListener extends Observable implements Net.HttpResponseListener
    {

        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            setChanged();
            notifyObservers(
                    new LogoutEvent(
                            httpResponse.getStatus().getStatusCode() == 200));
        }

        @Override
        public void failed(final Throwable t)
        {
            Gdx.app.error(loggingTag,
                          "Logout failed",
                          t);
        }

        @Override
        public void cancelled()
        {
            Gdx.app.error(loggingTag,
                          "Logout canceled");
        }
    }

    private class LoginListener extends Observable implements Net.HttpResponseListener
    {
        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            final String body = httpResponse.getResultAsString();

            Gdx.app.log(loggingTag,
                        "Login Response: " + body);

            if(httpResponse.getStatus().getStatusCode() == 401)
            {
                setChanged();
                notifyObservers(new LoginEvent(null, false));
            }
            else if(httpResponse.getStatus().getStatusCode() == 200)
            {
                try
                {
                    final LoginRequest message = objectMapper.readValue(body,
                                                                        LoginRequest.class);

                    final User user = new User(message.getName());
                    user.set_id(new Id(message.getUserId()));
                    user.setToken(message.getToken());

                    setChanged();
                    notifyObservers(new LoginEvent(user,
                                                   true));
                }
                catch (IOException e)
                {
                    Gdx.app.error(loggingTag,
                                  "login",
                                  e);
                }
            }
        }

        @Override
        public void failed(final Throwable throwable)
        {
            Gdx.app.error(loggingTag,
                          "failed",
                          throwable);
        }

        @Override
        public void cancelled()
        {
            Gdx.app.error(loggingTag,
                          "Login canceled!");
        }
    }

    private class RegistrationListener extends Observable implements Net.HttpResponseListener
    {
        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            setChanged();
            notifyObservers(
                    new RegistrationEvent(
                            httpResponse.getStatus().getStatusCode() == 201));
        }

        @Override
        public void failed(final Throwable t)
        {
            Gdx.app.error(loggingTag,
                          "registrate failed",
                          t);
        }

        @Override
        public void cancelled()
        {
            Gdx.app.error(loggingTag,
                          "registrate cancelled");
        }
    }
}

package de.core.network.clients;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashSet;
import java.util.Observable;

import de.core.MainGame;
import de.core.model.Id;
import de.core.network.messages.NewsMapper;
import de.core.network.NetworkClient;
import de.core.network.events.news.GetNewsEvent;
import de.core.network.events.news.MarkNewsAsReadedEvent;
import de.core.util.Utils;

public class NewsClient extends Client implements NetworkClient
{
    private final NewsMapper newsMapper = new NewsMapper();
    private final ObjectMapper objectMapper = new ObjectMapper();

    private String loggingTag = getClass().getSimpleName();
    private static String BASE_URL = "http://localhost:4567";
    private GetNewsListener getNewsListener = new GetNewsListener();
    private MarkNewsAsReadedListener markNewsAsReadedListener = new MarkNewsAsReadedListener();

    public NewsClient(MainGame game)
    {
        super(game);
    }

    public void getNews()
    {
        Gdx.app.log(loggingTag,
                    "getNews " + game.getUser().get_id());

        final Net.HttpRequest httpRequest =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.GET)
                        .url(BASE_URL + "/news")
                        .header(Utils.AUTHENTICATION_HEADER,
                                "Bearer " + game.getUser().getToken())
                        .build();

        Gdx.net.sendHttpRequest(httpRequest,
                                getNewsListener);
    }

    public void markNewsAsReaded(final String newsId)
    {
        Gdx.app.log(loggingTag,
                    "markAsReaded " + newsId);

        final Net.HttpRequest request =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.PUT)
                        .url(BASE_URL + "/news/" + newsId)
                        .header(Utils.AUTHENTICATION_HEADER,
                                "Bearer " + game.getUser().getToken())
                        .build();

        Gdx.net.sendHttpRequest(request,
                                markNewsAsReadedListener);
    }

    @Override
    public HashSet<Observable> getObservable()
    {
        final HashSet<Observable> observables = new HashSet<>();
        observables.add(getNewsListener);
        observables.add(markNewsAsReadedListener);

        return observables;
    }

    private class MarkNewsAsReadedListener extends Observable implements Net.HttpResponseListener
    {
        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            try
            {
                setChanged();
                notifyObservers(new MarkNewsAsReadedEvent(
                        httpResponse.getStatus().getStatusCode() == 200,
                                objectMapper.readValue(httpResponse.getResultAsString(), Id.class)));
            }
            catch (final IOException e)
            {
                e.printStackTrace();
            }
        }

        @Override
        public void failed(final Throwable t)
        {
            Gdx.app.error(loggingTag,
                          "MarkNewsAsReaded failed",
                          t);
        }

        @Override
        public void cancelled()
        {
            Gdx.app.error(loggingTag,
                          "MarkNewsAsReaded canceled");
        }
    }

    private class GetNewsListener extends Observable implements Net.HttpResponseListener
    {
        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            if (httpResponse.getStatus().getStatusCode() == 200)
            {
                try
                {
                    setChanged();
                    notifyObservers(new GetNewsEvent(newsMapper.readValue(httpResponse
                                                                                  .getResultAsString())));
                }
                catch (final IOException e)
                {
                    Gdx.app.error(loggingTag,
                                  "handle News",
                                  e);
                }
            }
        }

        @Override
        public void failed(final Throwable throwable)
        {
            Gdx.app.error(loggingTag,
                          "Get News",
                          throwable);
        }

        @Override
        public void cancelled()
        {
            Gdx.app.error(loggingTag,
                          "Get News cancelled");
        }
    }
}

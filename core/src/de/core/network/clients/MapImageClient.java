package de.core.network.clients;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.net.HttpRequestBuilder;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Observable;

import de.core.MainGame;
import de.core.model.Id;
import de.core.network.NetworkClient;
import de.core.network.events.map.GetMapImageEvent;
import de.core.util.Utils;
import lombok.RequiredArgsConstructor;

public class MapImageClient extends Client implements NetworkClient
{
    private static String BASE_URL = "http://localhost:4567";
    private final String loggingTag = getClass().getSimpleName();

    public MapImageClient(MainGame game)
    {
        super(game);
    }

    public Observable getMapImageById(final Id mapId)
    {
        Gdx.app.log(loggingTag,
                    "getMapImageById " + mapId.getIdentifier());

        final Net.HttpRequest httpRequest =
                new HttpRequestBuilder()
                        .newRequest()
                        .method(Net.HttpMethods.GET)
                        .header(Utils.AUTHENTICATION_HEADER,
                                "Bearer " + game.getUser().getToken())
                        .url(BASE_URL + "/maps/image/" + mapId.getIdentifier())
                        .build();

        final GetMapImageListener listener = new GetMapImageListener(mapId);

        Gdx.net.sendHttpRequest(httpRequest,
                                listener);
        Gdx.app.log(loggingTag,
                    "End of Method");

        return listener;
    }

    @RequiredArgsConstructor
    private class GetMapImageListener extends Observable implements Net.HttpResponseListener
    {
        private final Id mapId;

        @Override
        public void handleHttpResponse(final Net.HttpResponse httpResponse)
        {
            if (httpResponse.getStatus().getStatusCode() == 200)
            {
                final byte[] responseBody = httpResponse.getResult();

                final File mapImageFile = new File("MapImage" + mapId.getIdentifier());

                try
                {
                    FileUtils.writeByteArrayToFile(mapImageFile,
                                                   responseBody);

                }
                catch (final IOException exception)
                {
                    Gdx.app.error(loggingTag,
                                  "writeByteArrayAsFile",
                                  exception);
                }

                setChanged();
                notifyObservers(new GetMapImageEvent(
                        mapId,
                        new FileHandle(mapImageFile)));
            }
        }

        @Override
        public void failed(final Throwable t)
        {
            Gdx.app.error(loggingTag,
                          "getMapImage failed",
                          t);
        }

        @Override
        public void cancelled()
        {
            Gdx.app.error(loggingTag,
                          "getMapImage cancelled");
        }
    }

    @Override
    public HashSet<Observable> getObservable()
    {
        return null;
    }
}

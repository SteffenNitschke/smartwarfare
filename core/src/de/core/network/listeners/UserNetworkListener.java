package de.core.network.listeners;

import java.util.Observable;
import java.util.Observer;

import de.core.MainGame;
import de.core.model.User;
import de.core.network.events.user.RefreshUserEvent;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserNetworkListener implements Observer
{
    private final MainGame game;

    @Override
    public void update(
            final Observable observable,
            final Object event)
    {
        if(event instanceof RefreshUserEvent)
        {
            handleRefreshUserEvent((RefreshUserEvent) event);
        }
    }

    private void handleRefreshUserEvent(final RefreshUserEvent event)
    {
        final User user = event.getUser();
        user.setToken(game.getUser().getToken());

        game.setUser(user);
    }
}

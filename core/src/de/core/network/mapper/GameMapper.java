package de.core.network.mapper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import de.core.model.Field;
import de.core.model.Game;
import de.core.model.Map;
import de.core.model.Player;
import de.core.model.buildungs.BuildingInterface;
import de.core.model.units.UnitInterface;
import de.core.util.GameStatus;
import de.core.util.GameTypes;
import de.core.model.buildungs.BuildingBuilder;
import de.core.model.units.UnitBuilder;

public class GameMapper
{

    private ObjectMapper objectMapper = new ObjectMapper();
    private String loggingTag = getClass().getSimpleName();

    public Game mapDBGameStringToGame(String string) throws IOException
    {

        Gdx.app.log(loggingTag,
                    "mapDBGameStringToGame " + string);
        JSONObject object = new JSONObject(string);
        Game game = new Game(object.getJSONObject("_id").getString("identifier"));
        game.setGameType(GameTypes.valueOf(object.getString("gameType")));
        game.setStatus(GameStatus.valueOf(object.getString("status")));
//        game.setChatId(object.getString("chatId"));

        mapDBMapToMap(game,
                      object.getJSONObject("map"));

        mapPlayer(game,
                  object.getJSONArray("players"));

        //connect Buildings with Player
        game.getMap().getFields().stream()
            .filter(field -> field.getBuilding() != null)
            .filter(field -> field.getBuilding().getPlayer() != null)
            .forEach(field -> field.getBuilding().setPlayer(
                    game.getPlayer().stream()
                        .filter(player -> player.getUserID().equals(
                                field.getBuilding().getPlayer().getUserID()))
                        .findFirst()
                        .orElse(null))
            );

        //connect Building with WannerCapture
        game.getMap().getFields().stream()
            .filter(field -> field.getBuilding() != null)
            .filter(field -> field.getBuilding().getWannerCapture() != null)
            .forEach(field -> field.getBuilding().setWannaCapture(
                    game.getPlayer().stream()
                        .filter(player -> player.getUserID().equals(
                                field.getBuilding().getWannerCapture().getUserID()))
                        .findFirst()
                        .orElse(null))
            );

        game.setCurrentPlayer(game.getPlayer().stream()
                                  .filter(player -> player.getUserID()
                                                          .equals(
                                                                  object.getJSONObject("currentPlayer")
                                                                        .getString("identifier")))
                                  .findFirst().orElse(null));

        return game;
    }

    private void mapPlayer(
            Game game,
            JSONArray playerArray) throws IOException
    {
        for (int i = 0; i < playerArray.length(); i++)
        {

            JSONObject pObject = playerArray.getJSONObject(i);
            Player player = new Player(pObject.getJSONObject("userId").getString("identifier"));
            player.setColor(objectMapper.readValue(pObject.getJSONObject("color").toString(),
                                                   Color.class));
            player.setGame(game);
            player.setName(pObject.getString("name"));
            game.getPlayer().add(player);
            player.getTroops().addAll(mapDBUnitsToUnit(pObject,
                                                       game));
            //TODO need same for heroes
        }

        for (int i = 0; i < playerArray.length(); i++)
        {
            final JSONObject pObject = playerArray.getJSONObject(i);
            final String playerId = pObject.getJSONObject("userId").getString("identifier");
            final String nextId = pObject.getJSONObject("nextPlayerId").getString("identifier");
            final String prevId = pObject.getJSONObject("prevPlayerId").getString("identifier");

            final Player currentPlayer = game.getPlayer()
                                       .stream()
                                       .filter(player -> player.getUserID().equals(playerId))
                                       .findFirst().orElse(null);

            game.getPlayer()
                .stream()
                .filter(player -> player.getUserID().equals(nextId))
                .forEach(player -> player.setPrev(currentPlayer));

            game.getPlayer()
                .stream()
                .filter(player -> player.getUserID().equals(prevId))
                .forEach(player -> player.setNext(currentPlayer));
            System.out.println("Player: " + currentPlayer);
        }

    }

    private Collection<? extends UnitInterface> mapDBUnitsToUnit(
            final JSONObject pObject,
            final Game game)
    {

        if (pObject.isNull("troops"))
        {
            return new HashSet<>();
        }

        final JSONArray units = pObject.getJSONArray("troops");
//        Gdx.app.log(loggingTag, "mapDBUnitToUnit " + units);
        final HashSet<UnitInterface> unitsSet = new HashSet<>();

        for (int i = 0; i < units.length(); i++)
        {
            final JSONObject jsonUnit = units.getJSONObject(i);
            UnitInterface unit = UnitBuilder.build(jsonUnit.getString("type"));
            unit.setUnitId(jsonUnit.getString("unitId"));
            unit.setRange(jsonUnit.getInt("range"));
            unit.setHealth(jsonUnit.getDouble("health"));
            unit.setDefence(jsonUnit.getInt("defence"));
            unit.setAttack(jsonUnit.getInt("attack"));
            unit.setFuel(jsonUnit.getInt("fuel"));
            unit.setAmmo(jsonUnit.getInt("ammo"));
            unit.setPlayer(game.getPlayer().stream()
                               .filter(player -> player.getUserID()
                                                       .equals(jsonUnit.getString("player")))
                               .findFirst().orElse(null));
            unit.setField(game.getMap().getFields().stream()
                              .filter(field -> field.getFieldID()
                                                    .equals(jsonUnit.getString("field")))
                              .findFirst().orElse(null));

            unitsSet.add(unit);
            System.out.println(unit);
        }

        return unitsSet;
    }

    public Map mapDBMapToMap(
            Game game,
            JSONObject mapObject) throws IOException
    {
//        Gdx.app.log(loggingTag, "mapDBMapToMap " + mapObject);
        Map map = new Map(mapObject.getString("name"));
        map.set_id(mapObject.getJSONObject("_id").getString("identifier"));
        map.setEditorName(mapObject.getString("editorName"));
        map.setNumberOfPlayers(mapObject.getInt("numberOfPlayers"));
        map.setMaxX(mapObject.getInt("maxX"));
        map.setMaxY(mapObject.getInt("maxY"));
        map.setFields(mapFields(mapObject.getJSONArray("fields")));
        if (game != null)
        {
            map.setGame(game);
            game.setMap(map);
        }

        for (Field field : map.getFields())
        {
            field.setMap(map);
            if (field.getX() != 0)
            {
                field.setLeft(map.getFields().stream()
                                 .filter(field1 -> field1.getY() == field.getY())
                                 .filter(field1 -> field1.getX() == field.getX() - 1)
                                 .findFirst().orElse(null));
            }
            if (field.getX() != map.getMaxX() - 1)
            {
                field.setRight(map.getFields().stream()
                                  .filter(field1 -> field1.getY() == field.getY())
                                  .filter(field1 -> field1.getX() == field.getX() + 1)
                                  .findFirst().orElse(null));
            }
            if (field.getY() != 0)
            {
                field.setUpper(map.getFields().stream()
                                  .filter(field1 -> field1.getX() == field.getX())
                                  .filter(field1 -> field1.getY() == field.getY() - 1)
                                  .findFirst().orElse(null));
            }
            if (field.getY() != map.getMaxY() - 1)
            {
                field.setLower(map.getFields().stream()
                                  .filter(field1 -> field1.getX() == field.getX())
                                  .filter(field1 -> field1.getY() == field.getY() + 1)
                                  .findFirst().orElse(null));
            }
        }

        JSONArray array = mapObject.getJSONArray("fields");

        for (int i = 0; i < array.length(); i++)
        {

            JSONObject field = array.getJSONObject(i);

            if (!field.get("building").toString().equals("null"))
            {
                String fieldId = field.getString("fieldID");
//                Gdx.app.log(loggingTag, "Field " + fieldId + " get Building");
                map.getFields().stream()
                   .filter(field1 -> field1.getFieldID().equals(fieldId))
                   .forEach(field1 -> field1.setBuilding(mapBuilding(game,
                                                                     field.getJSONObject("building"))));

            }
        }

        return map;
    }

    private List<Field> mapFields(JSONArray field) throws IOException
    {

//        Gdx.app.log(loggingTag, "mapFields " + field);
        List<Field> fields = new ArrayList<>();
        for (int i = 0; i < field.length(); i++)
        {
            Field field1 = objectMapper.readValue(field.getJSONObject(i).toString(),
                                                  Field.class);
//            Gdx.app.log(loggingTag, field1.getFieldID() + ", " + field1.getType());
            fields.add(field1);
        }
        return fields;
    }

    private BuildingInterface mapBuilding(
            Game game,
            JSONObject buildingObject)
    {

//        Gdx.app.log(loggingTag, "mapBuilding " + buildingObject);
        BuildingInterface buildingInterface = BuildingBuilder.build(buildingObject
                                                                            .getString("type"));
        buildingInterface.setCaptureValue(buildingObject.getInt("captureValue"));
        buildingInterface.setId(buildingObject.getString("_id"));
        buildingInterface.setGame(game);
        game.getBuildings().add(buildingInterface);
        if (!buildingObject.get("user").toString().equals("null"))
        {
//            Gdx.app.log(loggingTag, "Building have User");
            buildingInterface.setPlayer(new Player(buildingObject.getString("user")));
        }
        if (!buildingObject.get("wannerCapture").toString().equals("null"))
        {
            buildingInterface.setPlayer(new Player(buildingObject.getString("wannerCapture")));
        }
        return buildingInterface;
    }

    public String writeGameAsString(Game game) throws JsonProcessingException
    {

        JSONObject result = new JSONObject();

        result.put("round",
                   game.getRound());
        result.put("currentPlayer",
                   game.getCurrentPlayer().getUserID());
        result.put("gameType",
                   game.getGameType());
        result.put("status",
                   game.getStatus());
        result.put("chatId",
                   game.getChatId());
        result.put("players",
                   writePlayerAsString(game.getPlayer()));
        result.put("playerBuildings",
                   writeBuildingsAsString(game.getBuildings()));
        result.put("map",
                   writeMapAsString(game.getGameID(),
                                    game.getMap()));

        return result.toString();
    }

    public String writeMapAsString(
            String gameId,
            Map map)
    {

        JSONObject mapObj = new JSONObject();

        mapObj.put("_id",
                   map.get_id());
        mapObj.put("game",
                   gameId);
        mapObj.put("name",
                   map.getName());
        mapObj.put("maxY",
                   map.getMaxY());
        mapObj.put("maxX",
                   map.getMaxX());
        mapObj.put("editorName",
                   map.getEditorName());
        mapObj.put("numberOfPlayers",
                   map.getNumberOfPlayers());
        mapObj.put("fields",
                   writeMapAsString(map.getFields()));

        JSONArray units = new JSONArray();
        JSONArray oldFields = mapObj.getJSONArray("fields");
        JSONArray newFields = new JSONArray();

        for (int i = 0; i < oldFields.length(); i++)
        {

            JSONObject field = oldFields.getJSONObject(i);
            if (field.has("unit") && field.get("unit") != null)
            {
                units.put(field.getJSONObject("unit"));
                field.put("unit",
                          field.getJSONObject("unit").getString("unitId"));
            }
            newFields.put(field);
        }
        mapObj.remove("fields");
        mapObj.put("fields",
                   newFields);
        mapObj.put("units",
                   units);

        return mapObj.toString();
    }

    private JSONArray writeMapAsString(List<Field> fields)
    {

        JSONArray fieldArray = new JSONArray();

        for (Field field : fields)
        {

            JSONObject fieldObj = new JSONObject();
            fieldObj.put("type",
                         field.getType());
            fieldObj.put("x",
                         field.getX());
            fieldObj.put("y",
                         field.getY());
            fieldObj.put("defence",
                         field.getDefence());
            fieldObj.put("fieldID",
                         field.getFieldID());
            if (field.getUnit() != null)
            {
                fieldObj.put("unit",
                             writeUnitAsString(field.getUnit()));
            }
            if (field.getBuilding() != null)
            {
                fieldObj.put("building",
                             writeBuildingAsString(field.getBuilding()));
            }
            fieldArray.put(fieldObj);
        }

        return fieldArray;
    }

    private JSONObject writeUnitAsString(UnitInterface unit)
    {

        JSONObject unitObj = new JSONObject();
        unitObj.put("unitId",
                    unit.getUnitId());
        unitObj.put("type",
                    unit.getType());
        unitObj.put("attack",
                    unit.getAttack());
        unitObj.put("defence",
                    unit.getDefence());
        unitObj.put("health",
                    unit.getHealth());
        unitObj.put("field",
                    unit.getField().getFieldID());
        unitObj.put("range",
                    unit.getRange());
        unitObj.put("ammo",
                    unit.getAmmo());
        unitObj.put("fuel",
                    unit.getFuel());
        unitObj.put("minFireRange",
                    unit.getMinFireRange());
        unitObj.put("maxFireRange",
                    unit.getMaxFireRange());
        if (unit.getPlayer() != null)
        {
            unitObj.put("player",
                        unit.getPlayer().getUserID());
        }
        unitObj.put("possibleFieldTypes",
                    unit.getPossibleFieldTypes());
        unitObj.put("possibleTargets",
                    unit.getPossobleTargets());

        return unitObj;
    }

    private JSONArray writeBuildingsAsString(HashSet<BuildingInterface> buildings)
    {

        JSONArray buildingArray = new JSONArray();

        for (BuildingInterface building : buildings)
        {

            JSONObject buildingObj = writeBuildingAsString(building);

            buildingArray.put(buildingObj);
        }

        return buildingArray;
    }

    private JSONObject writeBuildingAsString(BuildingInterface building)
    {

        Field field = building.getField();
        JSONObject buildingObj = new JSONObject();
        buildingObj.put("type",
                        building.getType());
        buildingObj.put("captureValue",
                        building.getCaptureValue());
        buildingObj.put("_id",
                        building.getId());
        if (building.getPlayer() != null)
        {
            buildingObj.put("user",
                            building.getPlayer().getUserID() + field.getFieldID());
        }
        if (building.getWannerCapture() != null)
        {
            buildingObj.put("wannerCapture",
                            building.getWannerCapture().getUserID() + field.getFieldID());
        }

        return buildingObj;
    }

    private JSONArray writePlayerAsString(HashSet<Player> players) throws JsonProcessingException
    {

        JSONArray playerArray = new JSONArray();

        for (Player player : players)
        {

            JSONObject playerObj = new JSONObject();
            playerObj.put("userId",
                          player.getUserID());
            playerObj.put("color",
                          player.getColor());
            playerObj.put("nextPlayerId",
                          player.getNext().getUserID());
            playerObj.put("prevPlayerId",
                          player.getPrev().getUserID());
            playerObj.put("name",
                          player.getName());
            playerObj.put("money",
                          player.getMoney());
            playerObj.put("troops",
                          objectMapper.writeValueAsBytes(player.getTroops()));
            playerObj.put("heroes",
                          objectMapper.writeValueAsBytes(player.getHeroes()));

            playerArray.put(playerObj);
        }

        return playerArray;
    }
}
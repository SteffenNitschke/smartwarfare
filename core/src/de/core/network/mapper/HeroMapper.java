package de.core.network.mapper;

import de.core.model.units.heroes.Hero;
import de.core.model.units.heroes.HeroInterface;
import org.json.JSONObject;

public class HeroMapper {

    public HeroInterface mapHero(String message) {

        JSONObject hero = new JSONObject(message);
        HeroInterface heroInterface = new Hero(hero.getString("unitId"));
        heroInterface.setAttack(hero.getInt("attack"));
        heroInterface.setDefence(hero.getInt("defence"));
        heroInterface.setAmmo(hero.getInt("ammo"));
        heroInterface.setFuel(hero.getInt("fuel"));
        heroInterface.setHealth(hero.getDouble("health"));
        heroInterface.setRange(hero.getInt("range"));
        heroInterface.withFireRange(hero.getInt("minFireRange"), hero.getInt("maxFireRange"));
        //TODO set active and passive Abilities

        return heroInterface;
    }

}

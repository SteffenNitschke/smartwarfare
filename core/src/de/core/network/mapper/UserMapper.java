package de.core.network.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

import de.core.model.Id;
import de.core.model.User;
import de.core.model.units.heroes.HeroInterface;
import de.core.network.messages.PlayerMessage;

public class UserMapper {

    private ObjectMapper objectMapper = new ObjectMapper();
    private HeroMapper heroMapper = new HeroMapper();

    public User mapUser(String dbUser) throws IOException {

        JSONObject userObject = new JSONObject(dbUser);

        User user = new User(userObject.getString("name"));
        user.set_id(new Id(userObject.getString("_id")));
        user.setExperience(userObject.getLong("experience"));
        user.setLevel(userObject.getInt("level"));

        user.getFriends().addAll(mapFriends(userObject.getJSONArray("friends")));

        user.getHeroes().addAll(mapHeroes(userObject.getJSONArray("heroes")));

        return user;
    }

    private HashSet<HeroInterface> mapHeroes(JSONArray heros) {

        HashSet<HeroInterface> heroSet = new HashSet<>();

        if(heros != null) {
            for (int i = 0; i < heros.length(); i++) {
                heroSet.add(heroMapper.mapHero(heros.getJSONObject(i).toString()));
            }
        }
        return heroSet;
    }

    private Collection<PlayerMessage> mapFriends(JSONArray friends) throws IOException {

        HashSet<PlayerMessage> friendSet = new HashSet<>();

        if(friends != null) {
            for(int i = 0; i < friends.length(); i++) {
                friendSet.add(objectMapper.readValue(friends.getJSONObject(i).toString(), PlayerMessage.class));
            }
        }
        return friendSet;
    }
}

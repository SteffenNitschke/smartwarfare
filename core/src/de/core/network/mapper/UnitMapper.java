package de.core.network.mapper;

import de.core.model.units.UnitInterface;
import de.core.model.units.UnitBuilder;
import org.json.JSONObject;

public class UnitMapper {

    public UnitInterface mapUnit(String message) {

        JSONObject unit = new JSONObject(message);

        UnitInterface unitInterface = UnitBuilder.build(unit.getString("type"));

        unitInterface.setUnitId(unit.getString("unitId"));
        unitInterface.setAmmo(unit.getInt("ammo"));
        unitInterface.setFuel(unit.getInt("fuel"));
        unitInterface.setAttack(unit.getInt("attack"));
        unitInterface.setDefence(unit.getInt("defence"));
        unitInterface.setHealth(unit.getDouble("health"));
        unitInterface.setRange(unit.getInt("range"));
        unitInterface.withFireRange(unit.getInt("minFireRange"), unit.getInt("maxFireRange"));

        return unitInterface;
    }
}

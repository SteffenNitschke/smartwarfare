package de.core.network;

import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;

public interface NetworkClient
{
    HashSet<Observable> getObservable();

    void addObservers(
            final Observer observer);
}

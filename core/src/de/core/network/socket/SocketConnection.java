package de.core.network.socket;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.czyzby.websocket.AbstractWebSocketListener;
import com.github.czyzby.websocket.WebSocket;
import com.github.czyzby.websocket.WebSockets;
import com.github.czyzby.websocket.data.WebSocketCloseCode;

import java.io.IOException;
import java.util.Observable;

import de.core.network.messages.news.NewsDto;

public class SocketConnection extends Observable
{
    private WebSocket socket;
    private final ObjectMapper objectMapper = new ObjectMapper();

    private final String loggingTag = getClass().getSimpleName();

    public void connect(final String userId)
    {
        socket = WebSockets.newSocket(
                WebSockets.toWebSocketUrl(
                        "localhost",
                        4567,
                        "session/" + userId));

        socket.addListener(new WebSocketListener());
        Gdx.app.log(loggingTag,
                    "connect To Server");
        socket.connect();
    }

    private void refreshConnection()
    {
        socket.connect();
    }

    private class WebSocketListener extends AbstractWebSocketListener
    {
        @Override
        public boolean onOpen(final WebSocket webSocket)
        {
            final String myMessage = "Hello server!";
            webSocket.send(myMessage);
            return FULLY_HANDLED;
        }

        @Override
        public boolean onClose(
                final WebSocket webSocket,
                final WebSocketCloseCode code,
                final String reason)
        {
            Gdx.app.log(loggingTag,
                        "onClose");
            refreshConnection();
            return FULLY_HANDLED;
        }

        @Override
        protected boolean onMessage(
                final WebSocket webSocket,
                final Object packet)
        {
            Gdx.app.log(loggingTag,
                        "onMessage " + packet.getClass());
            final JsonValue value = (JsonValue) packet;

            try
            {
                setChanged();
                final String jsonString = value.toJson(JsonWriter.OutputType.json);

                Gdx.app.log(loggingTag, "JsonString " + jsonString);

                notifyObservers(objectMapper.readValue(jsonString,
                                                       NewsDto.class));
            }
            catch (final IOException exception)
            {
                Gdx.app.error(loggingTag,
                              "ObjectMapper",
                              exception);
            }
            return FULLY_HANDLED;
        }
    }
}

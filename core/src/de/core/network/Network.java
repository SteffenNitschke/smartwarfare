package de.core.network;

import de.core.network.clients.MapImageClient;
import de.core.network.socket.SocketConnection;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import de.core.network.clients.AccountClient;
import de.core.network.clients.DrawClient;
import de.core.network.clients.FriendInvitationClient;
import de.core.network.clients.GameClient;
import de.core.network.clients.GameInvitationClient;
import de.core.network.clients.MapClient;
import de.core.network.clients.NewsClient;
import de.core.network.clients.UserClient;

@Value
@RequiredArgsConstructor
public class Network
{
    private final AccountClient accountClient;
    private final UserClient userClient;
    private final DrawClient drawClient;
    private final MapClient mapClient;
    private final GameClient gameClient;
    private final GameInvitationClient gameInvitationClient;
    private final FriendInvitationClient friendInvitationClient;
    private final NewsClient newsClient;
    private final SocketConnection socketConnection;
    private final MapImageClient mapImageClient;
}

package de.core.network.messages;

import lombok.Value;

@Value
public class SmallMapMessage
{
    private String mapId;
    private int numberOfPlayers;
    private String name;
    private String creatorName;
    private int maxX;
    private int maxY;
}

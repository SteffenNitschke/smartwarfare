package de.core.network.messages.news;

import lombok.Value;
import de.core.model.Id;
import de.core.network.messages.PlayerMessage;
import de.core.util.NewsType;

@Value
public class NewsDto
{
    private Id _id;

    private Id userId;

    private Id gameId;

    private PlayerMessage sender;

    private boolean answer;

    private boolean readed;

    private NewsType type;
}

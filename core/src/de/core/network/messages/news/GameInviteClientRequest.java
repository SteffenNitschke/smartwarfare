package de.core.network.messages.news;

import lombok.Data;
import de.core.util.GameTypes;

import java.util.ArrayList;
import java.util.List;

/**
 * message from Client to Server
 */
@Data
public class GameInviteClientRequest
{
    private String mapId;
    private GameTypes gameType;
    private List<String> playerIds = new ArrayList<>();
}

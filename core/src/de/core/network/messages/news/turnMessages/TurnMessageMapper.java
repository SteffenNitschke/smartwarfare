package de.core.network.messages.news.turnMessages;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;

import java.io.IOException;

public class TurnMessageMapper {

    private ObjectMapper objectMapper = new ObjectMapper();

    public TurnMessageInterface readValue(String object) throws IOException {

        JSONObject json = new JSONObject(object);

        switch (json.getString("type")) {

            case "move":
                return objectMapper.readValue(object, MoveMessage.class);
            case "attack":
                return objectMapper.readValue(object, AttackMessage.class);
            case "capture":
                return objectMapper.readValue(object, CaptureMessage.class);
            case "combine":
                return objectMapper.readValue(object, CombineMessage.class);
            case "create":
                return objectMapper.readValue(object, CreateMessage.class);
                default:
                    return null;
        }
    }

    public String writeValueAsString(Object object) throws JsonProcessingException {
        return objectMapper.writeValueAsString(object);
    }

}

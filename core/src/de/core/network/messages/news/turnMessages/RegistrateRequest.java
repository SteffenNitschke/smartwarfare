package de.core.network.messages.news.turnMessages;

import lombok.Value;

@Value
public class RegistrateRequest
{
    private String userName;
    private String password;
}

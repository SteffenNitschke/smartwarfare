package de.core.network.messages.news.turnMessages;

/**
 * for easy handling
 * Created by Gandail on 28.05.2017.
 */
public interface TurnMessageInterface {
    String getType();
}

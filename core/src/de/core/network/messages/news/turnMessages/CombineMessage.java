package de.core.network.messages.news.turnMessages;

import lombok.Getter;
import lombok.Setter;

public class CombineMessage implements TurnMessageInterface {

    @Getter
    @Setter
    private String secondUnitID;
    @Getter
    @Setter
    private String unitID;

    private String type = "combine";

    @Override
    public String getType() {
        return this.type;
    }
}

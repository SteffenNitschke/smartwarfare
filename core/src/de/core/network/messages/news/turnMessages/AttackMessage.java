package de.core.network.messages.news.turnMessages;


import lombok.Getter;
import lombok.Setter;

public class AttackMessage implements TurnMessageInterface {

    @Getter
    @Setter
    private String enemyPlayer;
    @Getter
    @Setter
    private String enemyUnit;
    @Getter
    @Setter
    private String owenUnitID;
    @Getter
    @Setter
    private long seed;

    private String type = "attack";

    @Override
    public String getType() {
        return this.type;
    }
}

package de.core.network.messages.news.turnMessages;

import lombok.Getter;
import lombok.Setter;

public class CaptureMessage implements TurnMessageInterface {

    @Getter
    @Setter
    private String unitID;
    @Getter
    @Setter
    private String fieldID;

    private String type = "capture";

    @Override
    public String getType() {
        return this.type;
    }
}

package de.core.network.messages.news.turnMessages;


import lombok.Getter;
import lombok.Setter;

public class CreateMessage implements TurnMessageInterface {

    @Getter
    @Setter
    private String unitType;
    @Getter
    @Setter
    private String fieldID;

    private String type = "create";

    @Override
    public String getType() {
        return this.type;
    }
}

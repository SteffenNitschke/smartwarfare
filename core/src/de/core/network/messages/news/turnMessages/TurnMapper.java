package de.core.network.messages.news.turnMessages;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.core.network.messages.PlayerMessage;
import de.core.network.messages.news.TurnRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedList;

public class TurnMapper {

    private TurnMessageMapper turnMessageMapper = new TurnMessageMapper();
    private ObjectMapper objectMapper = new ObjectMapper();

    public String writeValueAsString(TurnRequest turn) throws JsonProcessingException {

        JSONObject object = new JSONObject();
        object.put("userID", turn.getPlayer().getId());

        JSONArray list = new JSONArray();

        for (TurnMessageInterface message : turn.getMoves()) {
            list.put(turnMessageMapper.writeValueAsString(message));
        }
        object.put("moves", list);

        return object.toString();
    }

    public TurnRequest readValue(String turn) throws IOException {

        JSONObject turnJson = new JSONObject(turn);
        PlayerMessage player = objectMapper.readValue(turnJson.getString("player"), PlayerMessage.class);
        TurnRequest returnRequest = new TurnRequest(player);


        JSONArray array = turnJson.getJSONArray("moves");
        LinkedList<TurnMessageInterface> list = new LinkedList<>();

        for(int i = 0; i < array.length(); i++) {

            list.add(turnMessageMapper.readValue(array.getString(i)));

        }
        returnRequest.setMoves(list);
        return returnRequest;
    }
}

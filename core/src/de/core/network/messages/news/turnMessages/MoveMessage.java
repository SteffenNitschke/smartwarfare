package de.core.network.messages.news.turnMessages;

import lombok.Getter;
import lombok.Setter;

public class MoveMessage implements TurnMessageInterface {

    @Getter
    @Setter
    private String unitID;
    @Getter
    @Setter
    private String fieldID;

    private String type = "move";

    @Override
    public String getType() {
        return this.type;
    }
}

package de.core.network.messages.news;

import java.util.LinkedList;

import lombok.Data;
import de.core.network.messages.PlayerMessage;
import de.core.network.messages.news.turnMessages.TurnMessageInterface;

/**
 * Created by Gandail on 28.05.2017.
 */
@Data
public class TurnRequest {

    private PlayerMessage player;
    private LinkedList<TurnMessageInterface> moves = new LinkedList<>();
    private String gameId;

    public TurnRequest(PlayerMessage player) {
        this.player = player;
    }

    public void addMessage(TurnMessageInterface move) {
        moves.addLast(move);
    }

    private String type = "turn";
}

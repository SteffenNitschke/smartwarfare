package de.core.network.messages.news;

import lombok.Data;
import lombok.EqualsAndHashCode;
import de.core.network.messages.PlayerMessage;

@Data
@EqualsAndHashCode
public class ChatRequest
{

    private PlayerMessage sender;
    private PlayerMessage recipient;
    private String timestamp;
    private String message;
    private String gameId;
    private String type = "chat";
}

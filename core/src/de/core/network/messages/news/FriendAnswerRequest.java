package de.core.network.messages.news;

import lombok.Value;
import de.core.network.messages.PlayerMessage;

@Value
public class FriendAnswerRequest
{
    private boolean wannaBeFriends;
    private PlayerMessage responder;
    private PlayerMessage sender;
    private String type = "friendAnswer";
}


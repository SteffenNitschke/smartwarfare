package de.core.network.messages.news;

import lombok.Data;

@Data
public class ChangeUserRequest
{

    private String name;
    private String password;
}

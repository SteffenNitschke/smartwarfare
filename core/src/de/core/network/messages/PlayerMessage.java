package de.core.network.messages;

import lombok.Value;
import de.core.model.Id;

@Value
public class PlayerMessage
{
    private Id id;
    private String name;
}

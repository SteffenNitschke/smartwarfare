package de.core.network.messages;

import lombok.Value;

@Value
public class GameAnswerRequest
{
    private final String gameId;

    private final PlayerMessage responder;

    private final boolean answer;
}

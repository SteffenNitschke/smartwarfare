package de.core.network.messages;

import com.badlogic.gdx.Gdx;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedList;

import de.core.network.messages.news.NewsDto;

public class NewsMapper
{

    private ObjectMapper objectMapper = new ObjectMapper();
//    private TurnMapper turnMapper = new TurnMapper();

    private String loggingTag = getClass().getSimpleName();

    public LinkedList<NewsDto> readValue(String message) throws IOException
    {
        Gdx.app.log(loggingTag,
                    "readValue");

        final LinkedList<NewsDto> news = new LinkedList<>();

        final JSONArray array = new JSONArray(message);

        Gdx.app.log(loggingTag,
                    "New News Array: " + array);
        Gdx.app.log(loggingTag,
                    "Size of the Array: " + array.length());

        for (int i = 0; i < array.length(); i++)
        {
            final JSONObject object = array.getJSONObject(i);

            Gdx.app.log(loggingTag,
                        "Parse Message: " + object);

//            if (NewsType.valueOf(object.getString("type")).equals(NewsType.TURN))
//            {
//                Gdx.app.log(loggingTag,
//                            "turn");//TODO handle Turns
//                news.addLast(turnMapper.readValue(object.toString()));
//            }
//            else
//            {
                Gdx.app.log(loggingTag,
                            "No Turn News");
                Gdx.app.log(loggingTag,
                            object.get("type").toString());
                news.addLast(objectMapper.readValue(object.toString(),
                                                    NewsDto.class));
        }

        return news;
    }
}


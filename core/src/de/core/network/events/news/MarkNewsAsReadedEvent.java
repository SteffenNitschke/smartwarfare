package de.core.network.events.news;

import lombok.Value;
import de.core.model.Id;

@Value
public class MarkNewsAsReadedEvent
{
    private final boolean successful;

    private final Id newsId;
}

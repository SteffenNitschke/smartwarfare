package de.core.network.events.news;

import java.util.LinkedList;

import lombok.Value;
import de.core.network.messages.news.NewsDto;

@Value
public class GetNewsEvent
{
    private final LinkedList<NewsDto> news;
}

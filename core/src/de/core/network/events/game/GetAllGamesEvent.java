package de.core.network.events.game;

import java.util.HashSet;

import lombok.Value;
import de.core.model.Id;

@Value
public class GetAllGamesEvent
{
    private final HashSet<Id> gameIds;
}

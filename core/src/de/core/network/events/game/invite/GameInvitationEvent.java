package de.core.network.events.game.invite;

import lombok.Value;
import de.core.model.Game;

@Value
public class GameInvitationEvent
{
    private final Game game;
}

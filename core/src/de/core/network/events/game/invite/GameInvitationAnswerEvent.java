package de.core.network.events.game.invite;

import de.core.model.Id;
import lombok.Value;
import de.core.model.Game;

@Value
public class GameInvitationAnswerEvent
{
    private final Id newsId;

    private final Game game;
}

package de.core.network.events.game;

import lombok.Value;
import de.core.model.Game;

@Value
public class GetGameEvent
{
    private final Game game;
}

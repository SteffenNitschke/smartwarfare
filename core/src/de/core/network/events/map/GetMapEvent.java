package de.core.network.events.map;

import lombok.Value;
import de.core.model.Map;

@Value
public class GetMapEvent
{
    private final Map map;
}

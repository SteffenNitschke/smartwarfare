package de.core.network.events.map;

import com.badlogic.gdx.files.FileHandle;

import de.core.model.Id;
import lombok.Value;

@Value
public class GetMapImageEvent
{
    private final Id mapId;

    private final FileHandle image;
}

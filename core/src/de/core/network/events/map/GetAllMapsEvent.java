package de.core.network.events.map;

import java.util.HashSet;

import lombok.Value;
import de.core.network.messages.SmallMapMessage;

@Value
public class GetAllMapsEvent
{
    private final HashSet<SmallMapMessage> maps;
}

package de.core.network.events.map;

import lombok.Value;
import de.core.model.Id;

@Value
public class AddMapEvent
{
    private final Id mapId;
}

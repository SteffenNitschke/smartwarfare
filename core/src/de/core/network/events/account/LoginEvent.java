package de.core.network.events.account;

import lombok.Value;
import de.core.model.User;

@Value
public class LoginEvent
{
    private final User user;
    private final boolean successful;
}

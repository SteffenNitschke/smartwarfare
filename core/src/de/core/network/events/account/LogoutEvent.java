package de.core.network.events.account;

import lombok.Value;

@Value
public class LogoutEvent
{
    private final boolean successful;
}

package de.core.network.events.account;

import lombok.Value;

@Value
public class RegistrationEvent
{
    private final boolean successful;
}

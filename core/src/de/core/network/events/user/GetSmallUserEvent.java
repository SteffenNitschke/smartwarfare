package de.core.network.events.user;

import lombok.Value;
import de.core.network.messages.PlayerMessage;

@Value
public class GetSmallUserEvent
{
    private final PlayerMessage player;
}

package de.core.network.events.user;

import lombok.Value;
import de.core.model.User;

@Value
public class RefreshUserEvent
{
    private final User user;
}

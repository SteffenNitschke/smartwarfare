package de.core.network.events.user.invite;

import de.core.model.Id;
import lombok.Value;

@Value
public class FriendAnswerEvent
{
    private final Id newsId;

    private final boolean successful;
}

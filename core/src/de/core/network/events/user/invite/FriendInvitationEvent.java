package de.core.network.events.user.invite;

import lombok.Value;

@Value
public class FriendInvitationEvent
{
    private final boolean successful;
}

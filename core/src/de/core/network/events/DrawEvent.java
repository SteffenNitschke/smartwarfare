package de.core.network.events;

import lombok.Value;

@Value
public class DrawEvent
{
    private final String gameId;
}

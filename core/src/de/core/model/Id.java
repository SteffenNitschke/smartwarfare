package de.core.model;

import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode(of = "identifier")
public class Id
{
    private final String identifier;
}

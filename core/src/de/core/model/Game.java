package de.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import de.core.model.buildungs.BuildingInterface;
import de.core.model.units.UnitInterface;
import de.core.util.GameStatus;
import de.core.util.GameTypes;

import java.beans.PropertyChangeSupport;
import java.util.HashSet;

/**
 * Created by Gandail on 28.05.2017.
 */
public class Game {

    private String gameID;

    private HashSet<Player> players = new HashSet<>();
    @JsonIgnore
    private HashSet<BuildingInterface> buildings = new HashSet<>();
    private Map map;
    private int round;
    private Player currentPlayer;
    private GameTypes gameType;
    @Getter
    @Setter
    private GameStatus status;
    @Getter
    @Setter
    private String chatId;

    @JsonIgnore
    private PropertyChangeSupport change = new PropertyChangeSupport(this);

    public Game(String gameID) {
        setGameID(gameID);
        this.map = new Map("testMap");
        this.round = 1;
    }

    public void nextRound() {
        System.out.println("next round");
        this.setRound(this.getRound() + 1);
        this.currentPlayer = currentPlayer.getNext();
        for (BuildingInterface building :
                this.buildings) {
            System.out.println("Trigger building " + building.getClass().getSimpleName());
            building.trigger();
        }
        for (UnitInterface unit :
                currentPlayer.getTroops()) {
            System.out.println("Reset Unit " + unit);
            unit.setHaveAttacked(false);
            unit.setIsMoved(false);
        }
        for (UnitInterface unit :
                currentPlayer.getHeroes()) {
            System.out.println("Reset Hero " + unit);
            unit.setHaveAttacked(false);
            unit.setIsMoved(false);
        }

    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_BUILDINGS = "buildings";

    public HashSet<BuildingInterface> getBuildings() {
        return buildings;
    }

    public void addBuilding(BuildingInterface building) {
        this.buildings.add(building);
        building.setGame(this);
    }

    public void removeBuilding(BuildingInterface building) {
        this.buildings.remove(building);
        building.setGame(null);

    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_CURRENT_PLAYER = "currentPlayer";

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    //-------------------------------------------

    public PropertyChangeSupport getChange() {
        return change;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_PLAYER = "players";

    public HashSet<Player> getPlayer() {
        return players;
    }

    public void addPlayer(Player player) {
        this.players.add(player);
        player.setGame(this);
    }

    public void removePlayer(Player player) {
        this.players.remove(player);
        player.setGame(null);
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_MAP = "map";

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_ROUND = "round";

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_GAME_ID = "_id";

    public String getGameID() {
        return gameID;
    }

    public void setGameID(String gameID) {
        this.gameID = gameID;
    }

    //-------------------------------------------

    public GameTypes getGameType() {
        return gameType;
    }

    public void setGameType(GameTypes gameType) {
        this.gameType = gameType;
    }
}

package de.core.model;

import com.badlogic.gdx.graphics.Color;
import com.fasterxml.jackson.annotation.JsonIgnore;
import de.core.model.units.UnitInterface;

import java.beans.PropertyChangeSupport;
import java.util.HashSet;

/**
 *
 * user in small only for one Game
 * Created by Gandail on 28.05.2017.
 */
public class Player {

    private String userID;
    private Color color;
    @JsonIgnore
    private Player next;
    @JsonIgnore
    private Player prev;
    private String name;

    private HashSet<UnitInterface> troops = new HashSet<UnitInterface>();
    private HashSet<UnitInterface> heroes = new HashSet<UnitInterface>();
    private int money;
    @JsonIgnore
    private Game game;

    @JsonIgnore
    private PropertyChangeSupport change = new PropertyChangeSupport(this);

    public Player(String userID) {
        this.userID = userID;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_PREV = "prev";

    public Player getPrev() {
        return prev;
    }

    public void setPrev(Player prev) {
        if (this.prev != prev) {
            if (prev != null) {
                this.prev = prev;
                this.prev.setNext(this);
            } else {
                this.prev.setNext(null);
                this.prev = null;
            }
        }
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_NEXT = "next";

    public Player getNext() {
        return next;
    }

    public void setNext(Player next) {
        if (this.next != next) {
            if (next != null) {
                this.next = next;
                this.next.setPrev(this);
            } else {
                this.next.setPrev(null);
                this.next = null;
            }
        }
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_NAME = "name";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Player withName(String name) {
        setName(name);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_GAME = "game";

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        if (this.game != game) {
            if (game != null) {
                this.game = game;
                this.game.addPlayer(this);
            } else {
                this.game.removePlayer(this);
                this.game = null;
            }
        }
    }

    public Player withGame(Game game) {
        setGame(game);
        return this;
    }

    //-------------------------------------------

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Player withColor(Color color) {
        this.color = color;
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_HEROES = "heroes";

    public HashSet<UnitInterface> getHeroes() {
        return heroes;
    }

    public void addHeroes(UnitInterface hero) {
        hero.setPlayer(this);
        heroes.add(hero);
    }

    public void removeHero(UnitInterface hero) {
        hero.setPlayer(null);
        heroes.remove(hero);
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_TROOPS = "troops";

    public void addTroops(UnitInterface unit) {
        unit.setPlayer(this);
        troops.add(unit);
    }

    public HashSet<UnitInterface> getTroops() {
        return troops;
    }

    public void removeTroop(UnitInterface unit) {
        unit.setPlayer(null);
        troops.remove(unit);
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_USER_ID = "userID";

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_MONEY = "money";

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    //-------------------------------------------

    public PropertyChangeSupport getChange() {
        return change;
    }

}

package de.core.model.units;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.core.model.Field;
import de.core.model.Player;
import de.core.util.FieldType;
import de.core.util.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.beans.PropertyChangeSupport;
import java.util.HashSet;
import java.util.Random;

/**
 * All Units exends from that class
 * Created by Gandail on 28.05.2017.
 */
public abstract class Unit implements UnitInterface
{

    private String unitId;
    protected String type;
    private int attack;
    private int defence;
    private double health;
    @JsonIgnore
    protected Field field;
    private int range;
    private int ammo;
    private int fuel;
    private int minFireRange;
    private int maxFireRange;
    protected HashSet<FieldType> possibleFieldTypes = new HashSet<>();
    protected HashSet<String> possibleTargets = new HashSet<>();
    @JsonIgnore
    protected Player player;

    @JsonIgnore
    protected PropertyChangeSupport change = new PropertyChangeSupport(this);

    @JsonIgnore
    private boolean isMoved = false;
    @JsonIgnore
    private boolean haveAttacked = false;

    public Unit(String unitId)
    {
        this.unitId = unitId;
    }

    public void combine(UnitInterface unit)
    {
        if (unit.getClass().getCanonicalName().equals(this.getClass().getCanonicalName()))
        {

            if (this.getPlayer() == unit.getPlayer())
            {

                if (getHealth() + unit.getHealth() < 100)
                {

                    setHealth(getHealth() + unit.getHealth());
                    setAmmo(getAmmo() + unit.getAmmo());
                    setFuel(getFuel() + unit.getFuel());
                    unit.removeYou();

                }
                else
                {

                    unit.setHealth(unit.getHealth() + getHealth());
                    unit.setHealth(unit.getHealth() - 100);
                    setHealth(100);

                    int tempAmmo = getAmmo() + unit.getAmmo();
                    int tempFuel = getFuel() + unit.getFuel();

                    if ((tempAmmo % 2) == 0)
                    {
                        unit.setAmmo(tempAmmo / 2);
                        setAmmo(tempAmmo / 2);
                    }
                    else
                    {
                        unit.setAmmo(tempAmmo / 2);
                        setAmmo((tempAmmo / 2) + 1);
                    }

                    if ((tempFuel % 2) == 0)
                    {
                        unit.setFuel(tempFuel / 2);
                        setFuel(tempFuel / 2);
                    }
                    else
                    {
                        unit.setFuel(tempFuel / 2);
                        setFuel((tempFuel / 2) + 1);
                    }
                }
            }
        }
    }

//    public boolean move(Field moveTo) {
//
//        if (!isMoved) {
//            if (this.getPlayer() == this.getPlayer().getGame().getCurrentPlayer()) {
//                int tempRange = Utils.calculateRange(this.field, moveTo);
//                if(tempRange <= this.range) {
//                    if (this.possibleFieldTypes.contains(moveTo.getType())) {
//                        HashSet<Field> possFields = Utils.possibleWay(this);
//                        if (possFields.contains(moveTo)) {
//
//                            HashSet<Field> way = Utils.findWay(this, moveTo);
//                            for (PropertyChangeListener listener :
//                                    this.change.getPropertyChangeListeners()) {
//                                ((UnitController) listener).setCurrentWay(way);
//                            }
//                            this.isMoved = true;
//                            setField(moveTo);
//                            return true;
//                        }
//                    }
//                }
//            }
//        }
//
//        return false;
//    }

    public long attack(
            UnitInterface enemy,
            Long seed)
    {

        if (seed == null)
        {
            seed = new Random().nextLong();
        }

        if (possibleTargets.contains(enemy.getType()))
        {
            System.out.println("Unit " + this + " want to attack " + enemy);
            if (Utils.getStrafeableFields(this).contains(enemy.getField()))
            {
                //TODO how attack
                enemy.removeYou();

            }

        }
        return seed;
    }

    //-------------------------------------------

    @Override
    public int getMinFireRange()
    {
        return minFireRange;
    }

    @Override
    public int getMaxFireRange()
    {
        return maxFireRange;
    }

    @Override
    public UnitInterface withFireRange(
            int min,
            int max)
    {
        this.minFireRange = min;
        this.maxFireRange = max;
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_AMMO = "ammo";

    public int getAmmo()
    {
        return ammo;
    }

    public void setAmmo(int ammo)
    {
        int old = this.ammo;
        this.ammo = ammo;
        this.change.firePropertyChange(PROPERTY_NAME_AMMO,
                                       old,
                                       this.ammo);
    }

    public Unit withAmmo(int ammo)
    {
        setAmmo(ammo);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_FUEL = "fuel";

    public int getFuel()
    {
        return fuel;
    }

    public void setFuel(int fuel)
    {
        int old = this.fuel;
        this.fuel = fuel;
        this.change.firePropertyChange(PROPERTY_NAME_FUEL,
                                       old,
                                       this.fuel);
    }

    public Unit withFuel(int fuel)
    {
        setFuel(fuel);
        return this;
    }

    //-------------------------------------------

    public static final String FabricCounter = "counter";

    //-------------------------------------------

    public static final String PROPERTY_NAME_USER = "user";

    public Player getPlayer()
    {
        return player;
    }

    public void setPlayer(Player player)
    {
        if (player != null)
        {
            Player event = this.player;
            this.player = player;
            player.getTroops().add(this);
            change.firePropertyChange(PROPERTY_NAME_USER,
                                      event,
                                      player);
        }
        else
        {
            this.player.getTroops().remove(this);
            change.firePropertyChange(PROPERTY_NAME_USER,
                                      this.player,
                                      null);
            this.player = null;
        }
    }

    //-------------------------------------------

    public PropertyChangeSupport getChange()
    {
        return this.change;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_UNIT_ID = "unitId";

    public void setUnitId(String id)
    {
        this.unitId = id;
    }

    public String getUnitId()
    {
        return unitId;
    }

    public UnitInterface withUnitId(String id)
    {
        setUnitId(id);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_FIELD = "field";

    public Field getField()
    {
        return field;
    }

    public void setField(Field field)
    {
        if (field != this.field)
        {
            if (field != null)
            {
                Field event = this.field;
//                System.out.println("Set Field to " + field);
                this.field = field;
                this.field.setUnit(this);
                change.firePropertyChange(PROPERTY_NAME_FIELD,
                                          event,
                                          field);
            }
            else
            {
                Field old = this.field;
                this.field = null;
                old.setUnit(null);
                change.firePropertyChange(PROPERTY_NAME_FIELD,
                                          this.field,
                                          null);
            }
        }
    }

    public Unit withField(Field field)
    {
        setField(field);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_TYPE = "type";

    public String getType()
    {
        return this.type;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_ATTACK = "attack";

    public void setAttack(int attack)
    {
        this.attack = attack;
    }

    public int getAttack()
    {
        return this.attack;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_DEFENCE = "defence";

    public void setDefence(int defence)
    {
        this.defence = defence;
    }

    public int getDefence()
    {
        return this.defence;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_HEALTH = "health";

    public void setHealth(double health)
    {
        double old = this.health;
        this.health = health;
        this.change.firePropertyChange(PROPERTY_NAME_HEALTH,
                                       old,
                                       this.health);
    }

    public double getHealth()
    {
        return this.health;
    }

    public UnitInterface withHealth(double health)
    {
        setHealth(health);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_RANGE = "range";

    public void setRange(int range)
    {
        this.range = range;
    }

    public int getRange()
    {
        return this.range;
    }

    public UnitInterface withRange(int range)
    {
        setRange(range);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_MOVED = "moved";

    @Override
    public void setIsMoved(boolean isMoved)
    {
        boolean old = this.isMoved;
        this.isMoved = isMoved;
        this.change.firePropertyChange(PROPERTY_NAME_MOVED,
                                       old,
                                       this.isMoved);
    }

    @Override
    public boolean isMoved()
    {
        return this.isMoved;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_HAVE_ATTACKED = "haveAttacked";

    @Override
    public void setHaveAttacked(boolean haveAttacked)
    {
        boolean old = haveAttacked();
        this.haveAttacked = haveAttacked;
        this.change.firePropertyChange(PROPERTY_NAME_HAVE_ATTACKED,
                                       old,
                                       haveAttacked());
    }

    @Override
    public boolean haveAttacked()
    {
        return this.haveAttacked;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_POSSIBLE_FIELD_TYPES = "possibleFieldTypes";

    public HashSet<FieldType> getPossibleFieldTypes()
    {
        return this.possibleFieldTypes;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_POSSIBLE_TARGETS = "possibleTargets";

    public HashSet<String> getPossobleTargets()
    {
        return this.possibleTargets;
    }

    //-------------------------------------------

    public void removeYou()
    {
        System.out.println("RemoveYou Unit");
        this.setField(null);
        this.setPlayer(null);
    }

    @Override
    public JSONObject getJSONUnit()
    {

        JSONObject unit = new JSONObject();
        unit.put(PROPERTY_NAME_AMMO,
                 ammo);
        unit.put(PROPERTY_NAME_ATTACK,
                 attack);
        unit.put(PROPERTY_NAME_DEFENCE,
                 defence);
        unit.put(PROPERTY_NAME_FIELD,
                 field.getFieldID());
        unit.put(PROPERTY_NAME_FUEL,
                 fuel);
        unit.put(PROPERTY_NAME_HAVE_ATTACKED,
                 haveAttacked);
        unit.put(PROPERTY_NAME_HEALTH,
                 health);
        unit.put(PROPERTY_NAME_MOVED,
                 isMoved);
        JSONArray possibleFieldTypes = new JSONArray();
        for (FieldType type :
                this.possibleFieldTypes)
        {
            possibleFieldTypes.put(type);
        }
        unit.put(PROPERTY_NAME_POSSIBLE_FIELD_TYPES,
                 possibleFieldTypes);
        JSONArray possibleTargets = new JSONArray();
        for (String target :
                this.possibleTargets)
        {
            possibleTargets.put(target);
        }
        unit.put(PROPERTY_NAME_POSSIBLE_TARGETS,
                 possibleTargets);
        unit.put(PROPERTY_NAME_RANGE,
                 range);
        unit.put(PROPERTY_NAME_TYPE,
                 type);
        unit.put(PROPERTY_NAME_UNIT_ID,
                 unitId);
        unit.put(PROPERTY_NAME_USER,
                 player.getUserID());

        return unit;
    }

}

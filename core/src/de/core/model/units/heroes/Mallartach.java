package de.core.model.units.heroes;

import de.core.model.units.UnitInterface;

/**
 * Created by Gandail on 28.05.2017.
 */
public class Mallartach extends Hero implements HeroInterface, UnitInterface {


    public Mallartach(String id) {
        super(id);
    }

    public void passiveAbility() {

    }

    public void aktiveAbility() {

    }
}

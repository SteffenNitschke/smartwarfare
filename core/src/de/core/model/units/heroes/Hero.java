package de.core.model.units.heroes;

import de.core.model.Field;
import de.core.model.units.Unit;

/**
 * Created by Gandail on 28.05.2017.
 */
public class Hero extends Unit implements HeroInterface {

    private String gameId;

    public Hero(String id) {
        super(id);
        this.type = "hero";
        //TODO add hero specific variable decleration
    }

    public void combine(HeroInterface hero) {
        //TODO what happen when you combine two heroes
    }

    public void split(Field field) {
        //TODO what happen when you split two heroes
    }

    @Override
    public void passiveAbility() {

        //TODO passive Abilities

    }

    @Override
    public void aktiveAbility() {

        //TODO aktive Abilities

    }

    @Override
    public String getGameId() {
        return this.gameId;
    }

    @Override
    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    @Override
    public boolean move(Field moveTo) {
        return false;
    }
}

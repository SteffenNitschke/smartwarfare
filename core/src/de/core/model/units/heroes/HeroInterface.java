package de.core.model.units.heroes;

import de.core.model.Field;
import de.core.model.units.UnitInterface;

import java.beans.PropertyChangeSupport;

/**
 * Created by Gandail on 28.05.2017.
 */
public interface HeroInterface extends UnitInterface {

    void combine(HeroInterface hero);

    void split(Field field);

    void passiveAbility();

    void aktiveAbility();

    String getGameId();

    void setGameId(String gameId);

    PropertyChangeSupport getChange();
}

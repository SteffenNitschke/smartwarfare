package de.core.model.units;

import com.badlogic.gdx.Gdx;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.core.model.units.heroes.Hero;
import de.core.util.Utils;
import org.json.JSONObject;

import de.core.model.units.troops.infantry.Infantry;
import de.core.model.units.troops.planes.Plane;
import de.core.model.units.troops.ships.Ship;
import de.core.model.units.troops.submarines.Submarine;
import de.core.model.units.troops.vehicles.Vehicle;

import java.io.IOException;

public class UnitBuilder {

//    private static JSONObject stats = new JSONObject(
//            OwenFileHandler.getFile("data/UnitSpecs", ".json")
//                    .readString());

    private static ObjectMapper objectMapper = new ObjectMapper();
    private static String loggingTag = UnitBuilder.class.getSimpleName();

    public static UnitInterface build(String type) {

        Gdx.app.log(loggingTag, "Type: " + type);
//        JSONObject unitSpecs = stats.getJSONObject(type);
        UnitInterface unit;

        switch (type) {
            case Utils.TROOPS_TYPE_INFANTRY:
                unit = new Infantry("new");
                break;
            case Utils.TROOPS_TYPE_PLANE:
                unit = new Plane("new");
                break;
            case Utils.TROOPS_TYPE_SHIP:
                unit = new Ship("new");
                break;
            case Utils.TROOPS_TYPE_SUBMARINE:
                unit = new Submarine("new");
                break;
            case Utils.TROOPS_TYPE_VEHICLE:
                unit = new Vehicle("new");
                break;
                default:
                    unit = new Plane("new");
        }
        //TODO add unitSpecs
        //TODO add possibleFieldTypes and possibleTargetTypes to class constructor
//        unit.setAmmo(unitSpecs.getInt(Unit.PROPERTY_NAME_AMMO));
//        unit.setFuel(unitSpecs.getInt(Unit.PROPERTY_NAME_FUEL));
//        unit.setAttack(unitSpecs.getInt(Unit.PROPERTY_NAME_ATTACK));
//        unit.setDefence(unitSpecs.getInt(Unit.PROPERTY_NAME_DEFENCE));
//        unit.setHealth(unitSpecs.getDouble(Unit.PROPERTY_NAME_HEALTH));
//        unit.setRange(unitSpecs.getInt(Unit.PROPERTY_NAME_RANGE));
        return unit;
    }

    public static UnitInterface build(String type, JSONObject obj) throws IOException {
        System.out.println("UnitBuilder build: " + obj);
        if(obj.has("field")) {
            obj.remove("field");
        }
        if(obj.has("player")) {
            obj.remove("player");
        }
        if(obj.has("possibleTargets")) {
            obj.remove("possibleTargets");
        }
        if(obj.has("possibleFieldTypes")) {
            obj.remove("possibleFieldTypes");
        }
        System.out.println("Object now: " + obj);
        UnitInterface unit;

        switch (type) {
            case Utils.TROOPS_TYPE_INFANTRY:
                unit = objectMapper.readValue(obj.toString(), Infantry.class);
                break;
            case Utils.TROOPS_TYPE_PLANE:
                unit = objectMapper.readValue(obj.toString(), Plane.class);
                break;
            case Utils.TROOPS_TYPE_SHIP:
                unit = objectMapper.readValue(obj.toString(), Ship.class);
                break;
            case Utils.TROOPS_TYPE_SUBMARINE:
                unit = objectMapper.readValue(obj.toString(), Submarine.class);
                break;
            case Utils.TROOPS_TYPE_VEHICLE:
                unit = objectMapper.readValue(obj.toString(), Vehicle.class);
                break;
            case Utils.TROOPS_TYPE_HERO:
                unit = objectMapper.readValue(obj.toString(), Hero.class);
                break;
                default:
                    return null;
        }

        return unit;
    }
}

package de.core.model.units;

import de.core.model.Field;
import de.core.model.Player;
import de.core.util.FieldType;

import org.json.JSONObject;

import java.beans.PropertyChangeSupport;
import java.util.HashSet;

/**
 * Created by Gandail on 28.05.2017.
 */
public interface UnitInterface {

    void combine(UnitInterface unit);

    boolean move(Field moveTo);

    long attack(UnitInterface enemy, Long seed);

    //-------------------------------------------

    int getMinFireRange();

    int getMaxFireRange();

    UnitInterface withFireRange(int min, int max);

    //-------------------------------------------

    void setAmmo(int ammo);

    int getAmmo();

    UnitInterface withAmmo(int ammo);

    //-------------------------------------------

    void setFuel(int fuel);

    int getFuel();

    UnitInterface withFuel(int fuel);

    //-------------------------------------------

    void setPlayer(Player player);

    Player getPlayer();

    //-------------------------------------------

    PropertyChangeSupport getChange();

    //-------------------------------------------

    void setUnitId(String id);

    String getUnitId();

    UnitInterface withUnitId(String id);

    //-------------------------------------------

    Field getField();

    void setField(Field field);

    //-------------------------------------------

    String getType();

    //-------------------------------------------

    void setAttack(int attacke);

    int getAttack();

    //-------------------------------------------

    void setDefence(int defence);

    int getDefence();

    //-------------------------------------------

    void setHealth(double health);

    double getHealth();

    UnitInterface withHealth(double health);

    //-------------------------------------------

    void setRange(int range);

    int getRange();

    UnitInterface withRange(int range);

    //-------------------------------------------

    void setIsMoved(boolean isMoved);

    boolean isMoved();

    //-------------------------------------------

    void setHaveAttacked(boolean haveAttacked);

    boolean haveAttacked();

    //-------------------------------------------

    HashSet<FieldType> getPossibleFieldTypes();

    HashSet<String> getPossobleTargets();

    void removeYou();

    JSONObject getJSONUnit();
}

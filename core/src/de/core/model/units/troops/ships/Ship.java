package de.core.model.units.troops.ships;

import de.core.model.Field;
import de.core.model.units.Unit;
import de.core.model.units.UnitInterface;
import de.core.util.Utils;

/**
 * Created by Gandail on 28.05.2017.
 */
public class Ship extends Unit implements UnitInterface {

    public Ship(String unitId) {
        super(unitId);
        this.type = Utils.TROOPS_TYPE_SHIP;
    }

    @Override
    public boolean move(Field moveTo) {
        return false;
    }
}

package de.core.model.units.troops.submarines;

import de.core.model.Field;
import de.core.model.units.Unit;
import de.core.model.units.UnitInterface;
import de.core.util.Utils;

/**
 * Created by Gandail on 28.05.2017.
 */
public class Submarine extends Unit implements UnitInterface {

    public Submarine(String unitId) {
        super(unitId);
        this.type = Utils.TROOPS_TYPE_SUBMARINE;
    }

    public boolean move(Field moveTo) {
        return false;
    }

    public void attacke(UnitInterface enemy) {

    }

}

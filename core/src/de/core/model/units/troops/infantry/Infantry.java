package de.core.model.units.troops.infantry;

import de.core.model.Field;
import de.core.model.units.Unit;
import de.core.model.units.UnitInterface;
import de.core.util.Utils;

/**
 * Created by Gandail on 28.05.2017.
 */
public class Infantry extends Unit implements UnitInterface {

    public Infantry(String unitId) {
        super(unitId);
        this.type = Utils.TROOPS_TYPE_INFANTRY;
    }

    public void attacke(UnitInterface enemy) {

    }

    @Override
    public boolean move(Field moveTo) {
        return false;
    }
}

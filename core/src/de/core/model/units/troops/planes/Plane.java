package de.core.model.units.troops.planes;

import de.core.model.Field;
import de.core.model.units.Unit;
import de.core.model.units.UnitInterface;
import de.core.util.FieldType;
import de.core.util.Utils;

/**
 * Created by Gandail on 28.05.2017.
 */
public class Plane extends Unit implements UnitInterface {

    public Plane(String unitId) {
        super(unitId);
        possibleFieldTypes.add(FieldType.FIELD_TYPE_BEACH);
        possibleFieldTypes.add(FieldType.FIELD_TYPE_WATER);
        possibleFieldTypes.add(FieldType.FIELD_TYPE_BUILDING);
        possibleFieldTypes.add(FieldType.FIELD_TYPE_FOREST);
        possibleFieldTypes.add(FieldType.FIELD_TYPE_GRASS);
        possibleFieldTypes.add(FieldType.FIELD_TYPE_HILL);
        possibleFieldTypes.add(FieldType.FIELD_TYPE_MOUNTAIN);
        possibleFieldTypes.add(FieldType.FIELD_TYPE_STREET);

        possibleTargets.add(Utils.TROOPS_TYPE_PLANE);

        type = Utils.TROOPS_TYPE_PLANE;
    }

    public void attacke(UnitInterface enemy, Long seed) {
        super.attack(enemy, seed);
    }

    @Override
    public boolean move(Field moveTo) {
        return false;
    }
}

package de.core.model.units.troops.vehicles;

import de.core.model.Field;
import de.core.model.units.Unit;
import de.core.model.units.UnitInterface;
import de.core.util.Utils;

/**
 * Created by Gandail on 28.05.2017.
 */
public class Vehicle extends Unit implements UnitInterface {

    public Vehicle(String unitId) {
        super(unitId);
        this.type = Utils.TROOPS_TYPE_VEHICLE;
    }

    @Override
    public boolean move(Field moveTo) {
        return false;
    }
}

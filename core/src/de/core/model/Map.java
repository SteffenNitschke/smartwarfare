package de.core.model;

import com.badlogic.gdx.files.FileHandle;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import de.core.model.buildungs.Building;
import de.core.model.buildungs.BuildingInterface;
import de.core.model.units.Unit;
import de.core.model.units.UnitInterface;
import de.core.util.FieldType;
import de.core.model.buildungs.BuildingBuilder;
import de.core.model.units.UnitBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gandail on 28.05.2017.
 */
@Data
public class Map
{

    private String _id;
    @JsonIgnore
    private Game game;
    @JsonIgnore
    private List<Field> fields = new ArrayList<>();
    private String name;
    private int maxY;
    private int maxX;
    private String editorName;
    private int numberOfPlayers;

    private FileHandle image;

    public Map(
            JSONObject map,
            Game game)
    {
        this.game = game;
        this.name = map.getString(PROPERTY_NAME_NAME);
        this.maxX = Integer.parseInt(map.getString("maxX"));
        this.maxY = Integer.parseInt(map.getString("maxY"));
        if (map.has(PROPERTY_NAME_EDITOR_NAME))
        {
            this.editorName = map.getString(PROPERTY_NAME_EDITOR_NAME);
        }

        JSONArray array = map.getJSONArray(PROPERTY_NAME_FIELDS);

        for (int i = 0; i < array.length(); i++)
        {

            JSONObject field = array.getJSONObject(i);

            Field fieldObject = new Field();
            fieldObject.setFieldID(field.getString(Field.PROPERTY_NAME_FIELD_ID));
            fieldObject.setType(FieldType.valueOf(field.getString(Field.PROPERTY_NAME_TYPE)));

            //set Buildings
            if (field.has(Field.PROPERTY_NAME_BUILDING))
            {
                JSONObject building = field.getJSONObject(Field.PROPERTY_NAME_BUILDING);
                BuildingInterface buildingObj = BuildingBuilder
                        .build(building.getString(Building.PROPERTY_NAME_TYPE));
                for (Player player :
                        game.getPlayer())
                {
                    if (player.getUserID().equals(building.get(Building.PROPERTY_NAME_PLAYER)))
                    {
                        buildingObj.setPlayer(player);
                        break;
                    }
                    if (building.has(Building.PROPERTY_NAME_WANNA_CAPTURE))
                    {
                        if (player.getUserID()
                                  .equals(building.getString(Building.PROPERTY_NAME_WANNA_CAPTURE)))
                        {
                            buildingObj.setWannaCapture(player);
                        }
                    }
                }
                buildingObj.setField(fieldObject);
                buildingObj.setCaptureValue(building.getInt(Building.PROPERTY_NAME_CAPTURE_VALUE));
                buildingObj.setGame(game);
                buildingObj.setType(building.getString(Building.PROPERTY_NAME_TYPE));
            }

            //set Units
            if (field.has(Field.PROPERTY_NAME_UNIT))
            {
                JSONObject unit = field.getJSONObject(Field.PROPERTY_NAME_UNIT);
                UnitInterface unitObj = UnitBuilder
                        .build(unit.getString(Unit.PROPERTY_NAME_UNIT_ID));
                unitObj.setAmmo(unit.getInt(Unit.PROPERTY_NAME_AMMO));
                unitObj.setAttack(unit.getInt(Unit.PROPERTY_NAME_ATTACK));
                unitObj.setDefence(unit.getInt(Unit.PROPERTY_NAME_DEFENCE));
                unitObj.setField(fieldObject);
                unitObj.setFuel(unit.getInt(Unit.PROPERTY_NAME_FUEL));
                unitObj.setHaveAttacked(unit.getBoolean(Unit.PROPERTY_NAME_HAVE_ATTACKED));
                unitObj.setHealth(unit.getDouble(Unit.PROPERTY_NAME_HEALTH));
                unitObj.setIsMoved(unit.getBoolean(Unit.PROPERTY_NAME_MOVED));
                for (Player player :
                        game.getPlayer())
                {
                    if (player.getUserID().equals(unit.get(Building.PROPERTY_NAME_PLAYER)))
                    {
                        unitObj.setPlayer(player);
                        break;
                    }
                }
                unitObj.setRange(unit.getInt(Unit.PROPERTY_NAME_RANGE));
                unitObj.setUnitId(unit.getString(Unit.PROPERTY_NAME_UNIT_ID));
            }

            fieldObject.setMap(this);
            fieldObject.setX(field.getInt(Field.PROPERTY_NAME_X));
            fieldObject.setY(field.getInt(Field.PROPERTY_NAME_Y));

            this.fields.add(fieldObject);

        }

    }

    //-------------------------------------------

    public Map(String name)
    {
        this.name = name;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_MAP_ID = "_id";

//    public String get_id() {
//        return _id;
//    }
//
//    public void set_id(String _id) {
//        this._id = _id;
//    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_NAME = "name";

//    public String getName() {
//        return name;
//    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_FIELDS = "fields";

//    public List<Field> getFields() {
//        return fields;
//    }
//
//    public void addField(Field field) {
//        this.fields.add(field);
//    }
//
//    public void removeField(Field field) {
//        this.fields.remove(field);
//    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_EDITOR_NAME = "editorName";

//    public String getEditorName() {
//        return this.editorName;
//    }
//
//    public void setEditorName(String editorName) {
//        this.editorName = editorName;
//    }

    //-------------------------------------------

//    public int getMaxY() {
//        if (this.maxY == 0) {
//            int y = 0;
//            for (Field field :
//                    this.fields) {
//                if(field.getY() > y) y = field.getY();
//            }
//            maxY = y;
//            return y;
//        }
//
//        return this.maxY;
//    }
//TODO
//    public int getMaxX() {
//        if (this.maxX == 0) {
//            int x = 0;
//            for (Field field :
//                    this.fields) {
//                if(field.getX() > x) x = field.getX();
//            }
//            this.maxX = x;
//            return x;
//        }
//
//        return this.maxX;
//    }

    //-------------------------------------------

//    public Game getGame() {
//        return game;
//    }
//
//    public void setGame(Game game) {
//        this.game = game;
//    }

    //-------------------------------------------

//    public int getNumberOfPlayers() {
//        return numberOfPlayers;
//    }
//
//    public void setNumberOfPlayers(int numberOfPlayers) {
//        this.numberOfPlayers = numberOfPlayers;
//    }
}

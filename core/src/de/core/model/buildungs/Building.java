package de.core.model.buildungs;

import com.badlogic.gdx.Gdx;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.core.model.Field;
import de.core.model.Game;
import de.core.model.Player;
import de.core.model.units.UnitInterface;
import org.json.JSONObject;

import java.beans.PropertyChangeSupport;

/**
 * Created by Gandail on 28.05.2017.
 */
public abstract class Building implements BuildingInterface{

    protected String _id;
    @JsonIgnore
    protected Player player;
    protected String type;
    private int captureValue;
    @JsonIgnore
    private Player wannerCapture;
    @JsonIgnore
    protected Game game;
    @JsonIgnore
    protected Field field;

    private PropertyChangeSupport change = new PropertyChangeSupport(this);

    @Override
    public void trigger() {
    }

    @Override
    public PropertyChangeSupport getChange() {
        return this.change;
    }

    //-------------------------------------------

    public void setId(String id) {
        this._id = id;
    }

    public String getId() {
        return this._id;
    }

    //-------------------------------------------

    @Override
    public Game getGame() {
        return this.game;
    }

    @Override
    public void setGame(Game game) {
        if(this.game != game) {
            if(game != null) {
                this.game = game;
                game.addBuilding(this);
            } else {
               getGame().removeBuilding(this);
                setGame(null);
            }
        }
    }

    @Override
    public BuildingInterface withGame(Game game) {
        setGame(game);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_PLAYER = "player";

    @Override
    public Player getPlayer() {
        return player;
    }


    //there is no back-direction
    @Override
    public void setPlayer(Player player) {
        this.player = player;
    }

    public BuildingInterface withPlayer(Player player) {
        setPlayer(player);
        return this;
    }

    //-------------------------------------------

    private static final String PROPERTY_NAME_FIELD = "field";

    @Override
    public Field getField() {
        return field;
    }

    @Override
    public void setField(Field field) {
        if(field != this.field) {
            if (field != null) {
                Field old = this.field;
                this.field = field;
                getField().setBuilding(this);
                change.firePropertyChange(PROPERTY_NAME_FIELD, old, field);
            } else {
                Field old = this.field;
                this.field = null;
                old.setBuilding(null);
                change.firePropertyChange(PROPERTY_NAME_FIELD, this.field, null);
            }
        }
    }

    @Override
    public BuildingInterface withField(Field field) {
        setField(field);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_TYPE = "type";

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public BuildingInterface withType(String type) {
        setType(type);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_CAPTURE_VALUE = "captureValue";

    @Override
    public int getCaptureValue() {
        return this.captureValue;
    }

    @Override
    public void setCaptureValue(int captureValue) {
        this.captureValue = captureValue;
    }

    @Override
    public BuildingInterface withCaptureValue(int captureValue) {
        setCaptureValue(captureValue);
        return this;
    }

    @Override
    public void setWannaCapture(Player player) {
        this.wannerCapture = player;
    }

    @Override
    public Player getWannerCapture() {
        return this.wannerCapture;
    }

    @Override
    public void capture(UnitInterface unit) {
        Gdx.app.log("Building", "Building capture()");
        if(this.player != unit.getPlayer()) {
            if (unit.getPlayer() == this.wannerCapture) {
                int unitCaptureValue = (int) (unit.getHealth() / 10);
                this.captureValue -= unitCaptureValue;
                System.out.println("Capture Value: " + this.captureValue);
                if(this.captureValue <= 0) {
                    this.player = unit.getPlayer();
                    this.captureValue = 20;
                }
            } else {
                this.captureValue = 20;
                this.wannerCapture = unit.getPlayer();
                capture(unit);
            }
        }
    }

    public static final String PROPERTY_NAME_WANNA_CAPTURE = "wannaCapture";

    @Override
    public JSONObject getJSONBuilding() {
        JSONObject building = new JSONObject();
        building.put(PROPERTY_NAME_FIELD, field.getFieldID());

        if (player != null) {
            building.put(PROPERTY_NAME_PLAYER, player.getUserID());
        }
        building.put(PROPERTY_NAME_CAPTURE_VALUE, captureValue);
        building.put(PROPERTY_NAME_TYPE, type);

        return building;
    }

}

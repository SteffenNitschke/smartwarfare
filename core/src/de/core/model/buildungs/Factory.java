package de.core.model.buildungs;

import com.badlogic.gdx.Gdx;
import de.core.model.units.UnitInterface;
import de.core.util.Utils;

import java.util.HashSet;

/**
 *
 * All Factories (Haven, Airport, ...) extende that.
 * You only need to fill the productRange Set with Units it can create.
 * use Constructor to add <Classname>.class.getCanonicalName()
 *
 * Created by Gandail on 28.05.2017.
 */
public class Factory extends Building implements BuildingInterface {

    private UnitInterface unit;
    private UnitInterface order;
    //when bigger Units needs longer
    private int counter = 0;
    HashSet<String> productRange = new HashSet<>();
    private boolean unitOrdered = false;

    private String loggingTag = getClass().getSimpleName();

    public boolean orderUnit(UnitInterface unit) {

        Gdx.app.log(loggingTag, "orderUnit");
        if (this.productRange.contains(unit.getType()) && !unitOrdered) {
            Gdx.app.log(loggingTag, "canCreate");
            this.unit = unit;
            this.field.setUnit(unit);
            this.counter = Utils.getCounterForUnitCreation(unit);
            unitOrdered = true;

            return true;
        }
        return false;
    }

    @Override
    public void trigger() {
        if (this.counter == 0) {
            if(this.order != null) {

                this.player.addTroops(this.unit);
                this.field.setUnit(this.unit);
                this.order = null;
                this.unitOrdered = false;
            }
        } else {
            this.counter--;
        }
    }

    public HashSet<String> canCreate() {
        return this.productRange;
    }
}

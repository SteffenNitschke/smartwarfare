package de.core.model.buildungs;


import de.core.util.Utils;

/**
 * Created by Gandail on 28.05.2017.
 */
public class City extends Building {

    public City() {
        this.type = Utils.BUILDING_TYPE_CITY;
    }

    @Override
    public void trigger() {
        if(player != null) {
            player.setMoney(player.getMoney() + Utils.CITY_INCOME);
        }
    }

}

package de.core.model.buildungs;

import de.core.model.Game;
import de.core.model.Player;
import de.core.model.Field;
import de.core.model.units.UnitInterface;
import org.json.JSONObject;

import java.beans.PropertyChangeSupport;

/**
 * Created by Gandail on 28.05.2017.
 */
public interface BuildingInterface {

    void trigger();

    PropertyChangeSupport getChange();

    //-------------------------------------------

    void setId(String id);

    String getId();

    //-------------------------------------------

    Game getGame();

    void setGame(Game game);

    BuildingInterface withGame(Game game);

    //-------------------------------------------

    Player getPlayer();

    void setPlayer(Player player);

    BuildingInterface withPlayer(Player player);

    //-------------------------------------------

    Field getField();

    void setField(Field field);

    BuildingInterface withField(Field field);

    //-------------------------------------------

    String getType();

    void setType(String type);

    BuildingInterface withType(String type);

    //-------------------------------------------

    int getCaptureValue();

    void setCaptureValue(int captureValue);

    BuildingInterface withCaptureValue(int captureValue);

    //-------------------------------------------

    void setWannaCapture(Player player);

    Player getWannerCapture();

    void capture(UnitInterface unit);

    JSONObject getJSONBuilding();
}

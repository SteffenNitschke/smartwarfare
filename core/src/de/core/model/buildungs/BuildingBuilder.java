package de.core.model.buildungs;

import de.core.util.Utils;

public class BuildingBuilder
{
    public static BuildingInterface build(String type)
    {
        BuildingInterface building;
        if (Utils.isFactory(type))
        {
            switch (type)
            {
                case Utils.FACTORY_TYPE_BARRACK:
                    building = new Barracks();
                    break;
                case Utils.FACTORY_TYPE_AIRPORT:
                    building = new Airport();
                    break;
                case Utils.FACTORY_TYPE_HAVEN:
                    building = new Haven();
                    break;
                case Utils.FACTORY_TYPE_TANK_FACTORY:
                    building = new TankFactory();
                    break;
                default:
                    building = new Factory();
            }
        }
        else
        {
            building = new City();
        }

        return building;
    }

}

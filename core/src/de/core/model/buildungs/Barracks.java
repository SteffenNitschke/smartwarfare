package de.core.model.buildungs;

import de.core.util.Utils;

/**
 * Created by Gandail on 28.05.2017.
 */
public class Barracks extends Factory {

    public Barracks() {

        type = Utils.FACTORY_TYPE_BARRACK;
        this.productRange.add(Utils.TROOPS_TYPE_INFANTRY);

    }

}

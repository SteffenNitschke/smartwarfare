package de.core.model.buildungs;

import de.core.util.Utils;

/**
 * Created by Gandail on 28.05.2017.
 */
public class TankFactory extends Factory implements BuildingInterface {

    public TankFactory() {

        type = Utils.FACTORY_TYPE_TANK_FACTORY;
        this.productRange.add("");

    }

}

package de.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import de.core.network.messages.PlayerMessage;
import de.core.model.units.UnitInterface;
import de.core.model.units.heroes.HeroInterface;

import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Gandail on 28.05.2017.
 */
@AllArgsConstructor
public class User {

    private String name;
    @Getter
    @Setter
    private Id _id;
    @JsonIgnore
    private String token;
    private final HashSet<Game> games = new HashSet<>();
    private final HashSet<HeroInterface> heroes = new HashSet<>();
    @Getter
    private final HashSet<PlayerMessage> friends = new HashSet<>();
    @Getter
    @Setter
    private int level;
    @Getter
    @Setter
    private long experience;
    @Getter
    private final List<String> chats = new ArrayList<>();

    private final PropertyChangeSupport change = new PropertyChangeSupport(this);

    public User(String name) {
        this.name = name;
    }

    //-------------------------------------------

    public PropertyChangeSupport getChange() {
        return change;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_NAME = "name";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User withName(String name) {
        setName(name);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_TOKEN = "token";

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_ID = "_id";

    //-------------------------------------------

    public static final String PROPERTY_NAME_GAMES = "games";

    public HashSet<Game> getGames() {
        return games;
    }

    public void addGame(Game game) {
        games.add(game);
    }

    public void removeGame(Game game) {
        games.remove(game);
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_HEROES = "heroes";

    public HashSet<HeroInterface> getHeroes() {
        return heroes;
    }

    public void addHero(HeroInterface hero) {
        heroes.add(hero);
    }

    public void removeHero(UnitInterface hero) {
        heroes.remove(hero);
    }

    //-------------------------------------------
}

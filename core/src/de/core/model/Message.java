package de.core.model;

import lombok.Getter;
import lombok.Setter;
import de.core.network.messages.PlayerMessage;

public class Message {

    @Getter
    @Setter
    private PlayerMessage sender;
    @Getter
    @Setter
    private String timestamp;
    @Getter
    @Setter
    private String message;
}

package de.core.model;

import lombok.Getter;
import lombok.Setter;
import de.core.network.messages.PlayerMessage;

import java.util.HashSet;
import java.util.LinkedList;

public class Chat {

    @Getter
    @Setter
    private String chatId;
    @Getter
    @Setter
    private LinkedList<Message> messages;

    @Getter
    @Setter
    private HashSet<PlayerMessage> participants;
    @Getter
    @Setter
    private Game gameId;
}

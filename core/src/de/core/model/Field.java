package de.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.core.model.buildungs.BuildingInterface;
import de.core.model.units.UnitInterface;
import de.core.util.FieldType;

import org.json.JSONObject;

import java.beans.PropertyChangeSupport;
import java.util.HashSet;

/**
 * Created by Gandail on 28.05.2017.
 */
public class Field {

    private FieldType type;
    private int x;
    private int y;
    private int defence;
    private String fieldID;
    @JsonIgnore
    private UnitInterface unit;
    @JsonIgnore
    private BuildingInterface building;

    @JsonIgnore
    private Field upper;
    @JsonIgnore
    private Field lower;
    @JsonIgnore
    private Field left;
    @JsonIgnore
    private Field right;

    @JsonIgnore
    private Map map;

    @JsonIgnore
    private PropertyChangeSupport change = new PropertyChangeSupport(this);

    //-------------------------------------------

    public PropertyChangeSupport getChange() {
        return change;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_FIELD_ID = "fieldID";

    public String getFieldID() {
        return fieldID;
    }

    public void setFieldID(String fieldID) {
        this.fieldID = fieldID;
    }

    public Field withID(String id) {
        setFieldID(id);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_MAP = "map";

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public Field withMap(Map map) {
        setMap(map);
        return this;
    }

    //-------------------------------------------

    public HashSet<Field> getNeighbors() {

        HashSet<Field> neighbors = new HashSet<Field>();
        neighbors.add(this.upper);
        neighbors.add(this.right);
        neighbors.add(this.lower);
        neighbors.add(this.left);
        return neighbors;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_UPPER = "upper";

    public Field getUpper() {
        return upper;
    }

    public void setUpper(Field upper) {
        if(upper != this.upper) {
            this.upper = upper;
            if(upper != null) {
                this.upper.setLower(this);
            }
        }
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_LOWER = "lower";

    public Field getLower() {
        return lower;
    }

    public void setLower(Field lower) {
        if(lower != this.lower) {
            this.lower = lower;
            if(lower != null) {
                this.lower.setUpper(this);
            }
        }
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_RIGHT = "right";

    public Field getRight() {
        return right;
    }

    public void setRight(Field right) {
        if(right != this.right) {
            this.right = right;
            if(right != null) {
                this.right.setLeft(this);
            }
        }
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_LEFT = "left";

    public Field getLeft() {
        return left;
    }

    public void setLeft(Field left) {
        if(left != this.left) {
            this.left = left;
            if(left != null) {
                this.left.setRight(this);
            }
        }
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_BUILDING = "building";

    public BuildingInterface getBuilding() {
        return building;
    }

    public void setBuilding(BuildingInterface building) {
        if(building != this.building) {
            if (building != null) {
                BuildingInterface old = this.building;
                this.building = building;
                this.building.setField(this);
                change.firePropertyChange(PROPERTY_NAME_BUILDING, old, building);
            } else {
                BuildingInterface old = this.building;
                this.building = null;
                old.setField(null);
                change.firePropertyChange(PROPERTY_NAME_BUILDING, this.building, null);
            }
        }
    }

    public Field withBuilding(BuildingInterface building) {
        setBuilding(building);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_TYPE = "type";

    public FieldType getType() {
        return type;
    }

    public void setType(final FieldType type) {
        this.type = type;
    }

    public Field withType(final FieldType type) {
        setType(type);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_X = "x";

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public  Field withX(int x) {
        setX(x);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_Y = "y";

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public  Field withY(int y) {
        setY(y);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_UNIT = "unit";

    public UnitInterface getUnit() {
        return unit;
    }

    public void setUnit(UnitInterface unit) {
        if(unit != this.unit) {
            if (unit != null) {
                UnitInterface old = this.unit;
//                System.out.println("Set Unit to " + unit);
                this.unit = unit;
                this.unit.setField(this);
                change.firePropertyChange(PROPERTY_NAME_UNIT, old, unit);
            } else {
                UnitInterface old = this.unit;
                this.unit = null;
                old.setField(null);
                change.firePropertyChange(PROPERTY_NAME_UNIT, this.unit, null);
            }
        }
    }

    public Field withUnit(UnitInterface unit) {
        setUnit(unit);
        return this;
    }

    //-------------------------------------------

    public static final String PROPERTY_NAME_DEFENCE = "defence";

    public int getDefence() {
        return defence;
    }

    public void setDefence(int defence) {
        this.defence = defence;
    }

    public Field withDefence(int defence) {
        setDefence(defence);
        return  this;
    }

    public JSONObject getJSONField() {
        JSONObject jsonField = new JSONObject();
        jsonField.put(Field.PROPERTY_NAME_FIELD_ID, fieldID);
        jsonField.put(Field.PROPERTY_NAME_TYPE, type);
        if (building != null) {
            jsonField.put(Field.PROPERTY_NAME_BUILDING, building.getJSONBuilding());
        }
        if (unit != null) {
            jsonField.put(Field.PROPERTY_NAME_UNIT, unit.getJSONUnit());
        }
        jsonField.put(Field.PROPERTY_NAME_X, x);
        jsonField.put(Field.PROPERTY_NAME_Y, y);

        return jsonField;
    }
}

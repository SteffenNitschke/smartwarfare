package de.core.handler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

import java.util.HashMap;

import lombok.Getter;
import lombok.Setter;
import de.core.screens.MainScreenInterface;

public class ScreenHandler
{
    private final String loggingTag = getClass().getSimpleName();

    @Getter
    @Setter
    private boolean screenInFrontOf = false;
    private HashMap<String, Screen> screens = new HashMap<>();

    @Getter
    @Setter
    private MainScreenInterface mainScreen;

    public void addScreen(Screen screen)
    {
        Gdx.app.log(loggingTag,
                    "addScreen: " + screen.getClass().getCanonicalName());
        screens.put(screen.getClass().getCanonicalName(),
                    screen);
    }

    public Screen getScreen(String key)
    {
        Gdx.app.log(loggingTag,
                    "getScreen: " + key);
        return screens.get(key);
    }
}

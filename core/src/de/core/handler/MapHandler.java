package de.core.handler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.util.HashSet;

import de.core.MainGame;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import de.core.model.Field;
import de.core.model.Map;
import de.core.model.Player;
import de.core.model.buildungs.City;
import de.core.network.messages.SmallMapMessage;
import de.core.util.FieldType;
import de.core.util.Utils;

@RequiredArgsConstructor
public class MapHandler
{
    private final MainGame game;

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final String loggingTag = getClass().getSimpleName();

    @Getter
    @Setter
    private Map editableMap;

    public HashSet<SmallMapMessage> getLocalMapsAsSmallMap()
    {
        Gdx.app.log(loggingTag,
                    "getLocalMapAsSmallMap");
        HashSet<SmallMapMessage> maps = new HashSet<>();

        FileHandle[] mapFiles = getLocalMaps();

        for (FileHandle file : mapFiles)
        {

            JSONObject fileObj = new JSONObject(file.readString());
            Gdx.app.log(loggingTag,
                        fileObj.toString());
            SmallMapMessage smallMap = new SmallMapMessage(
                    fileObj.getString("_id"),
                    fileObj.getInt("numberOfPlayers"),
                    fileObj.getString("name"),
                    fileObj.getString("editorName"),
                    fileObj.getInt("maxX"),
                    fileObj.getInt("maxY")
            );

            maps.add(smallMap);
        }

        return maps;
    }

    private FileHandle[] getLocalMaps()
    {
        return Gdx.files.local(".\\core\\resources\\maps").list();
    }

    public Map loadMap(final String name)
    {
        FileHandle map = Gdx.files.local(".\\core\\resources\\maps\\" + name + ".json");

        return new Map(new JSONObject(map.readString()),
                       new de.core.model.Game("Editor"));
    }

    public void safeMap(
            final String name,
            final Map map)
    {

        FileHandle file = Gdx.files.local(".\\core\\resources\\maps\\" + name + ".json");
        try
        {
            file.writeString(objectMapper.writeValueAsString(map),
                             false);
        }
        catch (JsonProcessingException e)
        {
            Gdx.app.error(loggingTag,
                          "Save failure",
                          e);
        }

    }

    //for Editor (new empty map)
    public Map createNewDefaultMap(
            final String name,
            final String editorName,
            final int numberOfPlayers,
            final int width,
            final int height)
    {

        final Map map = new Map(name);
        map.setEditorName(editorName);
        map.setNumberOfPlayers(numberOfPlayers);
        map.setMaxX(width);
        map.setMaxY(height);

        de.core.model.Game game = new de.core.model.Game("Editor");

        final Player[] players = new Player[numberOfPlayers];
        for (int i = 0; i < numberOfPlayers; i++)
        {
            players[i] = new Player("Player" + i).withName("Player" + i)
                                                 .withColor(Utils.getColor(i));
        }
        for (int i = 0; i < numberOfPlayers - 1; i++)
        {
            if (i == 0)
            {
                players[i].setPrev(players[numberOfPlayers - 1]);
            }
            players[i].setNext(players[i + 1]);
        }
        map.setGame(game);
        map.getGame().setCurrentPlayer(players[0]);
        for (Player player :
                players)
        {
            game.addPlayer(player);
        }

        final HashSet<Field> fields = new HashSet<>();

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Field field = new Field();
                field.withX(x)
                     .withY(y)
                     .withID(field.getX() + "x" + field.getY())
                     .withMap(map)
                     .withType(FieldType.FIELD_TYPE_EMPTY)
                     .withDefence(Utils.getDefenceFromType(field.getType()));
                fields.add(field);
                if (field.getType().equals(FieldType.FIELD_TYPE_BUILDING))
                {
                    field.setBuilding(new City().withGame(game).withCaptureValue(20));
                }
            }
        }
        map.getFields().addAll(fields);

        return map;

    }
}

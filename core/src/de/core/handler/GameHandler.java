package de.core.handler;

import java.util.Observable;
import java.util.Observer;

import de.core.MainGame;
import de.core.model.Id;
import de.core.network.events.game.GetAllGamesEvent;
import de.core.network.events.game.GetGameEvent;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GameHandler implements Observer
{
    private final MainGame mainGame;

    @Override
    public void update(
            final Observable observable,
            final Object event)
    {
        if (event instanceof GetAllGamesEvent)
        {
            handleGetAllGamesEvent((GetAllGamesEvent) event);
            return;
        }
        if (event instanceof GetGameEvent)
        {
            handleGetGameEvent((GetGameEvent) event);
        }
    }

    private void handleGetGameEvent(final GetGameEvent event)
    {
        mainGame.getUser().getGames().add(event.getGame());
    }

    private void handleGetAllGamesEvent(final GetAllGamesEvent event)
    {
        for (Id gameId : event.getGameIds())
        {
            mainGame.getConnection().getGameClient().getGameById(gameId.getIdentifier());
        }
    }
}

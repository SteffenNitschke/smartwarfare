package de.core.handler;

import com.badlogic.gdx.Gdx;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;

import de.core.model.Chat;
import de.core.model.Id;
import de.core.model.Message;
import de.core.network.messages.PlayerMessage;
import de.core.network.messages.news.ChatRequest;


public class ChatHandler
{

    private ObjectMapper objectMapper = new ObjectMapper();
    private String loggingTag = getClass().getSimpleName();

    public void safeChat(final Chat chat) throws IOException
    {

        Gdx.files.internal("chat" + chat.getChatId() + ".json").writeString(objectMapper
                                                                                    .writeValueAsString(chat),
                                                                            false);
    }

    public Chat getChat(final String chatId) throws IOException
    {
        Gdx.app.log(loggingTag,
                    "getChat");
//        return getDummyChat();
        return objectMapper.readValue(Gdx.files.internal("chat" + chatId + ".json").readString(), Chat.class);
    }

    private Chat getDummyChat()
    {

        Chat chat = new Chat();
        chat.setChatId("12345");

        HashSet<PlayerMessage> participants = new HashSet<>();
        PlayerMessage kali = new PlayerMessage(new Id("987"),
                                               "Kali");
        PlayerMessage bob = new PlayerMessage(new Id("654"),
                                              "bob");
        PlayerMessage franzi = new PlayerMessage(new Id("123"),
                                                 "Amanda");
        PlayerMessage steffen = new PlayerMessage(new Id("fc60ffeb-a864-4a02-9f80-c776a866ceae"),
                                                  "Steffen");
        participants.add(kali);
        participants.add(bob);
        participants.add(franzi);
        participants.add(steffen);
        chat.setParticipants(participants);

        LinkedList<Message> messages = new LinkedList<>();
        Message m1 = new Message();
        m1.setTimestamp(System.currentTimeMillis() + "");
        m1.setSender(steffen);
        m1.setMessage("Hallo ihr lieben zu diesem TestChat!");
        messages.addFirst(m1);

        m1 = new Message();
        m1.setTimestamp(System.currentTimeMillis() + "");
        m1.setSender(franzi);
        m1.setMessage("Hallo Steffen :-) freut mich hier sein zu duerfen ;-)");
        messages.addFirst(m1);

        m1 = new Message();
        m1.setTimestamp(System.currentTimeMillis() + "");
        m1.setSender(kali);
        m1.setMessage("Hi");
        messages.addFirst(m1);

        m1 = new Message();
        m1.setTimestamp(System.currentTimeMillis() + "");
        m1.setSender(bob);
        m1.setMessage("K");
        messages.addFirst(m1);

        m1 = new Message();
        m1.setTimestamp(System.currentTimeMillis() + "");
        m1.setSender(franzi);
        m1.setMessage("Na ihr seid ja gespraechig :/");
        messages.addFirst(m1);

        m1 = new Message();
        m1.setTimestamp(System.currentTimeMillis() + "");
        m1.setSender(steffen);
        m1.setMessage("Na dann unterhalten eben nur wir beide uns, Suesse :*");
        messages.addFirst(m1);

        m1 = new Message();
        m1.setTimestamp(System.currentTimeMillis() + "");
        m1.setSender(franzi);
        m1.setMessage("Gerne =) :*");
        messages.addFirst(m1);

        chat.setMessages(messages);
        return chat;
    }

    public void safeMessage(final ChatRequest message) throws IOException
    {

        Gdx.app.log(loggingTag,
                    "safeMessage " + message);//TODO
//        Chat chat = getChat(message.getChatId());
//
//        Message newMessage = new Message();
//        newMessage.setMessage(message.getMessage());
//        newMessage.setSender(message.getSender());
//        newMessage.setTimestamp(message.getTimestamp());
//
//        chat.getMessages().addLast(newMessage);
//        safeChat(chat);
    }
}

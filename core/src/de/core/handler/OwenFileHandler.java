package de.core.handler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import java.net.URISyntaxException;
import java.util.HashMap;

import de.core.util.Utils;

public class OwenFileHandler
{
    private static String tag = OwenFileHandler.class.getSimpleName();
    private static HashMap<String, FileHandle> files = new HashMap<>();
    private static boolean isJar = true;

    static
    {
        try
        {
            String EXTERNAL_PATH = (Utils.class.getProtectionDomain().getCodeSource().getLocation()
                                               .toURI()
                                               .getPath());

            final String[] split = EXTERNAL_PATH.split("/");

            StringBuilder path = new StringBuilder();
            for (int i = 0; i < split.length - 1; i++)
            {
                path.append(split[i]).append("/");
            }
            EXTERNAL_PATH = path.append("assets/").toString();

            final FileHandle[] tempFiles = Gdx.files.absolute(EXTERNAL_PATH).list();

            for (FileHandle file : tempFiles)
            {
                files.put(file.name(),
                          file);
            }
        }
        catch (URISyntaxException e)
        {
            Gdx.app.error(tag,
                          e.getReason(),
                          e);
        }
    }

    public static FileHandle getFile(
            String name,
            String type)
    {
        FileHandle file;
        if (files.containsKey(name + type))
        {
            file = files.get(name + type);
        }
        else
        {
            if (isJar)
            {
                file = Gdx.files.internal(name + type);
            }
            else
            {
                file = Gdx.files.internal(Utils.ASSET_PATH + name + type);
            }
        }
        return file;
    }

    public static void setIsJar(boolean isJar)
    {
        OwenFileHandler.isJar = isJar;
    }
}

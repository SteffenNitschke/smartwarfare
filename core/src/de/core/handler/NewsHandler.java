package de.core.handler;

import com.badlogic.gdx.Gdx;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

import de.core.MainGame;
import de.core.network.events.game.invite.GameInvitationAnswerEvent;
import de.core.network.events.user.invite.FriendAnswerEvent;
import de.core.network.messages.news.FriendAnswerRequest;
import de.core.network.socket.SocketConnection;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import de.core.model.Id;
import de.core.network.events.news.GetNewsEvent;
import de.core.network.events.news.MarkNewsAsReadedEvent;
import de.core.network.messages.news.NewsDto;

@RequiredArgsConstructor
public class NewsHandler implements Observer
{
    private final MainGame game;

    private final String loggingTag = getClass().getSimpleName();

    @Getter
    private HashMap<Id, NewsDto> news = new HashMap<>();

    @Override
    public void update(
            final Observable observable,
            final Object event)
    {
        if(observable instanceof SocketConnection)
        {
            final NewsDto message = (NewsDto) event;

            news.put(message.get_id(), message);
            game.viewNews(message);
        }
        else if (event instanceof MarkNewsAsReadedEvent)
        {
            if (((MarkNewsAsReadedEvent) event).isSuccessful())
            {
                news.remove(((MarkNewsAsReadedEvent) event).getNewsId());
                game.removeNews(((MarkNewsAsReadedEvent) event).getNewsId());
            }
        }
        else if (event instanceof GetNewsEvent)
        {
            addNews(((GetNewsEvent) event).getNews());
        }
        else if (event instanceof FriendAnswerEvent)
        {
            game.removeNews(((FriendAnswerEvent) event).getNewsId());
        }
        else if(event instanceof GameInvitationAnswerEvent)
        {
            game.removeNews(((GameInvitationAnswerEvent) event).getNewsId());
        }
    }

    private void addNews(final LinkedList<NewsDto> messages)
    {
        Gdx.app.log(loggingTag,
                    "Number of Messages: " + messages.size());

        if (messages.size() > 0)
        {
            for (int i = 0; i < messages.size(); i++)
            {
                final NewsDto message = messages.get(i);
                news.put(message.get_id(),
                         message);

                game.viewNews(message);
            }
        }
    }

    public void handleFriendAnswer(
            final FriendAnswerRequest answer,
            final Id newsId)
    {
        try
        {
            game.getConnection().getFriendInvitationClient()
                .sendFriendAnswer(answer,
                                  newsId)
                .addObserver(this);
        }
        catch (final JsonProcessingException exception)
        {
            exception.printStackTrace();
        }
    }

    public void handleGameAnswer(
            final NewsDto gameAnswer,
            final Id newsId)
    {
        try
        {
            game.getConnection().getGameInvitationClient()
                .sendGameAnswer(gameAnswer, newsId)
                .addObserver(this);

        }
        catch (JsonProcessingException e)
        {
            e.printStackTrace();
        }
    }
}

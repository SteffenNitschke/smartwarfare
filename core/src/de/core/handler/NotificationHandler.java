package de.core.handler;

public interface NotificationHandler
{
    void showSystemNotification(final String text);
    void showUserNotification(final String text, final boolean successful);
}

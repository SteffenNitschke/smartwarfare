package de.core.util;

public enum GameTypes {
    TEAM_DEATH_MATCH,
    LAST_MAN_STANDING,
    SCENARIO,
    PARTY_GAME
}

package de.core.util;


import com.badlogic.gdx.Gdx;

import de.core.model.Id;
import de.core.network.messages.PlayerMessage;
import de.core.network.messages.news.TurnRequest;
import de.core.network.messages.news.turnMessages.*;
import de.core.model.Field;
import de.core.model.units.UnitInterface;

import java.util.HashMap;

/**
 * Registrate all Controller to track all moves, attacks, creates, combines, captures, etc.
 * knows about all games and save all tracked actions for that game.
 */
public class TurnEventListener
{

    private final String name;

    private HashMap<String, TurnRequest> turns = new HashMap<>();
    private String userId;
    private String loggingTag = getClass().getSimpleName();

    public TurnEventListener(
            String userId,
            String name)
    {
        Gdx.app.log(loggingTag,
                    "TurnEventListener");
        this.userId = userId;
        this.name = name;
    }

    public TurnRequest getTurnOf(
            final String gameId,
            final boolean delete)
    {
        Gdx.app.log(loggingTag,
                    "getTurnOf " + gameId);
        TurnRequest turn = turns.get(gameId);
        if (delete)
        {
            turns.remove(gameId);
        }
        return turn;
    }

    private void saveMessage(
            final String gameId,
            final TurnMessageInterface message)
    {
        Gdx.app.log(loggingTag,
                    "saveMessage" + message.toString());

        if (!turns.containsKey(gameId))
        {
            final PlayerMessage playerMessage =
                    new PlayerMessage(
                            new Id(this.userId),
                            this.name);

            final TurnRequest turn = new TurnRequest(playerMessage);
            turn.addMessage(message);

            turns.put(gameId,
                      turn);
        }
        else
        {
            turns.get(gameId).addMessage(message);
        }
    }

    public void handleMoveMessage(
            final UnitInterface unit,
            final Field to)
    {
        Gdx.app.log(loggingTag,
                    "handleMoveMessage");
        final MoveMessage move = new MoveMessage();
        move.setFieldID(to.getFieldID());
        move.setUnitID(unit.getUnitId());
        final String gameId = unit.getPlayer().getGame().getGameID();
        saveMessage(gameId,
                    move);
    }

    public void handleAttackMessage(
            final String enemyPlayerId,
            final UnitInterface owenUnit,
            final UnitInterface enemyUnit,
            final Long seed)
    {
        Gdx.app.log(loggingTag,
                    "handleAttackMessage");
        final AttackMessage attack = new AttackMessage();
        attack.setEnemyPlayer(enemyPlayerId);
        attack.setEnemyUnit(enemyUnit.getUnitId());
        attack.setOwenUnitID(owenUnit.getUnitId());
        attack.setSeed(seed);
        final String gameId = owenUnit.getPlayer().getGame().getGameID();
        saveMessage(gameId,
                    attack);
    }

    public void handleCaptureMessage(
            final UnitInterface unit,
            final Field field)
    {
        Gdx.app.log(loggingTag,
                    "handleCaptureMessage");
        final CaptureMessage capture = new CaptureMessage();
        capture.setFieldID(field.getFieldID());
        capture.setUnitID(unit.getUnitId());
        final String gameId = field.getMap().getGame().getGameID();
        saveMessage(gameId,
                    capture);
    }

    //TODO add combine when unitController have that method
    public void handleCombineMessage(
            final UnitInterface unit,
            final UnitInterface secondUnit)
    {
        Gdx.app.log(loggingTag,
                    "handleCombineMessage");
        final CombineMessage combine = new CombineMessage();
        combine.setUnitID(unit.getUnitId());
        combine.setSecondUnitID(secondUnit.getUnitId());
        final String gameId = unit.getPlayer().getGame().getGameID();
        saveMessage(gameId,
                    combine);
    }

    public void handleCreateMessage(
            final UnitInterface unit,
            final Field field)
    {
        Gdx.app.log(loggingTag,
                    "handleCreateMessage");
        final CreateMessage create = new CreateMessage();
        create.setFieldID(field.getFieldID());
        create.setUnitType(unit.getType());
        final String gameId = unit.getPlayer().getGame().getGameID();
        saveMessage(gameId,
                    create);
    }
}
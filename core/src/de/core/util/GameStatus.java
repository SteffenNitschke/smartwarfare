package de.core.util;

public enum GameStatus {

    INVITATION,
    RUNNING,
    CANCELLED
}

package de.core.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.Array;

import java.util.LinkedList;

/**
 * Created by Gandail on 22.06.2017.
 */
public class HudInputProcessor implements InputProcessor {

    private Stage stage;

    public HudInputProcessor(Stage stage, boolean isGameScreen) {
        Gdx.app.log(loggingTag, "HudInputProcessor");
        this.stage = stage;
        this.isGameScreen = isGameScreen;
    }

    private Array<Actor> actors = new Array<>();
    private Array<Actor> withoutIdle = new Array<>();
    private Array<Actor> idleActors = new Array<>();
    private boolean idle = false;
    private TextField selectedTextField;
    private String oldText;
    private boolean isGameScreen;

    private String imageTextButtonClass = ImageTextButton.class.toString();
    private String textButtonClass = TextButton.class.toString();
    private String buttonClass = Button.class.toString();
    private String scrollPaneClass = ScrollPane.class.toString();
    private String imageClass = Image.class.toString();
    private String textFieldClass = TextField.class.toString();
    private LinkedList<TextField> textFields = new LinkedList<>();
    private Button okButton;
    private Button exitButton;

    private String loggingTag = getClass().getSimpleName();

    @Override
    public boolean keyDown(int i) {
        stage.keyDown(i);
        return false;
    }

    @Override
    public boolean keyUp(int i) {
        stage.keyUp(i);
        return false;
    }

    @Override
    public boolean keyTyped(char c) {
//        stage.keyTyped(c);
        return false;
    }

    @Override
    public boolean touchDown(int i, int i1, int i2, int i3) {
//        Gdx.app.log(loggingTag, "Touch Down " + i + " " + i1 + " " + i2 + " " + i3);
//        Gdx.app.log(loggingTag, "Idle: " + idle);
        Array<Actor> actors = new Array<>();
        actors.addAll(idle ? withoutIdle : this.actors);
//        Gdx.app.log(loggingTag, "Actors size: " + actors.size);
        if (!isGameScreen) {
            actors.addAll(stage.getActors());
        }
//        Gdx.app.log(loggingTag, "Size: " + actors.size);
        if (idle) {
            actors.removeAll(idleActors, true);
//            Gdx.app.log(loggingTag, "Size: " + actors.size);
        }

        for (Actor actor :
                actors) {
            if (actor == null) {
                continue;
            }
            String className = actor.getClass().toString();
//            Gdx.app.log(loggingTag, "Actor: " + actor.getClass().getSimpleName());
            if (className.equals(imageTextButtonClass)
                    || className.equals(buttonClass)
                    || className.equals(scrollPaneClass)
                    || className.equals(imageClass)
                    || className.equals(textButtonClass)
                    ) {
//                Gdx.app.log(loggingTag, "Actor is ImageText-, Image- or Normal Button or ScrollPane");
                if (isPressed(actor, i, i1)) {
//                    Gdx.app.log(loggingTag, "Actor is pressed " + actor.getName() + ", " + actor.getClass().getSimpleName());
                    if (!className.equals(scrollPaneClass)) {
//                        Gdx.app.log(loggingTag, "Is no ScrollPane with " + actor.getListeners().size + " Listeners");
                        for (EventListener listener :
                                actor.getListeners()) {
//                            Gdx.app.log(loggingTag, listener.getClass().getSimpleName());
                            InputEvent inputEvent = new InputEvent();
                            inputEvent.setListenerActor(actor);
                            ((InputListener) listener).touchDown(inputEvent, i, i1, i2, i3);
                        }
                    } else {
//                        Gdx.app.log(loggingTag, "Is a ScrollPane");
                        ScrollPane pane = (ScrollPane) actor;
                        if (pane.getStage() != null) {
                            pane.getStage().touchDown(i, i1, i2, i3);
                        }
                    }
                }
            }
        }
        return true;
    }

    private boolean isPressed(Actor actor, int x, int y) {
        float positionX = actor.getX();
        float positionY = actor.getY();
        float width = actor.getWidth();
        float height = actor.getHeight();
        y = Gdx.graphics.getHeight() - y;

        return positionX <= x
                && x <= positionX + width
                && positionY <= y
                && y <= positionY + height;

    }

    @Override
    public boolean touchUp(int i, int i1, int i2, int i3) {
//        Gdx.app.log(loggingTag, "touchUp " + i + " " + i1 + " " + i2 + " " + i3);
        Array<Actor> actors = new Array<>();
        if (!isGameScreen) {
            actors.addAll(stage.getActors());
        }
        actors.addAll(idle ? withoutIdle : this.actors);
        if (idle) {
            actors.removeAll(idleActors, true);
        }

        for (Actor actor :
                actors) {
            if (actor == null) {
//                Gdx.app.log(loggingTag, "actor == null");????
                continue;
            }
            String className = actor.getClass().toString();
            if (className.equals(imageTextButtonClass)
                    || className.equals(buttonClass)
                    || className.equals(textFieldClass)
                    || className.equals(scrollPaneClass)
                    || className.equals(textButtonClass)
                    ) {
//                Gdx.app.log(loggingTag, "Actor is ImageText-, Image- or Normal Button, TextField or ScrollPane");
                if (isPressed(actor, i, i1)) {
//                    Gdx.app.log(loggingTag, "Is Pressed " + actor.getName() + ", " + actor.getClass().getSimpleName());

                    if (className.equals(textFieldClass)) {
//                        Gdx.app.log(loggingTag, "Is TextField");
                        selectTextField(actor);

                    } else if (!className.equals(scrollPaneClass)) {
//                        Gdx.app.log(loggingTag, "All other but not ScrollPane with " + actor.getListeners().size + " Listeners");
                        for (EventListener listener :
                                actor.getListeners()) {
                            if (listener != null) {
                                InputEvent inputEvent = new InputEvent();
                                inputEvent.setListenerActor(actor);
                                inputEvent.setTarget(actor);
//                                Gdx.app.log(loggingTag, "Trigger Listener " + listener);
                                ((InputListener) listener).touchUp(inputEvent, i, i1, i2, i3);
                            }
                        }
                    } else {
                        ScrollPane pane = (ScrollPane) actor;
                        if (pane.getStage() != null) {
                            pane.getStage().touchUp(i, i1, i2, i3);
                        }
                    }
                    break;
                }
            }
        }
        return true;
    }

    private void selectTextField(Actor actor) {

        TextField textField = (TextField) actor;
        if(this.selectedTextField != actor) {
            if(this.selectedTextField != null) {
                if (this.selectedTextField.getText().isEmpty()) {
                    this.selectedTextField.setText(this.oldText);
                }
            }
            this.selectedTextField = textField;
            this.oldText = textField.getText();
            this.selectedTextField.setText("");
        }

    }

    @Override
    public boolean touchDragged(int i, int i1, int i2) {
        stage.touchDragged(i, i1, i2);
        return false;
    }

    @Override
    public boolean mouseMoved(int i, int i1) {
        stage.mouseMoved(i, i1);
        return false;
    }

    @Override
    public boolean scrolled(int i) {
        stage.scrolled(i);
        return false;
    }

    private void handleTextField(char c) {

        if(this.selectedTextField != null) {
            selectedTextField.setText(selectedTextField.getText() + c);
        }
    }

    public void addActor(Actor actor) {
        actors.add(actor);
        if(actor instanceof TextField) {
            this.textFields.add((TextField) actor);
        }
    }

    public void removeActor(final Actor actor) {
        actors.removeValue(actor, true);
        if (withoutIdle.contains(actor, true)) {
            Gdx.app.log(loggingTag, "remove Actor: without Idle");
            withoutIdle.removeValue(actor, true);
        }
        if(idleActors.contains(actor, true)) {
            idleActors.removeValue(actor, true);
        }
        textFields.removeFirstOccurrence(actor);
    }

    public void addIdleActor(Actor actor) {
        idleActors.add(actor);
    }

    public void idle(boolean idle) {
        this.idle = idle;
        if (idle) {
            for (Actor actor :
                    actors) {
                if (!withoutIdle.contains(actor, true)) {
                    actor.setColor(Color.GRAY);
                }
            }
            for (Actor actor :
                    idleActors) {
                actor.setColor(Color.GRAY);
            }
        } else {
            for (Actor actor :
                    actors) {
                if (!withoutIdle.contains(actor, true)) {
                    actor.setColor(Color.WHITE);
                }
            }
            for (Actor actor :
                    idleActors) {
                actor.setColor(Color.WHITE);
            }
        }
    }

    public void withoutIdle(Actor actor) {
        this.withoutIdle.add(actor);
        this.actors.add(actor);
    }

    public boolean isIdle() {
        return this.idle;
    }

    public void handleKeyInput() {
        if(Gdx.input.isKeyJustPressed(Input.Keys.A)) {
            handleKey('a');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.B)) {
            handleKey('b');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.C)) {
            handleKey('c');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            handleKey('d');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.E)) {
            handleKey('e');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.F)) {
            handleKey('f');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.G)) {
            handleKey('g');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.H)) {
            handleKey('h');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.I)) {
            handleKey('i');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.J)) {
            handleKey('j');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.K)) {
            handleKey('k');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.L)) {
            handleKey('l');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.M)) {
            handleKey('m');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.N)) {
            handleKey('n');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.O)) {
            handleKey('o');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.P)) {
            handleKey('p');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.Q)) {
            handleKey('q');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.R)) {
            handleKey('r');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.S)) {
            handleKey('s');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.T)) {
            handleKey('t');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.U)) {
            handleKey('u');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.V)) {
            handleKey('v');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.W)) {
            handleKey('w');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.X)) {
            handleKey('x');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.Y)) {
            handleKey('y');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.Z)) {
            handleKey('z');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.BACKSPACE)) {
            handleBackspace();
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            handleKey(' ');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_0) || Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_0)) {
            handleKey('0');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_1) || Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_1)) {
            handleKey('1');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_2) || Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_2)) {
            handleKey('2');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_3) || Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_3)) {
            handleKey('3');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_4) || Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_4)) {
            handleKey('4');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_5) || Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_5)) {
            handleKey('5');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_6) || Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_6)) {
            handleKey('6');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_7) || Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_7)) {
            handleKey('7');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_8) || Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_8)) {
            handleKey('8');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_9) || Gdx.input.isKeyJustPressed(Input.Keys.NUMPAD_9)) {
            handleKey('9');
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.TAB)) {
            handleTab();
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            handleEnter();
        }else if(Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            handleEscape();
        }
    }

    private void handleEscape()
    {
        if(exitButton != null)
        {
            for (EventListener listener :
                    this.exitButton.getListeners()) {
                if (listener != null) {//???
                    final InputEvent inputEvent = new InputEvent();
                    inputEvent.setListenerActor(this.exitButton);
                    ((InputListener) listener).touchUp(inputEvent, 1f, 1f, 1, 1);
                }
            }
        }
    }

    private void handleEnter() {
        if (this.okButton != null) {
            for (EventListener listener :
                    this.okButton.getListeners()) {
                if (listener != null) {//???
                    final InputEvent inputEvent = new InputEvent();
                    inputEvent.setListenerActor(this.okButton);
                    ((InputListener) listener).touchUp(inputEvent, 1f, 1f, 1, 1);
                }
            }
        }
    }

    private void handleTab() {

        int index = textFields.indexOf(selectedTextField);
        if(index == textFields.size() - 1) index = -1;
        selectTextField(textFields.get(index + 1));

    }

    private void handleBackspace() {
        if (this.selectedTextField.getText().length() > 0) {
            this.selectedTextField.setText(this.selectedTextField.getText().substring(
                    0, this.selectedTextField.getText().length() - 1
            ));
        }
    }

    private void handleKey(char c) {

        if(Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {

            String st = c + "";
            handleTextField(st.toUpperCase().charAt(0));
        } else {
            handleTextField(c);
        }

    }

    public void setOkButton(Button button) {
        this.okButton = button;
    }

    public void removeOkButton() {
        this.okButton = null;
    }

    public void setExitButton(Button button) {
        this.exitButton = button;
    }

    public void removeExitButton() {
        this.exitButton = null;
    }
}

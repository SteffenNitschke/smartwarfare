package de.core.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;

import de.core.handler.OwenFileHandler;
import de.core.model.Field;
import de.core.model.Map;
import de.core.model.units.Unit;
import de.core.model.units.UnitInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

/**
 * Created by Gandail on 28.05.2017.
 */
public class Utils
{

    public static final String TROOPS_TYPE_INFANTRY = "infantry";
    public static final String TROOPS_TYPE_VEHICLE = "vehicles";
    public static final String TROOPS_TYPE_SHIP = "ship";
    public static final String TROOPS_TYPE_PLANE = "plane";
    public static final String TROOPS_TYPE_SUBMARINE = "submarine";

    public static final String[] ALL_UNIT_TYPES = {
            TROOPS_TYPE_INFANTRY, TROOPS_TYPE_PLANE,
            TROOPS_TYPE_SHIP, TROOPS_TYPE_SUBMARINE,
            TROOPS_TYPE_VEHICLE};

    public static final String FACTORY_TYPE_BARRACK = "barrack";
    public static final String FACTORY_TYPE_HAVEN = "haven";
    public static final String FACTORY_TYPE_AIRPORT = "airport";
    public static final String FACTORY_TYPE_FACTORY = "factory";
    public static final String FACTORY_TYPE_TANK_FACTORY = "tankFactory";
    public static final String[] ALL_FACTORY_TYPES = {
            FACTORY_TYPE_AIRPORT, FACTORY_TYPE_BARRACK,
            FACTORY_TYPE_HAVEN, FACTORY_TYPE_TANK_FACTORY};

    public static final String BUILDING_TYPE_CITY = "city";

    public static final String[] ALL_BUILDING_TYPES = {
            BUILDING_TYPE_CITY, FACTORY_TYPE_HAVEN,
            FACTORY_TYPE_BARRACK, FACTORY_TYPE_AIRPORT,
            FACTORY_TYPE_FACTORY};

    public static final String MOVE_TYPE_MOVE_UNIT = "move";
    public static final String MOVE_TYPE_ATTACK_UNIT = "attack";
    public static final String MOVE_TYPE_CREATE_UNIT = "create";
    public static final String MOVE_TYPE_COMBINE_UNIT = "combine";
    public static final String MOVE_TYPE_CAPTURE_BUILDING = "capture";

    public static final String ASSET_PATH = "./android/assets/";

    public static final String AUTHENTICATION_HEADER = "Authorization";

    public static final int CITY_INCOME = 100;
    public static final String TROOPS_TYPE_HERO = "hero";
    public static final int CHAT_ROW_LENGTH = 40;
    private static JSONObject prices;
    private static Color[] colors
            = {Color.BLUE, Color.RED, Color.GREEN, Color.YELLOW, Color.ORANGE, Color.BROWN, Color.VIOLET, Color.FOREST, Color.GOLD};
    private static String loggingTag = "Utils";

    public static int getDefenceFromType(final FieldType type)
    {

        switch (type)
        {
            case FIELD_TYPE_BUILDING:
                return 4;

            case FIELD_TYPE_BEACH:
                return 1;

            case FIELD_TYPE_FOREST:
                return 3;

            case FIELD_TYPE_GRASS:
                return 1;

            case FIELD_TYPE_MOUNTAIN:
                return 4;

            case FIELD_TYPE_STREET:
                return 0;

            case FIELD_TYPE_WATER:
                return 0;

            case FIELD_TYPE_HILL:
                return 2;

            default:
                return 0;
        }
    }

    public static int getPriceOf(String type)
    {

        if (prices == null)
        {
            FileHandle file = OwenFileHandler.getFile("Prices",
                                                      ".json");
            prices = new JSONObject(file.readString());
        }

        return prices.getInt(type);
    }

    public static int getCounterForUnitCreation(UnitInterface unit)
    {
        Gdx.app.log(loggingTag,
                    "getCounterForUnitCreation: " + unit.getType());
        String unitType = unit.getType();

        JSONObject unitStats = getUnitStats(unitType);

        return unitStats != null ? unitStats.getInt(Unit.FabricCounter) : 0;
    }

    private static boolean isValidJSONObject(String string)
    {

        try
        {
            new JSONObject(string);
        }
        catch (JSONException e)
        {
            return false;
        }

        return true;

    }

    public static HashSet<Field> possibleWay(UnitInterface unit)
    {

        Field root = unit.getField();
        int count = unit.getRange();
        HashSet<FieldType> possibleFieldTypes = unit.getPossibleFieldTypes();

        HashSet<Field> way = new HashSet<>();
        way.add(unit.getField());

        return new HashSet<>(getNextFields(way,
                                           root,
                                           count,
                                           possibleFieldTypes));

    }

    private static Set<Field> getNextFields(
            final HashSet<Field> way,
            final Field field,
            int count,
            final HashSet<FieldType> possibleFieldTypes)
    {

        final HashSet<Field> possibleFields = new HashSet<>();
        way.add(field);
        if (count > 0)
        {
            if (possibleFieldTypes.contains(field.getType()))
            {
                possibleFields.add(field);
                count--;
                Set<Field> temp = new HashSet<>();

                for (Field neighbor : field.getNeighbors())
                {
                    if (neighbor != null)
                    {
                        temp = getNextFields(way,
                                             neighbor,
                                             count,
                                             possibleFieldTypes);
                    }
                    possibleFields.addAll(temp);
                }
                return possibleFields;
            }
            return Collections.emptySet();

        }
        else if (count == 0)
        {
            possibleFields.add(field);
            return possibleFields;
        }
        return Collections.emptySet();
    }

    private static JSONObject getUnitStats(String className)
    {

        Gdx.app.log(loggingTag,
                    "getUnitStats: " + className);
        FileHandle handle = OwenFileHandler.getFile(className,
                                                    ".json");

        String string = handle.readString();

        if (!isValidJSONObject(string))
        {
            return null;
        }

        return new JSONObject(string);

    }

    public static HashSet<Field> findWay(
            UnitInterface unit,
            Field moveTo)
    {
        Gdx.app.log(loggingTag,
                    "findWay " + unit + ", " + moveTo);
        HashSet<Field> way;

        HashSet<FieldType> possibleFieldTypes = unit.getPossibleFieldTypes();
        Field from = unit.getField();
        HashSet<Field> goingWay = new HashSet<>();
        way = findNextWaypoint(goingWay,
                               from,
                               moveTo,
                               possibleFieldTypes,
                               unit.getRange() + 1);
        if (way != null)
        {
            Gdx.app.log(loggingTag,
                        "final Way length: " + way.size());
        }
        else
        {
            Gdx.app.log(loggingTag,
                        "way == null");
        }
        return way;
    }

    private static HashSet<Field> findNextWaypoint(
            final HashSet<Field> goingWay,
            final Field from,
            final Field to,
            final HashSet<FieldType> possibleFieldTypes,
            int range)
    {
        Gdx.app.log(loggingTag,
                    "findNextWaypoin " + goingWay.size());
        goingWay.add(from);
        range--;
        HashSet<Field> temp = new HashSet<>();
        if (from != to)
        {
            Gdx.app.log(loggingTag,
                        "from != to");
            for (Field field :
                    from.getNeighbors())
            {
                Gdx.app.log(loggingTag,
                            "next Neighbor");
                if (field != null
                        && possibleFieldTypes.contains(field.getType())
                        && range != 0)
                {
                    Gdx.app.log(loggingTag,
                                "possible next waypoint");
                    HashSet<Field> temp1 = findNextWaypoint(goingWay,
                                                            field,
                                                            to,
                                                            possibleFieldTypes,
                                                            range);
                    //find a possible way
                    if (temp1 != null)
                    {
                        Gdx.app.log(loggingTag,
                                    "new Way != null");
                        //choose the shorter one
                        if (temp.size() >= temp1.size())
                        {
                            Gdx.app.log(loggingTag,
                                        "shorter");
                            temp = temp1;
                        }
                        else if (temp.size() == 0)
                        {
                            Gdx.app.log(loggingTag,
                                        "longer");
                            temp = temp1;
                        }
                    }
                }
            }


            //find no way
            if (temp.size() == 0)
            {
                Gdx.app.log(loggingTag,
                            "temp way.size == 0");
                return null;
            }

        }
        Gdx.app.log(loggingTag,
                    "from == to");
        temp.add(from);
        return temp;

    }

    public static HashMap<String, Field> getMap(Map map)
    {

        HashMap<String, Field> map1 = new HashMap<>();

        List<Field> set = map.getFields();

        for (Field field :
                set)
        {
            map1.put(field.getFieldID(),
                     field);
        }

        return map1;
    }

    public static HashSet<Field> getStrafeableFields(UnitInterface unit)
    {
        int minRange = unit.getMinFireRange();
        int maxRange = unit.getMaxFireRange();
        Field root = unit.getField();

        HashSet<Field> innerSpace = possibleFields(root,
                                                   minRange);
        HashSet<Field> outerSpace = possibleFields(root,
                                                   maxRange);

        Gdx.app.log(loggingTag,
                    "InnerSpace: " + innerSpace.size());
        Gdx.app.log(loggingTag,
                    "OuterSpace: " + outerSpace.size());

        HashSet<Field> fields = new HashSet<>();
        for (Field field :
                outerSpace)
        {
            if (!innerSpace.contains(field))
            {
                Gdx.app.log(loggingTag,
                            field + " contains inner and outer.");
                fields.add(field);
            }
        }
        Gdx.app.log(loggingTag,
                    "Space: " + fields.size());
        return fields;
    }

    private static HashSet<Field> possibleFields(
            Field root,
            int range)
    {
        HashSet<FieldType> possibleFieldTypes = new HashSet<>(Arrays.asList(FieldType.values()));

        HashSet<Field> possibleFields = new HashSet<>();

        HashSet<Field> way = new HashSet<>();
        way.add(root);
        possibleFields.addAll(getNextFields(way,
                                            root,
                                            range,
                                            possibleFieldTypes));

        return possibleFields;
    }

    public static boolean isBuilding(String type)
    {

        for (String string : ALL_BUILDING_TYPES)
            if (string.equals(type))
            {
                return true;
            }

        return false;
    }

    public static boolean isFactory(String type)
    {

        for (String string : ALL_FACTORY_TYPES)
            if (string.equals(type))
            {
                return true;
            }

        return false;
    }

    public static boolean isUnit(String type)
    {
        for (String string : ALL_UNIT_TYPES)
            if (string.equals(type))
            {
                return true;
            }
        return false;
    }

    public static Color getColor(int i)
    {
        return colors[i];
    }
}

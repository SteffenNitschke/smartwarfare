package de.core.util;

import lombok.Getter;
import de.core.network.messages.news.TurnRequest;
import de.core.network.messages.news.turnMessages.*;
import de.core.model.Field;
import de.core.model.Game;
import de.core.model.Player;
import de.core.model.buildungs.Factory;
import de.core.model.units.UnitInterface;
import de.core.model.units.UnitBuilder;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

public class TurnPerformer
{
    private String tag = getClass().getSimpleName();

    private HashMap<String, HashSet<UnitInterface>> allUnitsOf = new HashMap<>();
    private HashMap<String, LinkedList<TurnRequest>> turnsOfRunningGames = new HashMap<>();
    @Getter
    private boolean perform = false;

    public void addTurnsOfGame(
            final String gameId,
            final TurnRequest turn)
    {
        if (turnsOfRunningGames.containsKey(gameId))
        {
            turnsOfRunningGames.get(gameId).addLast(turn);
        }
        else
        {
            final LinkedList<TurnRequest> turns = new LinkedList<>();
            turns.addLast(turn);
            turnsOfRunningGames.put(gameId,
                                    turns);
        }
    }

    public void performTurnOfGame(final Game game)
    {
        perform = true;
        if (turnsOfRunningGames.get(game.getGameID()) != null)
        {
            for (TurnRequest message : turnsOfRunningGames.get(game.getGameID()))
            {
                performTurnMessage(game,
                                   message);
            }
        }
        perform = false;
    }

    private void performTurnMessage(
            final Game game,
            final TurnRequest turnRequest)
    {
        System.out.println(tag + " performTurnMessage");
        //findPlayer
        final Player currentPlayer = getPlayer(game,
                                               turnRequest.getPlayer().getId().getIdentifier());

        for (TurnMessageInterface message : turnRequest.getMoves())
        {

            switch (message.getType())
            {
                case "move":
                    performMove(game,
                                currentPlayer,
                                (MoveMessage) message);
                    break;
                case "attack":
                    performAttack(game,
                                  currentPlayer,
                                  (AttackMessage) message);
                    break;
                case "capture":
                    performCapture(game,
                                   currentPlayer,
                                   (CaptureMessage) message);
                    break;
                case "combine":
                    performCombine(currentPlayer,
                                   (CombineMessage) message);
                    break;
                case "create":
                    performCreate(game,
                                  currentPlayer,
                                  (CreateMessage) message);
                    break;
            }
        }

        game.setCurrentPlayer(game.getCurrentPlayer().getNext());
        this.allUnitsOf = new HashMap<>();

    }

    private void performCreate(
            final Game game,
            final Player currentPlayer,
            final CreateMessage message)
    {
        System.out.println(tag + " performCreate");
        final Field field = getField(game,
                                     message.getFieldID());

        assert field != null;

        final UnitInterface unitInterface =
                UnitBuilder.build(message.getType());
        unitInterface.setPlayer(currentPlayer);

        ((Factory) field.getBuilding()).orderUnit(unitInterface);
    }

    private void performCombine(
            final Player currentPlayer,
            final CombineMessage message)
    {
        System.out.println(tag + " performCombine");
        final UnitInterface firstUnit = getUnitOf(currentPlayer,
                                                  message.getUnitID());
        final UnitInterface secondUnit = getUnitOf(currentPlayer,
                                                   message.getSecondUnitID());

        assert firstUnit != null;
        firstUnit.combine(secondUnit);
    }

    private void performCapture(
            Game game,
            Player currentPlayer,
            CaptureMessage message)
    {
        System.out.println(tag + " performCapture");
        Field field = getField(game,
                               message.getFieldID());
        UnitInterface unit = getUnitOf(currentPlayer,
                                       message.getUnitID());

        assert field != null;
        field.getBuilding().capture(unit);
    }

    private void performAttack(
            Game game,
            Player currentPlayer,
            AttackMessage message)
    {
        System.out.println(tag + " performAttack");
        Player enemyPlayer = getPlayer(game,
                                       message.getEnemyPlayer());
        UnitInterface enemyUnit = getUnitOf(enemyPlayer,
                                            message.getEnemyUnit());
        UnitInterface owenUnit = getUnitOf(currentPlayer,
                                           message.getOwenUnitID());

        assert owenUnit != null;
        owenUnit.attack(enemyUnit,
                        message.getSeed());
    }

    private void performMove(
            Game game,
            Player currentPlayer,
            MoveMessage message)
    {
        System.out.println(tag + " performMove");
        Field currentField = getField(game,
                                      message.getFieldID());
        UnitInterface currentUnit = getUnitOf(currentPlayer,
                                              message.getUnitID());

        assert currentUnit != null;
        currentUnit.move(currentField);
    }

    private HashSet<UnitInterface> getAllUnitsOf(final Player player)
    {

        if (this.allUnitsOf.containsKey(player.getUserID()))
        {
            return this.allUnitsOf.get(player.getUserID());
        }
        else
        {
            HashSet<UnitInterface> allPlayerUnits = player.getHeroes();
            allPlayerUnits.addAll(player.getTroops());
            this.allUnitsOf.put(player.getUserID(),
                                allPlayerUnits);
            return allPlayerUnits;
        }
    }

    private UnitInterface getUnitOf(
            final Player player,
            final String unitId)
    {
        for (UnitInterface unit : getAllUnitsOf(player))
            if (unit.getUnitId().equals(unitId))
            {
                return unit;
            }
        return null;
    }

    private Player getPlayer(
            Game game,
            String playerId)
    {
        for (Player player : game.getPlayer())
            if (player.getUserID().equals(playerId))
            {
                return player;
            }
        return null;
    }

    private Field getField(
            Game game,
            String fieldId)
    {
        for (Field field : game.getMap().getFields())
            if (field.getFieldID().equals(fieldId))
            {
                return field;
            }
        return null;
    }
}

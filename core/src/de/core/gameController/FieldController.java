package de.core.gameController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.core.handler.OwenFileHandler;
import de.core.model.Field;
import de.core.model.Player;
import de.core.model.buildungs.Building;
import de.core.model.units.UnitInterface;
import de.core.util.FieldType;
import de.core.util.Utils;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Objects;

/**
 * Created by Gandail on 02.06.2017.
 */
public class FieldController extends InputListener implements PropertyChangeListener
{

    private final WorldController worldController;
    private Image fieldTexture;
    private Field field;
    private boolean marked = false;

    private String loggingTag = getClass().getSimpleName();

    FieldController(
            final Field field,
            final Image fieldTexture,
            final WorldController worldController)
    {
        this.field = field;
        this.fieldTexture = fieldTexture;
        this.worldController = worldController;
        field.getChange().addPropertyChangeListener(this);
        if (field.getBuilding() != null)
        {
            field.getBuilding().getChange().addPropertyChangeListener(this);
        }

        fieldTexture.addListener(this);

        viewPicture(false);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt)
    {
        Gdx.app.log(loggingTag,
                    "propertyChange: " + evt.getPropertyName());
        String eventName = evt.getPropertyName();
        UnitInterface unit = evt.getPropertyName()
                                .equals(Field.PROPERTY_NAME_UNIT) ? (UnitInterface) evt
                .getNewValue() : null;

        if (eventName.equals(Building.PROPERTY_NAME_PLAYER))
        {

            fieldTexture.setColor(((Player) evt.getNewValue()).getColor());
        }
        if (eventName.equals(Field.PROPERTY_NAME_UNIT) &&
                unitHaveNoController(Objects.requireNonNull(unit)))
        {
            Gdx.app.log(loggingTag,
                        "haveNoController!");
            worldController.addNewUnit(unit);
        }
    }

    private boolean unitHaveNoController(final UnitInterface newValue)
    {
        Gdx.app.log(loggingTag,
                    "haveNoController?");
        boolean haveNoListener = true;
        if (newValue.getChange().getPropertyChangeListeners().length > 0)
        {
            haveNoListener = false;
        }
        Gdx.app.log(loggingTag,
                    "Length: " + newValue.getChange().getPropertyChangeListeners().length);

        return haveNoListener;
    }

    public void enter(
            final InputEvent event,
            final float x,
            final float y,
            final int pointer,
            final Actor fromActor)
    {
        this.worldController.getGameScreen().getScreenController().viewCurrentFieldInInfoBox(this);
        viewPicture(true);
    }

    public void exit(
            final InputEvent event,
            final float x,
            final float y,
            final int pointer,
            final Actor toActor)
    {
        this.worldController.getGameScreen().getScreenController().viewCurrentFieldInInfoBox(null);
        viewPicture(false);
    }

    public boolean touchDown(
            final InputEvent event,
            final float x,
            final float y,
            final int pointer,
            final int button)
    {
        if (this.field.getBuilding() != null)
        {
            //Field with Building
            if (this.field.getBuilding().getPlayer().getUserID().equals(
                    this.worldController.getCurrentPlayer().getUserID())
                    &&
                    Utils.isBuilding(this.field.getBuilding().getType()))
            {
                //Building belonged current Player
                this.worldController.showBuildingMenu(this.field.getBuilding());
            }
        }

        if (this.marked)
        {
//            Gdx.app.log(loggingTag, "FieldController field is marked");
            //Field is marked
            //Unit want to move to that Field
            //TODO not working when Field is in fireRange
//            Gdx.app.log(loggingTag, "Want to move to Field " + field.getX() + " " + field.getY());
            if (this.fieldTexture.getColor().equals(Color.BLUE) ||
                    this.fieldTexture.getColor().equals(Color.RED))
            {
                Gdx.app.log(loggingTag,
                            "right color");
                this.worldController.getUnitController().setCurrentWay(Utils.findWay(
                        this.worldController.getUnitController().getUnit(),
                        field
                ));
                this.worldController.move(this.field);
            }
        }
        else if (this.worldController.getUnitController() != null)
        {
//            Gdx.app.log(loggingTag, "FieldController field isn't marked");
            //want to deselect the Unit
            this.worldController.selectUnit(null);
            this.worldController.removeMarked();
        }

        return true;
    }

    private void viewPicture(final boolean hover)
    {
        final String type = !field.getType().equals(FieldType.FIELD_TYPE_BUILDING) ?
                field.getType().name() : field.getBuilding().getType();

        final String textureName = !hover ? type : type + "_hover";

        final Texture image = new Texture(
                OwenFileHandler.getFile(textureName,
                                        ".jpg"));

        Drawable drawable = new TextureRegionDrawable(new TextureRegion(image));
        fieldTexture.setDrawable(drawable);

    }

    void mark(
            boolean marked,
            String type)
    {
        boolean isMarked = this.marked;
        this.marked = marked;
        if (marked)
        {
            if (type.equals("move"))
            {
                //shall be marked
                this.fieldTexture.setColor(Color.BLUE);
            }
            else if (type.equals("attack"))
            {
                this.fieldTexture.setColor(Color.ORANGE);
            }
        }
        else if (isMarked)
        {
            //remove marked because it was marked (because of removeMarked-Method)
            this.fieldTexture.setColor(Color.WHITE);
        }

    }

    boolean isMarked()
    {
        return this.marked;
    }

    public Field getField()
    {
        return field;
    }
}

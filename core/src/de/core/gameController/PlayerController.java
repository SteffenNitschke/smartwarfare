package de.core.gameController;

import com.badlogic.gdx.scenes.scene2d.ui.Label;

import de.core.model.Player;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created by Gandail on 03.06.2017.
 */
public class PlayerController implements PropertyChangeListener
{

    private Player player;
    private Label money;

    public PlayerController(
            final Player player,
            final Label moneyLabel)
    {
        this.player = player;
        this.money = moneyLabel;
    }

    @Override
    public void propertyChange(final PropertyChangeEvent evt)
    {
        final String eventName = evt.getPropertyName();

        if (eventName.equals(Player.PROPERTY_NAME_MONEY))
        {
            money.setText(evt.getNewValue() + "");
        }

    }
}

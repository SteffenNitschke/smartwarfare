package de.core.gameController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.HashSet;

import de.core.MainGame;
import de.core.model.Field;
import de.core.model.Game;
import de.core.model.Player;
import de.core.model.buildungs.BuildingInterface;
import de.core.model.units.UnitInterface;
import de.core.screens.GameScreenInterface;
import de.core.screens.windows.FactoryWindowInterface;
import de.core.util.TurnEventListener;

/**
 * Created by Gandail on 03.06.2017.
 * <p>
 * create and control the map
 */
public class WorldController
{

    private final GameScreenInterface gameScreen;
    private final TurnEventListener eventListener;
    private final String loggingTag = getClass().getSimpleName();
    private final MainGame mainGame;
    private Game currentGame;
    private Stage stage;
    private UnitController unitController;
    private float fieldSize;

    public WorldController(
            final Game currentGame,
            final Stage stage,
            final GameScreenInterface gameScreen,
            final TurnEventListener listener,
            final MainGame mainGame)
    {
        this.currentGame = currentGame;
        this.stage = stage;
        this.gameScreen = gameScreen;
        this.eventListener = listener;
        this.mainGame = mainGame;

        int mapWidth = Gdx.graphics.getWidth();
        int mapHeight = Gdx.graphics.getHeight();
        int width = currentGame.getMap().getMaxX() + 1;
        int height = currentGame.getMap().getMaxY() + 1;

        fieldSize = ((float) mapWidth / width) > ((float) mapHeight / height)
                ? ((float) mapHeight / width) : ((float) mapHeight / height);
        gameScreen.setMapSize(width * fieldSize,
                              height * fieldSize);

        //create Map

        Field[][] map = new Field[width][height];
        Gdx.app.log(loggingTag,
                    "MapSize: " + width + ", " + height);

        for (Field field :
                currentGame.getMap().getFields())
        {
            map[field.getX()][field.getY()] = field;
        }

        showMap(stage,
                gameScreen,
                width,
                height,
                map);

        showUnits(currentGame,
                  stage,
                  gameScreen);

    }

    private void showMap(
            final Stage stage,
            final GameScreenInterface gameScreen,
            final int width,
            final int height,
            final Field[][] map)
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {

                Field field = map[i][j];

                if (field != null)
                {
                    Image image = new Image();
                    image.setSize(fieldSize,
                                  fieldSize);
                    image.setPosition(i * fieldSize,
                                      j * fieldSize);
                    image.setTouchable(Touchable.enabled);
                    new FieldController(field,
                                        image,
                                        this);
                    stage.addActor(image);
                    gameScreen.getHudProcessor().addIdleActor(image);
                }
            }
        }
    }

    private void showUnits(
            final Game currentGame,
            final Stage stage,
            final GameScreenInterface gameScreen)
    {
        for (Player player :
                currentGame.getPlayer())
        {
            for (UnitInterface unit :
                    player.getTroops())
            {
                Image image = new Image();
                image.setSize(fieldSize,
                              fieldSize);
                image.setPosition(unit.getField().getX() * fieldSize,
                                  unit.getField().getY() * fieldSize);
                image.setTouchable(Touchable.enabled);
                new UnitController(unit,
                                   image,
                                   fieldSize,
                                   this,
                                   eventListener);
                stage.addActor(image);
                gameScreen.getHudProcessor().addIdleActor(image);
            }
            for (UnitInterface hero :
                    player.getHeroes())
            {
                Image image = new Image();
                image.setSize(fieldSize,
                              fieldSize);
                image.setPosition(hero.getField().getX() * fieldSize,
                                  hero.getField().getY() * fieldSize);
                image.setTouchable(Touchable.enabled);
                new UnitController(hero,
                                   image,
                                   fieldSize,
                                   this,
                                   eventListener);
                stage.addActor(image);
            }
        }
    }

    void markFields(
            HashSet<Field> fields,
            String type)
    {
        Gdx.app.log(loggingTag,
                    "markFields()");
        for (Field field :
                fields)
        {
            FieldController controller = (FieldController) field.getChange()
                                                                .getPropertyChangeListeners()[0];
            controller.mark(true,
                            type);
        }
    }

    void removeMarked()
    {
        Gdx.app.log(loggingTag,
                    "Remove markets");
        if (this.unitController != null)
        {
            this.unitController.mark(false,
                                     "");
        }
        for (Field field :
                currentGame.getMap().getFields())
        {
            ((FieldController) field.getChange().getPropertyChangeListeners()[0]).mark(false,
                                                                                       "");
        }
    }

    void showBuildingMenu(final BuildingInterface building)
    {
        Gdx.app.log(loggingTag,
                    "showBuildingMenu");

        final FactoryWindowInterface fscreen = gameScreen.getNewFactoryWindow(gameScreen
                                                                                  .getHudProcessor(),
                                                                          building,
                                                                          eventListener,
                                                                          mainGame);
        fscreen.resize();
        gameScreen.setScreen(fscreen);
        gameScreen.getHudProcessor().idle(true);
    }

    /**
     * is called from FieldController, throw field to Unit
     *
     * @param field to move to
     */
    void move(Field field)
    {
        Gdx.app.log(loggingTag,
                    "move");
        if (this.unitController != null)
        {
            Gdx.app.log(loggingTag,
                        "unitController != null");
            UnitInterface unit = unitController.getUnit();
            unit.setField(field);
            this.unitController = null;
            removeMarked();
            Gdx.app.log(loggingTag,
                        "UnitController after move " + unitController);
        }
    }

    void selectUnit(final UnitController unitController)
    {
        Gdx.app.log(loggingTag,
                    "selectUnit()");
        if (unitController != null)
        {
            Gdx.app.log(loggingTag,
                        "New Controller");
            if (this.unitController != unitController && this.unitController != null)
            {
                removeMarked();
            }
            this.unitController = unitController;
        }
        else
        {
            Gdx.app.log(loggingTag,
                        "Remove Controller");
            if (this.unitController != null)
            {
                this.unitController.mark(false,
                                         "");
                this.unitController = null;
            }
        }
    }

    //getter setter

    UnitController getUnitController()
    {
        return this.unitController;
    }

    /**
     * remove the Actor of a Unit that will be destroyed
     *
     * @param actor remove an actor of a destroyed unit
     */
    public void removeActor(Actor actor)
    {
        Gdx.app.log(loggingTag,
                    "Remove Actor");
        this.stage.getActors().removeValue(actor,
                                           false);
    }

    /**
     * Add Actor when Unit will be created
     *
     * @param actor add the actor of a created Unit
     */
    public void addActor(Actor actor)
    {
        this.stage.getActors().add(actor);
    }

    GameScreenInterface getGameScreen()
    {
        return gameScreen;
    }

    Player getCurrentPlayer()
    {
        return currentGame.getCurrentPlayer();
    }

    void addNewUnit(UnitInterface unit)
    {

        Gdx.app.log(loggingTag,
                    "addNewUnit");
        Image image = new Image();
        image.setSize(fieldSize,
                      fieldSize);
        image.setPosition(unit.getField().getX() * fieldSize,
                          unit.getField().getY() * fieldSize);
        image.setTouchable(Touchable.enabled);
        image.setVisible(true);
        stage.addActor(image);

        image.toFront();
        gameScreen.getHudProcessor().addIdleActor(image);
        Gdx.app.log(loggingTag,
                    image.getZIndex() + "");
        new UnitController(unit,
                           image,
                           fieldSize,
                           this,
                           eventListener);
    }
}

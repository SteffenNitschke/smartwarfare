package de.core.gameController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashSet;

import de.core.handler.OwenFileHandler;
import de.core.model.Field;
import de.core.model.units.Unit;
import de.core.model.units.UnitInterface;
import de.core.util.TurnEventListener;
import de.core.util.Utils;

/**
 * Created by Gandail on 16.06.2017.
 */
public class UnitController extends InputListener implements PropertyChangeListener
{

    private final TurnEventListener listener;
    private UnitInterface unit;
    private Image unitTexture;
    private final float fieldSize;
    private boolean isSelected = false;
    private WorldController worldController;
    private HashSet<Field> currentWay; //only for moving

    private boolean isDead = false;
    private String loggingTag = getClass().getSimpleName();

    UnitController(
            final UnitInterface unit,
            final Image unitTexture,
            final float fieldSize,
            final WorldController worldController,
            final TurnEventListener listener)
    {
        this.unit = unit;
        this.unitTexture = unitTexture;
        this.fieldSize = fieldSize;
        this.worldController = worldController;
        this.listener = listener;
        unit.getChange().addPropertyChangeListener(this);
        unitTexture.addListener(this);

        hoverUnit(false);

        if (unit.isMoved() && unit.haveAttacked())
        {
            propertyChange(new PropertyChangeEvent(unit,
                                                   Unit.PROPERTY_NAME_HAVE_ATTACKED,
                                                   false,
                                                   true));
        }

    }

    //trigger

    @Override
    public void propertyChange(PropertyChangeEvent evt)
    {
        Gdx.app.log(loggingTag,
                    "Property Change Listener");
        String eventName = evt.getPropertyName();
        Gdx.app.log(loggingTag,
                    "EventName: " + eventName);
        if (eventName.equals(Unit.PROPERTY_NAME_FIELD) && evt.getNewValue() != null)
        {
            move(evt);
        }

        if (eventName.equals(Unit.PROPERTY_NAME_USER))
        {
            this.worldController.removeMarked();
            removeYou();
        }

        if (eventName.equals(Unit.PROPERTY_NAME_HAVE_ATTACKED) ||
                eventName.equals(Unit.PROPERTY_NAME_MOVED))
        {
            Gdx.app.log(loggingTag,
                        "move or attack");
            if (this.unit.isMoved() && this.unit.haveAttacked())
            {

                Gdx.app.log(loggingTag,
                            "both true");
                this.unitTexture.setColor(Color.GRAY);
            }
            else if (!this.unit.isMoved() && !this.unit.haveAttacked())
            {

                Gdx.app.log(loggingTag,
                            "both false");
                this.unitTexture.setColor(Color.WHITE);
            }
        }
    }

    public void enter(
            final InputEvent event,
            final float x,
            final float y,
            final int pointer,
            final Actor fromActor)
    {
        this.worldController.getGameScreen().getScreenController().viewCurrentUnitInInfoBox(this);
        hoverUnit(true);
    }

    public void exit(
            InputEvent event,
            float x,
            float y,
            int pointer,
            Actor toActor)
    {
        this.worldController.getGameScreen().getScreenController().viewCurrentUnitInInfoBox(null);
        if (!isDead)
        {
            hoverUnit(false);
        }
    }

    private void hoverUnit(final boolean hover)
    {
        final String textureName = !hover ? unit.getType() : unit.getType() + "_hover";

        final Texture image = new Texture(OwenFileHandler.getFile(textureName,
                                                                  ".jpg"));
        final Drawable drawable = new TextureRegionDrawable(new TextureRegion(image));

        unitTexture.setDrawable(drawable);
    }

    public boolean touchDown(
            final InputEvent event,
            final float x,
            final float y,
            final int pointer,
            final int button)
    {
        Gdx.app.log(loggingTag,
                    "Unit Controller touchDown");
        if (this.unit.isMoved() && this.unit.haveAttacked())
        {
            return true;
        }
        if (this.worldController.getUnitController() == this)
        {
            clickOnSameUnitAgain();
        }
        else if (this.worldController.getUnitController() != null &&
                getFieldController().isMarked())
        {
            //will be attacked
            Gdx.app.log(loggingTag,
                        "Can attack");
            this.worldController.getUnitController().attack(this);
            if (isDead)
            {
                return true;//need when unit is dead after attack
            }
        }

        boolean temp = false;
        if (!this.isSelected)
        {
            //not selected
            Gdx.app.log(loggingTag,
                        "isNotSelected");
            if (!this.unit.isMoved())
            {
                //not moved
                temp = true;
                mark(true,
                     "move");
                this.worldController.markFields(Utils.possibleWay(this.unit),
                                                "move");
                Gdx.app.log(loggingTag,
                            "Mark fields for movement");
                //now, could move
            }

            if (!this.unit.haveAttacked())
            {
                //not attacked
                temp = true;
                mark(true,
                     "attack");
                this.worldController.markFields(Utils.getStrafeableFields(this.unit),
                                                "attack");
                Gdx.app.log(loggingTag,
                            "Mark fields for attack");
                //now, could attack
            }
            Gdx.app.log(loggingTag,
                        "isSelected = " + temp);
            //false when Unit have attack and move
            this.isSelected = temp;

        }
        else
        {
            Gdx.app.log(loggingTag,
                        "is selected, remove all markets");
            //is selected
            //remove selection
            mark(false,
                 "");
            //this.worldController.selectUnit(null);
            this.worldController.removeMarked();
            this.isSelected = false;
        }

        return true;
    }

    private void clickOnSameUnitAgain()
    {
        if (!this.unit.haveAttacked() &&
                this.getFieldController().getField().getBuilding() != null)
        {
            //want to capture a Building
            Gdx.app.log(loggingTag,
                        "Wanna capture");
            this.getFieldController().getField().getBuilding().capture(this.unit);
            listener.handleCaptureMessage(unit,
                                          this.getFieldController().getField());
            this.worldController.removeMarked();
            this.getUnit().setHaveAttacked(true);
        }
        else
        {
            //want to remove the marked
            this.worldController.selectUnit(null);
            this.worldController.removeMarked();
        }
    }


    void mark(
            final boolean selected,
            final String type)
    {
        Gdx.app.log(loggingTag,
                    "UnitController: Mark()");

        if (!isDead)
        {
            return;
        }
        if (type.equals("move") && !unit.isMoved())
        {
            if (selected)
            {
                this.worldController.selectUnit(this);
                this.unitTexture.setColor(Color.YELLOW);
            }
            else
            {
                this.unitTexture.setColor(Color.WHITE);
                if (this.worldController.getUnitController() == this)
                {
                    this.worldController.selectUnit(null);
                }
            }
        }
        else if (type.equals("attack") && unit.haveAttacked())
        {
            if (selected)
            {
                this.worldController.selectUnit(this);
                this.unitTexture.setColor(Color.YELLOW);
            }
            else
            {
                this.unitTexture.setColor(Color.WHITE);
                if (this.worldController.getUnitController() == this)
                {
                    this.worldController.selectUnit(null);
                }
            }
        }
        else if (unit.isMoved() && unit.haveAttacked())
        {
            this.unitTexture.setColor(Color.WHITE);
            if (this.worldController.getUnitController() == this)
            {
                Gdx.app.log(loggingTag,
                            "unitController is the same like this");
            }
        }
    }

    /**
     * called from propertyChange-Method
     *
     * @param evt event the gameScreenController have to handle
     */
    private void move(PropertyChangeEvent evt)
    {
        Gdx.app.log(loggingTag,
                    "UnitController move()");
        Field from = (Field) evt.getOldValue();
        viewWay(currentWay,
                from);
        this.currentWay = null;
        mark(false,
             "");
        this.isSelected = false;
        listener.handleMoveMessage(this.unit,
                                   (Field) evt.getNewValue());
    }

    /**
     * called from move-Method
     *
     * @param way      the way the unit can go
     * @param oldField the startfield
     */
    private void viewWay(
            final HashSet<Field> way,
            final Field oldField)
    {
        int counter = 0;
        boolean isMovedToDestination = false;
        Field currentField = oldField;

        SequenceAction sequenceAction = new SequenceAction();

        HashSet<Field> goingWay = new HashSet<>();
        goingWay.add(currentField);

        while (!isMovedToDestination)
        {

            for (Field field :
                    way)
            {

                if (currentField.getNeighbors().contains(field) && !goingWay.contains(field))
                {

                    MoveToAction moveUnit = new MoveToAction();
                    moveUnit.setPosition((float) field.getX() * fieldSize,
                                         (float) field.getY() * fieldSize);
                    moveUnit.setDuration(0.2f);

                    sequenceAction.addAction(moveUnit);

                    currentField = field;
                    goingWay.add(field);
                }
            }
            counter++;
            if (way.size() == goingWay.size() || counter == 30)
            {
                isMovedToDestination = true;
            }
        }
        unitTexture.addAction(sequenceAction);

    }


    private void attack(UnitController enemy)
    {
        Gdx.app.log(loggingTag,
                    "UnitController say unit to attack");
        UnitInterface enemyUnit = enemy.getUnit();
        long seed = this.unit.attack(enemyUnit,
                                     null);
        listener.handleAttackMessage(unit.getPlayer().getUserID(),
                                     unit,
                                     enemyUnit,
                                     seed);
        this.unit.setHaveAttacked(true);
        this.unit.setIsMoved(true);
        this.worldController.removeMarked();

    }

    //getter setter

    public UnitInterface getUnit()
    {
        return unit;
    }

    void setCurrentWay(HashSet<Field> way)
    {
        this.currentWay = way;
    }

    private FieldController getFieldController()
    {

        PropertyChangeSupport support = this.unit.getField().getChange();

        return ((FieldController) support.getPropertyChangeListeners()[0]);
    }

    private void removeYou()
    {
        Gdx.app.log(loggingTag,
                    "RemoveYou UnitController");
        this.isDead = true;
        this.worldController.removeActor(unitTexture);
        this.unitTexture.remove();
        this.unitTexture = null;
        this.unit = null;
        this.worldController = null;

    }
}

package de.core.uiListeners;

import com.badlogic.gdx.Gdx;
import com.fasterxml.jackson.core.JsonProcessingException;

import de.core.MainGame;
import lombok.RequiredArgsConstructor;
import de.core.network.messages.PlayerMessage;
import de.core.network.messages.news.FriendAnswerRequest;
import de.core.network.messages.news.NewsDto;
import de.core.util.NewsType;

@RequiredArgsConstructor
public class NewsButtonListener
{
    private final MainGame game;
    protected final NewsDto message;
    protected final String loggingTag = getClass().getSimpleName();

    /**
     * @param message message that have to handle
     * @param button  0 -> confirmButton, 1 -> denyButton, 2 -> okButton
     */
    void handleNewsButton(
            final NewsDto message,
            final NewsButton button)
    {
        Gdx.app.log(loggingTag,
                    "handleNewsButton: " + button + ", " + message);
        switch (button)
        {
            case CONFIRM_BUTTON:
                handleNews(message,
                           true);
                break;
            case DENY_BUTTON:
                handleNews(message,
                           false);
                break;
            case READED_BUTTON:
                game.getConnection().getNewsClient()
                    .markNewsAsReaded(message.get_id().getIdentifier());
        }
    }


    private void handleNews(
            final NewsDto message,
            final boolean confirm)
    {
        final PlayerMessage player = new PlayerMessage(game.getUser().get_id(),
                                                       game.getUser().getName());
        switch (message.getType())
        {
            case FRIEND_INVITATION:
                final FriendAnswerRequest answer = new FriendAnswerRequest(
                        confirm,
                        message.getSender(),
                        player
                        );

                game.getNewsHandler().handleFriendAnswer(answer, message.get_id());
                break;
            case GAME_INVITATION:
                final NewsDto gameAnswer = new NewsDto(
                        null,
                        game.getUser().get_id(),
                        message.getGameId(),
                        player,
                        confirm,
                        true,
                        NewsType.GAME_ANSWER
                );

                game.getNewsHandler().handleGameAnswer(gameAnswer, message.get_id());
                break;
        }
    }
}

package de.core.uiListeners;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.fasterxml.jackson.core.JsonProcessingException;

import de.core.MainGame;
import de.core.network.messages.news.NewsDto;

public class NewsConfirmListener extends NewsButtonListener implements EventListener
{
    public NewsConfirmListener(
            final MainGame game,
            final NewsDto message)
    {
        super(game,
              message);
    }

    @Override
    public boolean handle(Event event)
    {
        if (!event.toString().equals("mouseMoved") && !event.toString().equals("touchDown"))
        {
            Gdx.app.log("NewsConfirmButton",
                        "Event: " + event.toString());
            handleNewsButton(message,
                             NewsButton.CONFIRM_BUTTON);
        }
        return false;
    }
}

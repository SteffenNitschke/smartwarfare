package de.core;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

import de.core.handler.ChatHandler;
import de.core.handler.GameHandler;
import de.core.handler.MapHandler;
import de.core.handler.NewsHandler;
import de.core.handler.NotificationHandler;
import de.core.handler.ScreenHandler;
import de.core.model.Id;
import de.core.model.User;
import de.core.network.Network;
import de.core.network.clients.AccountClient;
import de.core.network.clients.DrawClient;
import de.core.network.clients.FriendInvitationClient;
import de.core.network.clients.GameClient;
import de.core.network.clients.GameInvitationClient;
import de.core.network.clients.MapClient;
import de.core.network.clients.MapImageClient;
import de.core.network.clients.NewsClient;
import de.core.network.clients.UserClient;
import de.core.network.listeners.UserNetworkListener;
import de.core.network.messages.news.NewsDto;
import de.core.network.socket.SocketConnection;
import de.core.screens.MainScreenInterface;
import de.core.uiController.NewsScreenController;
import de.core.util.TurnEventListener;
import de.core.util.TurnPerformer;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;


public class MainGame extends Game
{
    private String loggingTag = getClass().getSimpleName();

    private BitmapFont font;

    @Getter @Setter
    private User user;

    @Getter @Setter
    private de.core.model.Game newGame;
    @Getter
    private TurnEventListener eventListener;
    @Getter
    private TurnPerformer performer = new TurnPerformer();

    @Getter
    private Network connection;

    @Getter
    private NewsHandler newsHandler;

    @Getter
    private ChatHandler chatHandler;

    @Getter
    private MapHandler mapHandler;

    @Getter
    private ScreenHandler screenHandler = new ScreenHandler();

    @Setter
    private NewsScreenController newsScreenController;

    @Getter
    @Setter
    @Accessors(fluent = true, chain = false)
    private NotificationHandler notificationHandler;


    public void initializeUser(final User user)
    {
        eventListener = new TurnEventListener(user.get_id().getIdentifier(),
                                              user.getName());

        connection.getSocketConnection().connect(user.get_id().getIdentifier());

        this.user = user;

        connection.getNewsClient().getNews();
        connection.getUserClient().getUser(user.get_id().getIdentifier());
    }

    public void create()
    {
        initNetwork();

        this.mapHandler = new MapHandler(this);
        chatHandler = new ChatHandler();

        registrateObservers();

        Gdx.app.setLogLevel(Application.LOG_DEBUG);

        Gdx.app.log(loggingTag,
                    "Start Desktop");

        font = new BitmapFont();

        final MainScreenInterface mainScreenInterface = this.screenHandler.getMainScreen();
        this.screenHandler.addScreen(mainScreenInterface);
        this.setScreen(mainScreenInterface);
    }

    private void registrateObservers()
    {
        Gdx.app.log(loggingTag, "registrateObservers");
        newsHandler = new NewsHandler(this);
        connection.getNewsClient().addObservers(newsHandler);
        connection.getSocketConnection().addObserver(newsHandler);

        connection.getUserClient().addObservers(new UserNetworkListener(this));
        connection.getGameClient().addObservers(new GameHandler(this));
    }

    private void initNetwork()
    {
        this.connection = new Network(
                new AccountClient(this),
                new UserClient(this),
                new DrawClient(this),
                new MapClient(this),
                new GameClient(this),
                new GameInvitationClient(this),
                new FriendInvitationClient(this),
                new NewsClient(this),
                new SocketConnection(),
                new MapImageClient(this));
    }

    public void render()
    {
        super.render(); //important!
    }

    public void dispose()
    {
        font.dispose();
    }

    public void restartGame(final String gameID)
    {
        Gdx.app.log(loggingTag,
                    "restartGame " + gameID);
        //TODO just for szenarios
        //read the Information from the Storage (json-Object) and
        //set a new GameScreen with the gameId
    }

    public void viewNews(final NewsDto message)
    {
        newsScreenController.updateNewsTable(message);
    }

    public void removeNews(final Id newsId)
    {
        newsScreenController.removeNewsFromTable(newsId);
    }
}

package test;

import com.badlogic.gdx.graphics.Color;
import de.core.model.Field;
import de.core.model.Game;
import de.core.model.Map;
import de.core.model.Player;
import de.core.model.units.UnitInterface;
import de.core.network.mapper.GameMapper;
import de.core.util.Utils;
import de.core.model.units.UnitBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static de.core.util.FieldType.FIELD_TYPE_BEACH;

public class GameMapperTest {

    @Test
    public void shouldMapStringToGame() throws IOException {

        GameMapper mapper = new GameMapper();
        Game game = mapper.mapDBGameStringToGame(TEST_GAME);

        Assert.assertEquals("4024b58a-c4b6-4715-8fb8-26cac25ebdaf", game.getGameID());
        Assert.assertEquals("INVITATION", game.getStatus().name());
        Assert.assertEquals("LAST_MAN_STANDING", game.getGameType().name());
        Assert.assertEquals(4, game.getPlayer().size());
        Assert.assertEquals(6, game.getBuildings().size());
        Assert.assertEquals(1, game.getRound());

        Player currentPlayer = new Player("a636c149-9719-432e-9de5-a1fb81c7bb5f");
        currentPlayer.setColor(Color.BLUE);
        Player next = new Player("9e53d17f-7cf1-4364-acbf-faa4b3d9b411");
        Player prev = new Player("21df1e12-8973-431f-849b-886f83116e9b");
        currentPlayer.setNext(next);
        currentPlayer.setPrev(prev);
        currentPlayer.setMoney(0);
        currentPlayer.setName("Steffen");

        Assert.assertEquals(currentPlayer.getUserID(), game.getCurrentPlayer().getUserID());
        Assert.assertEquals(currentPlayer.getColor(), game.getCurrentPlayer().getColor());
        Assert.assertEquals(currentPlayer.getMoney(), game.getCurrentPlayer().getMoney());
        Assert.assertEquals(currentPlayer.getName(), game.getCurrentPlayer().getName());
        Assert.assertEquals(currentPlayer.getNext().getUserID(), game.getCurrentPlayer().getNext().getUserID());
        Assert.assertEquals(currentPlayer.getPrev().getUserID(), game.getCurrentPlayer().getPrev().getUserID());

        Map map = game.getMap();
        Assert.assertEquals("aacbb69e-6efc-4841-9392-58b600d38404", map.get_id());
        Assert.assertEquals(100, map.getFields().size());
        Assert.assertEquals(game, map.getGame());
        Assert.assertEquals("Steffen", map.getEditorName());
        Assert.assertEquals("TestMap0", map.getName());
        Assert.assertEquals(2, map.getNumberOfPlayers());
    }

    @Test
    public void shouldMapAMapToString() {

        Map map = new Map("TestMap");
        map.set_id("1234");
        map.setMaxY(5);
        map.setMaxX(5);
        map.setNumberOfPlayers(3);
        map.setEditorName("Ich");

        List<Field> fields = new ArrayList<>();
        for(int i = 0; i < 5; i++) {
            for(int j = 0; j < 5; j++) {
                Field field = new Field();
                field.setMap(map);
                field.setDefence(1);
                field.setType(FIELD_TYPE_BEACH);
                field.setFieldID(i + "" + j);
                field.setX(i);
                field.setY(j);
                if(new Random().nextBoolean()) {
                    UnitInterface unit = UnitBuilder.build(Utils.TROOPS_TYPE_INFANTRY);
                    unit.setUnitId("Player0");
                    field.setUnit(unit);
                }

                fields.add(field);
            }
        }
        //neighbors not important for mapping

        map.setFields(fields);

        GameMapper gameMapper = new GameMapper();

        JSONObject result = new JSONObject(gameMapper.writeMapAsString(null, map));

        JSONArray fieldsArray = result.getJSONArray("fields");
        int unitCounter = 0;
        for (int i = 0; i < fieldsArray.length(); i++) {

            JSONObject field = fieldsArray.getJSONObject(i);
            if(field.has("unit") && field.get("unit") != null) {
                unitCounter++;
            }
        }

        Assert.assertEquals(unitCounter, result.getJSONArray("units").length());

        System.out.println(result);
    }

    private final String TEST_GAME = "{\n" +
            "  \"status\": \"INVITATION\",\n" +
            "  \"_id\": \"4024b58a-c4b6-4715-8fb8-26cac25ebdaf\",\n" +
            "  \"gameType\": \"LAST_MAN_STANDING\",\n" +
            "  \"players\": [\n" +
            "    {\n" +
            "      \"userId\": \"a636c149-9719-432e-9de5-a1fb81c7bb5f\",\n" +
            "      \"color\": {\n" +
            "        \"r\": 0.0,\n" +
            "        \"g\": 0.0,\n" +
            "        \"b\": 1.0,\n" +
            "        \"a\": 1.0\n" +
            "      },\n" +
            "      \"nextPlayerId\": \"9e53d17f-7cf1-4364-acbf-faa4b3d9b411\",\n" +
            "      \"prevPlayerId\": \"21df1e12-8973-431f-849b-886f83116e9b\",\n" +
            "      \"wannaPlayTheGame\": false,\n" +
            "      \"name\":\"Steffen\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"userId\": \"9e53d17f-7cf1-4364-acbf-faa4b3d9b411\",\n" +
            "      \"color\": {\n" +
            "        \"r\": 1.0,\n" +
            "        \"g\": 0.0,\n" +
            "        \"b\": 0.0,\n" +
            "        \"a\": 1.0\n" +
            "      },\n" +
            "      \"nextPlayerId\": \"0e80e65f-43ad-41cf-a63a-2739218a9bb9\",\n" +
            "      \"prevPlayerId\": \"a636c149-9719-432e-9de5-a1fb81c7bb5f\",\n" +
            "      \"wannaPlayTheGame\": false,\n" +
            "      \"name\":\"Steffen\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"userId\": \"0e80e65f-43ad-41cf-a63a-2739218a9bb9\",\n" +
            "      \"color\": {\n" +
            "        \"r\": 0.0,\n" +
            "        \"g\": 1.0,\n" +
            "        \"b\": 0.0,\n" +
            "        \"a\": 1.0\n" +
            "      },\n" +
            "      \"nextPlayerId\": \"21df1e12-8973-431f-849b-886f83116e9b\",\n" +
            "      \"prevPlayerId\": \"9e53d17f-7cf1-4364-acbf-faa4b3d9b411\",\n" +
            "      \"wannaPlayTheGame\": false,\n" +
            "      \"name\":\"Steffen\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"userId\": \"21df1e12-8973-431f-849b-886f83116e9b\",\n" +
            "      \"color\": {\n" +
            "        \"r\": 1.0,\n" +
            "        \"g\": 1.0,\n" +
            "        \"b\": 0.0,\n" +
            "        \"a\": 1.0\n" +
            "      },\n" +
            "      \"nextPlayerId\": \"a636c149-9719-432e-9de5-a1fb81c7bb5f\",\n" +
            "      \"prevPlayerId\": \"0e80e65f-43ad-41cf-a63a-2739218a9bb9\",\n" +
            "      \"wannaPlayTheGame\": false,\n" +
            "      \"name\":\"Steffen\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"playerUnits\": null,\n" +
            "  \"playerBuildings\": null,\n" +
            "  \"map\": {\n" +
            "    \"_id\": \"aacbb69e-6efc-4841-9392-58b600d38404\",\n" +
            "    \"game\": null,\n" +
            "    \"fields\": [\n" +
            "      {\n" +
            "        \"type\": \"grass\",\n" +
            "        \"x\": 0,\n" +
            "        \"y\": 0,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"00\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"grass\",\n" +
            "        \"x\": 0,\n" +
            "        \"y\": 1,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"01\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 0,\n" +
            "        \"y\": 2,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"02\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 0,\n" +
            "        \"y\": 3,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"03\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"street\",\n" +
            "        \"x\": 0,\n" +
            "        \"y\": 4,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"04\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"beach\",\n" +
            "        \"x\": 0,\n" +
            "        \"y\": 5,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"05\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"hill\",\n" +
            "        \"x\": 0,\n" +
            "        \"y\": 6,\n" +
            "        \"defence\": 2,\n" +
            "        \"fieldID\": \"06\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"grass\",\n" +
            "        \"x\": 0,\n" +
            "        \"y\": 7,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"07\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 0,\n" +
            "        \"y\": 8,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"08\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 0,\n" +
            "        \"y\": 9,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"09\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 1,\n" +
            "        \"y\": 0,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"10\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"water\",\n" +
            "        \"x\": 1,\n" +
            "        \"y\": 1,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"11\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 1,\n" +
            "        \"y\": 2,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"12\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"water\",\n" +
            "        \"x\": 1,\n" +
            "        \"y\": 3,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"13\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"street\",\n" +
            "        \"x\": 1,\n" +
            "        \"y\": 4,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"14\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"hill\",\n" +
            "        \"x\": 1,\n" +
            "        \"y\": 5,\n" +
            "        \"defence\": 2,\n" +
            "        \"fieldID\": \"15\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"hill\",\n" +
            "        \"x\": 1,\n" +
            "        \"y\": 6,\n" +
            "        \"defence\": 2,\n" +
            "        \"fieldID\": \"16\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 1,\n" +
            "        \"y\": 7,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"17\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"street\",\n" +
            "        \"x\": 1,\n" +
            "        \"y\": 8,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"18\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"street\",\n" +
            "        \"x\": 1,\n" +
            "        \"y\": 9,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"19\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"grass\",\n" +
            "        \"x\": 2,\n" +
            "        \"y\": 0,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"20\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"beach\",\n" +
            "        \"x\": 2,\n" +
            "        \"y\": 1,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"21\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"hill\",\n" +
            "        \"x\": 2,\n" +
            "        \"y\": 2,\n" +
            "        \"defence\": 2,\n" +
            "        \"fieldID\": \"22\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"grass\",\n" +
            "        \"x\": 2,\n" +
            "        \"y\": 3,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"23\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"water\",\n" +
            "        \"x\": 2,\n" +
            "        \"y\": 4,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"24\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 2,\n" +
            "        \"y\": 5,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"25\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 2,\n" +
            "        \"y\": 6,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"26\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"grass\",\n" +
            "        \"x\": 2,\n" +
            "        \"y\": 7,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"27\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"hill\",\n" +
            "        \"x\": 2,\n" +
            "        \"y\": 8,\n" +
            "        \"defence\": 2,\n" +
            "        \"fieldID\": \"28\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"water\",\n" +
            "        \"x\": 2,\n" +
            "        \"y\": 9,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"29\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 3,\n" +
            "        \"y\": 0,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"30\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"street\",\n" +
            "        \"x\": 3,\n" +
            "        \"y\": 1,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"31\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"grass\",\n" +
            "        \"x\": 3,\n" +
            "        \"y\": 2,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"32\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 3,\n" +
            "        \"y\": 3,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"33\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"water\",\n" +
            "        \"x\": 3,\n" +
            "        \"y\": 4,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"34\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 3,\n" +
            "        \"y\": 5,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"35\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"water\",\n" +
            "        \"x\": 3,\n" +
            "        \"y\": 6,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"36\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"water\",\n" +
            "        \"x\": 3,\n" +
            "        \"y\": 7,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"37\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"beach\",\n" +
            "        \"x\": 3,\n" +
            "        \"y\": 8,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"38\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 3,\n" +
            "        \"y\": 9,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"39\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"street\",\n" +
            "        \"x\": 4,\n" +
            "        \"y\": 0,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"40\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"building\",\n" +
            "        \"x\": 4,\n" +
            "        \"y\": 1,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"41\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": {\n" +
            "          \"user\": null,\n" +
            "          \"type\": \"city\",\n" +
            "          \"captureValue\": 20,\n" +
            "          \"wannerCapture\": null\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 4,\n" +
            "        \"y\": 2,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"42\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"water\",\n" +
            "        \"x\": 4,\n" +
            "        \"y\": 3,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"43\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 4,\n" +
            "        \"y\": 4,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"44\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 4,\n" +
            "        \"y\": 5,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"45\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"hill\",\n" +
            "        \"x\": 4,\n" +
            "        \"y\": 6,\n" +
            "        \"defence\": 2,\n" +
            "        \"fieldID\": \"46\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"street\",\n" +
            "        \"x\": 4,\n" +
            "        \"y\": 7,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"47\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 4,\n" +
            "        \"y\": 8,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"48\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 4,\n" +
            "        \"y\": 9,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"49\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"grass\",\n" +
            "        \"x\": 5,\n" +
            "        \"y\": 0,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"50\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 5,\n" +
            "        \"y\": 1,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"51\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 5,\n" +
            "        \"y\": 2,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"52\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 5,\n" +
            "        \"y\": 3,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"53\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"grass\",\n" +
            "        \"x\": 5,\n" +
            "        \"y\": 4,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"54\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"hill\",\n" +
            "        \"x\": 5,\n" +
            "        \"y\": 5,\n" +
            "        \"defence\": 2,\n" +
            "        \"fieldID\": \"55\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"water\",\n" +
            "        \"x\": 5,\n" +
            "        \"y\": 6,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"56\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"grass\",\n" +
            "        \"x\": 5,\n" +
            "        \"y\": 7,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"57\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"hill\",\n" +
            "        \"x\": 5,\n" +
            "        \"y\": 8,\n" +
            "        \"defence\": 2,\n" +
            "        \"fieldID\": \"58\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"hill\",\n" +
            "        \"x\": 5,\n" +
            "        \"y\": 9,\n" +
            "        \"defence\": 2,\n" +
            "        \"fieldID\": \"59\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"grass\",\n" +
            "        \"x\": 6,\n" +
            "        \"y\": 0,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"60\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"beach\",\n" +
            "        \"x\": 6,\n" +
            "        \"y\": 1,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"61\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"building\",\n" +
            "        \"x\": 6,\n" +
            "        \"y\": 2,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"62\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": {\n" +
            "          \"user\": null,\n" +
            "          \"type\": \"city\",\n" +
            "          \"captureValue\": 20,\n" +
            "          \"wannerCapture\": null\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 6,\n" +
            "        \"y\": 3,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"63\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 6,\n" +
            "        \"y\": 4,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"64\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"beach\",\n" +
            "        \"x\": 6,\n" +
            "        \"y\": 5,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"65\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 6,\n" +
            "        \"y\": 6,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"66\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 6,\n" +
            "        \"y\": 7,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"67\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 6,\n" +
            "        \"y\": 8,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"68\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"beach\",\n" +
            "        \"x\": 6,\n" +
            "        \"y\": 9,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"69\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"grass\",\n" +
            "        \"x\": 7,\n" +
            "        \"y\": 0,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"70\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 7,\n" +
            "        \"y\": 1,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"71\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"street\",\n" +
            "        \"x\": 7,\n" +
            "        \"y\": 2,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"72\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"water\",\n" +
            "        \"x\": 7,\n" +
            "        \"y\": 3,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"73\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 7,\n" +
            "        \"y\": 4,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"74\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 7,\n" +
            "        \"y\": 5,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"75\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 7,\n" +
            "        \"y\": 6,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"76\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 7,\n" +
            "        \"y\": 7,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"77\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 7,\n" +
            "        \"y\": 8,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"78\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"hill\",\n" +
            "        \"x\": 7,\n" +
            "        \"y\": 9,\n" +
            "        \"defence\": 2,\n" +
            "        \"fieldID\": \"79\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"building\",\n" +
            "        \"x\": 8,\n" +
            "        \"y\": 0,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"80\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": {\n" +
            "          \"user\": null,\n" +
            "          \"type\": \"city\",\n" +
            "          \"captureValue\": 20,\n" +
            "          \"wannerCapture\": null\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"street\",\n" +
            "        \"x\": 8,\n" +
            "        \"y\": 1,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"81\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"water\",\n" +
            "        \"x\": 8,\n" +
            "        \"y\": 2,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"82\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"water\",\n" +
            "        \"x\": 8,\n" +
            "        \"y\": 3,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"83\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"water\",\n" +
            "        \"x\": 8,\n" +
            "        \"y\": 4,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"84\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"street\",\n" +
            "        \"x\": 8,\n" +
            "        \"y\": 5,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"85\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"building\",\n" +
            "        \"x\": 8,\n" +
            "        \"y\": 6,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"86\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": {\n" +
            "          \"user\": null,\n" +
            "          \"type\": \"city\",\n" +
            "          \"captureValue\": 20,\n" +
            "          \"wannerCapture\": null\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"building\",\n" +
            "        \"x\": 8,\n" +
            "        \"y\": 7,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"87\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": {\n" +
            "          \"user\": null,\n" +
            "          \"type\": \"city\",\n" +
            "          \"captureValue\": 20,\n" +
            "          \"wannerCapture\": null\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"water\",\n" +
            "        \"x\": 8,\n" +
            "        \"y\": 8,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"88\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"mountain\",\n" +
            "        \"x\": 8,\n" +
            "        \"y\": 9,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"89\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 9,\n" +
            "        \"y\": 0,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"90\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"hill\",\n" +
            "        \"x\": 9,\n" +
            "        \"y\": 1,\n" +
            "        \"defence\": 2,\n" +
            "        \"fieldID\": \"91\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"hill\",\n" +
            "        \"x\": 9,\n" +
            "        \"y\": 2,\n" +
            "        \"defence\": 2,\n" +
            "        \"fieldID\": \"92\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"street\",\n" +
            "        \"x\": 9,\n" +
            "        \"y\": 3,\n" +
            "        \"defence\": 0,\n" +
            "        \"fieldID\": \"93\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"grass\",\n" +
            "        \"x\": 9,\n" +
            "        \"y\": 4,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"94\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"grass\",\n" +
            "        \"x\": 9,\n" +
            "        \"y\": 5,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"95\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"beach\",\n" +
            "        \"x\": 9,\n" +
            "        \"y\": 6,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"96\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"forest\",\n" +
            "        \"x\": 9,\n" +
            "        \"y\": 7,\n" +
            "        \"defence\": 3,\n" +
            "        \"fieldID\": \"97\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"beach\",\n" +
            "        \"x\": 9,\n" +
            "        \"y\": 8,\n" +
            "        \"defence\": 1,\n" +
            "        \"fieldID\": \"98\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": null\n" +
            "      },\n" +
            "      {\n" +
            "        \"type\": \"building\",\n" +
            "        \"x\": 9,\n" +
            "        \"y\": 9,\n" +
            "        \"defence\": 4,\n" +
            "        \"fieldID\": \"99\",\n" +
            "        \"unit\": null,\n" +
            "        \"building\": {\n" +
            "          \"user\": null,\n" +
            "          \"type\": \"city\",\n" +
            "          \"captureValue\": 20,\n" +
            "          \"wannerCapture\": null\n" +
            "        }\n" +
            "      }\n" +
            "    ],\n" +
            "    \"name\": \"TestMap0\",\n" +
            "    \"maxY\": 10,\n" +
            "    \"maxX\": 10,\n" +
            "    \"editorName\": \"Steffen\",\n" +
            "    \"numberOfPlayers\": 2\n" +
            "  },\n" +
            "  \"round\": 1,\n" +
            "  \"currentPlayer\": \"a636c149-9719-432e-9de5-a1fb81c7bb5f\"," +
            "  \"chatId\": \"fcb751a0-7038-4e28-b657-bfce069aa798\"\n" +
            "}";

}

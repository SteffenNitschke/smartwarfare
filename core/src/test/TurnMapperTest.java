package test;

import de.core.model.Id;
import de.core.network.messages.PlayerMessage;
import de.core.network.messages.news.TurnRequest;
import de.core.network.messages.news.turnMessages.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class TurnMapperTest {

    @Test
    public void testWriteTurnAsString() throws IOException {

        TurnMapper mapper = new TurnMapper();

        PlayerMessage playerMessage = new PlayerMessage(new Id("1234"), "Bob");

        //init message
        TurnRequest message = new TurnRequest(playerMessage);

        AttackMessage attack = new AttackMessage();
        attack.setEnemyPlayer("enemy");
        attack.setEnemyUnit("eUnit");
        attack.setSeed(1L);

        CaptureMessage capture = new CaptureMessage();
        capture.setFieldID("fieldID");
        capture.setUnitID("unit");

        CombineMessage combine = new CombineMessage();
        combine.setUnitID("firstUnit");
        combine.setSecondUnitID("secondUnit");

        CreateMessage create = new CreateMessage();
        create.setFieldID("fieldID");
        create.setUnitType("unitType");

        MoveMessage move = new MoveMessage();
        move.setFieldID("fieldID");
        move.setUnitID("unit");

        message.addMessage(attack);
        message.addMessage(capture);
        message.addMessage(combine);
        message.addMessage(create);
        message.addMessage(move);

        //doing
        String turnString = mapper.writeValueAsString(message);

        TurnRequest newTurnMessage = mapper.readValue(turnString);

        //assert

        Assert.assertEquals(message.getPlayer().getId(), newTurnMessage.getPlayer().getId());
        Assert.assertEquals(message.getPlayer().getName(), newTurnMessage.getPlayer().getName());
        Assert.assertEquals(message.getMoves().size(), newTurnMessage.getMoves().size());

    }

}

package test;

import de.core.network.messages.news.turnMessages.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class TurnMessageMapperTest {

    @Test
    public void testInterfaceMapper() throws IOException {

        //init mapper
        TurnMessageMapper mapper = new TurnMessageMapper();

        //init message
        AttackMessage attack = new AttackMessage();
        attack.setEnemyPlayer("enemy");
        attack.setEnemyUnit("eUnit");
        attack.setSeed(1L);

        CaptureMessage capture = new CaptureMessage();
        capture.setFieldID("fieldID");
        capture.setUnitID("unit");

        CombineMessage combine = new CombineMessage();
        combine.setUnitID("firstUnit");
        combine.setSecondUnitID("secondUnit");

        CreateMessage create = new CreateMessage();
        create.setFieldID("fieldID");
        create.setUnitType("unitType");

        MoveMessage move = new MoveMessage();
        move.setFieldID("fieldID");
        move.setUnitID("unit");

        //doing
        String attackString = mapper.writeValueAsString(attack);
        String captureString = mapper.writeValueAsString(capture);
        String combineString = mapper.writeValueAsString(combine);
        String createString = mapper.writeValueAsString(create);
        String moveString = mapper.writeValueAsString(move);

        System.out.println(attackString);
        System.out.println(captureString);
        System.out.println(combineString);
        System.out.println(createString);
        System.out.println(moveString);

        AttackMessage newAttack = (AttackMessage) mapper.readValue(attackString);
        CaptureMessage newCapture = (CaptureMessage) mapper.readValue(captureString);
        CombineMessage newCombine = (CombineMessage) mapper.readValue(combineString);
        CreateMessage newCreate = (CreateMessage) mapper.readValue(createString);
        MoveMessage newMove = (MoveMessage) mapper.readValue(moveString);

        //assert

        Assert.assertEquals(attack.getEnemyPlayer(),  newAttack.getEnemyPlayer());
        Assert.assertEquals(attack.getEnemyUnit(),    newAttack.getEnemyUnit());
        Assert.assertEquals(attack.getSeed(),         newAttack.getSeed());
        Assert.assertEquals(attack.getType(),         newAttack.getType());

        Assert.assertEquals(capture.getFieldID(), newCapture.getFieldID());
        Assert.assertEquals(capture.getUnitID(),  newCapture.getUnitID());
        Assert.assertEquals(capture.getType(),    newCapture.getType());

        Assert.assertEquals(combine.getUnitID(),       newCombine.getUnitID());
        Assert.assertEquals(combine.getSecondUnitID(), newCombine.getSecondUnitID());
        Assert.assertEquals(combine.getType(),         newCombine.getType());

        Assert.assertEquals(create.getFieldID(),  newCreate.getFieldID());
        Assert.assertEquals(create.getUnitType(), newCreate.getUnitType());
        Assert.assertEquals(create.getType(),     newCreate.getType());

        Assert.assertEquals(move.getFieldID(), newMove.getFieldID());
        Assert.assertEquals(move.getUnitID(),  newMove.getUnitID());
        Assert.assertEquals(move.getType(),    newMove.getType());
    }
}

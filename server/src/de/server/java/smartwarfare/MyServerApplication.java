package de.server.java.smartwarfare;

import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;

import de.server.java.smartwarfare.access.AccessContextEventListener;
import de.server.java.smartwarfare.access.AccessRepository;
import de.server.java.smartwarfare.access.AccessService;
import de.server.java.smartwarfare.access.web.AccountsController;
import de.server.java.smartwarfare.debug.DebugService;
import de.server.java.smartwarfare.debug.web.DebugController;
import de.server.java.smartwarfare.files.mapImages.MapImageEventListener;
import de.server.java.smartwarfare.files.mapImages.MapImageRepository;
import de.server.java.smartwarfare.files.mapImages.MapImageService;
import de.server.java.smartwarfare.files.mapImages.web.MapImageController;
import de.server.java.smartwarfare.games.GameEventListener;
import de.server.java.smartwarfare.games.GameRepository;
import de.server.java.smartwarfare.games.GameService;
import de.server.java.smartwarfare.games.draw.DrawController;
import de.server.java.smartwarfare.games.draw.DrawService;
import de.server.java.smartwarfare.games.invite.GameInviteController;
import de.server.java.smartwarfare.games.invite.InviteService;
import de.server.java.smartwarfare.games.web.GameController;
import de.server.java.smartwarfare.maps.MapRepository;
import de.server.java.smartwarfare.maps.MapService;
import de.server.java.smartwarfare.maps.web.MapController;
import de.server.java.smartwarfare.news.listener.AccountsNewsEventListener;
import de.server.java.smartwarfare.news.listener.GameNewsEventListener;
import de.server.java.smartwarfare.news.listener.FriendNewsEventListener;
import de.server.java.smartwarfare.news.NewsRepository;
import de.server.java.smartwarfare.news.NewsService;
import de.server.java.smartwarfare.news.web.NewsController;
import de.server.java.smartwarfare.properties.Properties;
import de.server.java.smartwarfare.properties.PropertyConstructor;
import de.server.java.smartwarfare.socket.WebSocketHandler;
import de.server.java.smartwarfare.user.UserContextEventListener;
import de.server.java.smartwarfare.user.UserRepository;
import de.server.java.smartwarfare.user.UserService;
import de.server.java.smartwarfare.user.friends.FriendService;
import de.server.java.smartwarfare.user.friends.communication.MessageRepository;
import de.server.java.smartwarfare.user.friends.communication.MessageService;
import de.server.java.smartwarfare.user.friends.communication.web.MessageController;
import de.server.java.smartwarfare.user.friends.web.FriendController;
import de.server.java.smartwarfare.user.web.UserController;
import lombok.extern.slf4j.Slf4j;

import static spark.Spark.webSocket;

@Slf4j
public class MyServerApplication
{
    private static final HashSet<Observer> listener = new HashSet<>();

    private static AccessRepository accessRepository;
    private static MapImageRepository mapImageRepository;
    private static GameRepository gameRepository;
    private static MapRepository mapRepository;
    private static NewsRepository newsRepository;
    private static UserRepository userRepository;
    private static MessageRepository messageRepository;

    public static void main(String[] args)
    {
        log.info("Server will be started.");
        initRepositories();

        initListeners();

        final Properties properties = PropertyConstructor.construct();

        webSocket("/session/*",
                  WebSocketHandler.class);

        new AccountsController((AccessService)
                                       addAllObserversTo(new AccessService(accessRepository)));

        new UserController((UserService)
                                   addAllObserversTo(new UserService(userRepository)));

        new FriendController((FriendService)
                                     addAllObserversTo(new FriendService(userRepository)));

        new GameController((GameService) addAllObserversTo(new GameService(gameRepository)));

        new GameInviteController((InviteService)
                                         addAllObserversTo(new InviteService(
                                                 properties,
                                                 userRepository,
                                                 mapRepository,
                                                 gameRepository
                                         )));
        new DrawController((DrawService)
                                   addAllObserversTo(new DrawService(gameRepository)));


        new MapController((MapService)
                                  addAllObserversTo(new MapService(mapRepository)));
        final MapImageService mapImageService = new MapImageService(mapImageRepository);
        new MapImageController((MapImageService)
                                       addAllObserversTo(mapImageService));


        new MessageController((MessageService)
                                      addAllObserversTo(new MessageService(messageRepository)));
        new NewsController((NewsService)
                                   addAllObserversTo(new NewsService(newsRepository)));


        new DebugController(
                (DebugService) addAllObserversTo(
                        new DebugService(
                                userRepository,
                                accessRepository,
                                mapRepository,
                                mapImageService
                        )));
    }

    private static void initListeners()
    {
        log.info("Init Listeners");
        listener.add(new AccessContextEventListener(accessRepository));
        listener.add(new UserContextEventListener(userRepository));
        listener.add(new GameEventListener(gameRepository));

        listener.add(new MapImageEventListener(mapImageRepository));

        listener.add(new FriendNewsEventListener(newsRepository));
        listener.add(new GameNewsEventListener(newsRepository));
        listener.add(new AccountsNewsEventListener(newsRepository));
    }

    private static void initRepositories()
    {
        log.info("Init Repositories");
        accessRepository = new AccessRepository();
        mapImageRepository = new MapImageRepository();
        gameRepository = new GameRepository();
        mapRepository = new MapRepository();
        newsRepository = new NewsRepository();
        userRepository = new UserRepository();
        messageRepository = new MessageRepository();
    }

    private static Object addAllObserversTo(final Observable observable)
    {
        for (Observer observer : listener)
        {
            observable.addObserver(observer);
        }

        return observable;
    }
}

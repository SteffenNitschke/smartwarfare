package de.server.java.smartwarfare.files;

import de.server.java.smartwarfare.maps.data.MapId;
import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity("mapImage")
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class DBMapImage
{
    @Embedded
    @Id
    private MapId _id;

    private byte[] image;
}

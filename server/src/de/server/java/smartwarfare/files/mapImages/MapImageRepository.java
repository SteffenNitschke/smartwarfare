package de.server.java.smartwarfare.files.mapImages;

import com.mongodb.MongoClient;
import com.mongodb.WriteResult;

import de.server.java.smartwarfare.files.DBMapImage;
import de.server.java.smartwarfare.maps.data.MapId;
import dev.morphia.Datastore;
import dev.morphia.Key;
import dev.morphia.Morphia;

public class MapImageRepository
{
    private final Datastore datastore;

    public MapImageRepository()
    {
        System.out.println("MapImageRepo");
        final Morphia morphia = new Morphia();

        morphia.mapPackage("de.server.java.smartwarfare.files");

        this.datastore = morphia.createDatastore(new MongoClient(), "smartwarfare");
        datastore.ensureIndexes();
    }

    public Key<DBMapImage> save(final DBMapImage game)
    {
        return datastore.save(game);
    }

    DBMapImage findMapImageById(final MapId id)
    {
        return datastore.createQuery(DBMapImage.class)
                        .field("_id.identifier").equal(id.getIdentifier())
                        .first();
    }

    public WriteResult deleteById(final MapId id)
    {
        return datastore.delete(findMapImageById(id));
    }
}

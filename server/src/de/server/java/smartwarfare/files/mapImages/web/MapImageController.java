package de.server.java.smartwarfare.files.mapImages.web;


import java.io.OutputStream;

import de.server.java.smartwarfare.files.DBMapImage;
import de.server.java.smartwarfare.files.mapImages.MapImageService;
import de.server.java.smartwarfare.maps.data.MapId;

import static spark.Spark.get;


public class MapImageController
{
    public MapImageController(final MapImageService service)
    {
        get("/maps/image/:map_id",
            (request, response) ->
            {
                final MapId mapId = MapId.of(request.params(":map_id"));

                final DBMapImage dbMapImage = service.getMapImage(mapId);

                if (dbMapImage == null)
                {
                    response.status(400);
                }
                else
                {
                    response.type("image/jpeg");

                    response.status(200);

                    final OutputStream outputStream = response.raw().getOutputStream();
                    outputStream.write(dbMapImage.getImage());
                    outputStream.flush();

                }

                return response;
            });
    }
}

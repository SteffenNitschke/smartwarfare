package de.server.java.smartwarfare.files.mapImages;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import de.server.java.smartwarfare.files.DBMapImage;
import de.server.java.smartwarfare.maps.MapCreatedEvent;
import lombok.RequiredArgsConstructor;
import spark.utils.IOUtils;

@RequiredArgsConstructor
public class MapImageEventListener implements Observer
{
    private final MapImageRepository mapImageRepository;

    @Override
    public void update(
            final Observable observable,
            final Object event)
    {
        if (event instanceof MapCreatedEvent)
        {
            handleMapCreatedEvent((MapCreatedEvent) event);
        }
    }

    private void handleMapCreatedEvent(final MapCreatedEvent event)
    {
        try
        {
            byte[] image = IOUtils.toByteArray(new FileInputStream(event.getImage()));

            mapImageRepository.save(new DBMapImage(
                    event.getId(),
                    image
            ));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}

package de.server.java.smartwarfare.files.mapImages;

import java.util.Observable;

import de.server.java.smartwarfare.files.DBMapImage;
import de.server.java.smartwarfare.maps.data.MapId;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MapImageService extends Observable
{
    private final MapImageRepository mapImageRepository;

    public DBMapImage getMapImage(final MapId mapId)
    {
        return mapImageRepository.findMapImageById(mapId);
    }
}

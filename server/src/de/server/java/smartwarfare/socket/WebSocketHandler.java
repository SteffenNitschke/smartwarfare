package de.server.java.smartwarfare.socket;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;

import de.server.java.smartwarfare.user.UserId;

@WebSocket
public class WebSocketHandler
{
    private static final HashMap<UserId, Session> sessions = new HashMap<>();

    @OnWebSocketConnect
    public void connected(final Session session)
    {
        final URI uri = session.getUpgradeRequest().getRequestURI();

        final UserId userId = UserId.of(uri.getPath().split("/")[2]);

        System.out.println("WebSocket: " + userId.getIdentifier());

        sessions.put(userId, session);
    }

    @OnWebSocketClose
    public void closed(Session session, int statusCode, String reason)
    {
        System.out.println("WebSocket closed");
    }

    public static void sendMessageToUser(final UserId userId, final String message) throws IOException
    {
        System.out.println("WebSocket send Message: " + userId.getIdentifier() + " " + message);
        try
        {
            sessions.get(userId).getRemote().sendString(message);
        }
        catch (NullPointerException e)
        {
            System.out.println("Session is Closed");
            sessions.remove(userId);
        }
    }

}

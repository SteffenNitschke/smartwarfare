package de.server.java.smartwarfare.builder;

import de.server.java.smartwarfare.maps.data.DBUnit;
import org.json.JSONObject;

public class DBUnitBuilder {

    private JSONObject unitSpecs = new JSONObject();

    public DBUnit build(String type) {

        DBUnit unit = new DBUnit();
        unit.setType(type);

        JSONObject unitTypeSpecs = unitSpecs.getJSONObject(type);

//        unit.setRange(unitTypeSpecs.getInt("range"));TODO and so on
        return unit;
    }

}

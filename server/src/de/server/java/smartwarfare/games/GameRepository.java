package de.server.java.smartwarfare.games;

import com.mongodb.MongoClient;
import com.mongodb.WriteResult;

import java.util.List;

import de.server.java.smartwarfare.games.data.DBGame;
import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.user.UserId;
import dev.morphia.Datastore;
import dev.morphia.Key;
import dev.morphia.Morphia;

public class GameRepository
{
    private final Datastore datastore;

    public GameRepository()
    {
        System.out.println("GameRepo");
        final Morphia morphia = new Morphia();

        morphia.mapPackage("de.server.java.smartwarfare.games");

        this.datastore = morphia.createDatastore(new MongoClient(), "smartwarfare");
        datastore.ensureIndexes();
    }

    public Key<DBGame> save(final DBGame game)
    {
        return datastore.save(game);
    }

    public DBGame findGameById(final GameId id)
    {
        return datastore.createQuery(DBGame.class)
                .field("_id.identifier").equal(id.getIdentifier())
                .first();
    }

    List<DBGame> findAllGamesByUserId(final UserId userId)
    {
        return datastore.createQuery(DBGame.class)
                .field("players.userId.identifier").equal(userId.getIdentifier())
                .find().toList();
    }

    public List<DBGame> getAllPublicGames()
    {
        return datastore.createQuery(DBGame.class)
                .field("publicGame").equal(true)
                .find().toList();
    }

    public WriteResult deleteGameById(final GameId uuid)
    {
        return datastore.delete(findGameById(uuid));
    }

}

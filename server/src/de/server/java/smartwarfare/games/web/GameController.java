package de.server.java.smartwarfare.games.web;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

import de.server.java.smartwarfare.games.GameService;
import de.server.java.smartwarfare.games.data.DBGame;
import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.user.UserId;

import static spark.Spark.get;
import static spark.Spark.post;

public class GameController
{
    private final ObjectMapper objectMapper = new ObjectMapper();

    public GameController(final GameService gameService)
    {
        get("/games/of/:player_id",
            (request, response) ->
            {
                final UserId userID = UserId.of(request.params(":player_id"));

                final List<GameId> games = gameService.getAllGamesOf(userID);

                if (games == null)
                {
                    response.status(404);
                }
                else if (games.isEmpty())
                {
                    response.status(204);
                }
                else
                {
                    response.status(200);
                    response.type("application/json");

                    return objectMapper.writeValueAsString(games);
                }

                return response;
            }
        );

        get("/games/:game_id",
            (request, response) ->
            {
                final GameId gameId = GameId.of(request.params(":game_id"));

                final DBGame game = gameService.getGame(gameId);

                if (game == null)
                {
                    response.status(404);
                }
                else
                {
                    response.status(200);
                    response.type("application/json");

                    return objectMapper.writeValueAsString(game);
                }

                return response;
            }
        );

        //TODO add Public Games
        get("/games",
            ((request, response) ->
            {
                final List<DBGame> games = gameService.getAllPublicGames();

                if (games == null || games.isEmpty())
                {
                    response.status(404);
                }
                else
                {
                    response.status(200);
                    response.type("application/json");

                    return objectMapper.writeValueAsString(games);
                }

                return response;
            })
        );

        post("/games/public",
             (request, response) ->
             {
                 response.status(418);
                 return response;
             });
    }
}

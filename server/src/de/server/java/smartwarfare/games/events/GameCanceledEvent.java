package de.server.java.smartwarfare.games.events;

import java.util.HashSet;

import de.server.java.smartwarfare.games.data.DBPlayer;
import de.server.java.smartwarfare.games.data.GameId;
import lombok.Value;

@Value
public class GameCanceledEvent
{
    private final GameId gameId;

    private final HashSet<DBPlayer> player;
}

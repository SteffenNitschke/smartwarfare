package de.server.java.smartwarfare.games.events;

import java.util.HashSet;
import java.util.Optional;

import de.server.java.smartwarfare.games.data.DBGame;
import de.server.java.smartwarfare.games.data.DBPlayer;
import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.maps.data.DBMap;
import de.server.java.smartwarfare.user.UserId;
import lombok.Getter;
import de.core.network.messages.SmallMapMessage;
import de.core.util.GameTypes;

@Getter
public class GameInvitationEvent
{
    private final GameId gameId;

    private final HashSet<DBPlayer> players;

    private final UserId operator;

    private final String operatorName;

    private final GameTypes gameType;

    private final SmallMapMessage map;

    public GameInvitationEvent(final DBGame game)
    {
        this.gameId = game.get_id();
        this.players = game.getPlayers();
        this.operator = game.getCurrentPlayer();
        final Optional<String> operatorName =
                game.getPlayers().stream()
                    .filter(dbPlayer -> dbPlayer.getUserId().equals(operator))
                    .map(DBPlayer::getName).findFirst();

        this.operatorName = operatorName.orElse("");

        this.gameType = game.getGameType();

        final DBMap map = game.getMap();

        this.map = new SmallMapMessage(
                map.get_id().getIdentifier(),
                map.getNumberOfPlayers(),
                map.getName(),
                map.getEditorName(),
                map.getMaxX(),
                map.getMaxY());
    }
}

package de.server.java.smartwarfare.games.events;

import java.util.List;

import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.user.UserId;
import lombok.Value;

@Value
public class TurnEvent
{
    private final UserId sender;
    private final List<UserId> players;
    private final GameId gameId;
}

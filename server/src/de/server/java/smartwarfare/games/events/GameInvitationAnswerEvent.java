package de.server.java.smartwarfare.games.events;

import java.util.HashSet;

import de.server.java.smartwarfare.games.data.DBPlayer;
import de.server.java.smartwarfare.games.data.GameId;
import lombok.Value;
import de.core.network.messages.PlayerMessage;

@Value
public class GameInvitationAnswerEvent
{
    private final PlayerMessage responder;
    private final GameId gameId;
    private final boolean answer;
    private final HashSet<DBPlayer> players;
}

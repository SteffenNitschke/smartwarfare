package de.server.java.smartwarfare.games.pushNotifications;

import de.core.util.NewsType;
import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.news.NewsId;
import de.server.java.smartwarfare.user.SmallUser;
import lombok.Value;

@Value
public class GameInitializedNotification
{
    private final NewsId _id;

    private final GameId gameId;

    private final SmallUser sender;

    private final NewsType type;
}

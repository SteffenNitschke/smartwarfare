package de.server.java.smartwarfare.games.pushNotifications;

import de.core.util.NewsType;
import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.news.NewsId;
import de.server.java.smartwarfare.user.SmallUser;
import lombok.Value;

@Value
public class TurnNotification
{
    private NewsId _id;

    private GameId gameId;

    private SmallUser sender;

    private final NewsType type;
}

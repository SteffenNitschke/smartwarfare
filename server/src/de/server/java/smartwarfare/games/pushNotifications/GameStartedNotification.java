package de.server.java.smartwarfare.games.pushNotifications;

import de.core.util.NewsType;
import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.news.NewsId;
import lombok.Value;

@Value
public class GameStartedNotification
{
    private final NewsId _id;

    private final GameId gameId;

    private final NewsType type;
}

//package de.server.java.main.games;
//
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import de.server.java.main.maps.data.DBBuilding;
//import de.server.java.main.games.data.DBGame;
//import de.server.java.main.maps.data.DBMap;
//import main.java.util.GameStatus;
//import main.java.util.GameTypes;
//import org.json.JSONObject;
//
//import java.io.IOException;
//import java.util.HashSet;
//
//public class DBGameMapper {
//
//    private ObjectMapper objectMapper = new ObjectMapper();
//
//    public DBGame readValue(String value) throws IOException {
//
//        JSONObject jsonGame = new JSONObject(value);
//        DBGame game = new DBGame();
//
//        game.set_id(jsonGame.getString("_id"));
//        game.setRound(jsonGame.getInt("round"));
//        game.setStatus(GameStatus.valueOf(jsonGame.getString("status")));
//        game.setGameType(GameTypes.valueOf(jsonGame.getString("gameType")));
//        game.setCurrentPlayer(jsonGame.getString("currentPlayer"));
//        game.setPlayerBuildings(
//                objectMapper.readValue(jsonGame.getJSONArray("playerBuildings").toString()
//                        , new TypeReference<HashSet<DBBuilding>>() {}));
//        game.setChatId(jsonGame.getString("chatId"));
//        game.setMap(readMapValue(jsonGame.getJSONObject("map")));
//
//        return game;
//    }
//
//    private DBMap readMapValue(JSONObject jsonMap) {
//
//        DBMap map = new DBMap();
//
//
//
//        return map;
//    }
//}

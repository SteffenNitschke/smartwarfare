package de.server.java.smartwarfare.games.invite;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.server.java.smartwarfare.access.AccessToken;
import de.server.java.smartwarfare.games.data.DBGame;
import de.server.java.smartwarfare.user.UserId;
import de.core.network.messages.GameAnswerRequest;
import de.core.network.messages.news.GameInviteClientRequest;

import static spark.Spark.post;

public class GameInviteController
{
    private ObjectMapper objectMapper = new ObjectMapper();

    public GameInviteController(final InviteService inviteService)
    {
        post("/invite",
             ((request, response) ->
             {
                 final UserId userId =
                         ((AccessToken) request
                                 .session()
                                 .attribute("userId"))
                                 .getUserId();

                 final GameInviteClientRequest message = objectMapper.readValue(
                         request.body(),
                         GameInviteClientRequest.class);

                 final DBGame game = inviteService.inviteToGame(userId,
                                                                message);

                 if (game == null)
                 {
                     response.status(400);
                 }
                 else
                 {
                     response.status(200);
                     response.type("application/json");

                     return objectMapper.writeValueAsString(game);
                 }

                 return response;
             }
             ));


        post("/answer/games",
             ((request, response) ->
             {
                 final GameAnswerRequest message = objectMapper.readValue(
                         request.body(),
                         GameAnswerRequest.class);

                 final DBGame game = inviteService.answerGameInvite(message);

                 if (game == null)
                 {
                     response.status(400);
                 }
                 else
                 {
                     response.status(200);
                     response.type("application/json");

                     return objectMapper.writeValueAsString(game);
                 }

                 return response;
             }
             ));
    }
}

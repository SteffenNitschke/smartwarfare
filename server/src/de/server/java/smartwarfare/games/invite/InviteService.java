package de.server.java.smartwarfare.games.invite;

import org.bson.types.ObjectId;

import java.util.HashSet;
import java.util.Objects;
import java.util.Observable;

import de.server.java.smartwarfare.games.GameRepository;
import de.server.java.smartwarfare.games.data.DBGame;
import de.server.java.smartwarfare.games.data.DBPlayer;
import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.games.events.GameCanceledEvent;
import de.server.java.smartwarfare.games.events.GameInvitationAnswerEvent;
import de.server.java.smartwarfare.games.events.GameInvitationEvent;
import de.server.java.smartwarfare.games.events.GameReadyEvent;
import de.server.java.smartwarfare.maps.MapRepository;
import de.server.java.smartwarfare.maps.data.DBField;
import de.server.java.smartwarfare.maps.data.DBMap;
import de.server.java.smartwarfare.maps.data.MapId;
import de.server.java.smartwarfare.properties.Properties;
import de.server.java.smartwarfare.user.DBUser;
import de.server.java.smartwarfare.user.UserId;
import de.server.java.smartwarfare.user.UserRepository;
import lombok.RequiredArgsConstructor;
import de.core.network.messages.GameAnswerRequest;
import de.core.network.messages.news.GameInviteClientRequest;
import de.core.util.GameStatus;
import de.core.util.Utils;

@RequiredArgsConstructor
public class InviteService extends Observable
{
    private final Properties properties;

    private final UserRepository userRepository;

    private final MapRepository mapRepository;

    private final GameRepository gameRepository;


    DBGame inviteToGame(
            final UserId operatorId,
            final GameInviteClientRequest invite)
    {
        System.out.println("inviteToGame()");

        //new game Dao
        final DBGame game = new DBGame();
        game.setPublicGame(false);

        final DBUser dbOperator = userRepository.findUserById(operatorId);

        //for new game
        final HashSet<DBPlayer> dBPlayers = new HashSet<>();

        //operator
        final DBPlayer operator =
                new DBPlayer(operatorId,
                             dbOperator.getName(),
                             Utils.getColor(0),
                             null,
                             null,
                             properties.getStartMoney(),
                             true,
                             new HashSet<>(),
                             new HashSet<>());

        dBPlayers.add(operator);

        //other players
        int colorNumber = 1;
        for (String id : invite.getPlayerIds())
        {
            final DBUser player = userRepository.findUserById(UserId.of(id));
            System.out.println("Find Player " + player);
            final DBPlayer dBPlayer1 =
                    new DBPlayer(player.get_id(),
                                 player.getName(),
                                 Utils.getColor(colorNumber),
                                 null,
                                 null,
                                 properties.getStartMoney(),
                                 false,
                                 null,
                                 null);
            colorNumber++;
            dBPlayers.add(dBPlayer1);
        }

        //set previous and next player
        setPrevAndNext(dBPlayers);
        game.setPlayers(dBPlayers);

        //init map
        final DBMap map = mapRepository.findMapById(MapId.of(invite.getMapId()));
        System.out.println("Find Map " + map);
        if (map.getNumberOfPlayers() < game.getPlayers().size())
        {
            System.err.println("Not enough Space for all Players!!!");
            return null;
        }
        game.setMap(map);
        game.getMap().getFields().stream()
            .map(DBField::getBuilding)
            .filter(Objects::nonNull)
            .forEach(dbBuilding -> dbBuilding.set_id(new ObjectId().toHexString()));

        //set init game state
        game.setPlayers(dBPlayers);
        game.setRound(1);
        game.setCurrentPlayer(operator.getUserId());
        game.setStatus(GameStatus.INVITATION);
        game.setGameType(invite.getGameType());
        game.set_id(GameId.random());

        //save game
        gameRepository.save(game);

        System.out.println("GameId: " + game.get_id());

        //invite all players

        setChanged();
        notifyObservers(new GameInvitationEvent(game));

        return game;
    }

    private void setPrevAndNext(HashSet<DBPlayer> dBPlayers)
    {
        DBPlayer[] array = new DBPlayer[dBPlayers.size()];

        int i = 0;
        for (DBPlayer player : dBPlayers)
        {
            array[i] = player;
            i++;
        }
        for (i = 0; i < array.length; i++)
        {
            if (i == array.length - 1)
            {
                array[i].setNextPlayerId(array[0].getUserId());
            }
            else
            {
                array[i].setNextPlayerId(array[i + 1].getUserId());
            }
            if (i == 0)
            {
                array[i].setPrevPlayerId(array[array.length - 1].getUserId());
            }
            else
            {
                array[i].setPrevPlayerId(array[i - 1].getUserId());
            }
        }
    }

    DBGame answerGameInvite(final GameAnswerRequest answer)
    {
        System.out.println("answerGameInvite " + answer);
        final DBGame game = gameRepository.findGameById(GameId.of(answer.getGameId()));

        if(game == null)
        {
            return null;
        }
        System.out.println("Find Game: " + game);
        game.getPlayers().stream().filter(dbPlayer -> dbPlayer.getUserId().getIdentifier()
                                                              .equals(answer.getResponder()
                                                                            .getId().getIdentifier()))
            .forEach(dbPlayer -> dbPlayer.setWannaPlayTheGame(answer.isAnswer()));
        System.out.println("Set answer in Game " + game);

        gameRepository.save(game);

        setChanged();
        notifyObservers(
                new GameInvitationAnswerEvent(
                        answer.getResponder(),
                        GameId.of(answer.getGameId()),
                        answer.isAnswer(),
                        game.getPlayers()));

        if (!answer.isAnswer())
        {
            notifyObservers(new GameCanceledEvent(
                    game.get_id(),
                    game.getPlayers()));
        }
        else
        {
            if (allPlayerSaidYes(game))
            {
                System.out.println("All of us wanner play this game!");
                notifyObservers(
                        new GameReadyEvent(
                                game.get_id(),
                                game.getPlayers()));
            }
        }
        return game;
    }

    private boolean allPlayerSaidYes(DBGame game)
    {
        boolean answer = true;

        for (DBPlayer player : game.getPlayers())
        {
            if (!player.isWannaPlayTheGame())
            {
                answer = false;
            }
        }

        return answer;
    }
}
package de.server.java.smartwarfare.games;

import java.util.HashSet;
import java.util.Objects;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import java.util.UUID;

import de.server.java.smartwarfare.games.data.DBGame;
import de.server.java.smartwarfare.games.data.DBPlayer;
import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.games.events.GameCanceledEvent;
import de.server.java.smartwarfare.games.events.GameReadyEvent;
import de.server.java.smartwarfare.maps.data.DBBuilding;
import de.server.java.smartwarfare.maps.data.DBField;
import de.server.java.smartwarfare.maps.data.DBUnit;
import lombok.RequiredArgsConstructor;

import static de.core.util.GameStatus.RUNNING;

@RequiredArgsConstructor
public class GameEventListener implements Observer
{
    private final GameRepository gameRepository;

    @Override
    public void update(
            final Observable observable,
            final Object event)
    {
        if (event instanceof GameReadyEvent)
        {
            handleGameReadyEvent((GameReadyEvent) event);
        }
        else if(event instanceof GameCanceledEvent)
        {
            handleGameCanceledEvent((GameCanceledEvent) event);
        }
    }

    private void handleGameCanceledEvent(final GameCanceledEvent event)
    {
        gameRepository.deleteGameById(event.getGameId());
    }

    private void handleGameReadyEvent(final GameReadyEvent event)
    {
        final GameId gameId = event.getGameId();

        final DBGame game = gameRepository.findGameById(gameId);

        if (game == null)
        {
            return;
        }
        game.setStatus(RUNNING);

        //set random first player
        DBPlayer currentPlayer = game.getPlayers().stream()
                                     .filter(dbPlayer -> dbPlayer.getUserId()
                                                                 .equals(game.getCurrentPlayer()))
                                     .findFirst()
                                     .orElse(null);

        int i = new Random().nextInt(game.getPlayers().size());
        for (int index = 0; index < i; i++)
        {
            DBPlayer finalCurrentPlayer = currentPlayer;
            currentPlayer = game.nextPlayer(finalCurrentPlayer);
        }
        game.setCurrentPlayer(Objects.requireNonNull(currentPlayer).getUserId());

        connectUnitsAndBuildingsToPlayer(game);

        //save game
        gameRepository.save(game);
    }

    private void connectUnitsAndBuildingsToPlayer(final DBGame game)
    {
        System.out.println("GameService::connectUnitsAndBuildingsToPlayer");
        HashSet<DBUnit> units = game.getMap().getUnits();
        System.out.println("Unit Size: " + units.size());

        HashSet<DBField> fields = game.getMap().getFields();

        HashSet<DBBuilding> buildings = new HashSet<>();

        for (DBField field : fields)
        {
            if (field.getBuilding() != null)
            {
                buildings.add(field.getBuilding());
                System.out.println("add Building " + field.getBuilding());
            }
        }

        System.out.println("Building Size: " + buildings.size());
        int i = 0;
        for (DBPlayer player : game.getPlayers())
        {
            for (DBUnit unit : units)
            {
                if (unit.getUnitId().startsWith("Player" + i))
                {
                    System.out.println("Find Player for Unit: " + unit + " " + player.getName());
                    unit.setPlayer(player.getUserId());
                    unit.setUnitId(UUID.randomUUID().toString());
                    player.getTroops().add(unit);
                }
            }
            for (DBBuilding building : buildings)
            {
                if (building.getUser().getIdentifier().startsWith("Player" + i))
                {
                    System.out.println(
                            "Find Player for Building: " + building + " " + player.getName());
                    building.setUser(player.getUserId());
                    game.getPlayerBuildings().add(building);
                }
            }
            i++;
        }

        System.out.println("Number Of Buildings: " + game.getPlayerBuildings().size());

        units.forEach(dbUnit ->
                              game.getMap().getFields().stream()
                                  .filter(dbField -> dbField.getFieldID().equals(dbUnit.getField()))
                                  .forEach(dbField -> dbField.setUnit(dbUnit.getUnitId()))
        );

    }
}

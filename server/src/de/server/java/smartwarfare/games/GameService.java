package de.server.java.smartwarfare.games;

//todo Invite Butten in IMPScreen trigger with enter too.
//todo find another solution for the MapNames and how to get the mapId
//todo why is map name in newsScreen null???

import java.util.List;
import java.util.Observable;
import java.util.stream.Collectors;

import de.server.java.smartwarfare.games.data.DBGame;
import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.user.UserId;
import lombok.RequiredArgsConstructor;
import spark.Response;

@RequiredArgsConstructor
public class GameService extends Observable
{
    private final GameRepository gameRepository;

    public DBGame getGame(final GameId id)
    {
        return gameRepository.findGameById(id);
    }

    public List<GameId> getAllGamesOf(final UserId userId)
    {
        System.out.println("getAllGamesOf " + userId);

        return gameRepository.findAllGamesByUserId(userId)
                             .stream()
                             .map(DBGame::get_id)
                             .collect(Collectors.toList());
    }

    public List<DBGame> getAllPublicGames()
    {
        return gameRepository.getAllPublicGames();
    }

    //TODO
    public Response surender(
            final UserId userId,
            final GameId gameId,
            final Response response)
    {
        return response;
    }

    public Response gameEnding(
            final GameId gameId,
            final Response response)
    {
        return response;
    }
}

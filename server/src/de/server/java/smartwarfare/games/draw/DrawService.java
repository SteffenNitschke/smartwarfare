package de.server.java.smartwarfare.games.draw;

import java.util.HashSet;
import java.util.Observable;
import java.util.Optional;
import java.util.stream.Collectors;

import de.server.java.smartwarfare.builder.DBUnitBuilder;
import de.server.java.smartwarfare.games.GameRepository;
import de.server.java.smartwarfare.games.data.DBGame;
import de.server.java.smartwarfare.games.data.DBPlayer;
import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.games.events.TurnEvent;
import de.server.java.smartwarfare.maps.data.DBBuilding;
import de.server.java.smartwarfare.maps.data.DBField;
import de.server.java.smartwarfare.maps.data.DBUnit;
import de.server.java.smartwarfare.user.UserId;
import lombok.RequiredArgsConstructor;
import de.core.network.messages.news.TurnRequest;
import de.core.network.messages.news.turnMessages.AttackMessage;
import de.core.network.messages.news.turnMessages.CaptureMessage;
import de.core.network.messages.news.turnMessages.CombineMessage;
import de.core.network.messages.news.turnMessages.CreateMessage;
import de.core.network.messages.news.turnMessages.MoveMessage;
import de.core.network.messages.news.turnMessages.TurnMessageInterface;
import de.core.util.Utils;

@RequiredArgsConstructor
public class DrawService extends Observable
{
    private DBUnitBuilder dbUnitBuilder = new DBUnitBuilder();
    private final GameRepository gameRepository;

    boolean draw(
            final GameId gameId,
            final TurnRequest turnRequest)
    {
        final DBGame game = gameRepository.findGameById(gameId);
        if (game == null || !refreshGame(game,
                                         turnRequest))
        {
            return false;
        }

        gameRepository.save(game);

        setChanged();
        notifyObservers(new TurnEvent(//TODO catch TurnEvent
                        game.getCurrentPlayer(),
                        game.getPlayers()
                            .stream()
                            .map(DBPlayer::getUserId)
                            .collect(Collectors.toList()),
                        game.get_id()));

        return true;
    }

    private boolean refreshGame(
            final DBGame game,
            final TurnRequest turn)
    {
        if (game.getCurrentPlayer().getIdentifier().equals(turn.getPlayer().getId().getIdentifier()))
        {
            for (TurnMessageInterface move : turn.getMoves())
            {
                final String type = move.getType();
                switch (type)
                {
                    case Utils.MOVE_TYPE_MOVE_UNIT:
                        makeAMove((MoveMessage) move,
                                  game);
                        break;
                    case Utils.MOVE_TYPE_ATTACK_UNIT:
                        makeAttack((AttackMessage) move,
                                   game);
                        break;
                    case Utils.MOVE_TYPE_CREATE_UNIT:
                        createAUnit((CreateMessage) move,
                                    game);
                        break;
                    case Utils.MOVE_TYPE_COMBINE_UNIT:
                        combineUnits((CombineMessage) move,
                                     game);
                        break;
                    case Utils.MOVE_TYPE_CAPTURE_BUILDING:
                        captureBuilding((CaptureMessage) move,
                                        game);
                        break;
                }
            }
        }
        else
        {
            return false;
        }

        return true;
    }

    private void combineUnits(
            final CombineMessage combineMessage,
            final DBGame game)
    {
        final Optional<DBPlayer> player = getUserOf(game,
                                                    game.getCurrentPlayer());

        if (!player.isPresent())
        {
            return;
        }

        final Optional<DBUnit> firstUnit =
                player.get()
                      .getTroops()
                      .stream()
                      .filter(dbUnit ->
                                      dbUnit.getUnitId().equals(combineMessage
                                                                        .getUnitID()))
                      .findFirst();

        final Optional<DBUnit> seconUnit =
                player.get()
                      .getTroops()
                      .stream()
                      .filter(dbUnit ->
                                      dbUnit.getUnitId().equals(combineMessage
                                                                        .getSecondUnitID()))
                      .findFirst();

        //TODO
    }

    private void createAUnit(
            final CreateMessage create,
            final DBGame game)
    {
        final HashSet<DBField> fields = game.getMap().getFields();
        fields.stream()
              .filter(dbField ->
                              dbField.getFieldID().equals(create.getFieldID()))
              .findFirst()
              .ifPresent(field ->
                         {
                             if (Utils.isFactory(field.getBuilding().getType()))
                             {
                                 final DBUnit unit = dbUnitBuilder
                                         .build(create.getUnitType());
                                 game.getPlayers().stream()
                                     .filter(dbPlayer ->
                                                     dbPlayer.getUserId().getIdentifier()
                                                             .equals(game.getCurrentPlayer()
                                                                         .getIdentifier()))
                                     .findFirst().get()
                                     .getTroops().add(unit);
                             }
                         });
    }

    private void captureBuilding(
            final CaptureMessage capture,
            final DBGame game)
    {
        game.getPlayers()
            .stream()
            .filter(dbPlayer -> dbPlayer.getUserId().getIdentifier()
                                        .equals(game.getCurrentPlayer().getIdentifier()))
            .findFirst()
            .ifPresent(
                    dbPlayer ->
                            dbPlayer.getTroops()
                                    .stream()
                                    .filter(dbUnit -> dbUnit.getUnitId()
                                                            .equals(capture.getUnitID()))
                                    .findFirst()
                                    .ifPresent(
                                            dbUnit ->
                                                    game.getMap().getFields()
                                                        .stream()
                                                        .filter(dbField -> dbField.getFieldID()
                                                                                  .equals(capture.getFieldID()))
                                                        .findFirst()
                                                        .ifPresent(
                                                                dbField ->
                                                                {
                                                                    final DBBuilding building
                                                                            = dbField
                                                                            .getBuilding();
                                                                    if (building
                                                                            .getWannerCapture()
                                                                            .getIdentifier()
                                                                            .equals(game
                                                                                            .getCurrentPlayer()
                                                                                            .getIdentifier()))
                                                                    {
                                                                        building.setCaptureValue((int) (
                                                                                building.getCaptureValue() +
                                                                                        (dbUnit.getHealth() /
                                                                                                2)));
                                                                    }
                                                                    else
                                                                    {
                                                                        building.setCaptureValue(
                                                                                (int) dbUnit
                                                                                        .getHealth() /
                                                                                        2);
                                                                    }
                                                                }
                                                        )
                                    )
            );
    }

    private void makeAttack(
            final AttackMessage attackMessage,
            final DBGame game)
    {
        final Optional<DBPlayer> player = getUserOf(
                game,
                game.getCurrentPlayer()
        );

        if (!player.isPresent())
        {
            return;
        }

        final Optional<DBUnit> unit =
                player.get()
                      .getTroops()
                      .stream()
                      .filter(dbUnit ->
                                      dbUnit.getUnitId().equals(attackMessage
                                                                        .getOwenUnitID()))
                      .findFirst();

        final Optional<DBPlayer> enemy = getUserOf(
                game,
                UserId.of(attackMessage.getEnemyPlayer())
        );

        final Optional<DBUnit> enemyUnit =
                enemy.get()
                     .getTroops()
                     .stream()
                     .filter(dbUnit ->
                                     dbUnit.getUnitId().equals(attackMessage
                                                                       .getEnemyUnit()))
                     .findFirst();

        //TODO calculate attack
    }

    private void makeAMove(
            final MoveMessage move,
            final DBGame game)
    {

        final String unitId = move.getUnitID();
        final String fieldId = move.getFieldID();

        HashSet<DBUnit> units = game.getPlayers().stream()
                                    .filter(dbPlayer -> dbPlayer.getUserId().getIdentifier()
                                                                .equals(game.getCurrentPlayer()
                                                                            .getIdentifier()))
                                    .findFirst().get().getTroops();

        DBUnit unit = units.stream().filter(dbUnit -> dbUnit.getUnitId().equals(unitId))
                           .findFirst().get();

        HashSet<DBField> fields = game.getMap().getFields();

        DBField field = fields.stream()
                              .filter(dbField -> dbField.getFieldID().equals(fieldId))
                              .findFirst().get();

        fields.stream()
              .filter(dbField -> dbField.getFieldID().equals(unit.getField()))
              .findFirst().get().setUnit(null);

        field.setUnit(unit.getUnitId());
        unit.setField(field.getFieldID());
    }

    private Optional<DBPlayer> getUserOf(
            final DBGame game,
            final UserId userID)
    {
        return game.getPlayers()
                   .stream()
                   .filter(dbPlayer ->
                                   dbPlayer.getUserId().getIdentifier()
                                           .equals(userID.getIdentifier()))
                   .findFirst();
    }
}

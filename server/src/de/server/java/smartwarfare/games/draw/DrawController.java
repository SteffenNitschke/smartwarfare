package de.server.java.smartwarfare.games.draw;

import de.server.java.smartwarfare.games.data.GameId;
import de.core.network.messages.news.TurnRequest;
import de.core.network.messages.news.turnMessages.TurnMapper;

import static spark.Spark.post;

public class DrawController
{
    private final TurnMapper turnMapper = new TurnMapper();

    public DrawController(final DrawService service)
    {
        post("/draw/:game_id",
             ((request, response) ->
             {
                 final GameId gameId = GameId.of(":game_id");

                 final TurnRequest message = turnMapper.readValue(
                         request.body());

                 if (service.draw(gameId,
                                  message))
                 {
                     response.status(200);
                     response.type("application/json");

                     return message.getGameId();
                 }
                 else
                 {
                     response.status(400);
                 }

                 return response;
             }
             )
        );
    }
}

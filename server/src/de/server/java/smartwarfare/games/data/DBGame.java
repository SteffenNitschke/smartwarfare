package de.server.java.smartwarfare.games.data;

import java.util.HashSet;

import de.server.java.smartwarfare.maps.data.DBBuilding;
import de.server.java.smartwarfare.maps.data.DBMap;
import de.server.java.smartwarfare.user.UserId;
import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import de.core.util.GameStatus;
import de.core.util.GameTypes;

@Entity("games")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DBGame
{
    @Id
    @Embedded
    private GameId _id;

    @Embedded
    private HashSet<DBPlayer> players;

    @Embedded
    private HashSet<DBBuilding> playerBuildings = new HashSet<>();

    @Embedded
    private DBMap map;

    private int round;

    @Embedded
    private UserId currentPlayer;

    private GameTypes gameType;
    private GameStatus status;

    private String chatId;

    private boolean publicGame;

    public DBPlayer nextPlayer(DBPlayer player)
    {
        return this.players.stream()
                .filter(dbPlayer -> dbPlayer.getUserId().getIdentifier()
                                            .equals(player.getNextPlayerId().getIdentifier()))
                .findAny().orElse(null);
    }
}

package de.server.java.smartwarfare.games.data;

import org.bson.types.ObjectId;

import dev.morphia.annotations.Embedded;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Embedded
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(of = {"identifier"})
public class GameId
{
    private String identifier;

    public static GameId of(final String identifier)
    {
        return new GameId(identifier);
    }

    public static GameId random()
    {
        return new GameId(new ObjectId().toHexString());
    }
}

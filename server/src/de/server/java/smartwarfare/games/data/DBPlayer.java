package de.server.java.smartwarfare.games.data;

import com.badlogic.gdx.graphics.Color;

import java.util.HashSet;

import de.server.java.smartwarfare.maps.data.DBUnit;
import de.server.java.smartwarfare.user.SmallUser;
import de.server.java.smartwarfare.user.UserId;
import dev.morphia.annotations.Embedded;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embedded
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class DBPlayer
{
    private UserId userId;

    private String name;

    private Color color;

    @Embedded
    @Setter
    private UserId nextPlayerId;

    @Embedded
    @Setter
    private UserId prevPlayerId;

    private int money;

    @Setter
    private boolean wannaPlayTheGame;

    @Embedded
    private HashSet<DBUnit> troops = new HashSet<>();

    @Embedded
    private HashSet<DBUnit> heroes = new HashSet<>();

    public void updateMoney(final int newValue)
    {
        this.money = newValue;
    }

}

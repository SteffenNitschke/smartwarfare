package de.server.java.smartwarfare.news;

import org.bson.types.ObjectId;

import dev.morphia.annotations.Embedded;
import lombok.*;

@Getter
@Embedded
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(of = {"identifier"})
public class NewsId
{
    private String identifier;

    public static NewsId of(final String identifier)
    {
        return new NewsId(identifier);
    }

    public static NewsId random()
    {
        return new NewsId(new ObjectId().toHexString());
    }
}

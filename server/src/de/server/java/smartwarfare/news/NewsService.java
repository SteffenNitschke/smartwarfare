package de.server.java.smartwarfare.news;

import java.util.List;
import java.util.Observable;

import de.server.java.smartwarfare.exceptions.NewsNotFoundException;
import de.server.java.smartwarfare.user.UserId;
import dev.morphia.Key;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class NewsService extends Observable
{
    private final NewsRepository newsRepository;

    public List<DBNews> getLatestNewsOfPlayer(final UserId userId)
    {
        return newsRepository.findAllNewsByUserIdAndNotReaded(userId);
    }

    public boolean markNewsAsRead(final NewsId newsId) throws NewsNotFoundException
    {
        final DBNews news = newsRepository.findNewsByIdAndNotReaded(newsId);

        if (news == null)
        {
            return false;
        }

        news.markAsReaded();

        newsRepository.save(news);

        return true;
    }

    public Key<DBNews> saveNews(final DBNews news)
    {
        return newsRepository.save(news);
    }
}

package de.server.java.smartwarfare.news;

import com.mongodb.MongoClient;
import com.mongodb.WriteResult;

import java.time.LocalDateTime;
import java.util.List;

import de.server.java.smartwarfare.exceptions.NewsNotFoundException;
import de.server.java.smartwarfare.user.UserId;
import dev.morphia.Datastore;
import dev.morphia.Key;
import dev.morphia.Morphia;

public class NewsRepository
{
    private final Datastore datastore;

    public NewsRepository()
    {
        System.out.println("NewsRepo");
        final Morphia morphia = new Morphia();

        // tell Morphia where to find your classes
        // can be called multiple times with different packages or classes
        morphia.mapPackage("de.server.java.smartwarfare.news");

        // create the Datastore connecting to the default port on the local host
        this.datastore = morphia.createDatastore(new MongoClient(),
                                                 "smartwarfare");
        datastore.ensureIndexes();
    }

    public Key<DBNews> save(final DBNews news)
    {
        return this.datastore.save(news);
    }

    List<DBNews> findAllNewsByUserIdAndNotReaded(final UserId userId)
    {
        List<DBNews> news = null;
        try
        {
            news = this.datastore.createQuery(DBNews.class)
                                 .field("userId.identifier").equal(userId.getIdentifier())
                                 .field("readed").equal(false)
                                 .find().toList();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return news;
    }

    DBNews findNewsByIdAndNotReaded(
            final NewsId id)
            throws NewsNotFoundException
    {
        System.out.println("findNewsByIdAndNotReaded " + id.getIdentifier());

        DBNews news = null;
        try
        {
            System.out.println("try");
            news = datastore.createQuery(DBNews.class)
                            .field("_id.identifier").equal(id.getIdentifier())
                            .field("readed").equal(false)
                            .first();
        }
        catch (final Exception exception)
        {
            System.out.println(exception.getMessage());
            exception.printStackTrace();
        }

        if (news == null)
        {
            throw new NewsNotFoundException();
        }

        System.out.println("return " + news);
        return news;
    }

    public String deleteAllNewsByUserId(final String userId)
    {
        final WriteResult result = datastore.delete(
                datastore.createQuery(DBNews.class).field("userId").equal(userId));

        if (result.wasAcknowledged())
        {
            return "Delete successful";
        }
        else
        {
            return "Delete unsuccessful";
        }
    }

    public void deleteAlleNewsBefore(final LocalDateTime date)
    {
        datastore.delete(
                datastore.createQuery(DBNews.class)
                         .field("createdDate").lessThanOrEq(date)
        );
    }
}

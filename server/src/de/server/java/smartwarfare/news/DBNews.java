package de.server.java.smartwarfare.news;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDateTime;

import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.user.SmallUser;
import de.server.java.smartwarfare.user.UserId;
import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.PrePersist;
import lombok.*;
import de.core.util.NewsType;

@Entity("news")
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class DBNews
{
    @Id
    @Embedded
    private NewsId _id;

    private UserId userId;

    private GameId gameId;

    @Embedded
    private SmallUser sender;

    private boolean answer;

    private boolean readed = false;

    private NewsType type;

    @JsonIgnore
    private LocalDateTime createdDate;

    void markAsReaded()
    {
        readed = true;
    }

    @PrePersist
    void prePersist()
    {
        this.createdDate = LocalDateTime.now();
    }
}

package de.server.java.smartwarfare.news.listener;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Observable;
import java.util.Observer;

import de.server.java.smartwarfare.news.DBNews;
import de.server.java.smartwarfare.news.NewsId;
import de.server.java.smartwarfare.news.NewsRepository;
import de.core.util.NewsType;
import de.server.java.smartwarfare.socket.WebSocketHandler;
import de.server.java.smartwarfare.user.SmallUser;
import de.server.java.smartwarfare.user.UserId;
import de.server.java.smartwarfare.user.events.FriendAnswerEvent;
import de.server.java.smartwarfare.user.events.FriendInvitationEvent;
import de.server.java.smartwarfare.user.friends.pushNotifications.FriendAnswerNotification;
import de.server.java.smartwarfare.user.friends.pushNotifications.FriendInvitationNotification;
import lombok.AllArgsConstructor;
import de.core.network.messages.news.FriendAnswerRequest;

@AllArgsConstructor
public class FriendNewsEventListener implements Observer
{
    private NewsRepository newsRepository;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void update(
            final Observable observable,
            final Object event)
    {
        System.out.println("FriendNewsEventListener " + event);

        if (event instanceof FriendInvitationEvent)
        {
            handleFriendInvitationEvent((FriendInvitationEvent) event);
        }
        else if (event instanceof FriendAnswerEvent)
        {
            handleFriendAnswerEvent((FriendAnswerEvent) event);
        }
    }

    private void handleFriendAnswerEvent(final FriendAnswerEvent event)
    {
        final FriendAnswerRequest message = event.getAnswer();

        final DBNews news = new DBNews(
                NewsId.random(),
                UserId.of(message.getResponder().getId().getIdentifier()),
                null,
                SmallUser.of(UserId.of(message.getSender().getId().getIdentifier()),
                             message.getSender().getName()),
                true,
                false,
                NewsType.FRIEND_ANSWER,
                LocalDateTime.now()
        );

        newsRepository.save(news);

        try
        {
            WebSocketHandler.sendMessageToUser(
                    UserId.of(message.getResponder().getId().getIdentifier()),
                    objectMapper.writeValueAsString(
                            new FriendAnswerNotification(
                                    news.get_id(),
                                    news.getSender().getId(),
                                    true,
                                    NewsType.FRIEND_ANSWER
                            )));
        }
        catch (final IOException exception)
        {
            exception.printStackTrace();
        }
    }

    private void handleFriendInvitationEvent(final FriendInvitationEvent event)
    {
        final DBNews news = event.getNews();

        newsRepository.save(news);

        try
        {
            WebSocketHandler.sendMessageToUser(
                    news.getUserId(),
                    objectMapper.writeValueAsString(
                            new FriendInvitationNotification(
                                    news.get_id(),
                                    news.getSender(),
                                    NewsType.FRIEND_INVITATION
                            )
                    )
            );
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        System.out.println("End of Method");
    }
}

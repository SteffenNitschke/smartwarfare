package de.server.java.smartwarfare.news.listener;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.stream.Collectors;

import de.server.java.smartwarfare.games.data.DBPlayer;
import de.server.java.smartwarfare.games.events.GameCanceledEvent;
import de.server.java.smartwarfare.games.events.GameInvitationAnswerEvent;
import de.server.java.smartwarfare.games.events.GameInvitationEvent;
import de.server.java.smartwarfare.games.events.GameReadyEvent;
import de.server.java.smartwarfare.games.events.TurnEvent;
import de.server.java.smartwarfare.games.pushNotifications.GameCanceledNotification;
import de.server.java.smartwarfare.games.pushNotifications.GameInitializedNotification;
import de.server.java.smartwarfare.games.pushNotifications.GameInvitationAnswerNotification;
import de.server.java.smartwarfare.games.pushNotifications.GameStartedNotification;
import de.server.java.smartwarfare.games.pushNotifications.TurnNotification;
import de.server.java.smartwarfare.news.DBNews;
import de.server.java.smartwarfare.news.NewsId;
import de.server.java.smartwarfare.news.NewsRepository;
import de.core.util.NewsType;
import de.server.java.smartwarfare.socket.WebSocketHandler;
import de.server.java.smartwarfare.user.SmallUser;
import de.server.java.smartwarfare.user.UserId;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GameNewsEventListener implements Observer
{
    private final NewsRepository newsRepository;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void update(
            final Observable observable,
            final Object event)
    {
        if (event instanceof TurnEvent)
        {
            handleTurnEvent((TurnEvent) event);
        }
        else if (event instanceof GameInvitationEvent)
        {
            handleGameInvitationEvent((GameInvitationEvent) event);
        }
        else if (event instanceof GameInvitationAnswerEvent)
        {
            handleGameInvitationAnswerEvent((GameInvitationAnswerEvent) event);
        }
        else if (event instanceof GameReadyEvent)
        {
            handleGameStartedEvent((GameReadyEvent) event);
        }
        else if (event instanceof GameCanceledEvent)
        {
            handleGameCanceledEvent((GameCanceledEvent) event);
        }
    }

    private void handleGameCanceledEvent(final GameCanceledEvent event)
    {
        for (DBPlayer player : event.getPlayer())
        {
            final DBNews news = new DBNews(
                    NewsId.random(),
                    player.getUserId(),
                    event.getGameId(),
                    null,
                    false,
                    false,
                    NewsType.GAME_CANCELLED,
                    LocalDateTime.now()
            );

            try
            {
                WebSocketHandler.sendMessageToUser(
                        player.getUserId(),
                        objectMapper.writeValueAsString(
                                new GameCanceledNotification(
                                        news.get_id(),
                                        event.getGameId(),
                                        NewsType.GAME_CANCELLED
                                )
                        )
                );
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            newsRepository.save(news);
        }
    }

    private void handleGameStartedEvent(final GameReadyEvent gameStartedEvent)
    {
        for (DBPlayer player : gameStartedEvent.getPlayers())
        {
            final DBNews news = new DBNews(
                    NewsId.random(),
                    player.getUserId(),
                    gameStartedEvent.getGameId(),
                    null,
                    false,
                    false,
                    NewsType.GAME_STARTED,
                    LocalDateTime.now()
            );

            try
            {
                WebSocketHandler.sendMessageToUser(
                        player.getUserId(),
                        objectMapper.writeValueAsString(
                                new GameStartedNotification(
                                        news.get_id(),
                                        news.getGameId(),
                                        NewsType.GAME_STARTED
                                )
                        ));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            newsRepository.save(news);
        }
    }

    private void handleGameInvitationAnswerEvent(final GameInvitationAnswerEvent gameInvitationAnswerEvent)
    {
        for (DBPlayer player : gameInvitationAnswerEvent.getPlayers())
        {
            final DBNews news = new DBNews(
                    NewsId.random(),
                    player.getUserId(),
                    gameInvitationAnswerEvent.getGameId(),
                    SmallUser.of(
                            UserId.of(gameInvitationAnswerEvent.getResponder().getId().getIdentifier()),
                            gameInvitationAnswerEvent.getResponder().getName()),
                    false,
                    false,
                    NewsType.GAME_ANSWER,
                    LocalDateTime.now()
            );

            try
            {
                WebSocketHandler.sendMessageToUser(
                        player.getUserId(),
                        objectMapper.writeValueAsString(
                                new GameInvitationAnswerNotification(
                                        news.get_id(),
                                        news.getGameId(),
                                        news.getSender(),
                                        NewsType.GAME_INVITATION_ANSWER
                                )
                        ));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            newsRepository.save(news);
        }

    }

    private void handleGameInvitationEvent(final GameInvitationEvent gameInvitationEvent)
    {
        final Set<UserId> players =
                gameInvitationEvent.getPlayers()
                                   .stream()
                                   .map(DBPlayer::getUserId)
                                   .collect(Collectors.toSet());
        players.remove(gameInvitationEvent.getOperator());

        for (UserId player : players)
        {
            final DBNews news = new DBNews(
                    NewsId.random(),
                    player,
                    gameInvitationEvent.getGameId(),
                    SmallUser.of(gameInvitationEvent.getOperator(),
                                 gameInvitationEvent.getOperatorName()),
                    false,
                    false,
                    NewsType.GAME_INVITATION,
                    LocalDateTime.now()
            );

            try
            {
                WebSocketHandler.sendMessageToUser(
                        player,
                        objectMapper.writeValueAsString(new GameInitializedNotification(
                                news.get_id(),
                                gameInvitationEvent.getGameId(),
                                news.getSender(),
                                NewsType.GAME_INITIALIZED
                        )));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            newsRepository.save(news);
        }
    }

    private void handleTurnEvent(final TurnEvent turn)
    {
        for (UserId player : turn.getPlayers())
        {
            final DBNews news = new DBNews(
                    NewsId.random(),
                    player,
                    turn.getGameId(),
                    SmallUser.of(turn.getSender(),
                                 ""),
                    false,
                    false,
                    NewsType.TURN,
                    LocalDateTime.now()
            );

            try
            {
                WebSocketHandler.sendMessageToUser(
                        player,
                        objectMapper.writeValueAsString(
                                new TurnNotification(
                                        news.get_id(),
                                        news.getGameId(),
                                        news.getSender(),
                                        NewsType.TURN
                                )
                        )
                );
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            newsRepository.save(news);
        }
    }
}

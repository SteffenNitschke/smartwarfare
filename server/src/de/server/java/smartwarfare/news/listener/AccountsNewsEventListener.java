package de.server.java.smartwarfare.news.listener;

import java.util.Observable;
import java.util.Observer;

import de.server.java.smartwarfare.access.events.AccountDeletedEvent;
import de.server.java.smartwarfare.news.NewsRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AccountsNewsEventListener implements Observer
{
    private final NewsRepository newsRepository;

    @Override
    public void update(
            final Observable observable,
            final Object event)
    {
        System.out.println("AccountNewsEventListener " + event);
        if (event instanceof AccountDeletedEvent)
        {
            handleAccountDeletionEvent((AccountDeletedEvent) event);
        }
    }

    private void handleAccountDeletionEvent(final AccountDeletedEvent event)
    {
        newsRepository.deleteAllNewsByUserId(event.getUserId().getIdentifier());
    }
}

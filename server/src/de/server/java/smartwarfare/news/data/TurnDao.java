package de.server.java.smartwarfare.news.data;

import java.util.LinkedList;

import lombok.Data;
import de.core.network.messages.news.turnMessages.TurnMessageInterface;

@Data
public class TurnDao
{
    private String userID;
    private String gameId;
    private LinkedList<TurnMessageInterface> moves = new LinkedList<>();
}

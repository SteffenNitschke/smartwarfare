package de.server.java.smartwarfare.news.web;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

import de.server.java.smartwarfare.access.AccessToken;
import de.server.java.smartwarfare.news.DBNews;
import de.server.java.smartwarfare.news.NewsId;
import de.server.java.smartwarfare.news.NewsService;
import de.server.java.smartwarfare.user.UserId;
import dev.morphia.Key;

import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.put;

public class NewsController
{
    private final ObjectMapper objectMapper = new ObjectMapper();

    public NewsController(final NewsService newsService)
    {
        get("/news",
            (request, response) ->
            {
                final UserId userId = ((AccessToken)
                        request.session().attribute("userId"))
                        .getUserId();

                final List<DBNews> news = newsService.getLatestNewsOfPlayer(userId);

                if (news == null)
                {
                    response.status(400);
                }
                else if (news.isEmpty())
                {
                    response.status(204);
                }
                else
                {
                    response.status(200);
                    response.type("application/json");

                    return objectMapper.writeValueAsString(news);
                }

                return response;
            }
        );

        put("/news/:news_id",
            (request, response) ->
            {
                final NewsId newsId = NewsId.of(request.params(":news_id"));

                if (newsService.markNewsAsRead(newsId))
                {
                    response.status(200);

                    return objectMapper.writeValueAsString(newsId);
                }
                else
                {
                    response.status(400);
                }

                return response;
            }
        );

        post("/news",
             (request, response) ->
             {
                 final DBNews news = objectMapper.readValue(
                         request.body(),
                         DBNews.class);

                 final Key<DBNews> key = newsService.saveNews(news);

                 if (key == null)
                 {
                     response.status(400);
                 }
                 else
                 {
                     response.status(201);
                     response.type("application/json");

                     return key.getId().toString();
                 }

                 return response;
             }
        );
    }
}

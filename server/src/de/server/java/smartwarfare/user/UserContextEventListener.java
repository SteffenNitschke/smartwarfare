package de.server.java.smartwarfare.user;

import java.util.Observable;
import java.util.Observer;

import de.server.java.smartwarfare.access.AccessService;
import de.server.java.smartwarfare.access.events.AccountCreatedEvent;
import de.server.java.smartwarfare.access.events.AccountDeletedEvent;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserContextEventListener implements Observer
{
    private final UserRepository userRepository;

    @Override
    public void update(
            final Observable observable,
            final Object event)
    {
        System.out.println("UserContextEventListener " + event);
        if (event instanceof AccountCreatedEvent) {
            handleAccountCreatedEvent((AccountCreatedEvent) event);
        }
        if (event instanceof AccountDeletedEvent) {
            handleAccountDeletedEvent((AccountDeletedEvent) event);
        }
    }

    private void handleAccountDeletedEvent(final AccountDeletedEvent event)
    {
        userRepository.deleteUserById(event.getUserId());
    }

    private void handleAccountCreatedEvent(final AccountCreatedEvent event)
    {
        System.out.println("handleAccountCreatedEvent " + event);
        final DBUser user = new DBUser();
        user.setName(event.getName());
        user.set_id(event.getUserId());
        System.out.println("DBUser: " + user);

        user.setExperience(0L);
        user.setLevel(1);

        userRepository.save(user);
        System.out.println("UserId: " + user.get_id());
    }
}

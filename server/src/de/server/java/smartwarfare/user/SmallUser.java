package de.server.java.smartwarfare.user;

import dev.morphia.annotations.Embedded;
import lombok.*;

@Embedded
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class SmallUser
{
    private UserId id;
    private String name;
//    private Image image; TODO

    public static SmallUser of(final UserId id, final String name)
    {
        return new SmallUser(id, name);
    }
}

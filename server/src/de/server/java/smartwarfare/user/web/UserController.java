package de.server.java.smartwarfare.user.web;


import com.fasterxml.jackson.databind.ObjectMapper;

import de.server.java.smartwarfare.access.AccessToken;
import de.server.java.smartwarfare.user.DBUser;
import de.server.java.smartwarfare.user.SmallUser;
import de.server.java.smartwarfare.user.UserId;
import de.server.java.smartwarfare.user.UserService;
import de.core.network.messages.news.ChangeUserRequest;

import static spark.Spark.get;
import static spark.Spark.post;

public class UserController
{
    private ObjectMapper objectMapper = new ObjectMapper();

    public UserController(final UserService service)
    {
        post("/users",
             ((request, response) ->
             {
                 final UserId userId =
                         ((AccessToken) request.session()
                                               .attribute("userId"))
                                 .getUserId();

                 final ChangeUserRequest message = objectMapper.readValue(request.body(),
                                                                          ChangeUserRequest.class);
                 final DBUser user = service.changePlayer(
                         userId,
                         message);

                 if (user == null)
                 {
                     response.status(400);
                 }
                 else
                 {
                     response.status(200);
                     response.type("application/json");

                     return objectMapper.writeValueAsString(user);
                 }

                 return response;
             }
             ));

        get("/users",
            (request, response) ->
            {
                final UserId userId =
                        ((AccessToken) request.session()
                                              .attribute("userId"))
                                .getUserId();

                final DBUser user = service.getPlayer(userId);

                if (user == null)
                {
                    response.status(400);
                }
                else
                {
                    response.status(200);
                    response.type("application/json");

                    return objectMapper.writeValueAsString(user);
                }

                return response;
            }
        );

        get("/users/:user_id",
            (request, response) ->
            {
                final UserId userId = UserId.of(request.params(":user_id"));

                final SmallUser user = service.getSmallPlayer(userId);

                if (user == null)
                {
                    response.status(404);
                }
                else
                {
                    response.status(200);
                    return objectMapper.writeValueAsString(user);
                }

                return response;
            });
    }
}

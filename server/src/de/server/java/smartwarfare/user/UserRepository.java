package de.server.java.smartwarfare.user;

import com.mongodb.MongoClient;
import com.mongodb.WriteResult;

import dev.morphia.Datastore;
import dev.morphia.Key;
import dev.morphia.Morphia;

public class UserRepository
{
    private final Datastore datastore;

    public UserRepository()
    {
        System.out.println("UserRepo");
        final Morphia morphia = new Morphia();

        morphia.mapPackage("de.server.java.smartwarfare.user");

        this.datastore = morphia.createDatastore(new MongoClient(), "smartwarfare");
        datastore.ensureIndexes();
    }

    public Key<DBUser> save(final DBUser user)
    {
        return datastore.save(user);
    }

    public DBUser findUserById(final UserId userId)
    {
        return datastore.createQuery(DBUser.class)
                .field("_id.identifier").equal(userId.getIdentifier())
                .first();
    }

    public DBUser findUserByName(final String name)
    {
        return datastore.createQuery(DBUser.class)
                .field("name").equal(name)
                .first();
    }

    WriteResult deleteUserById(final UserId userId)
    {
        return datastore.delete(findUserById(userId));
    }
}

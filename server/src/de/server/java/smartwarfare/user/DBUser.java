package de.server.java.smartwarfare.user;

import de.server.java.smartwarfare.maps.data.DBUnit;
import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Entity("users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DBUser
{
    @Id
    @Embedded
    private UserId _id;

    private String name;

    @Embedded
    private final HashSet<SmallUser> friends = new HashSet<>();

    //TODO use repo for heroes
    private final HashSet<DBUnit> heroes = new HashSet<>();

    //TODO delete?
    private final List<String> chats = new ArrayList<>();

    private int level;
    private long experience;
}

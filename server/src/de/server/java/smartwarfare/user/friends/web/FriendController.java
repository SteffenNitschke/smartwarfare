package de.server.java.smartwarfare.user.friends.web;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.server.java.smartwarfare.access.AccessToken;
import de.server.java.smartwarfare.user.UserId;
import de.server.java.smartwarfare.user.friends.FriendService;
import de.core.network.messages.news.FriendAnswerRequest;

import static spark.Spark.post;

public class FriendController
{
    private ObjectMapper objectMapper = new ObjectMapper();

    public FriendController(final FriendService service)
    {
        post("/users/friends/invite/:recipient_id",
             ((request, response) ->
             {
                 final UserId userId =
                         ((AccessToken) request.session().attribute("userId"))
                                 .getUserId();

                 final UserId recipient = UserId.of(request.params(":recipient_id"));

                 if (service.invite(userId,
                                    recipient))
                 {
                     response.status(204);
                 }
                 else
                 {
                     response.status(400);
                 }

                 return response;
             }
             ));

        post("/users/friends/answer",
             ((request, response) ->
             {
                 final FriendAnswerRequest message = objectMapper.readValue(
                         request.body(),
                         FriendAnswerRequest.class);

                 if (service.answerInvite(message))
                 {
                     response.status(204);
                 }
                 else
                 {
                     response.status(400);
                 }

                 return response;
             }
             ));
    }
}
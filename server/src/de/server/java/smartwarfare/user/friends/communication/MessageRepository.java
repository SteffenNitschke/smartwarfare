package de.server.java.smartwarfare.user.friends.communication;

import com.mongodb.MongoClient;
import com.mongodb.WriteResult;

import java.util.List;

import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.user.UserId;
import dev.morphia.Datastore;
import dev.morphia.Key;
import dev.morphia.Morphia;
import dev.morphia.query.Query;
import dev.morphia.query.Sort;

public class MessageRepository
{
    private final Datastore datastore;

    public MessageRepository()
    {
        System.out.println("ChatRepo");
        final Morphia morphia = new Morphia();

        morphia.mapPackage("de.server.java.smartwarfare.user.friends.communication");

        this.datastore = morphia.createDatastore(new MongoClient(),
                                                 "smartwarfare");
        datastore.ensureIndexes();
    }

    public Key<DBMessage> save(final DBMessage chat)
    {
        return datastore.save(chat);
    }

    List<DBMessage> findAllByUserId(
            final UserId userId)
    {
        System.out.println("findAllMessagesByUserId " + userId.getIdentifier());
        final Query<DBMessage> query = datastore.createQuery(DBMessage.class);

        query.order(Sort.ascending("timestamp"))
             .or(
                     query.criteria("sender.identifier")
                          .equal(userId.getIdentifier()),
                     query.criteria("recipient.identifier")
                          .equal(userId.getIdentifier())

             );
        return query.find().toList();
    }

    List<DBMessage> findAllByGameId(final GameId gameId)
    {
        return datastore.createQuery(DBMessage.class)
                        .order(Sort.ascending("timestamp"))
                        .field("gameId.identifier").equal(gameId.getIdentifier())
                        .find().toList();
    }

    public WriteResult deleteAllByGameId(final GameId gameId)
    {
        return datastore.delete(findAllByGameId(gameId));
    }
}

package de.server.java.smartwarfare.user.friends.communication;

import org.bson.types.ObjectId;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Observable;

import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.user.UserId;
import lombok.RequiredArgsConstructor;
import de.core.network.messages.news.ChatRequest;

@RequiredArgsConstructor
public class MessageService extends Observable
{
    private final MessageRepository messageRepository;

    public List<DBMessage> getMessages(final UserId userId)
    {
        return messageRepository.findAllByUserId(userId);
    }

    public boolean sendMessage(final ChatRequest message)
    {
        messageRepository.save(
                new DBMessage(
                        ObjectId.get().toHexString(),
                        UserId.of(message.getSender().getId().getIdentifier()),
                        UserId.of(message.getRecipient().getId().getIdentifier()),
                        GameId.of(message.getGameId()),
                        LocalDateTime.now(),
                        message.getMessage()
                ));

        return true;
    }

    public List<DBMessage> getMessagesByGameId(final GameId gameId)
    {
        return messageRepository.findAllByGameId(gameId);
    }
}

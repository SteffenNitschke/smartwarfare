package de.server.java.smartwarfare.user.friends.pushNotifications;

import de.server.java.smartwarfare.news.NewsId;
import de.core.util.NewsType;
import de.server.java.smartwarfare.user.UserId;
import lombok.Value;

@Value
public class FriendAnswerNotification
{
    private final NewsId _id;

    private final UserId sender;

    private final boolean answer;

    private final NewsType type;
}

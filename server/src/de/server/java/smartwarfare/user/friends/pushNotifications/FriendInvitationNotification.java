package de.server.java.smartwarfare.user.friends.pushNotifications;

import de.server.java.smartwarfare.news.NewsId;
import de.core.util.NewsType;
import de.server.java.smartwarfare.user.SmallUser;
import lombok.Value;

@Value
public class FriendInvitationNotification
{
    private final NewsId _id;

    private final SmallUser sender;

    private final NewsType type;
}

package de.server.java.smartwarfare.user.friends;

import java.util.Observable;

import de.server.java.smartwarfare.news.DBNews;
import de.server.java.smartwarfare.news.NewsId;
import de.core.util.NewsType;
import de.server.java.smartwarfare.user.DBUser;
import de.server.java.smartwarfare.user.SmallUser;
import de.server.java.smartwarfare.user.UserId;
import de.server.java.smartwarfare.user.UserRepository;
import de.server.java.smartwarfare.user.events.FriendAnswerEvent;
import de.server.java.smartwarfare.user.events.FriendInvitationEvent;
import lombok.RequiredArgsConstructor;
import de.core.network.messages.news.FriendAnswerRequest;

@RequiredArgsConstructor
public class FriendService extends Observable
{
    private final UserRepository userRepository;

    public boolean invite(
            final UserId senderId,
            final UserId recipientId)
    {
        final DBUser sender = userRepository.findUserById(senderId);

        if (sender == null)
        {
            return false;
        }

        final DBNews news = new DBNews(
                NewsId.random(),
                recipientId,
                null,
                SmallUser.of(sender.get_id(),
                             sender.getName()),
                false,
                false,
                NewsType.FRIEND_INVITATION,
                null
        );

        setChanged();
        notifyObservers(new FriendInvitationEvent(news));

        return true;
    }

    public boolean answerInvite(final FriendAnswerRequest answer)
    {
        System.out.println("answerFriendInvite");

        final DBUser sender = userRepository.findUserById(UserId.of(answer.getSender().getId()
                                                                          .getIdentifier()));

        final DBUser responder = userRepository.findUserById(UserId.of(answer.getResponder().getId()
                                                                             .getIdentifier()));

        if (sender == null || responder == null)
        {
            return false;
        }

        if (answer.isWannaBeFriends())
        {
            sender.getFriends().add(SmallUser.of(responder.get_id(),
                                                 responder.getName()));
            responder.getFriends().add(SmallUser.of(sender.get_id(),
                                                    sender.getName()));

            setChanged();
            notifyObservers(new FriendAnswerEvent(answer));
        }

        userRepository.save(sender);
        userRepository.save(responder);

        return true;
    }
}

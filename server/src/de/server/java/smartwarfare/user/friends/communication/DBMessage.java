package de.server.java.smartwarfare.user.friends.communication;

import java.time.LocalDateTime;

import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.user.UserId;
import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Field;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Index;
import dev.morphia.annotations.Indexes;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity("message")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Indexes(
        @Index(fields = {
                @Field("sender"),
                @Field("recipient"),
                @Field("timestamp")
        }))
public class DBMessage
{
    @Id
    private String id;

    @Embedded
    private UserId sender;

    @Embedded
    private UserId recipient;

    @Embedded
    private GameId gameId;

    private LocalDateTime timestamp;
    private String message;
}

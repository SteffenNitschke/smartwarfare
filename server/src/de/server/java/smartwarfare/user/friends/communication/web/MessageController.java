package de.server.java.smartwarfare.user.friends.communication.web;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

import de.server.java.smartwarfare.access.AccessToken;
import de.server.java.smartwarfare.games.data.GameId;
import de.server.java.smartwarfare.user.UserId;
import de.server.java.smartwarfare.user.friends.communication.DBMessage;
import de.server.java.smartwarfare.user.friends.communication.MessageService;
import de.core.network.messages.news.ChatRequest;

import static spark.Spark.get;
import static spark.Spark.post;

public class MessageController
{
    private ObjectMapper objectMapper = new ObjectMapper();

    public MessageController(MessageService messageService)
    {
        get("/messages/:player_id",
            ((request, response) ->
            {
                final UserId userId = ((AccessToken)
                        request.session()
                               .attribute("userId"))
                        .getUserId();

                final List<DBMessage> messages = messageService.getMessages(userId);

                if (messages == null)
                {
                    response.status(400);
                }
                else if (messages.isEmpty())
                {
                    response.status(204);
                }
                else
                {
                    response.status(200);
                    response.type("application/json");

                    return objectMapper.writeValueAsString(messages);
                }

                return response;
            }
            ));

        post("/messages",
             ((request, response) ->
             {
                 final ChatRequest message = objectMapper.readValue(request.body(),
                                                                    ChatRequest.class);
                 if (messageService.sendMessage(message))
                 {
                     response.status(201);
                 }
                 else
                 {
                     response.status(400);
                 }

                 return response;
             }));

        get("/messages/games/:game_id",
            (request, response) ->
            {
                final GameId gameId = GameId.of(request.params(":game_id"));

                final List<DBMessage> messages = messageService.getMessagesByGameId(gameId);

                if(messages == null)
                {
                    response.status(400);
                }
                else if (messages.isEmpty())
                {
                    response.status(204);
                }
                else
                {
                    response.status(200);
                    response.type("application/json");

                    return objectMapper.writeValueAsString(messages);
                }

                return response;
            });
    }
}

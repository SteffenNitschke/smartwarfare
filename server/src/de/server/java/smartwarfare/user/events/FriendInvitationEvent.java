package de.server.java.smartwarfare.user.events;

import de.server.java.smartwarfare.news.DBNews;
import lombok.Value;

@Value
public class FriendInvitationEvent
{
    private final DBNews news;
}

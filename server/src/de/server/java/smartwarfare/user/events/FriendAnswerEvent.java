package de.server.java.smartwarfare.user.events;

import lombok.Value;
import de.core.network.messages.news.FriendAnswerRequest;

@Value
public class FriendAnswerEvent
{
    private final FriendAnswerRequest answer;
}

package de.server.java.smartwarfare.user.events;


import de.server.java.smartwarfare.user.UserId;
import lombok.Getter;

public class ChangePasswordEvent
{
    @Getter
    private final String newPassword;

    @Getter
    private final UserId userId;

    public ChangePasswordEvent(final UserId userId, final String password)
    {
        this.newPassword = password;
        this.userId = userId;
    }
}

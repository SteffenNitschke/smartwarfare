package de.server.java.smartwarfare.user;

import org.bson.types.ObjectId;

import dev.morphia.annotations.Embedded;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Embedded
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(of = {"identifier"})
public class UserId
{
    private String identifier;

    public static UserId of(final String identifier)
    {
        return new UserId(identifier);
    }

    public static UserId random()
    {
        return new UserId(new ObjectId().toHexString());
    }
}

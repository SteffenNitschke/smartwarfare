package de.server.java.smartwarfare.user;

import java.util.Observable;

import de.server.java.smartwarfare.user.events.ChangePasswordEvent;
import lombok.RequiredArgsConstructor;
import de.core.network.messages.news.ChangeUserRequest;

@RequiredArgsConstructor
public class UserService extends Observable
{
    private final UserRepository userRepository;

    public DBUser changePlayer(final UserId userId, final ChangeUserRequest message)
    {
        System.out.println("changePlayer: " + message);

        if (message.getPassword() != null)
        {
            setChanged();
            notifyObservers(new ChangePasswordEvent(userId, message.getPassword()));
        }

        //TODO change UserName

        return userRepository.findUserById(userId);
    }

    public DBUser getPlayer(final UserId id)
    {
        return userRepository.findUserById(id);
    }

    public SmallUser getSmallPlayer(final UserId userId)
    {
        final DBUser user = userRepository.findUserById(userId);

        if(user == null)
        {
            return null;
        }

        return SmallUser.of(user.get_id(), user.getName());
    }
}

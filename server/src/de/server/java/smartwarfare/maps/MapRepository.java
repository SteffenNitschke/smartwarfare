package de.server.java.smartwarfare.maps;

import com.mongodb.MongoClient;
import com.mongodb.WriteResult;

import java.util.HashSet;

import de.server.java.smartwarfare.maps.data.DBMap;
import de.server.java.smartwarfare.maps.data.MapId;
import dev.morphia.Datastore;
import dev.morphia.Key;
import dev.morphia.Morphia;

public class MapRepository
{
    private final Datastore datastore;

    public MapRepository()
    {
        System.out.println("MapRepo");
        final Morphia morphia = new Morphia();

        morphia.mapPackage("de.server.java.smartwarfare.maps");

        this.datastore = morphia.createDatastore(new MongoClient(), "smartwarfare");
        datastore.ensureIndexes();
    }

    public Key<DBMap> save(final DBMap map)
    {
        return datastore.save(map);
    }

    public DBMap findMapById(final MapId id)
    {
        return datastore.createQuery(DBMap.class)
                .field("_id.identifier").equal(id.getIdentifier())
                .first();
    }

    public WriteResult deleteMapById(final MapId id)
    {
        return datastore.delete(findMapById(id));
    }

    //for the case when someone choose a Map for Game
    public HashSet<DBMap> findAllMaps()
    {
        return new HashSet<>(datastore.createQuery(DBMap.class).find().toList());
    }

}

package de.server.java.smartwarfare.maps;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;

import de.server.java.smartwarfare.maps.data.DBField;
import de.server.java.smartwarfare.maps.data.DBMap;
import de.core.util.FieldType;


public class MapImageCreator
{
    public static File generate(final DBMap map) throws Exception
    {
        int PIX_SIZE = 1;

        final int X = map.getMaxX();
        final int Y = map.getMaxY();
        final BufferedImage bi = new BufferedImage(PIX_SIZE * X,
                                                   PIX_SIZE * Y,
                                                   BufferedImage.TYPE_3BYTE_BGR);
        final Graphics2D g = (Graphics2D) bi.getGraphics();

        for (DBField field : map.getFields())
        {
            final int x = field.getX() * PIX_SIZE;
            final int y = field.getY() * PIX_SIZE;

            g.setColor(getColorByType(field.getType()));

            g.fillRect(y,
                       x,
                       PIX_SIZE,
                       PIX_SIZE);
        }

        g.dispose();
        return saveToFile(bi,
                          File.createTempFile(
                                  "temp",
                                  map.get_id().getIdentifier() + "_img.jpg"));
    }

    //end method /** Saves jpeg to file * */
    private static File saveToFile(
            final BufferedImage img,
            final File file) throws IOException
    {
        Iterator iter = ImageIO.getImageWritersByFormatName("jpg");
        if (iter.hasNext())
        {
            final ImageWriter writer = (ImageWriter) iter.next();

            final ImageOutputStream ios = ImageIO.createImageOutputStream(file);
            writer.setOutput(ios);
            final ImageWriteParam param = new JPEGImageWriteParam(java.util.Locale.getDefault());
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(0.98f);
            writer.write(null,
                         new IIOImage(img,
                                      null,
                                      null),
                         param);
        }

        return file;
    }

    private static Color getColorByType(final FieldType type)
    {
        switch (type)
        {
            case FIELD_TYPE_BEACH:
                return Color.YELLOW;
            case FIELD_TYPE_HILL:
                return Color.GREEN;
            case FIELD_TYPE_GRASS:
                return Color.LIGHT_GRAY;
            case FIELD_TYPE_WATER:
                return Color.BLUE;
            case FIELD_TYPE_FOREST:
                return Color.getHSBColor(0,
                                         153,
                                         0);
            case FIELD_TYPE_STREET:
                return Color.LIGHT_GRAY;
            case FIELD_TYPE_BUILDING:
                return Color.DARK_GRAY;
            case FIELD_TYPE_MOUNTAIN:
                return Color.GRAY;
            default:
                return Color.WHITE;
        }
    }
}

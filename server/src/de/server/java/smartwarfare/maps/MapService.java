package de.server.java.smartwarfare.maps;

import java.io.File;
import java.util.HashSet;
import java.util.Observable;

import de.server.java.smartwarfare.maps.data.DBMap;
import de.server.java.smartwarfare.maps.data.MapId;
import dev.morphia.Key;
import lombok.RequiredArgsConstructor;
import de.core.network.messages.SmallMapMessage;

@RequiredArgsConstructor
public class MapService extends Observable
{
    private final MapRepository mapRepository;

    public Key<DBMap> addMap(final DBMap dBMap)
    {
        System.out.println("MapService::addMap " + dBMap);

        final Key<DBMap> maiKey = mapRepository.save(dBMap);

        File image = null;
        try
        {
            image = MapImageCreator.generate(dBMap);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        setChanged();
        notifyObservers(new MapCreatedEvent(image, dBMap.get_id()));

        return maiKey;
    }

    public DBMap getMap(final MapId id)
    {
        return mapRepository.findMapById(id);
    }

    public HashSet<SmallMapMessage> getAllMaps()
    {
        System.out.println("Map Service Get All Maps");
        final HashSet<DBMap> maps = mapRepository.findAllMaps();
        System.out.println("Find " + maps.size() + " Maps.");
        final HashSet<SmallMapMessage> returnMaps = new HashSet<>();

        for (DBMap map : maps) {
            System.out.println("Map Map " + map);
            final SmallMapMessage dto = new SmallMapMessage(
                    map.get_id().getIdentifier(),
                    map.getNumberOfPlayers(),
                    map.getName(),
                    map.getEditorName(),
                    map.getMaxX(),
                    map.getMaxY()
            );
            returnMaps.add(dto);
        }

        return returnMaps;
    }
}

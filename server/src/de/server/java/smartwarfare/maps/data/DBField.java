package de.server.java.smartwarfare.maps.data;

import dev.morphia.annotations.Embedded;
import lombok.Data;
import de.core.util.FieldType;


@Data
public class DBField
{
    private FieldType type;
    private int x;
    private int y;
    private int defence;
    private String fieldID;
    private String unit;

    @Embedded
    private DBBuilding building;
}

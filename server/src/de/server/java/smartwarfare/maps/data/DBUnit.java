package de.server.java.smartwarfare.maps.data;

import de.server.java.smartwarfare.user.UserId;
import dev.morphia.annotations.Embedded;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;

@Embedded
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class DBUnit
{
    @Setter
    private String unitId;
    @Setter
    private String type;
    private int attack;
    private int defence;

    @Setter
    private double health;

    @Setter
    private String field;

    @Setter
    private int ammo;

    @Setter
    private int fuel;

    private int range;
    private int minFireRange;
    private int maxFireRange;
    private HashSet<String> possibleFieldTypes;
    private HashSet<String> possibleTargets;

    @Embedded
    @Setter
    private UserId player;
}

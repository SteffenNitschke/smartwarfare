package de.server.java.smartwarfare.maps.data;

import org.bson.types.ObjectId;

import dev.morphia.annotations.Embedded;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Embedded
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(of = {"identifier"})
public class MapId
{
    private String identifier;

    public static MapId of(final String identifier)
    {
        return new MapId(identifier);
    }

    public static MapId random()
    {
        return new MapId(new ObjectId().toHexString());
    }
}

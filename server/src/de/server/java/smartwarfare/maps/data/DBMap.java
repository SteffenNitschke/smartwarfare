package de.server.java.smartwarfare.maps.data;

import de.server.java.smartwarfare.games.data.GameId;
import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;

@Entity("maps")
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class DBMap
{
    @Id
    @Embedded
    private MapId _id;

    @Embedded
    @Setter
    private GameId game;

    @Embedded
    private HashSet<DBField> fields;

    //for mapping player with units
    @Embedded
    private HashSet<DBUnit> units;
    private String name;
    private int maxY;
    private int maxX;
    private String editorName;
    private int numberOfPlayers;
}

package de.server.java.smartwarfare.maps.data;

import de.server.java.smartwarfare.user.UserId;
import dev.morphia.annotations.Embedded;
import lombok.Data;

@Embedded
@Data
public class DBBuilding
{
    private String _id;

    @Embedded
    private UserId user;
    private String type;
    //value between 0-100
    private int captureValue;
    @Embedded
    private UserId wannerCapture;
}

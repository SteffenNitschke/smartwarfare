package de.server.java.smartwarfare.maps.web;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashSet;

import de.server.java.smartwarfare.maps.MapService;
import de.server.java.smartwarfare.maps.data.DBMap;
import de.server.java.smartwarfare.maps.data.MapId;
import dev.morphia.Key;
import de.core.network.messages.SmallMapMessage;

import static spark.Spark.get;
import static spark.Spark.post;

public class MapController
{
    private ObjectMapper objectMapper = new ObjectMapper();

    public MapController(final MapService service)
    {
        post("/maps",
             ((request, response) ->
             {
                 final DBMap map = objectMapper.readValue(request.body(),
                                                          DBMap.class);

                 final Key<DBMap> key = service.addMap(map);

                 if (key == null)
                 {
                     response.status(400);
                 }
                 else
                 {
                     response.status(200);
                     response.type("application/json");

                     return objectMapper.writeValueAsString(key.getId());
                 }

                 return response;
             }));

        get("/maps/:map_id",
            ((request, response) ->
            {
                final MapId mapId = MapId.of(request.params(":map_id"));

                final DBMap map = service.getMap(mapId);

                if (map == null)
                {
                    response.status(400);
                }
                else
                {
                    response.status(200);
                    response.type("application/json");

                    return objectMapper.writeValueAsString(map);
                }

                return response;
            }));

        get("/maps",
            ((request, response) ->
            {
                System.out.println("Map Controller getAllAMaps");
                final HashSet<SmallMapMessage> maps = service.getAllMaps();

                if (maps == null || maps.isEmpty())
                {
                    response.status(400);
                }
                else
                {
                    response.status(200);
                    response.type("application/json");

                    return objectMapper.writeValueAsString(maps);
                }

                return response;
            }));
    }
}

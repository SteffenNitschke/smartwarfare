package de.server.java.smartwarfare.maps;

import java.io.File;

import de.server.java.smartwarfare.maps.data.MapId;
import lombok.Value;

@Value
public class MapCreatedEvent
{
    private final File image;
    private final MapId id;
}

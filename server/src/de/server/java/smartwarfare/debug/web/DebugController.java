package de.server.java.smartwarfare.debug.web;

import de.server.java.smartwarfare.debug.DebugService;

import static spark.Spark.post;

public class DebugController
{
    public DebugController(final DebugService service)
    {
        post("/resetDb",
             (request, response) -> service.resetDB()
        );

        post("/generateMapImages",
             (request, response) -> service.generateMapImages());
    }
}

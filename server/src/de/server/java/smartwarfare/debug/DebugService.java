package de.server.java.smartwarfare.debug;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoIterable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashSet;
import java.util.Observable;

import de.server.java.smartwarfare.access.AccessRepository;
import de.server.java.smartwarfare.files.DBMapImage;
import de.server.java.smartwarfare.files.mapImages.MapImageService;
import de.server.java.smartwarfare.maps.MapCreatedEvent;
import de.server.java.smartwarfare.maps.MapImageCreator;
import de.server.java.smartwarfare.maps.MapRepository;
import de.server.java.smartwarfare.maps.data.DBMap;
import de.server.java.smartwarfare.user.UserRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DebugService extends Observable
{
    private ResetData resetData;

    private final UserRepository userRepository;
    private final AccessRepository accessRepository;
    private final MapRepository mapRepository;
    private final MapImageService mapImageService;

    public String resetDB()
    {
        String returnValue = reset();

        if (this.resetData == null) {
            resetData = new ResetData(
                    userRepository,
                    accessRepository,
                    mapRepository);
        }

        resetData.fill();

        return returnValue;
    }

    private static MongoClient client;
    private static Logger logger = LoggerFactory.getLogger(DebugService.class);

    protected static ObjectMapper objectMapper = new ObjectMapper();

    private static MongoClient getInstance()
    {
        logger.debug("getInstance");
        System.out.println("getInstance");
        return client == null ? client = new MongoClient("localhost", 27017) : client;
    }

    private static String reset()
    {
        System.out.println("resetDB");
        MongoIterable<String> dbs = getInstance().listDatabaseNames();
        for (String db :
                dbs) {
            System.out.println("Database Name: " + db);
            if (!db.equals("admin")) {
                getInstance().getDatabase(db).drop();
            }
        }
        System.out.println("Reset Database.");
        return "Reset Database";
    }

    public String generateMapImages()
    {
        final HashSet<DBMap> maps = mapRepository.findAllMaps();

        for (DBMap map : maps)
        {
            final DBMapImage mapImage = mapImageService.getMapImage(map.get_id());
            if(mapImage == null)
            {
                File image = null;
                try
                {
                    image = MapImageCreator.generate(map);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                setChanged();
                notifyObservers(new MapCreatedEvent(image, map.get_id()));
            }
        }

        return "ready";
    }
}

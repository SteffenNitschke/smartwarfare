package de.server.java.smartwarfare.debug;

import org.bson.types.ObjectId;

import java.util.HashSet;
import java.util.Random;

import de.server.java.smartwarfare.access.AccessRepository;
import de.server.java.smartwarfare.access.DBLogin;
import de.server.java.smartwarfare.access.Role;
import de.server.java.smartwarfare.maps.MapRepository;
import de.server.java.smartwarfare.maps.data.DBBuilding;
import de.server.java.smartwarfare.maps.data.DBField;
import de.server.java.smartwarfare.maps.data.DBMap;
import de.server.java.smartwarfare.maps.data.MapId;
import de.server.java.smartwarfare.user.DBUser;
import de.server.java.smartwarfare.user.SmallUser;
import de.server.java.smartwarfare.user.UserId;
import de.server.java.smartwarfare.user.UserRepository;
import lombok.RequiredArgsConstructor;
import de.core.util.FieldType;
import de.core.util.Utils;

@RequiredArgsConstructor
class ResetData
{
    private final UserRepository userRepository;
    private final AccessRepository accessRepository;
    private final MapRepository mapRepository;

    void fill()
    {
        createAdminUser();
        createDefaultMaps();
        createDefaultPlayer();
    }

    private void createDefaultPlayer()
    {
        final DBUser tim = new DBUser();
        tim.setName("Tim");
        tim.set_id(UserId.random());

        userRepository.save(tim);

        DBLogin login = new DBLogin(
                new ObjectId().toHexString(),
                tim.getName(),
                "Koch",
                tim.get_id(),
                null,
                Role.USER
        );
        accessRepository.save(login);

        //---------------------------------------

        final DBUser annisa = new DBUser();
        annisa.setName("Annisa");
        annisa.set_id(UserId.random());

        userRepository.save(annisa);

        login = new DBLogin(
                new ObjectId().toHexString(),
                annisa.getName(),
                "Mustermann",
                annisa.get_id(),
                null,
                Role.USER
        );
        accessRepository.save(login);

        //---------------------------------------

        final DBUser seb = new DBUser();
        seb.setName("Sebastian");
        seb.set_id(UserId.random());

        userRepository.save(seb);

        login = new DBLogin(
                new ObjectId().toHexString(),
                seb.getName(),
                "Copei",
                seb.get_id(),
                null,
                Role.USER
        );

        accessRepository.save(login);

        //---------------------------------------

        DBUser steffen = userRepository.findUserByName("Steffen");

        steffen.getFriends().add(SmallUser.of(annisa.get_id(),
                                              annisa.getName()));
        steffen.getFriends().add(SmallUser.of(seb.get_id(),
                                              seb.getName()));
        steffen.getFriends().add(SmallUser.of(tim.get_id(),
                                              tim.getName()));
        userRepository.save(steffen);

        seb.getFriends().add(SmallUser.of(steffen.get_id(),
                                          steffen.getName()));
        seb.getFriends().add(SmallUser.of(tim.get_id(),
                                          tim.getName()));
        seb.getFriends().add(SmallUser.of(annisa.get_id(),
                                          annisa.getName()));
        userRepository.save(seb);

        tim.getFriends().add(SmallUser.of(steffen.get_id(),
                                          steffen.getName()));
        tim.getFriends().add(SmallUser.of(seb.get_id(),
                                          seb.getName()));
        tim.getFriends().add(SmallUser.of(annisa.get_id(),
                                          annisa.getName()));
        userRepository.save(tim);

        annisa.getFriends().add(SmallUser.of(steffen.get_id(),
                                             steffen.getName()));
        annisa.getFriends().add(SmallUser.of(tim.get_id(),
                                             tim.getName()));
        annisa.getFriends().add(SmallUser.of(seb.get_id(),
                                             seb.getName()));
        userRepository.save(annisa);

        System.out.println("Reset Data finish");
    }

    private void createDefaultMaps()
    {

        for (int k = 0; k < 5; k++)
        {

            HashSet<DBField> fields = new HashSet<>();
            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    DBField field = new DBField();
                    field.setX(x);
                    field.setY(y);
                    field.setFieldID(field.getX() + "" + field.getY());
                    field.setType(getRandomFieldType());
                    field.setDefence(Utils.getDefenceFromType(field.getType()));
                    fields.add(field);
                    if (field.getType().equals(FieldType.FIELD_TYPE_BUILDING))
                    {
                        DBBuilding building = new DBBuilding();
                        building.setCaptureValue(20);
                        building.setType(Utils.BUILDING_TYPE_CITY);
                        field.setBuilding(building);
                    }
                }
            }
            final DBMap dbMap = new DBMap(
                    MapId.random(),
                    null,
                    fields,
                    new HashSet<>(),
                    "TestMap" + k,
                    10,
                    10,
                    "Steffen",
                    2
            );

            mapRepository.save(dbMap);
        }
    }

    private void createAdminUser()
    {
        final DBUser admin = new DBUser();
        admin.setName("Steffen");
        admin.set_id(UserId.random());

        userRepository.save(admin);

        admin.set_id(admin.get_id());

        final DBLogin adminLogin = new DBLogin(
                new ObjectId().toHexString(),
                admin.getName(),
                "Nitschke",
                admin.get_id(),
                null,
                Role.ADMIN
        );

        accessRepository.save(adminLogin);
    }

    private static FieldType getRandomFieldType()
    {
        return FieldType.values()[new Random().nextInt(FieldType.values().length)];
    }
}

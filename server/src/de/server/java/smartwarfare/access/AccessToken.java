package de.server.java.smartwarfare.access;

import java.time.LocalDateTime;
import java.util.Random;

import de.server.java.smartwarfare.user.UserId;
import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.PrePersist;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Embedded
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class AccessToken
{
    private String token;

    private LocalDateTime createdDate;

    private Role role;

    @Embedded
    private UserId userId;

    static AccessToken of(final DBLogin dbLogin)
    {
        return new AccessToken(
                generateToken(),
                LocalDateTime.now(),
                dbLogin.getRole(),
                dbLogin.getUserId()
                );
    }

    @PrePersist
    void prePersist()
    {
        this.createdDate = LocalDateTime.now();
    }

    private static String generateToken()
    {
        final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String lower = upper.toLowerCase();
        final String numbers = "0123456789";
        final String alphabet = upper + lower + numbers;

        System.out.println("generate Security Token.");

        Random random = new Random();
        StringBuilder generatedString = new StringBuilder();
        for (int i = 0; i < 64; i++) {
            generatedString.append(alphabet.charAt(random.nextInt(alphabet.length())));
        }

        System.out.println("Token: " + generatedString);
        return generatedString.toString();
    }
}

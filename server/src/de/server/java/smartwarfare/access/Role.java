package de.server.java.smartwarfare.access;

public enum Role
{
    ADMIN, USER
}

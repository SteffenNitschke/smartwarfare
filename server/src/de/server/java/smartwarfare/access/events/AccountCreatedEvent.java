package de.server.java.smartwarfare.access.events;

import de.server.java.smartwarfare.user.UserId;
import lombok.Value;

@Value
public class AccountCreatedEvent
{
    private final String name;
    private final UserId userId;
}

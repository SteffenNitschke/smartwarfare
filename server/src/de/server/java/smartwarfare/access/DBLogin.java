package de.server.java.smartwarfare.access;

import de.server.java.smartwarfare.user.UserId;
import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity("login")
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class DBLogin
{
    @Id
    private String _id;

    private String name;

    private String password;

    @Embedded
    private UserId userId;

    @Embedded
    private AccessToken token;

    private Role role;

    void removeToken()
    {
        this.token = null;
    }

    void setToken(final AccessToken token)
    {
        this.token = token;
    }

    void updatePassword(final String newPassword)
    {
        this.password = newPassword;
    }
}

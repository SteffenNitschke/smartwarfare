package de.server.java.smartwarfare.access;

import org.bson.types.ObjectId;

import java.util.Observable;

import de.server.java.smartwarfare.access.events.AccountCreatedEvent;
import de.server.java.smartwarfare.access.events.AccountDeletedEvent;
import de.server.java.smartwarfare.user.UserId;
import lombok.RequiredArgsConstructor;
import de.core.network.messages.LoginRequest;
import de.core.network.messages.news.turnMessages.RegistrateRequest;
import sun.misc.BASE64Decoder;

@RequiredArgsConstructor
public class AccessService extends Observable
{
    private final AccessRepository accessRepository;

    public boolean createPlayer(RegistrateRequest message)
    {
        System.out.println("createPlayer: " + message);

        if (userNameDoesNotExist(message.getUserName()))
        {
            System.out.println("Name does exist.");
            return false;
        }

        final DBLogin login = new DBLogin(
                new ObjectId().toHexString(),
                message.getUserName(),
                message.getPassword(),
                UserId.random(),
                null,
                Role.USER
        );

        accessRepository.save(login);

        setChanged();
        notifyObservers(new AccountCreatedEvent(login.getName(),
                                                login.getUserId()));

        System.out.println("DBLogin: " + login);

        return true;
    }

    private boolean userNameDoesNotExist(final String name)
    {
        final DBLogin user = accessRepository.findByUserName(name);
        return user != null;
    }

    public boolean logout(final String header)
    {
        try
        {
            System.out.println("logout " + header);

            final DBLogin login = accessRepository.findByToken(header.substring("Bearer ".length()));

            login.removeToken();

            accessRepository.save(login);

            return true;
        } catch (Exception e)
        {
            e.printStackTrace();

            return false;
        }
    }

    public AccessToken authenticate(final String bearerToken)
    {
        return accessRepository.findByToken(bearerToken.substring("Bearer ".length())).getToken();
    }

    public LoginRequest authorization(final String header)
    {
        System.out.println("authorization");
        final String auth = header.substring("Basic ".length());

        try
        {
            byte[] message = new BASE64Decoder().decodeBuffer(auth);

            String messageStr = new String(message);
            String[] user_password = messageStr.split(":");

            final DBLogin login = accessRepository.findByUserName(user_password[0]);

            System.out.println(login.getName());
            if (user_password[1].equals(login.getPassword()))
            {
                login.setToken(AccessToken.of(login));
                accessRepository.save(login);

                final LoginRequest loginRequest = new LoginRequest();
                loginRequest.setUserId(login.getUserId().getIdentifier());
                loginRequest.setToken(login.getToken().getToken());
                loginRequest.setName(login.getName());

                return loginRequest;
            }

            return null;
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public boolean deleteAccount(final UserId userId)
    {
        if (accessRepository.deleteLoginByUserId(userId).wasAcknowledged())
        {
            setChanged();
            notifyObservers(new AccountDeletedEvent(userId));

            return true;
        }
        return false;
    }
}

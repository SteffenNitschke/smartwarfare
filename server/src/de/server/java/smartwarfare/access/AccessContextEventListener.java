package de.server.java.smartwarfare.access;

import java.util.Observable;
import java.util.Observer;

import de.server.java.smartwarfare.user.events.ChangePasswordEvent;
import de.server.java.smartwarfare.user.UserService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AccessContextEventListener implements Observer
{
    private final AccessRepository accessRepository;

    @Override
    public void update(Observable observable, Object o)
    {
        if(observable instanceof UserService && o instanceof ChangePasswordEvent)
        {
            final ChangePasswordEvent event = (ChangePasswordEvent) o;

            final DBLogin login = accessRepository.findByUserId(event.getUserId());

            login.updatePassword(event.getNewPassword());

            accessRepository.save(login);
        }
    }
}

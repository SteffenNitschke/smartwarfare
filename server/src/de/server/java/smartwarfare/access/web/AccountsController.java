package de.server.java.smartwarfare.access.web;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.server.java.smartwarfare.access.AccessService;
import de.server.java.smartwarfare.access.AccessToken;
import de.server.java.smartwarfare.user.UserId;
import de.core.network.messages.LoginRequest;
import de.core.network.messages.news.turnMessages.RegistrateRequest;
import de.core.util.Utils;

import static spark.Spark.before;
import static spark.Spark.delete;
import static spark.Spark.halt;
import static spark.Spark.post;

/**
 * handle POST /account/registrate
 * handle POST /accounts/login
 * handle POST /accounts/logout
 * handle DELETE /accounts/:player_id
 * handle before-filter
 */
public class AccountsController
{
    private final ObjectMapper objectMapper = new ObjectMapper();

    public AccountsController(final AccessService service)
    {
        before("/*",
               (req, res) ->
               {
                   String path = req.pathInfo();
                   System.out.println("Pfad: " + path);
                   if (!(path.equals("/accounts/login")
                           || path.equals("/resetDb")
                           || path.equals("/accounts/registrate")
                           || path.startsWith("/session")
                   ))
                   {
                       final String token = req.headers(Utils.AUTHENTICATION_HEADER);

                       final AccessToken accessToken = service.authenticate(token);

                       if (accessToken == null)
                       {
                           halt(401);
                       }
                       else
                       {
                           req.session().attribute("userId",
                                                   accessToken);
                       }
                   }

               });

        post("/accounts/registrate",
             (request, response) ->
             {
                 final RegistrateRequest message = objectMapper.readValue(request.body(),
                                                                          RegistrateRequest.class);

                 if (service.createPlayer(message))
                 {
                     response.status(201);
                 }
                 else
                 {
                     response.status(400);
                 }

                 return response;
             }
        );

        post("/accounts/login",
             (request, response) ->
             {
                 final String header = request.headers(Utils.AUTHENTICATION_HEADER);

                 final LoginRequest login = service.authorization(header);

                 if (login == null)
                 {
                     response.status(401);
                 }
                 else
                 {
                     response.status(200);
                     response.type("application/json");

                     return objectMapper.writeValueAsString(login);
                 }

                 return response;
             });

        post("/accounts/logout",
             (request, response) ->
             {
                 final String token = request.headers(Utils.AUTHENTICATION_HEADER);

                 if (service.logout(token))
                 {
                     response.status(200);
                 }
                 else
                 {
                     response.status(400);
                 }

                 return response;
             });

        delete("/accounts/:player_id",
               (request, response) ->
               {
                   final UserId userId = UserId.of(request.params(":player_id"));

                   if (((AccessToken) request.session().attribute("userId")).getUserId()
                                                                            .getIdentifier()
                                                                            .equals(userId.getIdentifier()))
                   {
                       if (service.deleteAccount(userId))
                       {
                           response.status(200);
                       }
                       else
                       {
                           response.status(400);
                       }
                   }
                   else
                   {
                       response.status(401);
                   }

                   return response;
               });
    }
}

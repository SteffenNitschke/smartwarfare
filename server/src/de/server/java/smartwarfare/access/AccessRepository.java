package de.server.java.smartwarfare.access;

import com.mongodb.MongoClient;
import com.mongodb.WriteResult;

import de.server.java.smartwarfare.user.UserId;
import dev.morphia.Datastore;
import dev.morphia.Key;
import dev.morphia.Morphia;

public class AccessRepository
{
    private final Datastore datastore;

    public AccessRepository()
    {
        System.out.println("AccessRepo");
        final Morphia morphia = new Morphia();

        morphia.mapPackage("de.server.java.smartwarfare.access");

        this.datastore = morphia.createDatastore(new MongoClient(), "smartwarfare");
        datastore.ensureIndexes();
    }

    public Key<DBLogin> save(final DBLogin login)
    {
        return datastore.save(login);
    }

    DBLogin findByUserName(final String name)
    {
        return datastore.createQuery(DBLogin.class)
                .field("name").equal(name).first();
    }

    DBLogin findByToken(final String token)
    {
        return datastore.createQuery(DBLogin.class)
                .field("token.token").equal(token).first();
    }

    DBLogin findByUserId(final UserId userId)
    {
        return datastore.createQuery(DBLogin.class)
                .field("userId.identifier").equal(userId.getIdentifier()).first();
    }

    WriteResult deleteLoginByUserId(final UserId userId)
    {
        return datastore.delete(datastore.createQuery(DBLogin.class)
                .field("userId.identifier").equal(userId.getIdentifier()).first());
    }
}

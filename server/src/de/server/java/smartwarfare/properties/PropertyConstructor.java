package de.server.java.smartwarfare.properties;

import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;

public class PropertyConstructor
{
    public static Properties construct()
    {
        final Yaml yaml = new Yaml();
        final InputStream inputStream = PropertyConstructor.class
                .getClassLoader()
                .getResourceAsStream("application.yaml");


        return (Properties) yaml.load(inputStream);
    }
}

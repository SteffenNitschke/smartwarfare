package de.server.java.smartwarfare.properties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Properties
{
    private int startMoney;
    private int maxFuel;
    private int maxAmmo;
}

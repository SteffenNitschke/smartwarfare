package de.desktop.notification;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;


import de.core.handler.OwenFileHandler;

class NotificationFactory
{
    private static final Skin skin = new Skin(OwenFileHandler.getFile("skin/glassy-ui",
                                                                      ".json"));

    static Actor build(final Notification notification)
    {//TODO view/positioning
        final Table notificationActor = new Table();
        final Image image = new Image();
        image.setDrawable(
                getSymbol(notification.symbol()));
        notificationActor.setName(notification.id() + "");
        notificationActor.add(image);

        notificationActor.add(new Label(notification.text(),
                                        skin));
        notificationActor.setUserObject(notification);

        return notificationActor;
    }


    private static TextureRegionDrawable getSymbol(final NotificationSymbol symbol)
    {
        switch (symbol)
        {
            case FAILURE:
                return symbol("notification.failure");
            case SUCCESS:
                return symbol("notification.check");
            case INFO:
            default:
                return symbol("notification.info");
        }
    }

    private static TextureRegionDrawable symbol(final String symbol)
    {
        return new TextureRegionDrawable(
                new TextureRegion(
                        new Texture(OwenFileHandler.getFile(symbol,
                                                            ".png"))));
    }

}

package de.desktop.notification;

public enum NotificationSymbol
{
    INFO, SUCCESS, FAILURE
}

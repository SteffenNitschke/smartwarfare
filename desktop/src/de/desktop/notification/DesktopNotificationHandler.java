package de.desktop.notification;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.ArrayList;
import java.util.List;

import de.core.handler.NotificationHandler;
import lombok.Setter;

public class DesktopNotificationHandler implements NotificationHandler
{
    private final List<Actor> notifications = new ArrayList<>();
    @Setter
    private Stage stage;

    private int nextId = 0;

    public void showSystemNotification(final String text)
    {
        this.nextId++;

        showNotification(Notification.of(this.nextId - 1,
                                         text,
                                         NotificationSymbol.INFO));
    }

    public void showUserNotification(
            final String text,
            final boolean successful)
    {
        this.nextId++;

        showNotification(Notification.of(this.nextId - 1,
                                         text,
                                         successful ?
                                                 NotificationSymbol.SUCCESS :
                                                 NotificationSymbol.FAILURE));
    }

    private void showNotification(final Notification notification)
    {
        final Actor notificationActor = NotificationFactory.build(notification);

        this.stage.addActor(notificationActor);

        this.notifications.add(notificationActor);

        //TODO timing 2000ms
        new Thread(() ->
                           this.notifications
                                   .stream()
                                   .filter(notification1 ->
                                                   ((Notification) notification1
                                                           .getUserObject()).id() == notification.id())
                                   .forEach(Actor::remove));
    }


}

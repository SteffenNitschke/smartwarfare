package de.desktop.notification;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

@RequiredArgsConstructor(staticName = "of")
@Getter
@Accessors(fluent = true, chain = false)
class Notification
{
    private final int id;
    private final String text;

    private final NotificationSymbol symbol;
}

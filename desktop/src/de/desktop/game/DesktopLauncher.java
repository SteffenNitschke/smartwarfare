package de.desktop.game;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.github.czyzby.websocket.CommonWebSockets;

import de.desktop.notification.DesktopNotificationHandler;
import de.desktop.screens.MainDesktopScreen;
import de.core.MainGame;
import de.core.handler.OwenFileHandler;
import de.core.screens.MainScreenInterface;

public class DesktopLauncher
{
    public static void main(String[] arg)
    {
        CommonWebSockets.initiate();
        final LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "smartWarfare";
        config.fullscreen = true;
        config.width = 1600;//1860
        config.height = 900;//1050
        final MainGame game = new MainGame();
        game.notificationHandler(new DesktopNotificationHandler());
        final MainScreenInterface mainScreen = new MainDesktopScreen(game);
        game.getScreenHandler().setMainScreen(mainScreen);
        new LwjglApplication(mainScreen.getMainGame(),
                             config);

        if (arg.length > 0)
        {
            if (arg[0].equals("false"))
            {
                OwenFileHandler.setIsJar(false);
            }
        }
    }
}

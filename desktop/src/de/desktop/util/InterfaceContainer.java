package de.desktop.util;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.HashSet;
import java.util.Vector;

public class InterfaceContainer extends Actor {

    private HashSet<Actor> actors = new HashSet<Actor>();
    private float positionX;
    private float positionY;
    private float width;
    private float height;
    private int columns;
    private float topMargin;
    private float bottomMargin;
    private float leftMargin;
    private float rightMargin;
    private float margin;
    private float horizontalMargin;
    private float verticalMargin;

    private Image background;

    public InterfaceContainer(float width, float height) {
        this.width = width;
        this.height = height;
    }

    //-----------------------------------------------------

    public void order() throws Exception {

        if(margin == 0.0) throw new Exception();
        //set margins
        if(0.0 == topMargin) topMargin = margin;
        if(0.0 == bottomMargin) bottomMargin = margin;
        if(0.0 == leftMargin) leftMargin = margin;
        if(0.0 == rightMargin) rightMargin = margin;
        if(0.0 == horizontalMargin) horizontalMargin = margin;
        if(0.0 == verticalMargin) verticalMargin = margin;

        //find positions-----------------------------------

        float childSize = (float) ((width - (rightMargin + leftMargin + ((columns - 1.0) * verticalMargin))) / columns);
        System.out.println("ChildSize: " + childSize);
        int count = 0;
        for (Actor actor :
                actors) {

            actor.setSize(childSize, childSize);
            float actorPositionX = (positionX + (leftMargin + (count % columns) * (verticalMargin + childSize)));
            float actorPositionY = (float) (positionY + height - (topMargin + ((count / columns) + 1.0) * childSize + (count / columns) * horizontalMargin));
            actor.setPosition(actorPositionX, actorPositionY);
            count++;

        }


    }

    //Getter and Setter------------------------------------

    public void setPosition(float x, float y) {
        this.positionX = x;
        this.positionY = y;
    }

    public void setBackground(Image background) {
        this.background = background;
        this.background.setSize(width, height);
        this.background.setPosition(positionX, positionY);
    }

    public Image getBackground() {
        return background;
    }

    public HashSet<Actor> getActors() {
        return actors;
    }

    public void removeActor(Actor actor) {
        actors.remove(actor);
    }

    public void addActor(Actor actor) {
        this.actors.add(actor);
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getTopMargin() {
        return topMargin;
    }

    public void setTopMargin(float topMargin) {
        this.topMargin = topMargin;
    }

    public float getBottomMargin() {
        return bottomMargin;
    }

    public void setBottomMargin(float bottomMargin) {
        this.bottomMargin = bottomMargin;
    }

    public float getLeftMargin() {
        return leftMargin;
    }

    public void setLeftMargin(float leftMargin) {
        this.leftMargin = leftMargin;
    }

    public float getRightMargin() {
        return rightMargin;
    }

    public void setRightMargin(float rightMargin) {
        this.rightMargin = rightMargin;
    }

    public float getMargin() {
        return margin;
    }

    public void setMargin(float margin) {
        this.margin = margin;
    }

    public float getHorizontalMargin() {
        return horizontalMargin;
    }

    public void setHorizontalMargin(float horizontalMargin) {
        this.horizontalMargin = horizontalMargin;
    }

    public float getVerticalMargin() {
        return verticalMargin;
    }

    public void setVerticalMargin(float verticalMargin) {
        this.verticalMargin = verticalMargin;
    }
}

package de.desktop.util;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;

import de.core.model.Id;
import de.core.uiController.InternetMultiPlayerScreenController;
import de.desktop.screens.GameScreen;
import de.core.MainGame;
import de.core.model.Game;
import de.core.model.Player;
import de.core.util.GameStatus;

import java.util.HashSet;

public class GameScrollingPane extends Actor
{

    private final MainGame mainGame;
    private final Skin skin;
    private final HashSet<Actor> viewActors;
    private final InternetMultiPlayerScreenController controller;

//    private String loggingTag = getClass().getSimpleName();

    private List<String> mapNameList;
    private List<String> numOfPlayersList;
    private List<String> currentPlayerList;
    private List<String> gameStatusList;
    private List<String> gameTypeList;
    private List<String> roundList;
    private List<Label> playerNamesList;

    private ScrollPane mapNamePane;
    private ScrollPane numOfPlayersPane;
    private ScrollPane currentPlayerPane;
    private ScrollPane gameStatusPane;
    private ScrollPane gameTypePane;
    private ScrollPane roundPane;
    private ScrollPane playerNamesPane;

    private int selectedIndex;
    private double lastClick = System.currentTimeMillis();

    public GameScrollingPane(
            final MainGame mainGame,
            final Skin skin,
            final HashSet<Actor> viewActors,
            final InternetMultiPlayerScreenController controller)
    {
        this.controller = controller;
        this.mainGame = mainGame;
        this.skin = skin;
        this.viewActors = viewActors;
        fill();
    }

    private void fill()
    {
        Array<String> mapNameLabelArray = new Array<>();
        Array<String> numOfPlayersLabelArray = new Array<>();
        Array<String> currentPlayerNameLabelArray = new Array<>();
        Array<Label> playersNameLabelArray = new Array<>();
        Array<String> gameStatusLabelArray = new Array<>();
        Array<String> gameTypeLabelArray = new Array<>();
        Array<String> roundLabelArray = new Array<>();

        mapNameLabelArray.add("Map Name:");
        numOfPlayersLabelArray.add("NoP:");
        currentPlayerNameLabelArray.add("Current Player:");
        playersNameLabelArray.add(new Label("Players:",
                                            skin));
        gameStatusLabelArray.add("Status:");
        gameTypeLabelArray.add("Game Type:");
        roundLabelArray.add("Round:");

        StringBuilder added = new StringBuilder();
        for (Game game : this.mainGame.getUser().getGames())
        {
//            Gdx.app.log(loggingTag, "Game: ");
            added.append(" ");

            StringBuilder playerNames = new StringBuilder();
            for (Player player : game.getPlayer())
            {
                if (!player.getName().equals(game.getCurrentPlayer().getName()))
                {
                    playerNames.append(player.getName()).append(", ");
                }
            }
            playerNames.delete(playerNames.length() - 2,
                               playerNames.length());
            Label allPlayerNames = new Label(playerNames.toString() + added.toString(),
                                             skin);
            allPlayerNames.setUserObject(game.getGameID());
            playersNameLabelArray.add(allPlayerNames);

            mapNameLabelArray.add(game.getMap().getName() + added.toString());
            numOfPlayersLabelArray.add(game.getMap().getNumberOfPlayers() + added.toString());
            currentPlayerNameLabelArray.add(game.getCurrentPlayer().getName() + added.toString());
            gameStatusLabelArray.add(game.getStatus().name() + added.toString());
            gameTypeLabelArray.add(game.getGameType().name() + added.toString());
            roundLabelArray.add(game.getRound() + added.toString());
        }
        mapNameList = new List<>(skin);
        numOfPlayersList = new List<>(skin);
        currentPlayerList = new List<>(skin);
        gameStatusList = new List<>(skin);
        gameTypeList = new List<>(skin);
        roundList = new List<>(skin);
        playerNamesList = new List<>(skin);

        mapNameList.setItems(mapNameLabelArray);
        numOfPlayersList.setItems(numOfPlayersLabelArray);
        currentPlayerList.setItems(currentPlayerNameLabelArray);
        gameStatusList.setItems(gameStatusLabelArray);
        gameTypeList.setItems(gameTypeLabelArray);
        roundList.setItems(roundLabelArray);
        playerNamesList.setItems(playersNameLabelArray);

        mapNameList.addListener(new ListInputListener());
        numOfPlayersList.addListener(new ListInputListener());
        currentPlayerList.addListener(new ListInputListener());
        gameStatusList.addListener(new ListInputListener());
        gameTypeList.addListener(new ListInputListener());
        roundList.addListener(new ListInputListener());
        playerNamesList.addListener(new ListInputListener());

        mapNamePane = new ScrollPane(mapNameList);
        numOfPlayersPane = new ScrollPane(numOfPlayersList);
        currentPlayerPane = new ScrollPane(currentPlayerList);
        gameStatusPane = new ScrollPane(gameStatusList);
        gameTypePane = new ScrollPane(gameTypeList);
        roundPane = new ScrollPane(roundList);
        playerNamesPane = new ScrollPane(playerNamesList);

        mapNamePane.setSmoothScrolling(false);
        numOfPlayersPane.setSmoothScrolling(false);
        currentPlayerPane.setSmoothScrolling(false);
        gameStatusPane.setSmoothScrolling(false);
        gameTypePane.setSmoothScrolling(false);
        roundPane.setSmoothScrolling(false);
        playerNamesPane.setSmoothScrolling(false);

        mapNamePane.setTransform(true);
        numOfPlayersPane.setTransform(true);
        currentPlayerPane.setTransform(true);
        gameStatusPane.setTransform(true);
        gameTypePane.setTransform(true);
        roundPane.setTransform(true);
        playerNamesPane.setTransform(true);

        mapNamePane.setScale(1f);
        numOfPlayersPane.setScale(1f);
        currentPlayerPane.setScale(1f);
        gameStatusPane.setScale(1f);
        gameTypePane.setScale(1f);
        roundPane.setScale(1f);
        playerNamesPane.setScale(1f);

        if (viewActors != null)
        {
            viewActors.add(mapNamePane);
            viewActors.add(numOfPlayersPane);
            viewActors.add(currentPlayerPane);
            viewActors.add(gameStatusPane);
            viewActors.add(gameTypePane);
            viewActors.add(roundPane);
            viewActors.add(playerNamesPane);
        }
    }

    public void resize()
    {

        mapNamePane.setSize(getWidth() * 0.2f,
                            getHeight());
        numOfPlayersPane.setSize(getWidth() * 0.05f,
                                 getHeight());
        currentPlayerPane.setSize(getWidth() * 0.1f,
                                  getHeight());
        gameStatusPane.setSize(getWidth() * 0.08f,
                               getHeight());
        gameTypePane.setSize(getWidth() * 0.12f,
                             getHeight());
        roundPane.setSize(getWidth() * 0.05f,
                          getHeight());
        playerNamesPane.setSize(getWidth() * 0.4f,
                                getHeight());

        mapNamePane.setPosition(getX(),
                                getY());
        numOfPlayersPane.setPosition(getX() + getWidth() * 0.2f,
                                     getY());
        currentPlayerPane.setPosition(getX() + getWidth() * 0.25f,
                                      getY());
        gameStatusPane.setPosition(getX() + getWidth() * 0.35f,
                                   getY());
        gameTypePane.setPosition(getX() + getWidth() * 0.43f,
                                 getY());
        roundPane.setPosition(getX() + getWidth() * 0.55f,
                              getY());
        playerNamesPane.setPosition(getX() + getWidth() * 0.6f,
                                    getY());

        getStage().addActor(mapNamePane);
        getStage().addActor(numOfPlayersPane);
        getStage().addActor(currentPlayerPane);
        getStage().addActor(gameStatusPane);
        getStage().addActor(gameTypePane);
        getStage().addActor(roundPane);
        getStage().addActor(playerNamesPane);
    }

    private boolean isDoubleClick()
    {
        return (lastClick + 200) > System.currentTimeMillis()
                && (lastClick + 100) < System.currentTimeMillis();
    }

    class ListInputListener extends InputListener
    {
        @Override
        public void touchUp(
                final InputEvent event,
                final float x,
                final float y,
                final int pointer,
                final int button)
        {
            int index = ((List) event.getTarget()).getSelectedIndex();
            setAllSelected(index);
            String gameId = (String) playerNamesList.getSelected().getUserObject();

            if (isDoubleClick() && selectedIndex == index)
            {
                if (gameId != null && mainGame.getUser().getGames().stream()
                                              .anyMatch(game -> game.getStatus().name()
                                                                    .equals(GameStatus.RUNNING
                                                                                    .name())))
                {
                    mainGame.setScreen(new GameScreen(mainGame,
                                                      getCurrentGame(gameId)));
                }
            }
            else
            {
                selectedIndex = index;
                lastClick = System.currentTimeMillis();
                controller.showMapImageOfGame(new Id(gameId));
            }
        }

        private Game getCurrentGame(String gameId)
        {
            return mainGame.getUser().getGames().stream()
                    .filter(game1 -> game1.getGameID()
                                          .equals(gameId))
                    .findFirst().orElse(null);
        }

        @Override
        public boolean touchDown(
                final InputEvent event,
                final float x,
                final float y,
                final int pointer,
                final int button)
        {
            return true;
        }

    }

    private void setAllSelected(int index)
    {
        mapNameList.setSelectedIndex(index);
        numOfPlayersList.setSelectedIndex(index);
        currentPlayerList.setSelectedIndex(index);
        gameStatusList.setSelectedIndex(index);
        gameTypeList.setSelectedIndex(index);
        roundList.setSelectedIndex(index);
        playerNamesList.setSelectedIndex(index);
    }
}

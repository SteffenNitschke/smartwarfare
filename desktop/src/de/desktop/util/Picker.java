package de.desktop.util;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import de.core.util.HudInputProcessor;

public class Picker extends Actor  {

    private final float width;
    private final float height;
    private final double x;
    private final double y;
    private final HudInputProcessor hudInputProcessor;
    private List<Actor> list;
    private Object object;
    private ScrollPane pane;

    public Picker(final HudInputProcessor hudInputProcessor, Stage stage, List<Actor> list,
                  float width, float height, double x, double y) {
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.hudInputProcessor = hudInputProcessor;
        this.list = list;

        pane = new ScrollPane(list);
        pane.setSmoothScrolling(false);
        pane.setTransform(true);
        pane.setScale(1f);
        pane.setSize(width * 4, height * 4);
        pane.setPosition((float) x, (float) (y - pane.getHeight()));
//        stage.addActor(pane);
        stage.addActor(pane);
        hudInputProcessor.addActor(pane);

    }

    public void draw(SpriteBatch batch, float alphaParent) {
        super.draw(batch, alphaParent);
        pane.draw(batch, alphaParent);
    }

    public Picker workingOn(Object object) {
        this.object = object;
        return this;
    }

    public Picker withListener(InputListener listener) {
        this.list.addListener(listener);
        return this;
    }

}

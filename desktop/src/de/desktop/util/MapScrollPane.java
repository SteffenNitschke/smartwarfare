package de.desktop.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;

import de.core.model.Id;
import de.core.network.messages.SmallMapMessage;
import de.core.uiController.InternetMultiPlayerScreenController;

import java.util.HashSet;

public class MapScrollPane extends Actor
{

    private final Skin skin;
    private HashSet<Actor> viewActors;
    private final InternetMultiPlayerScreenController controller;
    private final HashSet<SmallMapMessage> maps;
    private MapScrollPane thisPane = this;

    private List<Label> mapNameList;
    private List<String> numOfPlayersList;
    private List<String> mapCreatorNameList;
    private List<String> mapSizeList;

    private ScrollPane mapNamePane;
    private ScrollPane numOfPlayersPane;
    private ScrollPane mapCreatorPane;
    private ScrollPane mapSizePane;

    private int selectedIndex;
    private double lastClick = System.currentTimeMillis();

    private String loggingTag = getClass().getSimpleName();

    public MapScrollPane(
            final Skin skin,
            final HashSet<Actor> viewActors,
            final InternetMultiPlayerScreenController controller,
            final HashSet<SmallMapMessage> maps)
    {

        this.skin = skin;
        this.viewActors = viewActors;
        this.controller = controller;
        this.maps = maps;
        fill();
    }

    private void fill()
    {

        Array<Label> mapNameLabelArray = new Array<>();
        Array<String> numOfPlayersLabelArray = new Array<>();
        Array<String> mapCreatorNameArray = new Array<>();
        Array<String> mapSizeArray = new Array<>();

        mapNameLabelArray.add(new Label("Map Name:",
                                        skin));
        numOfPlayersLabelArray.add("NOP:");
        mapCreatorNameArray.add("Creator:");
        mapSizeArray.add("Size:");

        StringBuilder added = new StringBuilder();
        for (SmallMapMessage map : maps)
        {

            added.append(" ");
            Label mapName = new Label(map.getName() + added.toString(),
                                      skin);
            mapName.setUserObject(map.getMapId());

            mapNameLabelArray.add(mapName);
            numOfPlayersLabelArray.add(String.valueOf(map.getNumberOfPlayers()) + added.toString());
            mapCreatorNameArray.add(map.getCreatorName() + added.toString());
            mapSizeArray.add(map.getMaxX() + " x " + map.getMaxY() + added.toString());

        }
        mapNameList = new List<>(skin);
        numOfPlayersList = new List<>(skin);
        mapCreatorNameList = new List<>(skin);
        mapSizeList = new List<>(skin);

        mapNameList.setItems(mapNameLabelArray);
        numOfPlayersList.setItems(numOfPlayersLabelArray);
        mapCreatorNameList.setItems(mapCreatorNameArray);
        mapSizeList.setItems(mapSizeArray);

        mapNameList.addListener(new ListInputListener());
        numOfPlayersList.addListener(new ListInputListener());
        mapCreatorNameList.addListener(new ListInputListener());
        mapSizeList.addListener(new ListInputListener());

        mapNamePane = new ScrollPane(mapNameList);
        numOfPlayersPane = new ScrollPane(numOfPlayersList);
        mapCreatorPane = new ScrollPane(mapCreatorNameList);
        mapSizePane = new ScrollPane(mapSizeList);

        mapNamePane.setSmoothScrolling(false);
        numOfPlayersPane.setSmoothScrolling(false);
        mapCreatorPane.setSmoothScrolling(false);
        mapSizePane.setSmoothScrolling(false);

        mapNamePane.setTransform(true);
        numOfPlayersPane.setTransform(true);
        mapCreatorPane.setTransform(true);
        mapSizePane.setTransform(true);

        mapNamePane.setScale(1f);
        numOfPlayersPane.setScale(1f);
        mapCreatorPane.setScale(1f);
        mapSizePane.setScale(1f);

        viewActors.add(mapNamePane);
        viewActors.add(numOfPlayersPane);
        viewActors.add(mapCreatorPane);
        viewActors.add(mapSizePane);
    }

    public void resize()
    {
        mapNamePane.setSize(getWidth() * 0.325f,
                            getHeight());
        numOfPlayersPane.setSize(getWidth() * 0.1f,
                                 getHeight());
        mapCreatorPane.setSize(getWidth() * 0.325f,
                               getHeight());
        mapSizePane.setSize(getWidth() * 0.25f,
                            getHeight());

        mapNamePane.setPosition(getX(),
                                getY());
        numOfPlayersPane.setPosition(getX() + getWidth() * 0.325f,
                                     getY());
        mapCreatorPane.setPosition(getX() + getWidth() * 0.425f,
                                   getY());
        mapSizePane.setPosition(getX() + getWidth() * 0.75f,
                                getY());

        getStage().addActor(mapNamePane);
        getStage().addActor(numOfPlayersPane);
        getStage().addActor(mapCreatorPane);
        getStage().addActor(mapSizePane);
    }

    public boolean remove()
    {
        viewActors.remove(mapNamePane);
        viewActors.remove(numOfPlayersPane);
        viewActors.remove(mapCreatorPane);
        viewActors.remove(mapSizePane);
        return super.remove();
    }

    private void selectRow(final int index)
    {
        mapNameList.setSelected(mapNameList.getItems().get(index));
        numOfPlayersList.setSelected(numOfPlayersList.getItems().get(index));
        mapCreatorNameList.setSelected(mapCreatorNameList.getItems().get(index));
        mapSizeList.setSelected(mapSizeList.getItems().get(index));
    }

    private class ListInputListener extends InputListener
    {
        @Override
        public void touchUp(
                final InputEvent event,
                final float x,
                final float y,
                final int pointer,
                final int button)
        {

            final int index = ((List) event.getTarget()).getSelectedIndex();
            selectRow(index);
            final String mapId = (String) mapNameList.getSelected().getUserObject();

            if (isDoubleClick() && selectedIndex == index)
            {
                if (mapId != null)
                {
                    chooseMapAndClose(mapId);
                }
            }
            else
            {
                selectedIndex = index;
                lastClick = System.currentTimeMillis();
                viewMapImage(mapId);
            }
        }

        private boolean isDoubleClick()
        {
            return (lastClick + 200) > System.currentTimeMillis()
                    && (lastClick + 100) < System.currentTimeMillis();
        }

        private void chooseMapAndClose(final String mapId)
        {
            controller.setMapName(String.valueOf(mapNameList.getSelected().getText()));
            controller.setMapId(mapId);
            controller.removeMapPane();
            getStage().getActors().removeValue(mapNamePane,
                                               true);
            getStage().getActors().removeValue(numOfPlayersPane,
                                               true);
            getStage().getActors().removeValue(mapCreatorPane,
                                               true);
            getStage().getActors().removeValue(mapSizePane,
                                               true);
            getStage().getActors().removeValue(thisPane,
                                               true);
            remove();
        }

        @Override
        public boolean touchDown(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button)
        {
            return true;
        }
    }

    private void viewMapImage(final String mapId)
    {
        Gdx.app.log(loggingTag, "viewMapImage");
        controller.showMapOfMapId(new Id(mapId));
    }
}

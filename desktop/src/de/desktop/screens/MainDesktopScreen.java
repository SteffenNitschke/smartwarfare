package de.desktop.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.core.MainGame;
import de.core.handler.OwenFileHandler;
import de.core.screens.MainScreenInterface;
import de.core.uiController.MainScreenController;
import de.core.util.HudInputProcessor;
import de.desktop.screens.windows.LoginScreen;

/**
 * Created by Gandail on 10.06.2017.
 */
public class MainDesktopScreen extends NewsScreen implements MainScreenInterface
{
    private LoginScreen loginScreen;

    private Stage stage;
    private HudInputProcessor inputProcessor;


    private String loggingTag = getClass().getSimpleName();

    public MainDesktopScreen(MainGame game)
    {
        super(game);
    }

    @Override
    public void show()
    {
        Gdx.app.log(loggingTag,
                    "show");

        stage = new Stage();

        MainScreenController controller = new MainScreenController(this,
                                                                   game);

        this.mySkin = new Skin(OwenFileHandler.getFile("skin/glassy-ui",
                                                       ".json"));

        inputProcessor = new HudInputProcessor(stage,
                                               false);
        final InputMultiplexer inputs = new InputMultiplexer();
        inputs.addProcessor(this);
        inputs.addProcessor(inputProcessor);
        Gdx.input.setInputProcessor(inputs);

        this.col_width = Gdx.graphics.getWidth() / (float) 16;
        this.row_height = Gdx.graphics.getHeight() / (float) 12;

        final Image background = new Image();
        background.setSize(Gdx.graphics.getWidth(),
                           Gdx.graphics.getHeight());
        background.setPosition(0,
                               0);
        final Drawable drawable = new TextureRegionDrawable(new TextureRegion(new Texture(OwenFileHandler
                                                                                            .getFile("mainScreen",
                                                                                                     ".jpg"))));
        background.setDrawable(drawable);
        stage.addActor(background);
        inputProcessor.addIdleActor(background);

        final Button singleplayerButton = new TextButton("Singleplayer",
                                                         mySkin);//
        singleplayerButton.setSize(col_width * 4,
                                   (float) (row_height * 0.8));

        final Button multiplayerButton = new TextButton("Multiplayer",
                                                  mySkin);//,"small"
        multiplayerButton.setSize(col_width * 4,
                                  (float) (row_height * 0.8));

        final Button editorButton = new TextButton("MapEditor",
                                             mySkin);
        editorButton.setSize(col_width * 4,
                             (float) (row_height * 0.8));

        final Button settings = new TextButton("Settings",
                                         mySkin);
        settings.setSize(col_width * 4,
                         (float) (row_height * 0.8));

        final Button contacts = new TextButton("Contacts",
                                         mySkin);
        contacts.setSize(col_width * 4,
                         (float) (row_height * 0.8));

        final Button exitButton = new TextButton("Exit",
                                           mySkin);//,"small"
        exitButton.setSize(col_width * 4,
                           (float) (row_height * 0.8));

        singleplayerButton.addListener(controller.getSinglePlayerButtonListener());

        multiplayerButton.addListener(controller.getMultiPlayerButtonListener());

        editorButton.addListener(controller.getEditorScreenButtonListener());

        settings.addListener(controller.getSettingsScreenListener());

        contacts.addListener(controller.getContactsScreenListener());

        exitButton.addListener(controller.getExitButtonListener());

        singleplayerButton.setPosition(col_width * 12,
                                       (float) (row_height * 10.5));
        stage.addActor(singleplayerButton);
        inputProcessor.addIdleActor(singleplayerButton);

        multiplayerButton.setPosition(col_width * 12,
                                      (float) (row_height * 9.5));
        stage.addActor(multiplayerButton);
        inputProcessor.addIdleActor(multiplayerButton);

        editorButton.setPosition(col_width * 12,
                                 (float) (row_height * 8.5));
        stage.addActor(editorButton);
        inputProcessor.addIdleActor(editorButton);

        settings.setPosition(col_width * 12,
                             (float) (row_height * 2.5));
        stage.addActor(settings);
        inputProcessor.addIdleActor(settings);

        contacts.setPosition(col_width * 12,
                             (float) (row_height * 1.5));
        stage.addActor(contacts);
        inputProcessor.addIdleActor(contacts);

        exitButton.setPosition(col_width * 12,
                               (float) (row_height * 0.5));
        stage.addActor(exitButton);
        inputProcessor.addIdleActor(exitButton);

        if (game.getUser() == null)
        {
            openLoginScreen();
        }

        super.show(stage, inputProcessor);
    }

    private void openLoginScreen()
    {
        loginScreen = new LoginScreen(game,
                                      inputProcessor)
                .withPosition(Gdx.graphics.getWidth() / 4.0,
                              Gdx.graphics.getHeight() / 4.0)
                .withSize(Gdx.graphics.getWidth() / 2.0,
                          Gdx.graphics.getHeight() / 2.0);
        inputProcessor.idle(true);
        game.getScreenHandler().setScreenInFrontOf(true);
    }



    @Override
    public void render(float v)
    {
        Gdx.gl.glClearColor(1,
                            1,
                            1,
                            1);
        Gdx.gl.glClear(GL20.GL_COLOR_WRITEMASK);
        stage.draw();
        inputProcessor.handleKeyInput();

        if (!game.getScreenHandler().isScreenInFrontOf())
        {
            loginScreen = null;
            inputProcessor.idle(false);
        }
        else
        {
            if (game.getUser() != null)
            {
                game.getScreenHandler().setScreenInFrontOf(false);
            }
            if (loginScreen != null)
            {
                loginScreen.draw();
            }
        }
    }

    @Override
    public void resize(
            int width,
            int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public MainGame getMainGame()
    {
        return game;
    }

    @Override
    public void dispose()
    {
        Gdx.app.log(loggingTag,
                    "Dispose");
        stage.dispose();
    }

    @Override
    public String getCanonicalNameOfSinglePlayerScreen()
    {
        return SinglePlayerScreen.class
                .getCanonicalName();
    }

    @Override
    public Screen getSinglePlayerScreen()
    {
        return new SinglePlayerScreen(game);
    }

    @Override
    public String getCanonicalNameOfMultiPlayerScreen()
    {
        return MultiPlayerStartScreen.class
                .getCanonicalName();
    }

    @Override
    public Screen getMultiPlayerScreen()
    {
        return new MultiPlayerStartScreen(game);
    }

    @Override
    public String getCanonicalNameOfEditorScreen()
    {
        return EditorScreen.class.getCanonicalName();
    }

    @Override
    public Screen getEditorScreen()
    {
        return new EditorScreen(game);
    }

    @Override
    public String getCanonicalNameOfSettingsScreen()
    {
        return null;
    }

    @Override
    public Screen getSettingsScreen()
    {
        return null;
    }

    @Override
    public String getCanonicalNameOfContactsScreen()
    {
        return null;
    }

    @Override
    public Screen getContactsScreen()
    {
        return null;
    }

    @Override
    public boolean keyDown(int i)
    {
        this.stage.keyDown(i);
        return false;
    }

    @Override
    public boolean keyUp(int i)
    {
        this.stage.keyUp(i);
        return false;
    }

    @Override
    public boolean keyTyped(char c)
    {
        this.stage.keyTyped(c);
        return false;
    }

    @Override
    public boolean touchDown(
            int i,
            int i1,
            int i2,
            int i3)
    {
        this.stage.touchDown(i,
                             i1,
                             i2,
                             i3);
        return false;
    }

    @Override
    public boolean touchUp(
            int i,
            int i1,
            int i2,
            int i3)
    {
        this.stage.touchUp(i,
                           i1,
                           i2,
                           i3);
        return false;
    }

    @Override
    public boolean touchDragged(
            int i,
            int i1,
            int i2)
    {
        this.stage.touchDragged(i,
                                i1,
                                i2);
        return false;
    }

    @Override
    public boolean mouseMoved(
            int i,
            int i1)
    {
        this.stage.mouseMoved(i,
                              i1);
        return false;
    }

    @Override
    public boolean scrolled(int i)
    {
        this.stage.scrolled(i);
        return false;
    }
}

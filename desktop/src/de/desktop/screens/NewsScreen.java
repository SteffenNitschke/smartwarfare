package de.desktop.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import de.core.MainGame;
import de.core.screens.NewsScreenInterface;
import de.core.screens.windows.NewsTableBuilderInterface;
import de.core.uiController.NewsScreenController;
import de.core.util.HudInputProcessor;
import de.desktop.screens.actors.NewsTableBuilder;
import lombok.Getter;

abstract class NewsScreen implements NewsScreenInterface, Screen
{
    final MainGame game;
    Skin mySkin;
    HudInputProcessor inputProcessor;
    Stage stage;

    float col_width;
    float row_height;

    private NewsTableBuilderInterface newsTable;

    private Table table;

    @Getter
    private Label numberOfNewsLabel;
    private String loggingTag = getClass().getSimpleName();

    NewsScreen(
            MainGame game)
    {
        this.game = game;
    }

    void show(final Stage stage, final HudInputProcessor inputProcessor)
    {
        this.stage = stage;
        this.inputProcessor = inputProcessor;
        Gdx.app.log(loggingTag, "show " + stage);
        NewsScreenController controller = new NewsScreenController(this);

        newsTable = new NewsTableBuilder(
                col_width,
                row_height * 10,
                col_width * 2,
                row_height * 8,
                mySkin,
                game
        );

        final Button newsButton = new TextButton("News",
                                                 mySkin);

        numberOfNewsLabel = new Label("",
                                      mySkin);

        table = newsTable.fill();

        table.setVisible(false);

        stage.addActor(table);
        inputProcessor.addActor(table);

        game.setNewsScreenController(controller);

        newsButton.setSize((float) (col_width * 0.8),
                           (float) (row_height * 0.8));

        newsButton.setPosition(col_width,
                               (row_height * 11));

        numberOfNewsLabel.setSize((float) (col_width * 0.4),
                                  (float) (row_height * 0.4));

        numberOfNewsLabel.setPosition((float) (col_width * 1.15),
                                      (float) (row_height * 11.5));

        numberOfNewsLabel.setText("1");
        numberOfNewsLabel.setColor(Color.RED);


        newsButton.addListener(controller.newNewsButtonListener());

        stage.addActor(newsButton);
        inputProcessor.addActor(newsButton);

        stage.addActor(numberOfNewsLabel);
        inputProcessor.addActor(numberOfNewsLabel);
    }

    @Override
    public NewsTableBuilderInterface getNewsTable()
    {
        return newsTable;
    }

    @Override
    public boolean isVisible()
    {
        Gdx.app.log(loggingTag, "isVisible");
        return table.isVisible();
    }

    @Override
    public void setVisible(final boolean visible)
    {
        Gdx.app.log(loggingTag, "setVisible " + visible);
        table.setVisible(visible);
    }
}

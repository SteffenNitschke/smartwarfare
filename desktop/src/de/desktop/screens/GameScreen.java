package de.desktop.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.desktop.screens.windows.FactoryWindow;
import de.desktop.screens.windows.MenuScreen;
import de.core.util.HudInputProcessor;
import de.core.MainGame;
import de.core.gameController.WorldController;
import de.core.handler.OwenFileHandler;
import de.core.model.Game;
import de.core.model.buildungs.BuildingInterface;
import de.core.screens.GameScreenInterface;
import de.core.screens.windows.FactoryWindowInterface;
import de.core.screens.windows.SmallScreen;
import de.core.uiController.GameScreenController;
import de.core.util.TurnEventListener;

/**
 * Created by Gandail on 11.06.2017.
 */
public class GameScreen extends GameScreenSuperclass implements GameScreenInterface
{

    private WorldController worldController;

    private ImageTextButton nextRoundButton;
    private Label currentPlayerName;
    private Image menuBackground;
    private Label unitPlayerName, unitNameType, unitFuel, unitAmmo;
    private Image unitImage;
    private Label fieldPlayerName, fieldType, defenceValue, captureValue;
    private Image fieldImage;
    private SmallScreen screen;


    public GameScreen(
            final MainGame game,
            final Game currentGame)
    {
        Gdx.app.log(loggingTag,
                    "Open GameScreen");
        this.game = game;
        this.currentGame = currentGame;
    }

    @Override
    public void show()
    {
        super.show();
        this.gameScreenController = new GameScreenController(game,
                                                             this);

        final Image background = new Image();
        background.setSize(Gdx.graphics.getWidth() * 3,
                           Gdx.graphics.getHeight() * 3);
        background.setPosition(-Gdx.graphics.getWidth(),
                               -Gdx.graphics.getHeight());
        final Drawable drawable = new TextureRegionDrawable(new TextureRegion(
                new Texture(OwenFileHandler.getFile("backgroundGameScreen",
                                                    ".jpg"))));
        background.setDrawable(drawable);
        stage.addActor(background);
        hudProcessor.addIdleActor(background);


        final Skin mySkin = new Skin(OwenFileHandler.getFile("skin/glassy-ui",
                                                             ".json"));

        //init HUD ------------------------------

        nextRoundButton = new ImageTextButton(
                currentGame.getRound() + "",
                mySkin);

        nextRoundButton.setSize((float) (col_width * 0.5),
                                (float) (row_height * 0.8));

        nextRoundButton.addListener(gameScreenController.getInputListener());

        nextRoundButton.setPosition(col_width,
                                    row_height * 11);

        hudProcessor.addActor(nextRoundButton);

        currentPlayerName = new Label(currentGame.getCurrentPlayer().getName(),
                                      mySkin);
        currentPlayerName.setSize((float) (col_width * 4),
                                  (float) (row_height * 0.8));
        currentPlayerName.setPosition(col_width * 2,
                                      row_height * 11);
        hudProcessor.addIdleActor(currentPlayerName);

        final Texture texture = new Texture(OwenFileHandler.getFile("backgroundGameScreenMenu",
                                                                    ".jpg"));
        final Drawable drawable1 = new TextureRegionDrawable(new TextureRegion(texture));
        menuBackground = new Image();
        menuBackground.setDrawable(drawable1);
        menuBackground.setSize((float) (col_width * 4.5),
                               (float) (row_height * 4.5));
        menuBackground.setPosition(0,
                                   0);
        hudProcessor.addIdleActor(menuBackground);

        unitPlayerName = new Label("",
                                   mySkin);
        unitPlayerName.setText("");
        unitPlayerName.setSize(col_width * 2,
                               (float) (row_height * 0.5));
        unitPlayerName.setPosition((float) (col_width * 0.3),
                                   (float) (row_height * 3.5));
        hudProcessor.addIdleActor(unitPlayerName);

        unitNameType = new Label("",
                                 mySkin);
        unitNameType.setText("");
        unitNameType.setSize(col_width * 2,
                             (float) (row_height * 0.5));
        unitNameType.setPosition((float) (col_width * 0.3),
                                 row_height * 3);
        hudProcessor.addIdleActor(unitNameType);

        unitFuel = new Label("",
                             mySkin);
        unitFuel.setText("");
        unitFuel.setSize(col_width,
                         (float) (row_height * 0.5));
        unitFuel.setPosition((float) (col_width * 0.3),
                             (float) (row_height * 2.5));
        hudProcessor.addIdleActor(unitFuel);

        unitAmmo = new Label("",
                             mySkin);
        unitAmmo.setText("");
        unitAmmo.setSize(col_width,
                         (float) (row_height * 0.5));
        unitAmmo.setPosition((float) (col_width * 1.5),
                             (float) (row_height * 2.5));
        hudProcessor.addIdleActor(unitAmmo);

        final Texture texture2 = new Texture(OwenFileHandler.getFile("emptyUnitImage",
                                                                     ".jpg"));
        final Drawable drawable2 = new TextureRegionDrawable(new TextureRegion(texture2));
        unitImage = new Image();
        unitImage.setDrawable(drawable2);
        unitImage.setSize(row_height * 2,
                          row_height * 2);
        unitImage.setPosition((float) (col_width * 0.3),
                              (float) (row_height * 0.3));
        hudProcessor.addIdleActor(unitImage);

        fieldPlayerName = new Label("",
                                    mySkin);
        fieldPlayerName.setText("");
        fieldPlayerName.setSize(col_width * 2,
                                (float) (row_height * 0.5));
        fieldPlayerName.setPosition((float) (col_width * 2.5),
                                    (float) (row_height * 3.5));
        hudProcessor.addIdleActor(fieldPlayerName);

        fieldType = new Label("",
                              mySkin);
        fieldType.setText("");
        fieldType.setSize(col_width * 2,
                          (float) (row_height * 0.5));
        fieldType.setPosition((float) (col_width * 2.5),
                              (float) (row_height * 3));
        hudProcessor.addIdleActor(fieldType);

        defenceValue = new Label("",
                                 mySkin);
        defenceValue.setText("");
        defenceValue.setSize(col_width,
                             (float) (row_height * 0.5));
        defenceValue.setPosition((float) (col_width * 2.5),
                                 (float) (row_height * 2.5));
        hudProcessor.addIdleActor(defenceValue);

        captureValue = new Label("",
                                 mySkin);
        captureValue.setText("");
        captureValue.setSize(col_width,
                             (float) (row_height * 0.5));
        captureValue.setPosition((float) (col_width * 3.7),
                                 (float) (row_height * 2.5));
        hudProcessor.addIdleActor(captureValue);

        final Texture texture3 = new Texture(OwenFileHandler.getFile("background",
                                                                     ".jpg"));
        final Drawable drawable3 = new TextureRegionDrawable(new TextureRegion(texture3));
        fieldImage = new Image();
        fieldImage.setDrawable(drawable3);
        fieldImage.setSize(row_height * 2,
                           row_height * 2);
        fieldImage.setPosition((float) (col_width * 2.5),
                               (float) (row_height * 0.3));
        hudProcessor.addIdleActor(fieldImage);

        //add Controller ------------------------

        if (this.worldController == null)
        {
            worldController = new WorldController(
                    currentGame,
                    stage,
                    this,
                    game.getEventListener(),
                    game);
        }
    }

    @Override
    public void render(float v)
    {

        if (!game.getPerformer().isPerform())
        {
            game.getPerformer().performTurnOfGame(this.currentGame);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE))
        {
            if (!game.getScreenHandler().isScreenInFrontOf())
            {
                screen = new MenuScreen(game,
                                        hudProcessor,
                                        this)
                        .withPosition(col_width * 6,
                                      row_height * 7)
                        .withSize(col_width * 4,
                                  row_height * 5);
                game.getScreenHandler().setScreenInFrontOf(true);
                hudProcessor.idle(true);
            }
            else
            {
                screen.close();
            }
        }

        super.render(v);

        hudBatch.begin();
        nextRoundButton.draw(hudBatch,
                             1f);
        currentPlayerName.draw(hudBatch,
                               1f);
        menuBackground.draw(hudBatch,
                            1f);

        unitPlayerName.draw(hudBatch,
                            1f);
        unitNameType.draw(hudBatch,
                          1f);
        unitFuel.draw(hudBatch,
                      1f);
        unitAmmo.draw(hudBatch,
                      1f);
        unitImage.draw(hudBatch,
                       1f);

        fieldPlayerName.draw(hudBatch,
                             1f);
        fieldType.draw(hudBatch,
                       1f);
        defenceValue.draw(hudBatch,
                          1f);
        captureValue.draw(hudBatch,
                          1f);
        fieldImage.draw(hudBatch,
                        1f);

        if (game.getScreenHandler().isScreenInFrontOf())
        {
            screen.draw();
        }
        else
        {
            screen = null;
        }

        hudBatch.end();

        camera.update();
    }

    @Override
    public void dispose()
    {
        super.dispose();
    }


    //-------------------------------------------

    public void setMapSize(
            final float width,
            final float height)
    {
        this.mapWidth = width;
        this.mapHeight = height;
    }

    public SmallScreen getScreen()
    {
        return screen;
    }

    public void setScreen(final SmallScreen screen)
    {
        this.screen = screen;
        if (screen != null)
        {
            game.getScreenHandler().setScreenInFrontOf(true);
        }
        else
        {
            game.getScreenHandler().setScreenInFrontOf(false);
        }
    }

    public HudInputProcessor getHudProcessor()
    {
        return hudProcessor;
    }

    public Game getCurrentGame()
    {
        return this.currentGame;
    }

    @Override
    public void setScreen(final FactoryWindowInterface factoryWindowInterface)
    {
        screen = factoryWindowInterface;
    }

    @Override
    public Label getInfoBoxUnitPlayerNameLabel()
    {
        return unitPlayerName;
    }

    @Override
    public Label getInfoBoxUnitNameTypeLabel()
    {
        return unitNameType;
    }

    @Override
    public Label getInfoBoxUnitAmmoLabel()
    {
        return unitAmmo;
    }

    @Override
    public Label getInfoBoxUnitFuelLabel()
    {
        return unitFuel;
    }

    @Override
    public Image getInfoBoxUnitImage()
    {
        return unitImage;
    }

    @Override
    public GameScreenController getScreenController()
    {
        return gameScreenController;
    }

    @Override
    public ImageTextButton getNextRoundButton()
    {
        return nextRoundButton;
    }

    @Override
    public Label getCurrentPlayerName()
    {
        return currentPlayerName;
    }

    @Override
    public Label getInfoBoxFieldTypeLabel()
    {
        return fieldType;
    }

    @Override
    public Label getInfoBoxDefenceValueLabel()
    {
        return defenceValue;
    }

    @Override
    public boolean isDragged()
    {
        return isDragged;
    }

    @Override
    public Image getInfoBoxFieldImage()
    {
        return fieldImage;
    }

    @Override
    public Label getInfoBoxFieldPlayerNameLabel()
    {
        return fieldPlayerName;
    }

    @Override
    public Label getInfoBoxCaptureValueLabel()
    {
        return captureValue;
    }

    @Override
    public FactoryWindowInterface getNewFactoryWindow(
            final HudInputProcessor hudProcessor,
            final BuildingInterface building,
            final TurnEventListener eventListener,
            final MainGame mainGame)
    {
        return new FactoryWindow(hudProcessor,
                                 building,
                                 eventListener,
                                 mainGame);
    }

    @Override
    public void showNotification(String message)
    {
        //TODO
    }
}

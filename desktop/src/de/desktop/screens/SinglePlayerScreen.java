package de.desktop.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.core.MainGame;
import de.core.screens.SinglePlayerScreenInterface;
import de.core.uiController.SinglePlayerScreenController;
import de.core.handler.OwenFileHandler;

/**
 * Created by Gandail on 10.06.2017.
 */
public class SinglePlayerScreen implements Screen, SinglePlayerScreenInterface
{
    private MainGame game;
    private Stage stage;

    private int col_width;
    private Table menuBar;

    SinglePlayerScreen(MainGame game)
    {
        this.game = game;
    }

    @Override
    public void show()
    {
        stage = new Stage();

        final SinglePlayerScreenController controller =
                new SinglePlayerScreenController(this, game);

        Gdx.input.setInputProcessor(stage);

        final Image image = new Image();
        image.setSize(Gdx.graphics.getWidth(),
                      Gdx.graphics.getHeight());
        image.setPosition(0,
                          0);
        final Drawable drawable = new TextureRegionDrawable(new TextureRegion(
                new Texture(OwenFileHandler.getFile("singleplayerScreen",
                                                    ".jpg"))));
        image.setDrawable(drawable);
        stage.addActor(image);

        col_width = Gdx.graphics.getWidth() / 16;
        int row_height = Gdx.graphics.getHeight() / 12;

        final Skin mySkin = new Skin(OwenFileHandler.getFile("skin/glassy-ui",
                                                             ".json"));

        menuBar = new Table();
        menuBar.setSize(col_width * 4,
                        Gdx.graphics.getHeight());
        menuBar.setPosition(col_width * 12,
                            0);

        Button hideButton = new TextButton(">",
                                           mySkin);
        hideButton.setSize((float) (col_width * 0.5),
                           (float) (row_height * 0.5));

        Button newButton = new TextButton("New",
                                          mySkin);
        newButton.setSize(col_width,
                          (float) (row_height * 0.8));

        newButton.setDisabled(true);//TODO change when first Scenario exist

        Button saveButton = new TextButton("Save",
                                           mySkin);
        saveButton.setSize(col_width,
                           (float) (row_height * 0.8));

        Button loadButton = new TextButton("Load",
                                           mySkin);
        loadButton.setSize(col_width,
                           (float) (row_height * 0.8));

        Button backButton = new TextButton("Back",
                                           mySkin);
        backButton.setSize(col_width,
                           (float) (row_height * 0.8));

        hideButton.addListener(controller.newHideButtonListener());

        newButton.addListener(controller.newNewGameButtonListener());

        backButton.addListener(controller.newBackButtonListener());

        menuBar.add(hideButton).top().left();
        menuBar.add(newButton).pad((float) 0.8).minWidth(col_width * 2).prefWidth(999);
        menuBar.row();
        menuBar.add();
        menuBar.add(saveButton).pad((float) 0.8);
        menuBar.row();
        menuBar.add();
        menuBar.add(loadButton).padBottom(row_height * 6);
        menuBar.row();
        menuBar.add();
        menuBar.add(backButton).pad((float) 0.8);

        stage.addActor(menuBar);

    }

    @Override
    public void render(float v)
    {
        Gdx.gl.glClearColor(1,
                            1,
                            1,
                            1);
        Gdx.gl.glClear(GL20.GL_COLOR_WRITEMASK);
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(
            int width,
            int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        stage.dispose();
    }

    public void expandMenuBar()
    {
        menuBar.moveBy(-(float) (col_width * 2),
                       0);
    }

    public void minimizeMenuBar()
    {
        menuBar.moveBy((float) (col_width * 2),
                       0);
    }

    @Override
    public String getCanonicalNameOfMainScreen()
    {
        return MainDesktopScreen.class.getCanonicalName();
    }

    @Override
    public Screen getGameScreen()
    {
        return new GameScreen(game,
                              null);
    }
}

package de.desktop.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import de.core.util.HudInputProcessor;
import de.core.MainGame;
import de.core.screens.PartyMultiplayerScreenInterface;
import de.core.handler.OwenFileHandler;

public class PartyMultiplayerScreen implements Screen, InputProcessor,
                                               PartyMultiplayerScreenInterface
{
    private final MainGame game;
    private final SpriteBatch hudBatch;
    private final Stage stage;
    private final InputMultiplexer inputs;
    private final int col_width;
    private final int row_height;
    private final Skin mySkin;
    private final HudInputProcessor inputProcessor;

    public PartyMultiplayerScreen(MainGame game) {
        this.game = game;

        hudBatch = new SpriteBatch();
        stage = new Stage();
        inputs = new InputMultiplexer();
        this.inputProcessor = new HudInputProcessor(stage, false);
        inputs.addProcessor(this);
        inputs.addProcessor(inputProcessor);
        Gdx.input.setInputProcessor(inputs);


        col_width = Gdx.graphics.getWidth() / 16;
        row_height = Gdx.graphics.getHeight() / 12;

        mySkin = new Skin(OwenFileHandler.getFile("skin/glassy-ui", ".json"));

        Image background = new Image();
        background.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        background.setPosition(0,0);
        Drawable drawable = new TextureRegionDrawable(new TextureRegion(
                new Texture(OwenFileHandler.getFile("mainScreen", ".jpg"))));
        background.setDrawable(drawable);
        stage.addActor(background);
        inputProcessor.addActor(background);

    }

    @Override
    public void show() {

    }

    @Override
    public void render(float v) {

    }

    @Override
    public void resize(int i, int i1) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public boolean keyDown(int i) {
        return false;
    }

    @Override
    public boolean keyUp(int i) {
        return false;
    }

    @Override
    public boolean keyTyped(char c) {
        return false;
    }

    @Override
    public boolean touchDown(int i, int i1, int i2, int i3) {
        return false;
    }

    @Override
    public boolean touchUp(int i, int i1, int i2, int i3) {
        return false;
    }

    @Override
    public boolean touchDragged(int i, int i1, int i2) {
        return false;
    }

    @Override
    public boolean mouseMoved(int i, int i1) {
        return false;
    }

    @Override
    public boolean scrolled(int i) {
        return false;
    }
}

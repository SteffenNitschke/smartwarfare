package de.desktop.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.core.MainGame;
import de.core.handler.OwenFileHandler;
import de.core.screens.MultiplayerStartScreenInterface;
import de.core.uiController.MultiplayerStartScreenController;
import de.core.util.HudInputProcessor;

/**
 * Created by Gandail on 10.06.2017.
 */
public class MultiPlayerStartScreen extends NewsScreen implements MultiplayerStartScreenInterface,
                                                                  InputProcessor
{
    private String loggingTag = getClass().getSimpleName();

    MultiPlayerStartScreen(final MainGame game)
    {
        super(game);
    }

    @Override
    public void show()
    {
        final MultiplayerStartScreenController controller
                = new MultiplayerStartScreenController(this);

        game.getConnection().getGameClient().getAllGameOf(game.getUser().get_id().getIdentifier());

        this.inputProcessor = new HudInputProcessor(stage,
                                                    false);

        final InputMultiplexer inputs = new InputMultiplexer();
        inputs.addProcessor(this);
        inputs.addProcessor(inputProcessor);
        Gdx.input.setInputProcessor(inputs);

        this.mySkin = new Skin(OwenFileHandler.getFile("skin/glassy-ui",
                                                       ".json"));

        this.stage = new Stage();

        final Image background = new Image();
        background.setSize(Gdx.graphics.getWidth(),
                           Gdx.graphics.getHeight());
        background.setPosition(0,
                               0);
        final Drawable drawable = new TextureRegionDrawable(new TextureRegion(
                new Texture(OwenFileHandler.getFile("mainScreen",
                                                    ".jpg"))));
        background.setDrawable(drawable);
        stage.addActor(background);
        inputProcessor.addActor(background);

        col_width = Gdx.graphics.getWidth() / (float) 16;
        row_height = Gdx.graphics.getHeight() / (float) 12;

        Button internetButton = new TextButton("Internet",
                                               mySkin);
        internetButton.setSize(col_width * 4,
                               (float) (row_height * 0.8));
        internetButton.setPosition(col_width * 12,
                                   (float) (row_height * 10.5));
        internetButton.addListener(controller.newInternetButtonListener());

        stage.addActor(internetButton);
        inputProcessor.addIdleActor(internetButton);

        Button partyButton = new TextButton("Party Modus",
                                            mySkin);
        partyButton.setSize(col_width * 4,
                            (float) (row_height * 0.8));
        partyButton.setPosition(col_width * 12,
                                (float) (row_height * 8.5));
        partyButton.addListener(controller.newPartyButtonListener());

        stage.addActor(partyButton);
        inputProcessor.addIdleActor(partyButton);

        Button backButton = new TextButton("Back",
                                           mySkin);
        backButton.setSize(col_width * 4,
                           (float) (row_height * 0.8));
        backButton.setPosition(col_width * 12,
                               (float) (row_height * 0.5));
        backButton.addListener(controller.newBackButtonListener());

        stage.addActor(backButton);
        inputProcessor.addIdleActor(backButton);

        super.show(stage, inputProcessor);

        Gdx.app.log(loggingTag,
                    "Open Multiplayer Screen");
    }

    public void render(float v)
    {

        Gdx.gl.glClearColor(1,
                            1,
                            1,
                            1);
        Gdx.gl.glClear(GL20.GL_COLOR_WRITEMASK);
        stage.draw();
    }

    @Override
    public void resize(
            int width,
            int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        stage.dispose();
        Gdx.app.log(loggingTag,
                    "Dispose");
    }

    @Override
    public boolean keyDown(int i)
    {
        this.stage.keyDown(i);
        return false;
    }

    @Override
    public boolean keyUp(int i)
    {
        this.stage.keyUp(i);
        return false;
    }

    @Override
    public boolean keyTyped(char c)
    {
        this.stage.keyTyped(c);
        return false;
    }

    @Override
    public boolean touchDown(
            int i,
            int i1,
            int i2,
            int i3)
    {
        this.stage.touchDown(i,
                             i1,
                             i2,
                             i3);
        return false;
    }

    @Override
    public boolean touchUp(
            int i,
            int i1,
            int i2,
            int i3)
    {
        this.stage.touchUp(i,
                           i1,
                           i2,
                           i3);
        return false;
    }

    @Override
    public boolean touchDragged(
            int i,
            int i1,
            int i2)
    {
        this.stage.touchDragged(i,
                                i1,
                                i2);
        return false;
    }

    @Override
    public boolean mouseMoved(
            int i,
            int i1)
    {
        this.stage.mouseMoved(i,
                              i1);
        return false;
    }

    @Override
    public boolean scrolled(int i)
    {
        this.stage.scrolled(i);
        return false;
    }

    @Override
    public boolean existInternetMultiPlayerScreen()
    {
        return game.getScreenHandler().getScreen(InternetMultiPlayerScreen.class
                                                         .getCanonicalName()) == null;
    }

    @Override
    public void addInternetMultiPlayerScreen()
    {
        game.getScreenHandler().addScreen(new InternetMultiPlayerScreen(game));
    }

    @Override
    public void gotoInternetMultiPlayerScreen()
    {
        game.setScreen(game.getScreenHandler()
                           .getScreen(InternetMultiPlayerScreen.class.getCanonicalName()));
    }

    @Override
    public void gotoMainScreen()
    {
        game.setScreen(game.getScreenHandler()
                           .getScreen(MainDesktopScreen.class.getCanonicalName()));
    }
}

package de.desktop.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.core.editorController.EditorController;
import de.core.editorController.EditorWorldController;
import de.desktop.screens.windows.LoadScreen;
import de.desktop.screens.windows.NewMapScreen;
import de.desktop.util.InterfaceContainer;
import lombok.Getter;
import de.core.MainGame;
import de.core.handler.OwenFileHandler;
import de.core.model.Map;
import de.core.screens.EditorScreenInterface;
import de.core.uiController.EditorScreenController;
import de.core.util.FieldType;
import de.core.util.Utils;


/**
 * Created by Gandail on 12.08.2017.
 */
public class EditorScreen extends GameScreenSuperclass implements EditorScreenInterface
{
    private Button newButton;
    private Button loadButton;
    private Button saveButton;
    private Button returnButton;
    private Image menuBackground;
    private Button choosePlayerButton;

    private EditorWorldController worldController;
    @Getter
    private EditorScreenController controller;
    private InterfaceContainer container;
    private LoadScreen loadScreen;
    private NewMapScreen newMapScreen;
    private EditorController selectedType;
    private Map oldMap;

    EditorScreen(MainGame game)
    {
        this.game = game;
    }

    @Override
    public void show()
    {
        //init Screen------------------------------------------------------------------------------

        super.show();
        this.controller = new EditorScreenController(game, this);

        Image background = new Image();
        background.setSize(Gdx.graphics.getWidth() * 3,
                           Gdx.graphics.getHeight() * 3);
        background.setPosition(-Gdx.graphics.getWidth(),
                               -Gdx.graphics.getHeight());
        final Drawable drawable = new TextureRegionDrawable(new TextureRegion(
                new Texture(OwenFileHandler.getFile("background",
                                                    ".jpg"))));
        background.setDrawable(drawable);
        hudProcessor.addActor(background);
        stage.addActor(background);

        final Skin mySkin = new Skin(OwenFileHandler.getFile("skin/glassy-ui",
                                                             ".json"));

        //init HUD --------------------------------------------------------------------------------

        container = new InterfaceContainer((float) (col_width * 4.0),
                                           (row_height * 6));
        container.setPosition(0,
                              (float) (row_height * 6.0));
        Texture texture = new Texture(OwenFileHandler.getFile("backgroundTuerkis",
                                                                    ".jpg"));
        Drawable drawable1 = new TextureRegionDrawable(new TextureRegion(texture));
        background = new Image();
        background.setDrawable(drawable1);
        container.setBackground(background);
        hudProcessor.addActor(background);

        //--add Container Child's----------------------------------------------------------------
        {
            for (FieldType type :
                    FieldType.values())
            {
                if (type.equals(FieldType.FIELD_TYPE_BUILDING))
                {
                    for (String buildingType :
                            Utils.ALL_BUILDING_TYPES)
                    {
                        addImage(buildingType);
                    }
                }
                else
                {
                    addImage(type.toString());
                }
            }
            for (String unitType : Utils.ALL_UNIT_TYPES)
            {
                addImage(unitType);
            }
        }
        try
        {
            container.setMargin(4);
            container.setColumns(5);
            container.order();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        for (Actor actor :
                container.getActors())
        {
            hudProcessor.addActor(actor);
        }

        //init Buttons-----------------------------------------------------------------------------

        newButton = new ImageTextButton("New",
                                        mySkin);
        newButton.setSize((float) (col_width * 3.0),
                          (float) (row_height * 0.8));
        newButton.addListener(new InputListener()
        {
            @Override
            public void touchUp(
                    InputEvent event,
                    float x,
                    float y,
                    int pointer,
                    int button)
            {
                if (hudProcessor.isIdle())
                {
                    hudProcessor.idle(false);
                    game.getScreenHandler().setScreenInFrontOf(false);
                    newMapScreen = null;
                }
                else
                {
                    hudProcessor.idle(true);
                    newMapScreen = new NewMapScreen(hudProcessor,
                                                    game)
                            .withPosition(Gdx.graphics.getWidth() / 4.0,
                                          Gdx.graphics.getHeight() / 4.0)
                            .withSize(Gdx.graphics.getWidth() / 2.0,
                                      Gdx.graphics.getHeight() / 2.0);
                    game.getScreenHandler().setScreenInFrontOf(true);
                }
            }
        });
        newButton.setPosition((float) (col_width * 0.5),
                              (float) (row_height * 3.5));
        hudProcessor.addActor(newButton);

        saveButton = new ImageTextButton("Save",
                                         mySkin);
        saveButton.setSize((float) (col_width * 3.0),
                           (float) (row_height * 0.8));
        saveButton.addListener(controller.getSaveButtonListener());
        saveButton.setPosition((float) (col_width * 0.5),
                               (float) (row_height * 2.5));
        hudProcessor.addActor(saveButton);

        loadButton = new ImageTextButton("Load",
                                         mySkin);
        loadButton.setSize((float) (col_width * 3.0),
                           (float) (row_height * 0.8));
        loadButton.addListener(new InputListener()
        {
            @Override
            public void touchUp(
                    InputEvent event,
                    float x,
                    float y,
                    int pointer,
                    int button)
            {
                if (hudProcessor.isIdle())
                {
                    hudProcessor.idle(false);
                    game.getScreenHandler().setScreenInFrontOf(false);
                    loadScreen = null;
                }
                else
                {
                    hudProcessor.idle(true);
                    loadScreen = new LoadScreen(game,
                                                hudProcessor)
                            .withPosition(Gdx.graphics.getWidth() / 4.0,
                                          Gdx.graphics.getHeight() / 4.0)
                            .withSize(Gdx.graphics.getWidth() / 2.0,
                                      Gdx.graphics.getHeight() / 2.0);
                    game.getScreenHandler().setScreenInFrontOf(true);
                }
            }
        });
        loadButton.setPosition((float) (col_width * 0.5),
                               (float) (row_height * 1.5));
        hudProcessor.withoutIdle(loadButton);
        hudProcessor.addActor(loadButton);

        returnButton = new ImageTextButton("Return",
                                           mySkin);
        returnButton.setSize((float) (col_width * 3.0),
                             (float) (row_height * 0.8));
        returnButton.addListener(controller.getReturnButtonListener());
        returnButton.setPosition((float) (col_width * 0.5),
                                 (float) (row_height * 0.5));
        hudProcessor.addActor(returnButton);

        choosePlayerButton = new TextButton("Player",
                                            mySkin);
        choosePlayerButton.setSize((float) (col_width * 3.0),
                                   (float) (row_height * 0.8));
        choosePlayerButton.setPosition((float) (col_width * 0.5),
                                       (float) (row_height * 4.5));
        choosePlayerButton.addListener(controller.getChoosePlayerButtonListener());

        hudProcessor.addActor(choosePlayerButton);

        //set Background---------------------------------------------------------------------------

        texture = new Texture(OwenFileHandler.getFile("backgroundBlack",
                                                      ".jpg"));
        drawable1 = new TextureRegionDrawable(new TextureRegion(texture));
        menuBackground = new Image();
        menuBackground.setDrawable(drawable1);
        menuBackground.setSize((float) (col_width * 4.0),
                               (row_height * 13));
        menuBackground.setPosition(0,
                                   0);
        hudProcessor.addActor(menuBackground);

        //new Map function-------------------------------------------------------------------------

        if (game.getMapHandler().getEditableMap() == null)
        {
            this.newMapScreen = new NewMapScreen(hudProcessor, game);
        }

    }

    @Override
    public void render(float v)
    {

        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE))
        {
            game.setScreen(new MainDesktopScreen(game));
        }
        hudProcessor.handleKeyInput();

        super.render(v);

        hudBatch.begin();
        menuBackground.draw(hudBatch,
                            1f);
        newButton.draw(hudBatch,
                       1f);
        loadButton.draw(hudBatch,
                        1f);
        saveButton.draw(hudBatch,
                        1f);
        returnButton.draw(hudBatch,
                          1f);
        choosePlayerButton.draw(hudBatch,
                                1f);

        container.getBackground().draw(hudBatch,
                                       1f);
        for (Actor actor : container.getActors())
        {
            actor.draw(hudBatch,
                       1f);
        }

        final Map editableMap = game.getMapHandler().getEditableMap();

        if (editableMap != null)
        {
            if (editableMap != this.oldMap)
            {
                if (this.worldController != null)
                {
                    this.worldController.removeYou();
                }
                editableMap.getGame().setMap(editableMap);
                worldController = new EditorWorldController(editableMap.getGame(),
                                                            this.stage,
                                                            this);
                this.oldMap = editableMap;
            }
        }

        if (!game.getScreenHandler().isScreenInFrontOf())
        {
            loadScreen = null;
            newMapScreen = null;
        }
        else
        {
            if (loadScreen != null)
            {
                loadScreen.draw();
            }
            if (newMapScreen != null)
            {
                newMapScreen.draw();
            }
        }

        hudBatch.end();

        camera.update();
    }
    @Override
    public void dispose()
    {
        super.dispose();
    }

    @Override
    public String getCanonicalNameOfMainScreen()
    {
        return MainDesktopScreen.class.getCanonicalName();
    }

    @Override
    public Button getChoosePlayerButton()
    {
        return this.choosePlayerButton;
    }

    @Override
    public boolean mouseMoved(
            int i,
            int i1)
    {
        stage.mouseMoved(i,
                         i1);
        return false;
    }

    @Override
    public boolean scrolled(int amount)
    {
        return super.scrolled(amount);
    }

    public void setMapSize(
            float width,
            float height)
    {
        this.mapWidth = width;
        this.mapHeight = height;
    }

    public EditorController getSelectedType()
    {
        return selectedType;
    }

    public void setSelectedType(final EditorController selectedType)
    {
        if (this.selectedType != null && !this.selectedType.getType().equals(selectedType
                                                                                     .getType()))
        {
            this.selectedType.removeMarked();
        }
        this.selectedType = selectedType;
        Gdx.app.log(loggingTag,
                    "Set selected Type " + selectedType);
    }

    private void addImage(final String type)
    {
        final Texture texture = new Texture(OwenFileHandler.getFile(type,
                                                              ".jpg"));
        final Drawable drawable1 = new TextureRegionDrawable(new TextureRegion(texture));
        Image image = new Image();
        image.setDrawable(drawable1);
        image.addListener(new EditorController(this,
                                               image,
                                               type));
        image.setName(type);
        container.addActor(image);
        hudProcessor.addActor(image);
    }
}

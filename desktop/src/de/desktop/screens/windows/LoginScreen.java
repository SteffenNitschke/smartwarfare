package de.desktop.screens.windows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.core.MainGame;
import de.core.handler.OwenFileHandler;
import de.core.screens.windows.LoginScreenInterface;
import de.core.uiController.windows.LoginScreenController;
import de.core.util.HudInputProcessor;

public class LoginScreen extends Actor implements LoginScreenInterface
{

    private MainGame game;
    private double y;
    private double x;
    private double height;
    private double width;

    private ImageTextButton login;
    private ImageTextButton exit;
    private ImageTextButton registrate;
    private TextField nameTextField;
    private TextField passwordTextField;
    private HudInputProcessor hudInputProcessor;
    private Image menuBackground;
    private Stage stage;
    private String loggingTag = getClass().getSimpleName();


    public LoginScreen(
            final MainGame game,
            final HudInputProcessor hudInputProcessor)
    {
        Gdx.app.log(loggingTag,
                    "LoginScreen");

        final LoginScreenController controller = new LoginScreenController(game,
                                                                     this);

        this.hudInputProcessor = hudInputProcessor;
        this.game = game;
        this.stage = new Stage();
        final Skin mySkin = new Skin(OwenFileHandler.getFile("skin/glassy-ui",
                                                       ".json"));

        final Texture texture = new Texture(OwenFileHandler.getFile("backgroundLoginWindow",
                                                              ".jpg"));
        final TextureRegionDrawable drawable1 = new TextureRegionDrawable(new TextureRegion(texture));
        menuBackground = new Image();
        menuBackground.setDrawable(drawable1);
        stage.addActor(menuBackground);
        hudInputProcessor.addActor(menuBackground);
        hudInputProcessor.withoutIdle(menuBackground);

        //-------------------------------------------------

        nameTextField = new TextField("Name",
                                      mySkin);
        stage.addActor(nameTextField);
        hudInputProcessor.addActor(nameTextField);
        hudInputProcessor.withoutIdle(nameTextField);


        passwordTextField = new TextField("Password",
                                          mySkin);
        stage.addActor(passwordTextField);
        hudInputProcessor.addActor(passwordTextField);
        hudInputProcessor.withoutIdle(passwordTextField);

        login = new ImageTextButton("Login",
                                    mySkin);
        login.addListener(controller.getLoginButtonListener());
        stage.addActor(login);
        hudInputProcessor.addActor(login);
        hudInputProcessor.withoutIdle(login);
        hudInputProcessor.setOkButton(login);

        exit = new ImageTextButton("Exit",
                                   mySkin);
        exit.addListener(controller.getExitButtonListener());
        stage.addActor(exit);
        hudInputProcessor.addActor(exit);
        hudInputProcessor.withoutIdle(exit);
        hudInputProcessor.setExitButton(exit);

        registrate = new ImageTextButton("Registrate",
                                         mySkin);
        registrate.addListener(controller.getRegistrationListener());
        stage.addActor(registrate);
        hudInputProcessor.addActor(registrate);
        hudInputProcessor.withoutIdle(registrate);
        hudInputProcessor.setExitButton(registrate);
    }

    private void resize()
    {
        Gdx.app.log(loggingTag,
                    "resize()");
        menuBackground.setSize((float) width,
                               (float) height);
        menuBackground.setPosition((float) x,
                                   (float) y);
        menuBackground.toBack();

        nameTextField.setWidth((float) (width * 0.9));
        nameTextField.setHeight((float) (height * 0.2));
        nameTextField.setX((float) (x + width * 0.05));
        nameTextField.setY((float) (y + height * 0.65));

        passwordTextField.setWidth((float) (width * 0.9));
        passwordTextField.setHeight((float) (height * 0.2));
        passwordTextField.setX((float) (x + width * 0.05));
        passwordTextField.setY((float) (y + height * 0.35));

        login.setWidth((float) (width * 0.3));
        login.setHeight((float) (height * 0.2));
        login.setX((float) (x + width * 0.05));
        login.setY((float) (y + height * 0.05));

        exit.setWidth((float) (width * 0.3));
        exit.setHeight((float) (height * 0.2));
        exit.setX((float) (x + width * 0.35));
        exit.setY((float) (y + height * 0.05));

        registrate.setWidth((float) (width * 0.3));
        registrate.setHeight((float) (height * 0.2));
        registrate.setX((float) (x + width * 0.65));
        registrate.setY((float) (y + height * 0.05));
    }

    public void close()
    {
        hudInputProcessor.removeActor(nameTextField);
        hudInputProcessor.removeActor(passwordTextField);
        hudInputProcessor.removeActor(login);
        hudInputProcessor.removeActor(exit);
        hudInputProcessor.removeActor(registrate);
        hudInputProcessor.removeActor(menuBackground);
        hudInputProcessor.removeExitButton();
        hudInputProcessor.removeOkButton();
        stage.clear();
        game.getScreenHandler().setScreenInFrontOf(false);
        Gdx.app.log(loggingTag,
                    "close()");
    }

    public LoginScreen withPosition(
            double x,
            double y)
    {
        this.x = x;
        this.y = y;
        resize();
        return this;
    }

    public LoginScreen withSize(
            double width,
            double height)
    {
        this.width = width;
        this.height = height;
        resize();
        return this;
    }

    public void draw()
    {
        stage.draw();
    }

    @Override
    public void loginSuccessful(final boolean successful)
    {
        if(successful)
        {
            close();
        }
        else
        {
            nameTextField.setMessageText("Wrong name");
            passwordTextField.setMessageText("Wrong password");
        }
    }

    @Override
    public TextField getNameTextField()
    {
        return nameTextField;
    }

    @Override
    public TextField getPasswordTextField()
    {
        return passwordTextField;
    }
}

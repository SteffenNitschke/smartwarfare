package de.desktop.screens.windows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.core.util.HudInputProcessor;
import de.desktop.screens.GameScreen;
import de.desktop.screens.MainDesktopScreen;
import de.core.MainGame;
import de.core.model.Game;
import de.core.handler.OwenFileHandler;
import de.core.screens.windows.SmallScreen;

import java.util.Iterator;

public class MenuScreen extends Actor implements SmallScreen
{

    private MainGame game;
    private double y;
    private double x;
    private double height;
    private double width;

    private HudInputProcessor hudInputProcessor;
    private Image menuBackground;
    private Stage stage;

    private ImageTextButton restartButton;
    private ImageTextButton switchButton;
    private ImageTextButton backToGameButton;
    private ImageTextButton backToMenuButton;
    private String loggingTag = this.getClass().getSimpleName();

    public MenuScreen(
            final MainGame game,
            final HudInputProcessor hudInputProcessor,
            GameScreen screen)
    {

        Gdx.app.log(loggingTag,
                    "MenuScreen");
        this.hudInputProcessor = hudInputProcessor;
        this.game = game;
        this.stage = new Stage();
        Skin mySkin = new Skin(OwenFileHandler.getFile("skin/glassy-ui",
                                                       ".json"));

        Texture texture = new Texture(OwenFileHandler.getFile("backgroundLoginWindow",
                                                              ".jpg"));
        TextureRegionDrawable drawable1 = new TextureRegionDrawable(new TextureRegion(texture));
        menuBackground = new Image();
        menuBackground.setDrawable(drawable1);
        stage.addActor(menuBackground);
        hudInputProcessor.addActor(menuBackground);
        hudInputProcessor.withoutIdle(menuBackground);

        //-------------------------------------------------


        restartButton = new ImageTextButton("Restart",
                                            mySkin);
        restartButton.addListener(new InputListener()
        {
            @Override
            public void touchUp(
                    InputEvent event,
                    float x,
                    float y,
                    int pointer,
                    int button)
            {
                Gdx.app.log(loggingTag,
                            "Restart Button");
                if (game.getScreenHandler().isScreenInFrontOf())
                {
                    game.restartGame(screen.getCurrentGame().getGameID());
                }
            }
        });
        stage.addActor(restartButton);
        hudInputProcessor.addActor(restartButton);
        hudInputProcessor.withoutIdle(restartButton);

        switchButton = new ImageTextButton("Switch Game",
                                           mySkin);
        switchButton.addListener(new InputListener()
        {
            @Override
            public void touchUp(
                    InputEvent event,
                    float x,
                    float y,
                    int pointer,
                    int button)
            {
                Gdx.app.log(loggingTag,
                            "SwitchButton");
                if (game.getScreenHandler().isScreenInFrontOf())
                {
                    screen.dispose();
                    Game nextGame;
                    Iterator<Game> iterator = game.getUser().getGames().iterator();
                    while (true)
                    {
                        Game game1 = iterator.next();
                        if (game1.getGameID().equals(screen.getCurrentGame().getGameID()))
                        {
                            if (iterator.hasNext())
                            {
                                nextGame = iterator.next();
                            }
                            else
                            {
                                nextGame = game.getUser().getGames().iterator().next();
                            }
                            break;
                        }
                    }

                    game.setScreen(new GameScreen(game,
                                                  nextGame));

                    game.getScreenHandler().setScreenInFrontOf(false);
                    hudInputProcessor.idle(false);
                }
            }
        });
        stage.addActor(switchButton);
        hudInputProcessor.addActor(switchButton);
        hudInputProcessor.withoutIdle(switchButton);

        backToGameButton = new ImageTextButton("Back to Game",
                                               mySkin);
        backToGameButton.addListener(new InputListener()
        {
            @Override
            public void touchUp(
                    InputEvent event,
                    float x,
                    float y,
                    int pointer,
                    int button)
            {
                Gdx.app.log(loggingTag,
                            "Back To Game Button");
                if (game.getScreenHandler().isScreenInFrontOf())
                {
                    close();
                }
            }
        });
        stage.addActor(backToGameButton);
        hudInputProcessor.addActor(backToGameButton);
        hudInputProcessor.withoutIdle(backToGameButton);

        backToMenuButton = new ImageTextButton("Back to Menu",
                                               mySkin);
        backToMenuButton.addListener(new InputListener()
        {
            @Override
            public void touchUp(
                    InputEvent event,
                    float x,
                    float y,
                    int pointer,
                    int button)
            {
                Gdx.app.log(loggingTag,
                            "Back To Menu Button");
                if (game.getScreenHandler().isScreenInFrontOf())
                {
                    game.setScreen(game.getScreenHandler()
                                       .getScreen(MainDesktopScreen.class.getCanonicalName()));
                    close();
                }
            }
        });
        stage.addActor(backToMenuButton);
        hudInputProcessor.addActor(backToMenuButton);
        hudInputProcessor.withoutIdle(backToMenuButton);
    }

    public void resize()
    {
        menuBackground.setSize((float) width,
                               (float) height);
        menuBackground.setPosition((float) x,
                                   (float) y);

        restartButton.setWidth((float) (width * 0.9));
        restartButton.setHeight((float) (height * 0.2));
        restartButton.setX((float) (x + width * 0.05));
        restartButton.setY((float) (y + height * 0.05));

        switchButton.setSize((float) (width * 0.9),
                             (float) (height * 0.2));
        switchButton.setPosition((float) (x + width * 0.05),
                                 (float) (y + height * 0.3));

        backToGameButton.setSize((float) (width * 0.9),
                                 (float) (height * 0.2));
        backToGameButton.setPosition((float) (x + width * 0.05),
                                     (float) (y + height * 0.55));

        backToMenuButton.setSize((float) (width * 0.9),
                                 (float) (height * 0.2));
        backToMenuButton.setPosition((float) (x + width * 0.05),
                                     (float) (y + height * 0.8));
    }

    public void close()
    {
        hudInputProcessor.removeActor(menuBackground);
        hudInputProcessor.removeActor(restartButton);
        hudInputProcessor.removeActor(switchButton);
        hudInputProcessor.removeActor(backToGameButton);
        hudInputProcessor.removeActor(backToMenuButton);
        stage.getActors().removeValue(menuBackground,
                                      true);
        stage.getActors().removeValue(restartButton,
                                      true);
        stage.getActors().removeValue(switchButton,
                                      true);
        stage.getActors().removeValue(backToGameButton,
                                      true);
        stage.getActors().removeValue(backToMenuButton,
                                      true);
        game.getScreenHandler().setScreenInFrontOf(false);
        hudInputProcessor.idle(false);
        Gdx.app.log(loggingTag,
                    "close()");
    }

    public MenuScreen withPosition(
            double x,
            double y)
    {
        this.x = x;
        this.y = y;
        resize();
        return this;
    }

    public MenuScreen withSize(
            double width,
            double height)
    {
        this.width = width;
        this.height = height;
        resize();
        return this;
    }

    public void draw()
    {
        stage.draw();
    }
}

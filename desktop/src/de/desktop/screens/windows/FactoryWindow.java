package de.desktop.screens.windows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

import java.util.HashSet;

import de.core.util.HudInputProcessor;
import de.desktop.screens.GameScreen;
import de.core.MainGame;
import de.core.model.buildungs.BuildingInterface;
import de.core.model.buildungs.Factory;
import de.core.model.units.UnitInterface;
import de.core.handler.OwenFileHandler;
import de.core.screens.windows.FactoryWindowInterface;
import de.core.util.TurnEventListener;
import de.core.model.units.UnitBuilder;
import de.core.util.Utils;

public class FactoryWindow extends Actor implements FactoryWindowInterface
{
    private final String loggingTag = getClass().getSimpleName();
    private final MainGame mainGame;
    private ScrollPane scrollPane;
    private HudInputProcessor hudInputProcessor;
    private Image menuBackground;
    private Stage stage;

    public FactoryWindow(
            final HudInputProcessor hudInputProcessor,
            final BuildingInterface building,
            final TurnEventListener listener,
            final MainGame mainGame)
    {
        this.hudInputProcessor = hudInputProcessor;
        this.mainGame = mainGame;
        this.stage = new Stage();
        final Skin mySkin = new Skin(OwenFileHandler.getFile("skin/glassy-ui",
                                                       ".json"));

        Gdx.app.log(loggingTag,
                    "start");

        final Texture backgroundTexture = new Texture(OwenFileHandler.getFile("backgroundBlack",
                                                              ".jpg"));
        final TextureRegionDrawable backgroundDrawable = new TextureRegionDrawable(new TextureRegion(backgroundTexture));
        menuBackground = new Image();
        menuBackground.setDrawable(backgroundDrawable);
        stage.addActor(menuBackground);
        hudInputProcessor.addActor(menuBackground);
        hudInputProcessor.withoutIdle(menuBackground);

        //-------------------------------------------------

        final List<TextField> scrollPaneContent = new List<>(mySkin);
        Gdx.app.log(loggingTag,
                    building.getType());
        final HashSet<String> productionRange = ((Factory) building).canCreate();
        for (String type : productionRange)
        {
            Gdx.app.log(loggingTag,
                        "Type: " + type);
        }
        final Array<UnitSelector> unitSelectors = new Array<>();

        for (String type : productionRange)
        {
            final UnitInterface unit = UnitBuilder.build(type);

            final UnitSelector selector = new UnitSelector(unit);
            selector.setSize(50,
                             50);
            unitSelectors.add(selector);
            hudInputProcessor.addActor(selector);
            hudInputProcessor.withoutIdle(selector);

        }
        scrollPaneContent.setItems(unitSelectors);
        scrollPaneContent.addListener(new InputListener()
        {
            @Override
            public void touchUp(
                    InputEvent event,
                    float x,
                    float y,
                    int pointer,
                    int button)
            {
                Gdx.app.log(loggingTag,
                            "touchUp");
                final UnitInterface unit = ((UnitSelector) ((List) event.getTarget()).getSelected())
                        .getUnit();
                unit.setPlayer(building.getPlayer());
                unit.setIsMoved(true);
                unit.setHaveAttacked(true);
                if (((Factory) building).orderUnit(unit))
                {
                    listener.handleCreateMessage(unit,
                                                 building.getField());
                    mainGame.setScreen(new GameScreen(mainGame,
                                                      building.getGame()));
                }
                close();
            }
        });

        scrollPane = new ScrollPane(scrollPaneContent);
        scrollPane.setSmoothScrolling(false);
        scrollPane.setTransform(true);
        scrollPane.setScale(1f);
        stage.addActor(scrollPane);
        hudInputProcessor.addActor(scrollPane);
        hudInputProcessor.withoutIdle(scrollPane);
    }

    public void resize()
    {

        float width = Gdx.graphics.getWidth() / 2f;
        float height = Gdx.graphics.getHeight() / 2f;
        float x = Gdx.graphics.getWidth() / 4f;
        float y = Gdx.graphics.getHeight() / 4f;
        menuBackground.setSize(width,
                               height);
        menuBackground.setPosition(x,
                                   y);
        menuBackground.toBack();

        scrollPane.setWidth(width * 0.98f);
        scrollPane.setHeight(height * 0.7f);
        scrollPane.setX(x + width * 0.01f);
        scrollPane.setY(y + height * 0.28f);

    }

    public void close()
    {
        Gdx.app.log(loggingTag,
                    "close");

        hudInputProcessor.idle(false);
        hudInputProcessor.removeActor(scrollPane);
        hudInputProcessor.removeActor(menuBackground);
        hudInputProcessor.removeExitButton();
        hudInputProcessor.removeOkButton();

        stage.getActors().removeValue(scrollPane,
                                      true);
        stage.getActors().removeValue(menuBackground,
                                      true);
        hudInputProcessor.idle(false);
        mainGame.getScreenHandler().setScreenInFrontOf(false);

    }

    public void draw()
    {
        stage.draw();
    }

    private class UnitSelector extends Actor
    {

        private UnitInterface unit;
        private int price;
        private Image unitImage;
        private TextField priceField;
        private Skin mySkin;

        private double width;
        private double height;
        private double positionX;
        private double positionY;

        UnitSelector(final UnitInterface unit)
        {

            this.unit = unit;
            Gdx.app.log(loggingTag,
                        "UnitInterface " + unit.getType());

            mySkin = new Skin(OwenFileHandler.getFile("skin/glassy-ui",
                                                      ".json"));
            Texture texture = new Texture(Gdx.files.internal(
                    Utils.ASSET_PATH + unit.getType() + ".jpg"));
            TextureRegionDrawable drawable1 = new TextureRegionDrawable(new TextureRegion(texture));
            unitImage = new Image();
            unitImage.setDrawable(drawable1);
            unitImage.setSize((float) height - 1,
                              (float) height - 1);
            unitImage.setPosition((float) positionX + 1,
                                  (float) positionY + 2.5f);

            stage.addActor(unitImage);
            hudInputProcessor.addActor(unitImage);
            hudInputProcessor.withoutIdle(unitImage);

            price = Utils.getPriceOf(unit.getType());

            priceField = new TextField(price + "C",
                                       mySkin);
            priceField.setSize((float) height - 3,
                               (float) (width - height));
            priceField.setPosition((float) (positionX + height + 2),
                                   (float) (positionY + ((height / 2) - (width - height) / 2)));
            stage.addActor(priceField);
            hudInputProcessor.addActor(priceField);
            hudInputProcessor.withoutIdle(priceField);

        }

        public void setSize(
                final double width,
                final double height)
        {
            this.width = width;
            this.height = height;
            resize();
        }

        public void setPosition(
                final double x,
                final double y)
        {
            positionX = x;
            positionY = y;
            resize();
        }

        private void resize()
        {
            unitImage.setSize((float) height - 5,
                              (float) height - 5);
            unitImage.setPosition((float) positionX + 2.5f,
                                  (float) positionY + 2.5f);
            priceField.setSize((float) height - 3,
                               (float) (width - height));
            priceField.setPosition((float) (positionX + height + 2),
                                   (float) (positionY + ((height / 2) - (width - height) / 2)));
        }

        UnitInterface getUnit()
        {
            return unit;
        }
    }

}

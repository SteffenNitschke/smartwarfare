package de.desktop.screens.windows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import de.core.MainGame;
import de.core.util.HudInputProcessor;
import de.core.handler.OwenFileHandler;

public class NewMapScreen extends Actor {

    private final TextField xField;
    private final TextField yField;
    private final TextField editorNameField;
    private final TextField mapNameField;
    private final TextField numberOfPlayersField;
    private MainGame game;
    private double y;
    private double x;
    private double height;
    private double width;

    private ImageTextButton okBtn;
    private ImageTextButton exit;
    private HudInputProcessor hudInputProcessor;
    private Image menuBackground;
    private Stage stage;

    private String loggingTag = getClass().getSimpleName();

    public NewMapScreen(HudInputProcessor hudInputProcessor, final MainGame game) {

        this.hudInputProcessor = hudInputProcessor;
        this.game = game;
        this.stage = new Stage();
        Skin mySkin = new Skin(OwenFileHandler.getFile("skin/glassy-ui", ".json"));

        Texture texture = new Texture(OwenFileHandler.getFile("backgroundBlack", ".jpg"));
        TextureRegionDrawable drawable1 = new TextureRegionDrawable(new TextureRegion(texture));
        menuBackground = new Image();
        menuBackground.setDrawable(drawable1);
        menuBackground.setSize((float) width, (float) height);
        menuBackground.setPosition((float) x, (float) y);
        stage.addActor(menuBackground);
        hudInputProcessor.addActor(menuBackground);
        hudInputProcessor.withoutIdle(menuBackground);

        //init Button------------------------------------------------------------------------------

        InputListener okListener = new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                try {
                    try{
                        Integer.parseInt(xField.getText());
                    } catch(NumberFormatException e) {
                        xField.setMessageText("Wrong Input");
                        xField.setText("");
                    }
                    try{
                        Integer.parseInt(yField.getText());
                    } catch(NumberFormatException e) {
                        yField.setMessageText("Wrong Input");
                        yField.setText("");
                    }
                    try{
                        Integer.parseInt(numberOfPlayersField.getText());
                    }catch(NumberFormatException e) {
                        numberOfPlayersField.setMessageText("Wrong Input");
                        numberOfPlayersField.setText("");
                    }
                    Gdx.app.log(loggingTag, "touchUp New Btn");
                    game.getMapHandler().setEditableMap(
                            game.getMapHandler().createNewDefaultMap(mapNameField.getText()
                            , editorNameField.getText()
                            , Integer.parseInt(numberOfPlayersField.getText())
                            , Integer.parseInt(xField.getText())
                            , Integer.parseInt(yField.getText())));
                    close();
                }catch(Exception e) {
                    Gdx.app.error(loggingTag, "Exception", e);
                }
            }
        };

        okBtn = new ImageTextButton("Ok", mySkin);
        okBtn.setWidth((float) (width * 0.1));
        okBtn.setHeight((float) (height * 0.2));
        okBtn.setX((float) (x + width * 0.05));
        okBtn.setY((float) (y + height * 0.05));
        okBtn.addListener(okListener);
        stage.addActor(okBtn);
        hudInputProcessor.addActor(okBtn);
        hudInputProcessor.withoutIdle(okBtn);
        hudInputProcessor.setOkButton(okBtn);

        InputListener exitListener = new InputListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("touchUp Exit Btn");
                close();
            }
        };

        exit = new ImageTextButton("Exit", mySkin);
        exit.setWidth((float) (width * 0.1));
        exit.setHeight((float) (height * 0.2));
        exit.setX((float) (x + width * 0.05));
        exit.setY((float) (y + height * 0.25));
        exit.addListener(exitListener);
        stage.addActor(exit);
        hudInputProcessor.addActor(exit);
        hudInputProcessor.withoutIdle(exit);
        hudInputProcessor.setExitButton(exit);

        //init TextFields--------------------------------------------------------------------------

        xField = new TextField("X", mySkin);
        xField.setWidth((float) (this.width * 0.2));
        xField.setHeight((float) (this.height * 0.2));
        xField.setX((float) (x + this.width * 0.05));
        xField.setY((float) (y + this.height * 0.75));
        xField.setTextFieldListener((textField, c) -> Gdx.app.log(loggingTag, "XFieldListener"));
        stage.addActor(xField);
        hudInputProcessor.addActor(xField);
        hudInputProcessor.withoutIdle(xField);

        yField = new TextField("Y", mySkin);
        yField.setWidth((float) (this.width * 0.2));
        yField.setHeight((float) (this.height * 0.2));
        yField.setX((float) (x + this.width * 0.05));
        yField.setY((float) (y + this.height * 0.50));
        yField.setTextFieldListener((textField, c) -> Gdx.app.log(loggingTag, "YFieldListener"));
        stage.addActor(yField);
        hudInputProcessor.addActor(yField);
        hudInputProcessor.withoutIdle(yField);

        this.editorNameField = new TextField("Editor", mySkin);
        editorNameField.setWidth((float) (this.width * 0.6));
        editorNameField.setHeight((float) (this.height * 0.2));
        editorNameField.setX((float) (x + this.width * 0.35));
        editorNameField.setY((float) (y + this.height * 0.75));
        editorNameField.setTextFieldListener((textField, c) -> Gdx.app.log(loggingTag, "EditorNameFieldListener"));
        stage.addActor(editorNameField);
        hudInputProcessor.addActor(editorNameField);
        hudInputProcessor.withoutIdle(editorNameField);

        this.mapNameField = new TextField("MapName", mySkin);
        mapNameField.setWidth((float) (this.width * 0.6));
        mapNameField.setHeight((float) (this.height * 0.2));
        mapNameField.setX((float) (x + this.width * 0.35));
        mapNameField.setY((float) (y + this.height * 0.50));
        mapNameField.addListener(new InputListener() {
            @Override
            public boolean handle(Event event) {
                Gdx.app.log(loggingTag, "handle");
                return false;
            }
        });
        mapNameField.setTextFieldListener((textField, c) -> System.out.println("MapNameFieldListener"));
        stage.addActor(mapNameField);
        hudInputProcessor.addActor(mapNameField);
        hudInputProcessor.withoutIdle(mapNameField);

        numberOfPlayersField = new TextField("Number Of Players", mySkin);
        numberOfPlayersField.setWidth((float) (this.width * 0.6));
        numberOfPlayersField.setHeight((float) (this.height * 0.2));
        numberOfPlayersField.setX((float) (x + this.width * 0.05));
        numberOfPlayersField.setY((float) (y + this.height * 0.25));
        numberOfPlayersField.setTextFieldListener((textField, c) -> System.out.println("NumberOfPlayerFieldListener"));
        stage.addActor(numberOfPlayersField);
        hudInputProcessor.addActor(numberOfPlayersField);
        hudInputProcessor.withoutIdle(numberOfPlayersField);

    }

    private void resize() {

        menuBackground.setSize((float) width, (float) height);
        menuBackground.setPosition((float) x, (float) y);
        menuBackground.toBack();

        okBtn.setWidth((float) (width * 0.3));
        okBtn.setHeight((float) (height * 0.2));
        okBtn.setX((float) (x + width * 0.05));
        okBtn.setY((float) (y + height * 0.05));

        exit.setWidth((float) (width * 0.3));
        exit.setHeight((float) (height * 0.2));
        exit.setX((float) (x + width * 0.35));
        exit.setY((float) (y + height * 0.05));

        xField.setWidth((float) (this.width * 0.2));
        xField.setHeight((float) (this.height * 0.2));
        xField.setX((float) (x + this.width * 0.05));
        xField.setY((float) (y + this.height * 0.75));

        yField.setWidth((float) (this.width * 0.2));
        yField.setHeight((float) (this.height * 0.2));
        yField.setX((float) (x + this.width * 0.05));
        yField.setY((float) (y + this.height * 0.50));

        editorNameField.setWidth((float) (this.width * 0.6));
        editorNameField.setHeight((float) (this.height * 0.2));
        editorNameField.setX((float) (x + this.width * 0.35));
        editorNameField.setY((float) (y + this.height * 0.75));

        mapNameField.setWidth((float) (this.width * 0.6));
        mapNameField.setHeight((float) (this.height * 0.2));
        mapNameField.setX((float) (x + this.width * 0.35));
        mapNameField.setY((float) (y + this.height * 0.50));

        numberOfPlayersField.setWidth((float) (this.width * 0.6));
        numberOfPlayersField.setHeight((float) (this.height * 0.2));
        numberOfPlayersField.setX((float) (x + this.width * 0.05));
        numberOfPlayersField.setY((float) (y + this.height * 0.25));

    }

    private void close() {
        hudInputProcessor.idle(false);
        hudInputProcessor.removeActor(xField);
        hudInputProcessor.removeActor(yField);
        hudInputProcessor.removeActor(editorNameField);
        hudInputProcessor.removeActor(mapNameField);
        hudInputProcessor.removeActor(numberOfPlayersField);
        hudInputProcessor.removeActor(exit);
        hudInputProcessor.removeActor(okBtn);
        hudInputProcessor.removeOkButton();
        hudInputProcessor.removeExitButton();
        game.getScreenHandler().setScreenInFrontOf(false);

    }

    public NewMapScreen withPosition(double x, double y) {
        this.x = x;
        this.y = y;
        resize();
        return this;
    }

    public NewMapScreen withSize(double width, double height) {
        this.width = width;
        this.height = height;
        resize();
        return this;
    }

    public void draw() {
        stage.draw();
    }

}

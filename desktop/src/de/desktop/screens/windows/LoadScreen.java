package de.desktop.screens.windows;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.desktop.util.MapScrollPane;
import de.core.MainGame;
import de.core.util.HudInputProcessor;
import de.core.handler.OwenFileHandler;

public class LoadScreen extends Actor
{

    private MainGame game;
    private double y;
    private double x;
    private double height;
    private double width;

    private MapScrollPane scrollPane;
    private ImageTextButton load;
    private ImageTextButton exit;
    private HudInputProcessor hudInputProcessor;
    private Image menuBackground;
    private Stage stage;
    private String mapName;


    public LoadScreen(
            final MainGame game,
            final HudInputProcessor hudInputProcessor)
    {
        this.hudInputProcessor = hudInputProcessor;
        this.game = game;
        this.stage = new Stage();
        Skin mySkin = new Skin(OwenFileHandler.getFile("skin/glassy-ui",
                                                       ".json"));

        Texture texture = new Texture(OwenFileHandler.getFile("backgroundBlack",
                                                              ".jpg"));
        TextureRegionDrawable drawable1 = new TextureRegionDrawable(new TextureRegion(texture));
        menuBackground = new Image();
        menuBackground.setDrawable(drawable1);
        menuBackground.setSize((float) width,
                               (float) height);
        menuBackground.setPosition((float) x,
                                   (float) y);
        stage.addActor(menuBackground);
        hudInputProcessor.addActor(menuBackground);
        hudInputProcessor.withoutIdle(menuBackground);

        //-------------------------------------------------

//TODO
//        scrollPane = new MapScrollPane(mySkin,
//                                       this,
//                                       game.getMapHandler().getLocalMapsAsSmallMap(),
//                                       hudInputProcessor);
        stage.addActor(scrollPane);
        hudInputProcessor.addActor(scrollPane);
        hudInputProcessor.withoutIdle(scrollPane);


        InputListener loadListener = new InputListener()
        {
            @Override
            public void touchUp(
                    InputEvent event,
                    float x,
                    float y,
                    int pointer,
                    int button)
            {
                game.getMapHandler().setEditableMap(game.getMapHandler().loadMap(mapName));
                close();
            }

            @Override
            public boolean touchDown(
                    InputEvent event,
                    float x,
                    float y,
                    int pointer,
                    int button)
            {
                return true;
            }
        };

        load = new ImageTextButton("Load",
                                   mySkin);
        load.setWidth((float) (width * 0.1));
        load.setHeight((float) (height * 0.2));
        load.setX((float) (x + width * 0.05));
        load.setY((float) (y + height * 0.05));
        load.addListener(loadListener);
        stage.addActor(load);
        hudInputProcessor.addActor(load);
        hudInputProcessor.withoutIdle(load);
        hudInputProcessor.setOkButton(load);

        InputListener exitListener = new InputListener()
        {
            @Override
            public void touchUp(
                    InputEvent event,
                    float x,
                    float y,
                    int pointer,
                    int button)
            {
                close();
            }

            @Override
            public boolean touchDown(
                    InputEvent event,
                    float x,
                    float y,
                    int pointer,
                    int button)
            {
                return true;
            }
        };

        exit = new ImageTextButton("Exit",
                                   mySkin);
        exit.setWidth((float) (width * 0.1));
        exit.setHeight((float) (height * 0.2));
        exit.setX((float) (x + width * 0.05));
        exit.setY((float) (y + height * 0.25));
        exit.addListener(exitListener);
        stage.addActor(exit);
        hudInputProcessor.addActor(exit);
        hudInputProcessor.withoutIdle(exit);
        hudInputProcessor.setExitButton(exit);

    }

    private void resize()
    {

        menuBackground.setSize((float) width,
                               (float) height);
        menuBackground.setPosition((float) x,
                                   (float) y);
        menuBackground.toBack();

        scrollPane.setWidth((float) (width * 0.98));
        scrollPane.setHeight((float) (height * 0.7));
        scrollPane.setX((float) (x + (width * 0.01)));
        scrollPane.setY((float) (y + (height * 0.28)));
        scrollPane.resize();

        load.setWidth((float) (width * 0.3));
        load.setHeight((float) (height * 0.2));
        load.setX((float) (x + width * 0.05));
        load.setY((float) (y + height * 0.05));

        exit.setWidth((float) (width * 0.3));
        exit.setHeight((float) (height * 0.2));
        exit.setX((float) (x + width * 0.35));
        exit.setY((float) (y + height * 0.05));

    }

    private void close()
    {
        hudInputProcessor.idle(false);
        hudInputProcessor.removeActor(scrollPane);
        hudInputProcessor.removeActor(load);
        hudInputProcessor.removeActor(exit);
        hudInputProcessor.removeExitButton();
        hudInputProcessor.removeOkButton();
        game.getScreenHandler().setScreenInFrontOf(false);

    }

    public LoadScreen withPosition(
            double x,
            double y)
    {
        this.x = x;
        this.y = y;
        resize();
        return this;
    }

    public LoadScreen withSize(
            double width,
            double height)
    {
        this.width = width;
        this.height = height;
        resize();
        return this;
    }

    public void draw()
    {
        stage.draw();
    }
}

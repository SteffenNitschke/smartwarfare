package de.desktop.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;

import java.util.HashSet;

import de.core.util.HudInputProcessor;
import de.desktop.util.GameScrollingPane;
import de.desktop.util.MapScrollPane;
import de.core.MainGame;
import de.core.handler.OwenFileHandler;
import de.core.model.Game;
import de.core.model.Player;
import de.core.network.messages.PlayerMessage;
import de.core.screens.InternetMultiPlayerScreenInterface;
import de.core.uiController.InternetMultiPlayerScreenController;
import de.core.util.GameTypes;

public class InternetMultiPlayerScreen implements InputProcessor,
                                                  InternetMultiPlayerScreenInterface,
                                                  Screen
{
    private Stage stage;
    private MainGame game;
    private HudInputProcessor inputProcessor;
    private int col_width;
    private int row_height;
    private Skin mySkin;

    private String loggingTag = getClass().getSimpleName();
    private MapScrollPane mapPane;
    private TextButton chooseMapButton;
    private TextButton chooseGameTypeButton;
    private ScrollPane playerPane;
    private Image mapImage;
    private InternetMultiPlayerScreenController controller;

    InternetMultiPlayerScreen(MainGame game)
    {
        this.game = game;
    }

    @Override
    public void show()
    {
        this.stage = new Stage();

        this.controller = new InternetMultiPlayerScreenController(game,
                                                                  this);
        this.inputProcessor = new HudInputProcessor(stage,
                                                    false);
        final InputMultiplexer inputs = new InputMultiplexer();
        inputs.addProcessor(this);
        inputs.addProcessor(inputProcessor);
        Gdx.input.setInputProcessor(inputs);

        final Image background = new Image();
        background.setSize(Gdx.graphics.getWidth(),
                           Gdx.graphics.getHeight());
        background.setPosition(0,
                               0);

        final Drawable drawable = new TextureRegionDrawable(new TextureRegion(
                new Texture(OwenFileHandler.getFile("mainScreen",
                                                    ".jpg"))));
        background.setDrawable(drawable);
        stage.addActor(background);

        col_width = Gdx.graphics.getWidth() / 16;
        row_height = Gdx.graphics.getHeight() / 12;

        mySkin = new Skin(OwenFileHandler.getFile("skin/glassy-ui",
                                                  ".json"));

        final Button partyButton = new TextButton("Party Modus",
                                                  mySkin);
        partyButton.setSize(col_width * 4,
                            (float) (row_height * 0.8));
        partyButton.setPosition(col_width * 12,
                                (float) (row_height * 8.5));
        partyButton.addListener(controller.getPartyButtonListener());

        stage.addActor(partyButton);

        final Button newGameButton = new TextButton("Neues Spiel",
                                                    mySkin);
        newGameButton.setSize(col_width * 4,
                              (float) (row_height * 0.8));
        newGameButton.setPosition(col_width * 12,
                                  (float) (row_height * 6.5));

        newGameButton.addListener(controller.getNewGameButtonListener());

        stage.addActor(newGameButton);

        final Button matchmakingButton = new TextButton("Matchmaking",
                                                        mySkin);
        matchmakingButton.setSize(col_width * 4,
                                  (float) (row_height * 0.8));
        matchmakingButton.setPosition(col_width * 12,
                                      (float) (row_height * 4.5));
        matchmakingButton.addListener(controller.getMatchmakingButtonListener());

        stage.addActor(matchmakingButton);

        final Button backButton = new TextButton("Back",
                                                 mySkin);
        backButton.setSize(col_width * 4,
                           (float) (row_height * 0.8));
        backButton.setPosition(col_width * 12,
                               (float) (row_height * 0.5));
        backButton.addListener(controller.getBackButtonListener());

        stage.addActor(backButton);

        showGames();
        Gdx.app.log(loggingTag,
                    "End of show-Method from InternetMultiPlayerScreen");
    }

    //-show all games or new game or matchmaking-
    private int screenCounter = 0;
    private HashSet<Actor> shownActors = new HashSet<>();

    private void hideViews()
    {
        for (Actor actor : shownActors)
        {
            actor.setVisible(false);
            stage.getActors().removeValue(actor,
                                          true);
        }
    }

    private void showGames()
    {
        int SHOW_GAMES_COUNTER = 1;
        if (screenCounter != SHOW_GAMES_COUNTER)
        {
            hideViews();
            screenCounter = SHOW_GAMES_COUNTER;
        }

        game.getConnection().getMapClient().getAllMaps();

        final GameScrollingPane pane =
                new GameScrollingPane(game,
                                      mySkin,
                                      shownActors,
                                      controller);
        stage.addActor(pane);

        pane.setSize(col_width * 11.5f,
                     row_height * 4);
        pane.setPosition(col_width * 0.5f,
                         row_height * 7.5f);
        pane.resize();

        mapImage = new Image();
        mapImage.setSize(row_height * 3,
                         row_height * 3);
        mapImage.setPosition(col_width * 0.5f,
                             row_height * 3);

        stage.addActor(mapImage);
        shownActors.add(mapImage);
    }

    private ScrollPane gameTypePane;

    public void showNewGame()
    {
        game.getConnection().getMapClient().getAllMaps();

        int NEW_GAME_COUNTER = 2;
        if (screenCounter != NEW_GAME_COUNTER)
        {
            hideViews();
            screenCounter = NEW_GAME_COUNTER;
        }
        else
        {
            showGames();
            return;
        }
        game.setNewGame(new Game("new"));

        chooseMapButton = new TextButton("Wähle Map",
                                         mySkin);
        chooseMapButton.setSize(col_width * 4,
                                (float) (row_height * 0.8));
        chooseMapButton.setPosition((float) (col_width * 1.5),
                                    (float) (row_height * 10.5));
        chooseMapButton.addListener(new InputListener()
        {
            @Override
            public void touchUp(
                    InputEvent event,
                    float x,
                    float y,
                    int pointer,
                    int button)
            {
                if (mapPane == null)
                {
                    mapPane = new MapScrollPane(mySkin,
                                                shownActors,
                                                controller,
                                                controller.getAllMaps());

                    mapPane.setSize(col_width * 4,
                                    (float) (row_height * 4));
                    mapPane.setPosition((float) (col_width * 1.5),
                                        (float) (row_height * 6.5));
                    stage.addActor(mapPane);
                    mapPane.resize();
                    inputProcessor.addActor(mapPane);
                }
                else
                {
                    removeMapPane();
                }
            }
        });
        stage.addActor(chooseMapButton);
        inputProcessor.addActor(chooseMapButton);
        shownActors.add(chooseMapButton);

        mapImage = new Image();
        mapImage.setSize(row_height * 3,
                         row_height * 3);
        mapImage.setPosition(col_width * 6.5f,
                             (float) (row_height * 8.0));

        stage.addActor(mapImage);
        shownActors.add(mapImage);

        chooseGameTypeButton = new TextButton("Waehle Game Type",
                                              mySkin);
        chooseGameTypeButton.setSize(col_width * 4,
                                     (float) (row_height * 0.8));
        chooseGameTypeButton.setPosition((float) (col_width * 1.5),
                                         (float) (row_height * 8.5));
        chooseGameTypeButton.addListener(new InputListener()
        {
            @Override
            public void touchUp(
                    InputEvent event,
                    float x,
                    float y,
                    int pointer,
                    int button)
            {

                if (gameTypePane == null)
                {
                    Gdx.app.log(loggingTag, "chooseGameTypeButtonListener");
                    showChooseMaPane();
                }
                else
                {
                    Gdx.app.log(loggingTag, "Close GameTypePane");
                    shownActors.remove(gameTypePane);
                    inputProcessor.removeActor(gameTypePane);
                    gameTypePane.remove();
                    gameTypePane = null;
                }
            }

            private void showChooseMaPane()
            {
                final Array<Label> groupArray = new Array<>();
                for (GameTypes type : GameTypes.values())
                {
                    final Label label1 = new Label(type.name(), mySkin);

                    label1.setAlignment(Align.center);
                    label1.setSize(col_width * 6,
                                   (float) (row_height * 0.4));

                    groupArray.add(label1);
                }
                final List<Label> list = new List<>(mySkin);
                list.setItems(groupArray);

                list.addListener(controller.getChooseGameTypeListListener());

                gameTypePane = new ScrollPane(list);
                gameTypePane.setVisible(true);
                gameTypePane.setSmoothScrolling(false);
//                gameTypePane.setTransform(true);
                gameTypePane.setScale(1f);
                gameTypePane.setSize(col_width * 4,
                                     (float) (row_height * 2));
                gameTypePane.setPosition((float) (col_width * 1.5),
                                         (float) (row_height * 6.5));
                shownActors.add(gameTypePane);
                stage.addActor(gameTypePane);
                inputProcessor.addActor(gameTypePane);
            }
        });
        stage.addActor(chooseGameTypeButton);
        inputProcessor.addActor(chooseGameTypeButton);
        shownActors.add(chooseGameTypeButton);

        final TextField playerName = new TextField("Name of Player",
                                                   mySkin);
        playerName.setSize(col_width * 4,
                           (float) (row_height * 0.8));
        playerName.setPosition((float) (col_width * 6.5),
                               (float) (row_height * 6.5));
        stage.addActor(playerName);
        shownActors.add(playerName);

        final Button inviteButton = new TextButton("Einladen",
                                                   mySkin);
        inviteButton.setSize(col_width * 4,
                             (float) (row_height * 0.8));
        inviteButton.setPosition((float) (col_width * 1.5),
                                 (float) (row_height * 6.5));

        stage.addActor(inviteButton);
        inputProcessor.setOkButton(inviteButton);
        shownActors.add(inviteButton);

        //---------------------------------------

        final List<Label> list = new List<>(mySkin);
        final Array<Label> labelArray = new Array<>();

        final Label label = new Label("Eingeladene Spieler:",
                                      mySkin);
        label.setAlignment(Align.center);
        label.setSize(col_width * 6,
                      (float) (row_height * 0.4));
        labelArray.add(label);

        for (Player player : game.getNewGame().getPlayer())
        {
            final Label label1 = new Label(player.getName(),
                                           mySkin);
            label1.setAlignment(Align.center);
            label1.setSize(col_width * 6,
                           (float) (row_height * 0.4));

            labelArray.add(label1);
        }

        inviteButton.addListener(new InputListener()
        {
            @Override
            public void touchUp(
                    InputEvent event,
                    float x,
                    float y,
                    int pointer,
                    int button)
            {
                final String name = playerName.getText();

                if(name.isEmpty())
                {
                    return;
                }

                boolean found = false;
                for (PlayerMessage friend : game.getUser().getFriends())
                {
                    if (friend.getName().equals(name))
                    {
                        labelArray.add(new Label(name,
                                                 mySkin));
                        list.setItems(labelArray);
                        playerName.setText("");
                        found = true;

                        controller.addInvitedPlayer(friend);
                        break;
                    }
                }
                if (!found)
                {
                    playerName.setText("Unknown Friend");
                }
            }

            @Override
            public boolean touchDown(
                    InputEvent event,
                    float x,
                    float y,
                    int pointer,
                    int button)
            {
                return true;
            }
        });

        list.setItems(labelArray);

        playerPane = new ScrollPane(list);
        playerPane.setSmoothScrolling(false);
        playerPane.setTransform(true);
        playerPane.setScale(1f);
        playerPane.setSize(col_width * 4,
                           (float) (row_height * 2));
        playerPane.setPosition((float) (col_width * 1.5),
                               (float) (row_height * 3.5));
        shownActors.add(playerPane);
        stage.addActor(playerPane);

        final Button starButton = new TextButton("Spiel Starten",
                                                 mySkin);
        starButton.setSize(col_width * 4,
                           (float) (row_height * 0.8));
        starButton.setPosition((float) (col_width * 1.5),
                               (float) (row_height * 1.5));
        starButton.addListener(controller.getStartGameButtonListenr());
        stage.addActor(starButton);
        shownActors.add(starButton);
    }

    public void clearNewGame()
    {
        chooseMapButton.setText("Wähle Map");
        chooseGameTypeButton.setText("Waehle Game Type");

        Array<Label> playerArray = new Array<>();

        List<Label> playerList = new List<>(mySkin);
        playerArray.add(new Label("Eingeladene Spieler:",
                                  mySkin));
        playerList.setItems(playerArray);

        playerPane.setWidget(playerList);
    }

    @Override
    public void showMapImage(final FileHandle fileHandle)
    {
        Gdx.app.log(loggingTag, "showMapImage");
        if(fileHandle == null)
        {
            Gdx.app.log(loggingTag, "fileHandle == null");
            mapImage.setVisible(false);
            return;
        }
        mapImage.setDrawable(
                new TextureRegionDrawable(
                        new TextureRegion(
                                new Texture(fileHandle))));
        Gdx.app.log(loggingTag, "ready");
    }

    @Override
    public TextButton getChooseGameTypeButton()
    {
        return chooseGameTypeButton;
    }

    @Override
    public ScrollPane getGameTypePane()
    {
        return gameTypePane;
    }

    public void showMatchmaking()
    {

        int MATCHMAKING_COUNTER = 3;
        if (screenCounter != MATCHMAKING_COUNTER)
        {
            hideViews();
            screenCounter = MATCHMAKING_COUNTER;
        }
        else
        {
            showGames();
            return;
        }

        Button startMatchmaking = new TextButton("Start Matchmaking",
                                                 mySkin);
        startMatchmaking.setSize(col_width * 4,
                                 (float) (row_height * 0.8));
        startMatchmaking.setPosition((float) (col_width * 5),
                                     (float) (row_height * 6.5));
        /*TODO Matchmaking For Public Games
               send Request
               wait for answer
                */
        stage.addActor(startMatchmaking);
        shownActors.add(startMatchmaking);

    }

    @Override
    public String getCanonicalNameOfMPStartScreen()
    {
        return MultiPlayerStartScreen.class.getCanonicalName();
    }

    //-------------------------------------------


    @Override
    public void render(float v)
    {

        Gdx.gl.glClearColor(1,
                            1,
                            1,
                            1);
        Gdx.gl.glClear(GL20.GL_COLOR_WRITEMASK);
        stage.draw();

    }

    @Override
    public void resize(
            int i,
            int i1)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        stage.dispose();
        System.out.println("InternetMultiPlayerScreen Dispose");
    }

    @Override
    public boolean keyDown(int i)
    {
//        Gdx.app.log(loggingTag, "keyDown");
        stage.keyDown(i);
        return false;
    }

    @Override
    public boolean keyUp(int i)
    {
//        Gdx.app.log(loggingTag, "keyUp");
        stage.keyUp(i);
        return false;
    }

    @Override
    public boolean keyTyped(char c)
    {
//        Gdx.app.log(loggingTag, "keyTyped");
        stage.keyTyped(c);
        return false;
    }

    @Override
    public boolean touchDown(
            int i,
            int i1,
            int i2,
            int i3)
    {
//        Gdx.app.log(loggingTag, "touchDown");
        stage.touchDown(i,
                        i1,
                        i2,
                        i3);
        return false;
    }

    @Override
    public boolean touchUp(
            int i,
            int i1,
            int i2,
            int i3)
    {
//        Gdx.app.log(loggingTag, "touchUp");
        stage.touchUp(i,
                      i1,
                      i2,
                      i3);
        return false;
    }

    @Override
    public boolean touchDragged(
            int i,
            int i1,
            int i2)
    {
//        Gdx.app.log(loggingTag, "touchDragged");
        stage.touchDragged(i,
                           i1,
                           i2);
        return false;
    }

    @Override
    public boolean mouseMoved(
            int i,
            int i1)
    {
        stage.mouseMoved(i,
                         i1);
        return false;
    }

    @Override
    public boolean scrolled(int i)
    {
//        Gdx.app.log(loggingTag, "scrolled");
        stage.scrolled(i);
        return false;
    }

    @Override
    public void removeMapPane()
    {
        chooseMapButton.setText(controller.getMapName());
        shownActors.remove(mapPane);
        inputProcessor.removeActor(mapPane);
        mapPane = null;
    }

    @Override
    public String getCanonicalNameOfPartyMultiPlayerScreen()
    {
        return PartyMultiplayerScreen.class.getCanonicalName();
    }

    @Override
    public Screen getPartyMultiPlayerScreen()
    {
        return new PartyMultiplayerScreen(game);
    }
}

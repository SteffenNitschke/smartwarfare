package de.desktop.screens.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import de.core.MainGame;
import de.core.model.Chat;
import de.core.model.Message;
import de.core.handler.OwenFileHandler;
import de.core.util.Utils;

import java.io.IOException;
import java.util.ArrayList;

public class ChatTable extends Actor {

    private final TextureRegionDrawable background;
    private Skin skin;
    private MainGame game;

    private Table table = new Table();
    private float width;
    private float height;

    private String currentChatId;

    public ChatTable(
            float positionX,
            float positionY,
            float width,
            float height,
            Skin skin,
            MainGame game) {

        this.width = width;
        this.height = height;
        this.skin = skin;
        this.game = game;
        this.background = new TextureRegionDrawable(new TextureRegion(new Texture(OwenFileHandler.getFile("NewsBackground", ".jpg"))));
        table.setSkin(skin);
        table.setSize(width, height);
        table.setFillParent(false);
        table.setPosition(positionX, positionY);
        Drawable drawable = new TextureRegionDrawable(new TextureRegion(new Texture(OwenFileHandler.getFile("NewsTableBackground", ".jpg"))));
        this.table.setBackground(drawable);
    }

    public Table fillTable(String chatId) throws IOException {

        this.currentChatId = chatId;
        Chat chat = game.getChatHandler().getChat(chatId);

        for(Message message : chat.getMessages()) {

            this.table.add(fillWithChatMessage(message));
            this.table.row();
            this.table.top();
        }

        return table;
    }

    public Table refresh() throws IOException {
        this.table.clearChildren();
        fillTable(this.currentChatId);
        return this.table;
    }

    private Table fillWithChatMessage(Message message) {

        Table table = new Table();
        table.setSkin(skin);
        table.row().height(height * 0.1f);
        String[] split = splitMessage(message.getMessage());
        for (String string : split) {
            Label label = new Label(string, skin);
            label.setFillParent(true);
            label.setWidth(width);
            if (message.getSender().getId().equals(game.getUser().get_id())) {
                label.setColor(Color.LIGHT_GRAY);
                Label fill = new Label("", skin);
                fill.setWidth(width / 0.3f);
                fill.setFillParent(true);
                table.add(fill);
                table.add(label).row();
            } else {
                label.setColor(Color.SLATE);
                table.add(label).row();
                table.add();
            }
            table.setBackground(background);

        }
        return table;
    }

    private String[] splitMessage(String message) {

        String[] split = message.split(" ");
        java.util.List<String> result = new ArrayList<>();
        StringBuilder row = new StringBuilder();
        for(String word : split) {
            if((row.length() + word.length()) < Utils.CHAT_ROW_LENGTH){
                row.append(" ").append(word);
            } else {
                result.add(row.toString());
                row = new StringBuilder(word);
            }
        }
        result.add(row.toString());
        String[] s = new String[result.size()];
        return result.toArray(s);
    }
}

package de.desktop.screens.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.core.MainGame;
import de.core.handler.OwenFileHandler;
import de.core.model.Id;
import de.core.network.messages.news.NewsDto;
import de.core.screens.windows.NewsTableBuilderInterface;
import de.core.uiListeners.NewsConfirmListener;
import de.core.uiListeners.NewsDenyListener;
import de.core.uiListeners.NewsOkButtonListener;


public class NewsTableBuilder implements NewsTableBuilderInterface
{

    private final TextureRegionDrawable background;
    private Skin skin;
    private MainGame game;

    private Table table = new Table();
    private float positionX;
    private float positionY;
    private float width;
    private float height;
    private String loggingTag = getClass().getSimpleName();

    public NewsTableBuilder(
            final float positionX,
            final float positionY,
            final float width,
            final float height,
            final Skin skin,
            final MainGame game
    )
    {
        this.positionX = positionX;
        this.positionY = positionY;
        this.width = width;
        this.height = height;
        this.skin = skin;
        this.game = game;
        this.background = new TextureRegionDrawable(
                new TextureRegion(
                        new Texture(OwenFileHandler
                                            .getFile("NewsBackground",
                                                     ".jpg"))));
        table.setSkin(skin);
        table.setSize(width,
                      height);
        table.setFillParent(false);
        table.setPosition(positionX,
                          positionY);
        final Drawable drawable = new TextureRegionDrawable(
                new TextureRegion(
                        new Texture(OwenFileHandler
                                            .getFile("NewsTableBackground",
                                                     ".jpg"))));
        this.table.setBackground(drawable);
    }

    public void viewNews(final NewsDto message)
    {
        Table newsTable;

        Gdx.app.log(loggingTag, "viewNews " + message);

        switch (message.getType())
        {
            case FRIEND_INVITATION:
                newsTable = fillWithFriendInvitation(message);
                break;
            case FRIEND_ANSWER:
                newsTable = fillWithFriendAnswer(message);
                break;
            case GAME_INVITATION:
                newsTable = fillWithGameInvitation(message);
                break;
            case GAME_ANSWER:
                newsTable = fillWithGameAnswer(message);
                break;
            case GAME_STARTED:
                newsTable = fillWithGameStatus(message);
                break;
            case GAME_CANCELLED:
                newsTable = fillWithGameStatus(message);
                break;
            case TURN:
                newsTable = fillWithTurn(message);
                break;
            default:
                newsTable = new Table();
        }

        newsTable.setUserObject(message.get_id());

        this.table.add(newsTable);
        this.table.row();
        this.table.top();
        resize();
    }

    @Override
    public void removeNewsFromTable(Id newsId)
    {
        for (Actor actor : this.table.getChildren())
        {
            if (((Id) actor.getUserObject()).getIdentifier().equals(newsId.getIdentifier()))
            {
                this.table.removeActor(actor);
            }
        }
    }

    public Table fill()
    {
        this.table.clearChildren();
        fillTable();
        return this.table;
    }

    private void fillTable()
    {
        for (NewsDto news : this
                .game
                .getNewsHandler()
                .getNews().values())
        {
            viewNews(news);
        }
    }

    private void resize()
    {
        int numberOfNews = this.table.getChildren().size;
        if (this.table.getChildren().size > 0)
        {
            float heightOfChildren = this.table.getChildren().first().getHeight();
            table.setPosition(positionX,
                              positionY - (numberOfNews * heightOfChildren));
            table.setSize(width,
                          numberOfNews * heightOfChildren);
        }
    }

    private Table fillWithTurn(final NewsDto message)
    {
        Table table = new Table();
        table.setSkin(skin);
        table.row().height(height * 0.1f);
        Label label = new Label(message.getSender().getName() + " makes his/her Turn.",
                                skin);
        label.setWidth(width);
        table.add(label).colspan(2);
        table.row();
        addButtons(table,
                   false,
                   false,
                   true,
                   message);
        table.setBackground(background);
        return table;
    }

    private Table fillWithGameStatus(final NewsDto message)
    {

        final Table table = new Table();
        table.setSkin(skin);
        table.setUserObject(message);
        table.row().height(height * 0.1f);
        Label label;
        switch (message.getType())
        {
            case GAME_STARTED:
                label = new Label("Game is started",
                                  skin);
                label.setWidth(width);
                table.add(label).colspan(2);
                break;
            case GAME_CANCELLED:
                label = new Label("Game is cancelled",
                                  skin);
                label.setWidth(width);
                table.add(label).colspan(2);
                break;
        }
        table.row();
        addButtons(table,
                   false,
                   false,
                   true,
                   message);
        table.setBackground(background);
        return table;
    }

    private Table fillWithGameAnswer(final NewsDto message)
    {
        Table table = new Table();
        table.setSkin(skin);
        table.row().height(height * 0.1f);
        StringBuilder text = new StringBuilder(message.getSender().getName());
        if (message.isAnswer())
        {
            text.append(" wanna play a game with you.");
        }
        else
        {
            text.append(" don't wanna play a game with you.");
        }
        Label label = new Label(text,
                                skin);
        label.setWidth(width);
        table.add(label).colspan(2);
        table.row();
        addButtons(table,
                   false,
                   false,
                   true,
                   message);
        table.setBackground(background);
        return table;
    }

    private Table fillWithGameInvitation(final NewsDto message)
    {

        Table table = new Table();
        table.setSkin(skin);
        table.row().height(height * 0.1f);
        StringBuilder text = new StringBuilder(
                message.getSender().getName() + " wanna play a game");
        text.append(" with ");
        text.append("you.");
        Label label = new Label(text,
                                skin);
        label.setWidth(width);
        table.add(label).colspan(2);
        table.row().height(height * 0.1f);
        text = new StringBuilder();
        label = new Label(text,
                          skin);
        label.setWidth(width);
        table.add(label).colspan(2);
        table.row();
        addButtons(table,
                   true,
                   true,
                   false,
                   message);
        table.setBackground(background);
        return table;
    }

    private Table fillWithFriendAnswer(final NewsDto message)
    {

        Table table = new Table();
        table.setSkin(skin);
        table.row().height(height * 0.1f);
        String text = message
                .isAnswer() ? " wanna be your Friend." : " don't wanna be your Friend?";
        Label label = new Label(message.getSender().getName() + text,
                                skin);
        label.setWidth(width);
        table.add(label).colspan(2);
        table.row();
        addButtons(table,
                   false,
                   false,
                   true,
                   message);
        table.setBackground(background);
        return table;
    }

    private Table fillWithFriendInvitation(final NewsDto message)
    {
        final Table table = new Table();
        table.setSkin(skin);
        table.row().height(height * 0.1f);
        final Label label = new Label(message.getSender().getName() + " want to be your Friend!",
                                      skin);
        label.setWidth(width);
        table.add(label).colspan(2);
        table.row();
        addButtons(table,
                   true,
                   true,
                   false,
                   message);
        table.setBackground(background);
        return table;
    }

    private void addButtons(
            final Table table,
            final boolean confirmButton,
            final boolean denyButton,
            final boolean okButton,
            final NewsDto message)
    {
        table.row().height(height * 0.1f);
        if (confirmButton)
        {
            Button confirmBtn = new TextButton("confirm",
                                               skin);
            confirmBtn.addListener(new NewsConfirmListener(game,
                                                           message));
            confirmBtn.setSize(width / 5,
                               height / 12);
            table.add(confirmBtn).width(width);
        }
        if (denyButton)
        {
            Button denyBtn = new TextButton("deny",
                                            skin);
            denyBtn.addListener(new NewsDenyListener(game,
                                                     message));
            denyBtn.setSize(width / 5,
                            height / 12);
            table.add(denyBtn).width(width);
        }
        if (okButton)
        {
            Button okBtn = new TextButton("ok",
                                          skin);
            okBtn.addListener(new NewsOkButtonListener(game,
                                                       message));
            okBtn.setSize(width / 2.5f,
                          height * 0.1f);
            table.add(okBtn).width(width * 2);
        }
    }
}

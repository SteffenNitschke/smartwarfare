package de.desktop.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import de.core.util.HudInputProcessor;
import de.core.MainGame;
import de.core.model.Game;
import de.core.uiController.GameScreenController;

public class GameScreenSuperclass implements Screen, InputProcessor
{
    protected MainGame game;
    protected Stage stage;
    GameScreenController gameScreenController;
    SpriteBatch hudBatch;

    Game currentGame;
    OrthographicCamera camera;

    HudInputProcessor hudProcessor;

    private int mousePositionX;
    private int mousePositionY;
    float mapHeight;
    float mapWidth;
    boolean isDragged;
    protected int col_width;
    protected int row_height;

    String loggingTag = getClass().getSimpleName();

    @Override
    public void show()
    {
        stage = new Stage();
        InputMultiplexer inputs = new InputMultiplexer();
        hudProcessor = new HudInputProcessor(stage,
                                             true);
        inputs.addProcessor(this);
        inputs.addProcessor(hudProcessor);
        camera = new OrthographicCamera();
        Gdx.input.setInputProcessor(inputs);

        hudBatch = new SpriteBatch();

        col_width = Gdx.graphics.getWidth() / 16;
        row_height = Gdx.graphics.getHeight() / 12;

    }

    @Override
    public void render(float delta)
    {
        Gdx.gl.glClearColor(1,
                            1,
                            1,
                            1);
        Gdx.gl.glClear(GL20.GL_COLOR_CLEAR_VALUE);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    @Override
    public void resize(
            int width,
            int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {
        stage.dispose();
    }

    @Override
    public boolean keyDown(int keycode)
    {
        stage.keyDown(keycode);
        return false;
    }

    @Override
    public boolean keyUp(int keycode)
    {
        stage.keyUp(keycode);
        return false;
    }

    @Override
    public boolean keyTyped(char character)
    {
        stage.keyTyped(character);
        return false;
    }

    @Override
    public boolean touchDown(
            int screenX,
            int screenY,
            int pointer,
            int button)
    {
        if (!game.getScreenHandler().isScreenInFrontOf())
        {
            mousePositionX = screenX;
            mousePositionY = screenY;
            this.isDragged = true;
            stage.touchDown(screenX,
                            screenY,
                            pointer,
                            button);
        }
        return false;
    }

    @Override
    public boolean touchUp(
            int screenX,
            int screenY,
            int pointer,
            int button)
    {
        if (!game.getScreenHandler().isScreenInFrontOf())
        {
            this.isDragged = false;
            stage.touchUp(screenX,
                          screenY,
                          pointer,
                          button);
        }
        return false;
    }

    @Override
    public boolean touchDragged(
            int screenX,
            int screenY,
            int pointer)
    {
        if (!game.getScreenHandler().isScreenInFrontOf())
        {
            OrthographicCamera camera = (OrthographicCamera) stage.getCamera();
            //Gdx.app.log(loggingTag, "x: " + i + " y: " + i1);
            float x = camera.position.x -
                    ((screenX - mousePositionX) * ((OrthographicCamera) stage.getCamera()).zoom);
            if (x < 0)
            {
                x = 0;
            }
            if (x > mapWidth)
            {
                x = mapWidth;
            }
            float y = camera.position.y +
                    ((screenY - mousePositionY) * ((OrthographicCamera) stage.getCamera()).zoom);
            if (y < 0)
            {
                y = 0;
            }
            if (y > mapHeight)
            {
                y = mapHeight;
            }
            camera.position.set(x,
                                y,
                                camera.position.z);
            mousePositionX = screenX;
            mousePositionY = screenY;

            stage.touchDragged(screenX,
                               screenY,
                               pointer);
        }
        return false;
    }

    @Override
    public boolean mouseMoved(
            int screenX,
            int screenY)
    {
        stage.mouseMoved(screenX,
                         screenY);
        return false;
    }

    @Override
    public boolean scrolled(int amount)
    {
        if (!game.getScreenHandler().isScreenInFrontOf())
        {
            if (amount == 1)
            {
                if (((OrthographicCamera) stage.getCamera()).zoom != 1.0)
                {
                    ((OrthographicCamera) stage.getCamera()).zoom += .1f;
                }
            }
            else if (amount == -1)
            {
                if (((OrthographicCamera) stage.getCamera()).zoom != 0.1)
                {
                    ((OrthographicCamera) stage.getCamera()).zoom -= .1f;
                }
            }
        }
        return false;
    }
}

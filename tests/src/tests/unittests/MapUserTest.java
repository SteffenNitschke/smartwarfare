package tests.unittests;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;

import java.io.IOException;

import de.core.model.User;
import de.server.java.smartwarfare.user.DBUser;
import de.server.java.smartwarfare.user.UserId;

import static org.junit.Assert.assertEquals;

public class MapUserTest
{


    @Test
    public void testMapDBtoUser() throws IOException
    {
        final DBUser dbUser = new DBUser(UserId.of("123456"),"Steffen", 0, 0);

        final String dbString = new ObjectMapper().writeValueAsString(dbUser);

        final User user = new ObjectMapper().readValue(dbString, User.class);

        assertEquals("123456", user.get_id().getIdentifier());
        assertEquals("Steffen", user.getName());
        assertEquals(0, user.getLevel());
        assertEquals(0, user.getExperience());
        assertEquals(0, user.getFriends().size());
        assertEquals(0, user.getHeroes().size());
        assertEquals(0, user.getGames().size());
    }
}
